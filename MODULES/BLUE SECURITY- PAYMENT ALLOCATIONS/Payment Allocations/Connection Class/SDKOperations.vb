﻿Imports Pastel.Evolution
Imports System.IO

Module SDKOperations

    'Create Connection

    Public Property ServerName As String = My.Settings.DBServer
    Public Property Login As String = My.Settings.DBUser
    Public Property Password As String = My.Settings.DBPassword
    Public Property CompanyName As String = My.Settings.PASCompany
    Public Property CommonName As String = My.Settings.PASCommon

    Public Function CreateConnection()

        Try

            Dim ServerName As String = My.Settings.DBServer
            Dim Login As String = My.Settings.DBUser
            Dim Password As String = My.Settings.DBPassword
            Dim CompanyName As String = My.Settings.PASCompany
            Dim CommonName As String = My.Settings.PASCommon

            DatabaseContext.CreateCommonDBConnection(ServerName, CommonName, Login, Password, False)
            DatabaseContext.SetLicense("DE09110073", "4730608")
            DatabaseContext.CreateConnection(ServerName, CompanyName, Login, Password, False)

        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

End Module
