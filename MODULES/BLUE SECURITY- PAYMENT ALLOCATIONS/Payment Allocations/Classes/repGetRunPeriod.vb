﻿Public Class repGetRunPeriod


    Private dbConn As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function getDebitOrderRuns() As DataTable

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable

        qry = "SELECT " & vbCrLf & _
                "invDescriptionSuffix" & vbCrLf & _
                "FROM" & vbCrLf & _
                "runMonth" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "monthID IN" & vbCrLf & _
                "(" & vbCrLf & _
                "SELECT TOP 2 " & vbCrLf & _
                "monthID  " & vbCrLf & _
                "FROM " & vbCrLf & _
                "runMonth " & vbCrLf & _
                "WHERE " & vbCrLf & _
                "isCompleted = 1 " & vbCrLf & _
                "ORDER BY monthID DESC" & vbCrLf & _
                ")" & vbCrLf & _
                "ORDER BY monthID "





        '"SELECT" & vbCrLf & _
        '                "DISTINCT (REPLACE((REPLACE(Message2, '_INVOICE', '')), 'RECRUN_', '')) AS DORun" & vbCrLf & _
        '                "FROM" & vbCrLf & _
        '                "InvNum I" & vbCrLf & _
        '                "WHERE" & vbCrLf & _
        '                "I.Message2 <> ''" & vbCrLf & _
        '                "AND" & vbCrLf & _
        '                "I.Message2 LIKE '%INVOICE%'"

        dt = dbConn.get_datatable(qry)

        Return dt

    End Function

End Class
