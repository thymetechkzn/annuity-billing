﻿Public Class Payment

    Public Property AccountCode As String
    Public Property strAmount As String
    Public Property Amount As Decimal
    Public Property DODate As String
    Public Property InvoiceDate As String
    Public Property InvoiceAmount As Decimal
    Public Property PostARID As String
    Public Property InvNumber As String
    Public Property InvDescription As String
    Public Property RadioAmount As Decimal
    Public Property RadioDescription As String
    Public Property RadioInvNumber As String
    Public Property RadioPostARID As String
    Public Property Processed As Boolean = False
    Public Property CTID As String


End Class
