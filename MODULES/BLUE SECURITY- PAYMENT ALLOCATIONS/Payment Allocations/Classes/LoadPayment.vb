﻿Imports System.IO

Public Class LoadPayment

    Dim FileLog As LogFile

    Public Function GetPayments(ByVal FilePath As String) As List(Of Payment)

        Dim lstPayments As New List(Of Payment)
        Dim FileLog As New LogFile(Path.GetDirectoryName(FilePath), "Allocation ErrorLog")
        Me.FileLog = FileLog

        'load from file
        FileLog.CreateLog("Reading File - Start")
        lstPayments = ReadFile(FilePath)
        FileLog.CreateLog("Reading File - End")
        'doCheck(lstPayments)
        'consolidate mathcing accounts
        FileLog.CreateLog("Consolidating Customers - Start")
        lstPayments = ConsolidateCustomers(lstPayments)
        FileLog.CreateLog("Consolidating Customers - End")
        'doCheck(lstPayments)
        'remove zero amounts
        FileLog.CreateLog("Removing Zero's - Start")
        lstPayments = RemoveZeroAmountPayments(lstPayments)
        FileLog.CreateLog("Removing Zero's - End")
        'doCheck(lstPayments)

        Return lstPayments

    End Function

    'loads customers and DO amounts into collection of 'payments' from file
    Private Function ReadFile(ByVal Path As String) As List(Of Payment)

        Dim lstPayments As New List(Of Payment)
        Dim fr As StreamReader
        Dim log As String = ""

        fr = New StreamReader(Path)

        'loop through file lines
        While fr.Peek <> -1

            Try

            Dim Payment As New Payment 'declare payment to hold value from line
            Dim FileLine As String = fr.ReadLine 'holds file line
            Dim ValueIndex As Integer = 0 'holds index of value column

            'read each char - assign to correct propert of payment
            For Each c As Char In FileLine
                If c = ";" Then
                    ValueIndex += 1
                Else
                    If ValueIndex = 0 Then
                        Payment.AccountCode += c
                    ElseIf ValueIndex = 1 Then
                        Payment.strAmount += c
                    ElseIf ValueIndex = 2 Then
                            Payment.DODate += c
                        Else
                            Payment.InvoiceDate += c
                        End If
                End If
            Next

            'convert string properties into correct form
            Payment.Amount = Convert.ToDecimal(Payment.strAmount)
            'Payment.PaymentDate = Convert.ToDateTime(Payment.strPaymentDate)
            'add payment to collection
                lstPayments.Add(Payment)

                log &= FileLine & " Loaded no errors" & vbCrLf
            Catch ex As Exception
                log &= ex.ToString & vbCrLf
            End Try

        End While


        FileLog.CreateLog(log)

        Return lstPayments

    End Function

    'combines customers with same customer codes amounts and removes duplicates -consolidate 
    Private Function ConsolidateCustomers(ByVal Payments As List(Of Payment)) As List(Of Payment)


        Dim Log As String = ""
        Dim NewPayments As New List(Of Payment)

        For i As Integer = 0 To Payments.Count - 1

            If Payments(i).Processed = False Then

                Dim item As New Payment

                item.AccountCode = Payments(i).AccountCode
                item.Amount = Payments(i).Amount
                item.DODate = Payments(i).DODate
                item.InvoiceDate = Payments(i).InvoiceDate
                For X As Integer = i + 1 To Payments.Count - 1

                    If item.AccountCode.Equals(Payments(X).AccountCode) Then
                        item.Amount += Payments(X).Amount
                        Payments(X).Processed = True
                    End If

                Next

                NewPayments.Add(item)
                Log &= "Update File;" & item.AccountCode & ";" & item.Amount & ";" & item.DODate & vbCrLf

            End If
        Next

        FileLog.CreateLog(Log)

        Return NewPayments

    End Function

    Private Function RemoveZeroAmountPayments(ByVal Payments As List(Of Payment)) As List(Of Payment)

        Dim log As String = ""
        Dim NewPayments As New List(Of Payment)

        For Each Payment As Payment In Payments
            If Payment.Amount > 0 Then
                Dim newPayment As New Payment
                newPayment.Amount = Payment.Amount
                newPayment.AccountCode = Payment.AccountCode
                newPayment.DODate = Payment.DODate
                newPayment.InvoiceDate = Payment.InvoiceDate
                NewPayments.Add(newPayment)
                log &= "Final File;" & newPayment.AccountCode & ";" & newPayment.Amount & ";" & newPayment.DODate & vbCrLf
            End If
        Next

        FileLog.CreateLog(log)

        Return NewPayments

    End Function

End Class
