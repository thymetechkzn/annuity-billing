﻿Imports Pastel.Evolution
Imports System.IO

Public Class Allocations

    Dim DBCON As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)
    Dim FileLog As LogFile

    'METHODS FOR REFACTORING

    Public Sub PostAndAllocate(ByVal Payments As List(Of Payment), ByVal FilePath As String, ByVal runDate As String)

        Dim lstPayments As New List(Of Payment)
        Dim FileLog As New LogFile(Path.GetDirectoryName(FilePath), "Allocation ErrorLog")
        Me.FileLog = FileLog

        SDKOperations.CreateConnection()

        'load additional payment information
        FileLog.CreateLog("Load IDX - Start")
        Payments = LoadARIdx(Payments, runDate)
        FileLog.CreateLog("Load IDX - End")

        FileLog.CreateLog("Load Invoice Info - Start")
        Payments = getInvoiceInfo(Payments, runDate)
        FileLog.CreateLog("Load Invoice Info - End")

        'create and allocates payments based on debits
        FileLog.CreateLog("Creating Payments - Start")
        CreatePayments(Payments)
        FileLog.CreateLog("Creating Payments - End")

        FileLog.CreateLog("Allocating Recurring Invoice - Start")
        Payments = AllocateRecurringInvoice(Payments)
        FileLog.CreateLog("Allocating Recurring Invoice - End")

        FileLog.CreateLog("Allocating Recurring Radio Invoice - Start")
        AllocateRadioInvoice(Payments)
        FileLog.CreateLog("Allocating Recurring Radio Invoice - End")

    End Sub

    Private Sub CreatePayments(ByVal Payments As List(Of Payment))

        Dim Logs As String = ""

        'CREATE PAYMENTS
        For Each Payment As Payment In Payments

            Try
                'CREATES A PAYMENT FOR A CUSTOMER
                Dim ct As New CustomerTransaction()
                ct.Customer = New Customer(Payment.AccountCode)
                ct.Date = Payment.InvoiceDate
                ct.Reference = Payment.DODate & " Payment"
                ct.TransactionCode = New TransactionCode([Module].AR, "RDOP")
                ct.Description = "Payment received - Thank you "
                ct.Amount = Payment.Amount
                ct.Post()
                Payment.CTID = ct.ID

                Logs &= "Payment Created ;" & Payment.AccountCode & vbCrLf

            Catch ex As Exception
                Logs += ex.Message & ";" & Payment.AccountCode & vbCrLf
            End Try

        Next

        FileLog.CreateLog(Logs)

    End Sub
    Private Function AllocateRecurringInvoice(ByVal Payments As List(Of Payment)) As List(Of Payment)


        ' MsgBox("Allocations will begin")
        Dim Logs As String = ""

        For Each objPayment As Payment In Payments
            Try
                If Not objPayment.PostARID.Equals("") Then

                    Dim invoice As New CustomerTransaction(objPayment.PostARID)
                    Dim Payment As New CustomerTransaction(objPayment.CTID)

                    If Payment.Amount >= invoice.Amount Then

                        Payment.Amount = invoice.Amount

                        invoice.Allocations.Add(Payment)
                        invoice.Allocations.Save()

                        objPayment.Amount -= Payment.Amount

                        Logs += "Allocation Created ;" & objPayment.AccountCode & vbCrLf

                    End If

                End If

            Catch ex As Exception
                Logs += ex.Message & ";" & objPayment.AccountCode & vbCrLf
            End Try

        Next

        FileLog.CreateLog(Logs)

        Return Payments


    End Function
    Private Sub AllocateRadioInvoice(ByVal Payments As List(Of Payment))

        Dim Logs As String = ""

        For Each objPayment As Payment In Payments

            If Not objPayment.RadioPostARID.Equals("") Then
                Try

                    'AutoIdx of debit transaction in PostAR table
                    'PASTEL INVOICE
                    Dim invoice As New CustomerTransaction(objPayment.RadioPostARID)
                    'AutoIdx of credit transaction in PostAR table
                    'PASTEL PAYMENT
                    Dim Payment As New CustomerTransaction(objPayment.CTID)

                    If Payment.Amount >= invoice.Amount Then

                        Payment.Amount = invoice.Amount

                        invoice.Allocations.Add(Payment)
                        invoice.Allocations.Save()

                        Logs += "Allocation Created " & objPayment.ToString & vbCrLf

                    End If

                Catch ex As Exception
                    Logs += ex.Message & ";" & objPayment.AccountCode
                End Try

            End If

        Next

        FileLog.CreateLog(Logs)

    End Sub

    'LOAD INVOICE TO BE RECIPETED INFO
    Private Function LoadARIdx(ByVal Payments As List(Of Payment), ByVal runDate As String) As List(Of Payment)

        Dim SQLQuery As String
        'Dim runDate As String = "May 2014"

        'LOADS AR ID FOR RECURRING FOR INVOICE
        For Each objPayment As Payment In Payments

            SQLQuery = "select AutoIdx from PostAR" & vbCrLf & _
                            "where InvNumKey =" & vbCrLf & _
                            "(select top 1 AutoIndex from InvNum" & vbCrLf & _
                            "where AccountID =" & vbCrLf & _
                            "(select top 1 DCLINK from Client" & vbCrLf & _
                            "where Account = '" & objPayment.AccountCode & "')" & vbCrLf & _
                            "AND" & vbCrLf & _
                            "Message2 = 'RECRUN_" & runDate & "_INVOICE')"

            'SQLQuery = "select AutoIdx from PostAR" & vbCrLf & _
            '                            "where InvNumKey =" & vbCrLf & _
            '                            "(select top 1 AutoIndex from InvNum" & vbCrLf & _
            '                            "where AccountID =" & vbCrLf & _
            '                            "(select top 1 DCLINK from Client" & vbCrLf & _
            '                            "where Account = '" & objPayment.AccountCode & "')" & vbCrLf & _
            '                            "AND" & vbCrLf & _
            '                            "Message2 = 'RECRUN_MAY 2014_INVOICE')"

            objPayment.PostARID = DBCON.get_Val(SQLQuery)

            ' Debug.WriteLine("POSTAR RECUR INVOICE " & objPayment.AccountCode)

        Next


        DBCON.CloseConnection()

        'LOAD AR ID FOR RADIO INVOICE
        For Each objPayment As Payment In Payments

            SQLQuery = "select AutoIdx from PostAR" & vbCrLf & _
                      "where InvNumKey =" & vbCrLf & _
                      "(select top 1 AutoIndex from InvNum" & vbCrLf & _
                      "where AccountID =" & vbCrLf & _
                      "(select top 1 DCLINK from Client" & vbCrLf & _
                      "where Account = '" & objPayment.AccountCode & "')" & vbCrLf & _
                      "AND" & vbCrLf & _
                      "Message2 = 'RECRUN_" & runDate & "_RADIO')"

            'SQLQuery = "select AutoIdx from PostAR" & vbCrLf & _
            '                     "where InvNumKey =" & vbCrLf & _
            '                     "(select top 1 AutoIndex from InvNum" & vbCrLf & _
            '                     "where AccountID =" & vbCrLf & _
            '                     "(select top 1 DCLINK from Client" & vbCrLf & _
            '                     "where Account = '" & objPayment.AccountCode & "')" & vbCrLf & _
            '                     "AND" & vbCrLf & _
            '                     "Message2 = 'RECRUN_MAY 2014_RADIO')"

            objPayment.RadioPostARID = DBCON.get_Val(SQLQuery)

            'Debug.WriteLine("POSTAR RECUR RADIO " & objPayment.AccountCode)

        Next

        ' Debug.WriteLine("-------------------POST AR LOADED!----------------------------")

        DBCON.CloseConnection()

        Return Payments

    End Function

    Private Function getInvoiceInfo(ByVal Payments As List(Of Payment), ByVal runDate As String) As List(Of Payment)

        Dim SQLQuery As String
        Dim DT As New DataTable


        'LOAD RECURRING INVOICE INFORMATION
        For Each objPayment As Payment In Payments
            SQLQuery = "select InvNumber from invnum" & vbCrLf & _
                               "where Message2 = 'RECRUN_" & runDate & "_INVOICE'" & vbCrLf & _
                               "AND" & vbCrLf & _
                               "AccountID in" & vbCrLf & _
                               "(select DCLINK from Client" & vbCrLf & _
                               "where Account = '" & objPayment.AccountCode & "')"




            objPayment.InvNumber = DBCON.get_Val(SQLQuery)
        Next

        DBCON.CloseConnection()

        'LOAD RADIO INVOICE INFORMATION
        For Each objPayment As Payment In Payments

            SQLQuery = "select InvNumber from invnum" & vbCrLf & _
                                   "where Message2 = 'RECRUN_" & runDate & "_RADIO'" & vbCrLf & _
                                   "AND" & vbCrLf & _
                                   "AccountID in" & vbCrLf & _
                                   "(select DCLINK from Client" & vbCrLf & _
                                   "where Account = '" & objPayment.AccountCode & "')"


            objPayment.RadioInvNumber = DBCON.get_Val(SQLQuery)

        Next

        DBCON.CloseConnection()

        Return Payments

    End Function

End Class