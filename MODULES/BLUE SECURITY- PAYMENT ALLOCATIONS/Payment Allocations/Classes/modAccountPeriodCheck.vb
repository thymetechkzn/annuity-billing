﻿'UNBLOCKS AND BLOCKS GL ACCOUNT PERIODS

Public Class GLDates

    Public Property GLMonth As Integer
    Public Property GLYear As Integer

End Class

Module modAccountPeriodCheck

    Private dbconn As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)
    

    'Gets IDs
    Public Function unBlockAccountingPeriods(ByVal lstPayment As List(Of Payment)) As List(Of String)

        Dim index As Integer = 0
        Dim DT As New DataTable
        Dim lstID As New List(Of String)

        Dim qry As String = "SELECT" & vbCrLf & _
                            "idPeriod" & vbCrLf & _
                            "FROM" & vbCrLf & _
                            "_etblPeriod " & vbCrLf & _
                            "WHERE" & vbCrLf & _
                            "bBlocked = '1'" & vbCrLf & _
                            "AND" & vbCrLf & _
                            "("

        For Each glDate In getGLDates(getInvDates(lstPayment))

            index = index + 1

            If index = 1 Then

                qry = qry + _
                        "(" & vbCrLf & _
                        "MONTH(dPeriodDate) = " & glDate.GLMonth & vbCrLf & _
                        "AND" & vbCrLf & _
                        "YEAR(dPeriodDate) = " & glDate.GLYear & vbCrLf & _
                        ")" & vbCrLf & _
                        ""

            Else

                qry = qry + _
                            "OR" & vbCrLf & _
                        "(" & vbCrLf & _
                        "MONTH(dPeriodDate) = " & glDate.GLMonth & vbCrLf & _
                        "AND" & vbCrLf & _
                        "YEAR(dPeriodDate) = " & glDate.GLYear & vbCrLf & _
                        ")" & vbCrLf & _
                        ""

            End If

        Next

        qry = qry + ")"

        'dbconn.createNewConnection()
        DT = dbconn.get_datatable(qry)
        dbconn.CloseConnection()

        For Each id In DT.Rows

            lstID.Add(id("idPeriod"))

        Next

        Return lstID

    End Function

    'Get dates from file - remove duplicates
    Private Function getInvDates(ByVal lstPayment As List(Of Payment)) As List(Of String)

        'Declare variables
        Dim lstDates As New List(Of String)

        For Each invDate In lstPayment

            lstDates.Add(invDate.InvoiceDate)

        Next

        lstDates = lstDates.Distinct().ToList

        Return lstDates

    End Function
    'Gets Months and Years
    Private Function getGLDates(ByVal lstFileDates As List(Of String)) As List(Of GLDates)

        Dim lstGLDate As New List(Of GLDates)
        Dim exists As Boolean = False

        For Each fileDate In lstFileDates

            Dim glDates As New GLDates

            glDates.GLMonth = Month(fileDate)
            glDates.GLYear = Year(fileDate)
            exists = False

            If lstGLDate.Count > 0 Then

                For Each addedDate In lstGLDate

                    If addedDate.GLMonth = glDates.GLMonth And addedDate.GLYear = glDates.GLYear Then

                        exists = True

                    End If

                Next

            End If

            If exists = False Then
                lstGLDate.Add(glDates)
            End If


        Next

        Return lstGLDate

    End Function


    'Updates Data
    Public Sub changeGLPeriodBlock(ByVal lstID As List(Of String), ByVal blockType As String)

        Dim qry As String = "UPDATE _etblPeriod" & vbCrLf & _
                            "SET bBlocked = '" & blockType & "'" & vbCrLf & _
                            "WHERE" & vbCrLf & _
                            "idPeriod IN ("
        Dim index As Integer = 0

        For Each id In lstID

            index = index + 1

            If index = 1 Then

                qry = qry & id

            Else

                qry = qry & "," & id

            End If

        Next

        qry = qry & ")"

        dbconn.setSqlString(qry)
        dbconn.createNewConnection()
        dbconn.executeNonQuery()
        dbconn.CloseConnection()

    End Sub



End Module
