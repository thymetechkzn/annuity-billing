﻿Public Class frmReceiptingMain

    Dim Path As String
    Dim MonthPeriod As String
    Dim lstID As List(Of String)

    Sub New(ByVal dBInstance As String, ByVal dBName As String, _
            ByVal dBUser As String, ByVal dBPassword As String, ByVal evoCommon As String)

        ' This call is required by the designer.
        InitializeComponent()

        My.Settings.DBServer = dBInstance
        My.Settings.PASCompany = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.PASCommon = evoCommon

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click

        MonthPeriod = cboReceiptingPeriodRun.SelectedItem.ToString()

        Try

            BackgroundWorker1.RunWorkerAsync()
            frmProcessing.WindowState = FormWindowState.Normal
            frmProcessing.Show()

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

        Dim Payments As New List(Of Payment)
        Dim LoadPayments As New LoadPayment
        Dim DOPaymentImport As New Allocations

        'load payments from file
        Payments = LoadPayments.GetPayments(Path)

        'Get dates to be invoiced
        lstID = modAccountPeriodCheck.unBlockAccountingPeriods(Payments)

        If lstID.Count > 0 Then

            modAccountPeriodCheck.changeGLPeriodBlock(lstID, "0")

        End If


        'post payments and allocate in pastel
        DOPaymentImport.PostAndAllocate(Payments, Path, MonthPeriod)

        If lstID.Count > 0 Then

            modAccountPeriodCheck.changeGLPeriodBlock(lstID, "1")

        End If


        If Me.InvokeRequired Then
            MsgBox("The receipting & allocations have completed!")
            Me.Invoke(New MethodInvoker(AddressOf CloseProcessing))
        Else
            Me.Close()
        End If

    End Sub

    Private Sub CloseProcessing() Handles BackgroundWorker1.RunWorkerCompleted
        frmProcessing.Close()
    End Sub

    Private Sub ExitToolStripMenuItem_Click_1(sender As System.Object, e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub SelectImportFileToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles SelectImportFileToolStripMenuItem.Click
        Dim FileDialog As New OpenFileDialog

        FileDialog.InitialDirectory = "C:\"
        FileDialog.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*"
        FileDialog.FilterIndex = 2
        FileDialog.RestoreDirectory = False

        If FileDialog.ShowDialog() = DialogResult.OK Then

            'Me.WindowState = FormWindowState.Minimized

            Path = FileDialog.FileName

            Button1.Enabled = True

        End If

    End Sub

    Private Sub frmReceiptingMain_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        loadDebitOrderRuns()
        Button1.Enabled = False

    End Sub

    'Populate Drop Down
    Private Sub loadDebitOrderRuns()

        'Declare Variables
        Dim repLoadDORuns As New repGetRunPeriod
        Dim dr As DataRow

        For Each dr In repLoadDORuns.getDebitOrderRuns().Rows

            cboReceiptingPeriodRun.Items.Add(dr("invDescriptionSuffix"))


        Next

    End Sub

End Class
