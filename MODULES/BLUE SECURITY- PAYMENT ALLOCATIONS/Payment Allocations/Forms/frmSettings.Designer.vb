﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSettings
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmSettings))
        Me.btnSave = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtDBServer = New System.Windows.Forms.TextBox()
        Me.txtDBUser = New System.Windows.Forms.TextBox()
        Me.txtDBPassword = New System.Windows.Forms.TextBox()
        Me.txtPASCompany = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtPASCommon = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'btnSave
        '
        Me.btnSave.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSave.Location = New System.Drawing.Point(234, 247)
        Me.btnSave.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnSave.Name = "btnSave"
        Me.btnSave.Size = New System.Drawing.Size(231, 34)
        Me.btnSave.TabIndex = 0
        Me.btnSave.Text = "Save Details"
        Me.btnSave.UseVisualStyleBackColor = False
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 38)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(88, 19)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "SQL SERVER"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 77)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(72, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "SQL USER"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 117)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(112, 19)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "SQL PASSWORD"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 156)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(158, 19)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "EVOLUTION COMPANY"
        '
        'txtDBServer
        '
        Me.txtDBServer.Location = New System.Drawing.Point(234, 36)
        Me.txtDBServer.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtDBServer.Name = "txtDBServer"
        Me.txtDBServer.Size = New System.Drawing.Size(231, 27)
        Me.txtDBServer.TabIndex = 5
        '
        'txtDBUser
        '
        Me.txtDBUser.Location = New System.Drawing.Point(234, 75)
        Me.txtDBUser.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtDBUser.Name = "txtDBUser"
        Me.txtDBUser.Size = New System.Drawing.Size(231, 27)
        Me.txtDBUser.TabIndex = 6
        '
        'txtDBPassword
        '
        Me.txtDBPassword.Location = New System.Drawing.Point(234, 115)
        Me.txtDBPassword.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtDBPassword.Name = "txtDBPassword"
        Me.txtDBPassword.Size = New System.Drawing.Size(231, 27)
        Me.txtDBPassword.TabIndex = 7
        Me.txtDBPassword.UseSystemPasswordChar = True
        '
        'txtPASCompany
        '
        Me.txtPASCompany.Location = New System.Drawing.Point(234, 154)
        Me.txtPASCompany.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPASCompany.Name = "txtPASCompany"
        Me.txtPASCompany.Size = New System.Drawing.Size(231, 27)
        Me.txtPASCompany.TabIndex = 8
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(23, 196)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(154, 19)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "EVOLUTON COMMON"
        '
        'txtPASCommon
        '
        Me.txtPASCommon.Location = New System.Drawing.Point(234, 193)
        Me.txtPASCommon.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.txtPASCommon.Name = "txtPASCommon"
        Me.txtPASCommon.Size = New System.Drawing.Size(231, 27)
        Me.txtPASCommon.TabIndex = 10
        '
        'frmSettings
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(502, 304)
        Me.Controls.Add(Me.txtPASCommon)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.txtPASCompany)
        Me.Controls.Add(Me.txtDBPassword)
        Me.Controls.Add(Me.txtDBUser)
        Me.Controls.Add(Me.txtDBServer)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.btnSave)
        Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmSettings"
        Me.Text = "Connection Settings"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnSave As System.Windows.Forms.Button
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtDBServer As System.Windows.Forms.TextBox
    Friend WithEvents txtDBUser As System.Windows.Forms.TextBox
    Friend WithEvents txtDBPassword As System.Windows.Forms.TextBox
    Friend WithEvents txtPASCompany As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtPASCommon As System.Windows.Forms.TextBox
End Class
