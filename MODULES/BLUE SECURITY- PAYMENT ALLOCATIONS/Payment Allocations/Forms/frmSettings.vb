﻿Imports Pastel.Evolution

Public Class frmSettings

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles btnSave.Click

        My.Settings.DBServer = txtDBServer.Text
        My.Settings.DBUser = txtDBUser.Text
        My.Settings.DBPassword = txtDBPassword.Text
        My.Settings.PASCompany = txtPASCompany.Text
        My.Settings.PASCommon = txtPASCommon.Text

        My.Settings.Save()

        MsgBox("Settings have been saved")

        Me.Close()

    End Sub

    Private Sub frmSettings_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        txtDBServer.Text = My.Settings.DBServer
        txtDBUser.Text = My.Settings.DBUser
        txtDBPassword.Text = My.Settings.DBPassword
        txtPASCompany.Text = My.Settings.PASCompany
        txtPASCommon.Text = My.Settings.PASCommon

    End Sub

    Private Sub btnTest_Click(sender As System.Object, e As System.EventArgs)

        Try

            My.Settings.DBServer = txtDBServer.Text
            My.Settings.DBUser = txtDBUser.Text
            My.Settings.DBPassword = txtDBPassword.Text
            My.Settings.PASCompany = txtPASCompany.Text
            My.Settings.PASCommon = txtPASCommon.Text

            My.Settings.Save()

            If SDKOperations.CreateConnection() = True Then
                MsgBox("Connection Successful")
            Else
                MsgBox("Connection Details Incorrect")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

End Class