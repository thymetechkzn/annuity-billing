﻿Public Class RepClients

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    '#New Contract Fee

    Public Function getClientAdhocDT(ByVal groupCodeQryLine As String, ByVal itemCodeQryLine As String, ByVal increaseMonthQryLine As String) As DataTable

        'Declare Variables
        Dim qry As String
        Dim clientDT As DataTable

        If increaseMonthQryLine = "All" Then

            qry = "SELECT" & vbCrLf & _
          "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
          "C.Account AS 'Customer Code'," & vbCrLf & _
          "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
          "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account) AS 'Debtor Account Code'," & vbCrLf & _
          "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
          "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
          "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
          "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
          "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
          "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
            "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
            "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
            "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
            "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
            "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
          "C.Name as 'Name'," & vbCrLf & _
          "C.Title AS 'Contract Title'," & vbCrLf & _
          "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
          "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
          "a.Code as 'Group Code'," & vbCrLf & _
          "si.Code as 'Item Code'," & vbCrLf & _
        "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
        "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
        "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
        "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
        "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
          "sl.cProductReference as 'Current Value'" & vbCrLf & _
          "FROM" & vbCrLf & _
          "Client C" & vbCrLf & _
          "INNER JOIN" & vbCrLf & _
          "Areas A" & vbCrLf & _
          "ON" & vbCrLf & _
          "C.iAreasID = A.idAreas" & vbCrLf & _
          "INNER JOIN" & vbCrLf & _
          "_rtblStockLinks SL" & vbCrLf & _
          "ON" & vbCrLf & _
          "C.DCLink = SL.iDCLink" & vbCrLf & _
          "INNER JOIN" & vbCrLf & _
          "StkItem SI" & vbCrLf & _
          "ON" & vbCrLf & _
          "SL.iStockID = SI.StockLink" & vbCrLf & _
          "where" & vbCrLf & _
          groupCodeQryLine & vbCrLf & _
          "AND" & vbCrLf & _
          itemCodeQryLine & vbCrLf & _
          "AND" & vbCrLf & _
          "C.ubARContractActive = 1" & vbCrLf & _
          "Order By 'Debtor Account Code'"

        Else

            qry = "SELECT" & vbCrLf & _
          "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
          "C.Account AS 'Customer Code'," & vbCrLf & _
          "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
          "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account) AS 'Debtor Account Code'," & vbCrLf & _
          "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
          "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
          "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
          "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
          "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
          "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
            "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
            "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
            "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
            "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
            "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
          "C.Name as 'Name'," & vbCrLf & _
          "C.Title AS 'Contract Title'," & vbCrLf & _
          "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
          "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
          "a.Code as 'Group Code'," & vbCrLf & _
          "si.Code as 'Item Code'," & vbCrLf & _
           "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
        "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
        "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
        "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
        "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
          "sl.cProductReference as 'Current Value'" & vbCrLf & _
          "FROM" & vbCrLf & _
          "Client C" & vbCrLf & _
          "INNER JOIN" & vbCrLf & _
          "Areas A" & vbCrLf & _
          "ON" & vbCrLf & _
          "C.iAreasID = A.idAreas" & vbCrLf & _
          "INNER JOIN" & vbCrLf & _
          "_rtblStockLinks SL" & vbCrLf & _
          "ON" & vbCrLf & _
          "C.DCLink = SL.iDCLink" & vbCrLf & _
          "INNER JOIN" & vbCrLf & _
          "StkItem SI" & vbCrLf & _
          "ON" & vbCrLf & _
          "SL.iStockID = SI.StockLink" & vbCrLf & _
          "where" & vbCrLf & _
          groupCodeQryLine & vbCrLf & _
          "AND" & vbCrLf & _
          itemCodeQryLine & vbCrLf & _
          "AND" & vbCrLf & _
          increaseMonthQryLine & vbCrLf & _
          "AND" & vbCrLf & _
          "C.ubARContractActive = 1" & vbCrLf & _
          "Order By 'Debtor Account Code'"

        End If

        clientDT = dbConn.get_datatable(qry)

        'Adding Contract Fee Datatables to Main Datatable
        For Each row As DataRow In (getClientContractServiceFeesDT(groupCodeQryLine, itemCodeQryLine, increaseMonthQryLine)).Rows
            clientDT.LoadDataRow(row.ItemArray, False)
        Next

        For Each row As DataRow In (getClientContractSMSFeesDT(groupCodeQryLine, itemCodeQryLine, increaseMonthQryLine)).Rows
            clientDT.LoadDataRow(row.ItemArray, False)
        Next

        For Each row As DataRow In (getClientContractReportsFeesDT(groupCodeQryLine, itemCodeQryLine, increaseMonthQryLine)).Rows
            clientDT.LoadDataRow(row.ItemArray, False)
        Next

        For Each row As DataRow In (getClientContractSurveilanceFeesDT(groupCodeQryLine, itemCodeQryLine, increaseMonthQryLine)).Rows
            clientDT.LoadDataRow(row.ItemArray, False)
        Next

        For Each row As DataRow In (getClientContractAngelsFeesDT(groupCodeQryLine, itemCodeQryLine, increaseMonthQryLine)).Rows
            clientDT.LoadDataRow(row.ItemArray, False)
        Next

        'Step 3 - Last Step for  #New Contract Fee - ADD NEW FUNCTION
        For Each row As DataRow In (getClientContractGuardingFeesDT(groupCodeQryLine, itemCodeQryLine, increaseMonthQryLine)).Rows
            clientDT.LoadDataRow(row.ItemArray, False)
        Next

        'Step 3 - Last Step for  #New Contract Fee - ADD NEW FUNCTION
        For Each row As DataRow In (getClientContractPanicAppFeesDT(groupCodeQryLine, itemCodeQryLine, increaseMonthQryLine)).Rows
            clientDT.LoadDataRow(row.ItemArray, False)
        Next

        Return clientDT

    End Function

    'CONTRACT FEES
    Private Function getClientContractServiceFeesDT(ByVal groupCodeQryLine As String, ByVal itemCodeQryLine As String, ByVal increaseMonthQryLine As String) As DataTable

        'Declare Variables
        Dim qry As String = ""


        If itemCodeQryLine.Contains("#SERFEE") = True Then

            If increaseMonthQryLine = "All" Then

                qry = "SELECT" & vbCrLf & _
                                    "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                                    "C.Account AS 'Customer Code'," & vbCrLf & _
                                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account) AS 'Debtor Account Code'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                                    "C.Name as 'Name'," & vbCrLf & _
                                    "C.Title AS 'Contract Title'," & vbCrLf & _
                                    "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                                    "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                                    "a.Code as 'Group Code'," & vbCrLf & _
                                    "'#SERFEE - Contract Fee' as 'ItemCode'," & vbCrLf & _
                                            "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                                "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                                "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                                "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                                "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                                    "C.ufARMonthlyServFeeValue 'Current Value'" & vbCrLf & _
                                    "FROM" & vbCrLf & _
                                    "Client C" & vbCrLf & _
                                    "INNER JOIN" & vbCrLf & _
                                    "Areas A" & vbCrLf & _
                                    "ON" & vbCrLf & _
                                    "C.iAreasID = A.idAreas" & vbCrLf & _
                                     "where" & vbCrLf & _
                                      groupCodeQryLine & vbCrLf & _
                                      "AND" & vbCrLf & _
                                      "C.ubARContractActive = 1" & vbCrLf & _
                                      "and" & vbCrLf & _
                                      "C.ufARMonthlyServFeeValue <> 0" & vbCrLf & _
                                    "Order By 'Debtor Account Code'"

            Else

                qry = "SELECT" & vbCrLf & _
                    "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                    "C.Account AS 'Customer Code'," & vbCrLf & _
                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account) AS 'Debtor Account Code'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                    "C.Name as 'Name'," & vbCrLf & _
                    "C.Title AS 'Contract Title'," & vbCrLf & _
                    "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                    "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                    "a.Code as 'Group Code'," & vbCrLf & _
                    "'#SERFEE - Contract Fee' as 'ItemCode'," & vbCrLf & _
                    "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                    "C.ufARMonthlyServFeeValue 'Current Value'" & vbCrLf & _
                    "FROM" & vbCrLf & _
                    "Client C" & vbCrLf & _
                    "INNER JOIN" & vbCrLf & _
                    "Areas A" & vbCrLf & _
                    "ON" & vbCrLf & _
                    "C.iAreasID = A.idAreas" & vbCrLf & _
                     "where" & vbCrLf & _
                      groupCodeQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      increaseMonthQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      "C.ubARContractActive = 1" & vbCrLf & _
                      "and" & vbCrLf & _
                      "C.ufARMonthlyServFeeValue <> 0" & vbCrLf & _
                    "Order By 'Debtor Account Code'"

            End If

        Else

            qry = "select" & vbCrLf & _
                    "*" & vbCrLf & _
                    "from" & vbCrLf & _
                    "Client" & vbCrLf & _
                    "where" & vbCrLf & _
                    "Account = '999'"

        End If


        Return dbConn.get_datatable(qry)

    End Function
    Private Function getClientContractSMSFeesDT(ByVal groupCodeQryLine As String, ByVal itemCodeQryLine As String, ByVal increaseMonthQryLine As String) As DataTable

        'Declare Variables
        Dim qry As String = ""


        If itemCodeQryLine.Contains("#OCSMS") = True Then

            If increaseMonthQryLine = "All" Then

                qry = "SELECT" & vbCrLf & _
                     "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                   "C.Account AS 'Customer Code'," & vbCrLf & _
                   "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                   "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                   "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                   "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                   "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                   "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                   "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                   "C.Name as 'Name'," & vbCrLf & _
                     "C.Title AS 'Contract Title'," & vbCrLf & _
                     "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                     "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                     "a.Code as 'Group Code'," & vbCrLf & _
                     "'#OCSMS - Contract Fee' as 'ItemCode'," & vbCrLf & _
                     "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                     "C.ufAROpenCloseSMSValue 'Current Value'" & vbCrLf & _
                     "FROM" & vbCrLf & _
                     "Client C" & vbCrLf & _
                     "INNER JOIN" & vbCrLf & _
                     "Areas A" & vbCrLf & _
                     "ON" & vbCrLf & _
                     "C.iAreasID = A.idAreas" & vbCrLf & _
                     "where" & vbCrLf & _
                     groupCodeQryLine & vbCrLf & _
                     "AND" & vbCrLf & _
                     "C.ubARContractActive = 1" & vbCrLf & _
                     "and" & vbCrLf & _
                     "C.ufAROpenCloseSMSValue <> 0" & vbCrLf & _
                     "Order By 'Debtor Account Code'"

            Else

                qry = "SELECT" & vbCrLf & _
                     "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                   "C.Account AS 'Customer Code'," & vbCrLf & _
                   "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                   "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                   "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                   "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                   "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                   "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                   "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                   "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                   "C.Name as 'Name'," & vbCrLf & _
                     "C.Title AS 'Contract Title'," & vbCrLf & _
                     "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                     "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                     "a.Code as 'Group Code'," & vbCrLf & _
                     "'#OCSMS - Contract Fee' as 'ItemCode'," & vbCrLf & _
                      "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                     "C.ufAROpenCloseSMSValue 'Current Value'" & vbCrLf & _
                     "FROM" & vbCrLf & _
                     "Client C" & vbCrLf & _
                     "INNER JOIN" & vbCrLf & _
                     "Areas A" & vbCrLf & _
                     "ON" & vbCrLf & _
                     "C.iAreasID = A.idAreas" & vbCrLf & _
                     "where" & vbCrLf & _
                     groupCodeQryLine & vbCrLf & _
                     "AND" & vbCrLf & _
                     increaseMonthQryLine & vbCrLf & _
                     "AND" & vbCrLf & _
                     "C.ubARContractActive = 1" & vbCrLf & _
                     "and" & vbCrLf & _
                     "C.ufAROpenCloseSMSValue <> 0" & vbCrLf & _
                     "Order By 'Debtor Account Code'"

            End If

        Else

            qry = "select" & vbCrLf & _
                    "*" & vbCrLf & _
                    "from" & vbCrLf & _
                    "Client" & vbCrLf & _
                    "where" & vbCrLf & _
                    "Account = '999'"

        End If

        Return dbConn.get_datatable(qry)

    End Function
    Private Function getClientContractReportsFeesDT(ByVal groupCodeQryLine As String, ByVal itemCodeQryLine As String, ByVal increaseMonthQryLine As String) As DataTable

        'Declare Variables
        Dim qry As String = ""


        If itemCodeQryLine.Contains("#MONREP") = True Then

            If increaseMonthQryLine = "All" Then

                qry = "SELECT" & vbCrLf & _
                        "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                    "C.Account AS 'Customer Code'," & vbCrLf & _
                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                    "C.Name as 'Name'," & vbCrLf & _
                      "C.Title AS 'Contract Title'," & vbCrLf & _
                      "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                      "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                      "a.Code as 'Group Code'," & vbCrLf & _
                      "'#MONREP - Contract Fee' as 'ItemCode'," & vbCrLf & _
                     "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                      "C.ufARMonthlyReportsValue 'Current Value'" & vbCrLf & _
                      "FROM" & vbCrLf & _
                      "Client C" & vbCrLf & _
                      "INNER JOIN" & vbCrLf & _
                      "Areas A" & vbCrLf & _
                      "ON" & vbCrLf & _
                      "C.iAreasID = A.idAreas" & vbCrLf & _
                      "where" & vbCrLf & _
                      groupCodeQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      "C.ubARContractActive = 1" & vbCrLf &
                      "and" & vbCrLf & _
                      "C.ufARMonthlyReportsValue <> 0" & vbCrLf & _
                      "Order By 'Debtor Account Code'"

            Else

                qry = "SELECT" & vbCrLf & _
                        "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                    "C.Account AS 'Customer Code'," & vbCrLf & _
                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                    "C.Name as 'Name'," & vbCrLf & _
                      "C.Title AS 'Contract Title'," & vbCrLf & _
                      "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                      "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                      "a.Code as 'Group Code'," & vbCrLf & _
                      "'#MONREP - Contract Fee' as 'ItemCode'," & vbCrLf & _
                      "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                      "C.ufARMonthlyReportsValue 'Current Value'" & vbCrLf & _
                      "FROM" & vbCrLf & _
                      "Client C" & vbCrLf & _
                      "INNER JOIN" & vbCrLf & _
                      "Areas A" & vbCrLf & _
                      "ON" & vbCrLf & _
                      "C.iAreasID = A.idAreas" & vbCrLf & _
                      "where" & vbCrLf & _
                      groupCodeQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      increaseMonthQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      "C.ubARContractActive = 1" & vbCrLf &
                      "and" & vbCrLf & _
                      "C.ufARMonthlyReportsValue <> 0" & vbCrLf & _
                      "Order By 'Debtor Account Code'"

            End If

        Else

            qry = "select" & vbCrLf & _
                    "*" & vbCrLf & _
                    "from" & vbCrLf & _
                    "Client" & vbCrLf & _
                    "where" & vbCrLf & _
                    "Account = '999'"

        End If

        Return dbConn.get_datatable(qry)

    End Function
    Private Function getClientContractSurveilanceFeesDT(ByVal groupCodeQryLine As String, ByVal itemCodeQryLine As String, ByVal increaseMonthQryLine As String) As DataTable

        'Declare Variables
        Dim qry As String = ""


        If itemCodeQryLine.Contains("#REMSUR") = True Then

            If increaseMonthQryLine = "All" Then

                qry = "SELECT" & vbCrLf & _
                        "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                    "C.Account AS 'Customer Code'," & vbCrLf & _
                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                    "C.Name as 'Name'," & vbCrLf & _
                      "C.Title AS 'Contract Title'," & vbCrLf & _
                      "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                      "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                      "a.Code as 'Group Code'," & vbCrLf & _
                      "'#REMSUR - Contract Fee' as 'ItemCode'," & vbCrLf & _
                      "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                      "C.ufARRemoteSurvValue 'Current Value'" & vbCrLf & _
                      "FROM" & vbCrLf & _
                      "Client C" & vbCrLf & _
                      "INNER JOIN" & vbCrLf & _
                      "Areas A" & vbCrLf & _
                      "ON" & vbCrLf & _
                      "C.iAreasID = A.idAreas" & vbCrLf & _
                      "where" & vbCrLf & _
                      groupCodeQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      "C.ubARContractActive = 1" & vbCrLf & _
                      "and" & vbCrLf & _
                      "C.ufARRemoteSurvValue <> 0" & vbCrLf & _
                      "Order By 'Debtor Account Code'"

            Else

                qry = "SELECT" & vbCrLf & _
                            "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                        "C.Account AS 'Customer Code'," & vbCrLf & _
                        "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                        "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                        "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                        "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                        "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                        "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                        "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                        "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                        "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                        "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                        "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                        "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                        "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                        "C.Name as 'Name'," & vbCrLf & _
                          "C.Title AS 'Contract Title'," & vbCrLf & _
                          "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                          "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                          "a.Code as 'Group Code'," & vbCrLf & _
                          "'#REMSUR - Contract Fee' as 'ItemCode'," & vbCrLf & _
                          "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                        "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                        "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                        "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                        "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                          "C.ufARRemoteSurvValue 'Current Value'" & vbCrLf & _
                          "FROM" & vbCrLf & _
                          "Client C" & vbCrLf & _
                          "INNER JOIN" & vbCrLf & _
                          "Areas A" & vbCrLf & _
                          "ON" & vbCrLf & _
                          "C.iAreasID = A.idAreas" & vbCrLf & _
                          "where" & vbCrLf & _
                          groupCodeQryLine & vbCrLf & _
                          "AND" & vbCrLf & _
                          increaseMonthQryLine & vbCrLf & _
                          "AND" & vbCrLf & _
                          "C.ubARContractActive = 1" & vbCrLf & _
                          "and" & vbCrLf & _
                          "C.ufARRemoteSurvValue <> 0" & vbCrLf & _
                          "Order By 'Debtor Account Code'"

            End If

        Else

            qry = "select" & vbCrLf & _
                    "*" & vbCrLf & _
                    "from" & vbCrLf & _
                    "Client" & vbCrLf & _
                    "where" & vbCrLf & _
                    "Account = '999'"

        End If

        Return dbConn.get_datatable(qry)

    End Function
    Private Function getClientContractAngelsFeesDT(ByVal groupCodeQryLine As String, ByVal itemCodeQryLine As String, ByVal increaseMonthQryLine As String) As DataTable

        'Declare Variables
        Dim qry As String = ""


        If itemCodeQryLine.Contains("#ANGELS") = True Then

            If increaseMonthQryLine = "All" Then

                qry = "SELECT" & vbCrLf & _
                                        "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                                    "C.Account AS 'Customer Code'," & vbCrLf & _
                                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                                    "C.Name as 'Name'," & vbCrLf & _
                                      "C.Title AS 'Contract Title'," & vbCrLf & _
                                      "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                                      "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                                      "a.Code as 'Group Code'," & vbCrLf & _
                                      "'#ANGELS - Contract Fee' as 'ItemCode'," & vbCrLf & _
                                     "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                                      "C.ufARBlueAngelsTrustValue 'Current Value'" & vbCrLf & _
                                      "FROM" & vbCrLf & _
                                      "Client C" & vbCrLf & _
                                      "INNER JOIN" & vbCrLf & _
                                      "Areas A" & vbCrLf & _
                                      "ON" & vbCrLf & _
                                      "C.iAreasID = A.idAreas" & vbCrLf & _
                                      "where" & vbCrLf & _
                                      groupCodeQryLine & vbCrLf & _
                                      "AND" & vbCrLf & _
                                      "C.ubARContractActive = 1" & vbCrLf & _
                                      "and" & vbCrLf & _
                                      "C.ufARBlueAngelsTrustValue <> 0" & vbCrLf & _
                                      "Order By 'Debtor Account Code'"

            Else

                qry = "SELECT" & vbCrLf & _
                        "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                    "C.Account AS 'Customer Code'," & vbCrLf & _
                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                    "C.Name as 'Name'," & vbCrLf & _
                      "C.Title AS 'Contract Title'," & vbCrLf & _
                      "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                      "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                      "a.Code as 'Group Code'," & vbCrLf & _
                      "'#ANGELS - Contract Fee' as 'ItemCode'," & vbCrLf & _
                      "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                      "C.ufARBlueAngelsTrustValue 'Current Value'" & vbCrLf & _
                      "FROM" & vbCrLf & _
                      "Client C" & vbCrLf & _
                      "INNER JOIN" & vbCrLf & _
                      "Areas A" & vbCrLf & _
                      "ON" & vbCrLf & _
                      "C.iAreasID = A.idAreas" & vbCrLf & _
                      "where" & vbCrLf & _
                      groupCodeQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      increaseMonthQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      "C.ubARContractActive = 1" & vbCrLf & _
                      "and" & vbCrLf & _
                      "C.ufARBlueAngelsTrustValue <> 0" & vbCrLf & _
                      "Order By 'Debtor Account Code'"

            End If

        Else

            qry = "select" & vbCrLf & _
                    "*" & vbCrLf & _
                    "from" & vbCrLf & _
                    "Client" & vbCrLf & _
                    "where" & vbCrLf & _
                    "Account = '999'"

        End If

        Return dbConn.get_datatable(qry)

    End Function
    'Step 1 - '#New Contract Fee - ADD NEW FUNCTION 
    Private Function getClientContractGuardingFeesDT(ByVal groupCodeQryLine As String, ByVal itemCodeQryLine As String, ByVal increaseMonthQryLine As String) As DataTable

        'Declare Variables
        Dim qry As String = ""


        If itemCodeQryLine.Contains("#GUARD") = True Then 'Step 2 - '#New Contract Fee - Change Code

            If increaseMonthQryLine = "All" Then

                'Step 3 - '#New Contract Fee - Add New Contract Fee Field to Script and remove the copied one (CHECK EVERYWHERE FOR COPIED FEE)
                qry = "SELECT" & vbCrLf & _
                                        "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                                    "C.Account AS 'Customer Code'," & vbCrLf & _
                                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                                    "C.Name as 'Name'," & vbCrLf & _
                                      "C.Title AS 'Contract Title'," & vbCrLf & _
                                      "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                                      "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                                      "a.Code as 'Group Code'," & vbCrLf & _
                                      "'#GUARD - Contract Fee' as 'ItemCode'," & vbCrLf & _
                                     "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                                      "C.ufARGuardingServices 'Current Value'" & vbCrLf & _
                                      "FROM" & vbCrLf & _
                                      "Client C" & vbCrLf & _
                                      "INNER JOIN" & vbCrLf & _
                                      "Areas A" & vbCrLf & _
                                      "ON" & vbCrLf & _
                                      "C.iAreasID = A.idAreas" & vbCrLf & _
                                      "where" & vbCrLf & _
                                      groupCodeQryLine & vbCrLf & _
                                      "AND" & vbCrLf & _
                                      "C.ubARContractActive = 1" & vbCrLf & _
                                      "and" & vbCrLf & _
                                      "C.ufARGuardingServices <> 0" & vbCrLf & _
                                      "Order By 'Debtor Account Code'"

            Else 'Step 4 - '#New Contract Fee - Add New Contract Fee Field to Script and remove the copied one (CHECK EVERYWHERE FOR COPIED FEE)

                qry = "SELECT" & vbCrLf & _
                        "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                    "C.Account AS 'Customer Code'," & vbCrLf & _
                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                    "C.Name as 'Name'," & vbCrLf & _
                      "C.Title AS 'Contract Title'," & vbCrLf & _
                      "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                      "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                      "a.Code as 'Group Code'," & vbCrLf & _
                      "'#GUARD - Contract Fee' as 'ItemCode'," & vbCrLf & _
                      "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                      "C.ufARGuardingServices 'Current Value'" & vbCrLf & _
                      "FROM" & vbCrLf & _
                      "Client C" & vbCrLf & _
                      "INNER JOIN" & vbCrLf & _
                      "Areas A" & vbCrLf & _
                      "ON" & vbCrLf & _
                      "C.iAreasID = A.idAreas" & vbCrLf & _
                      "where" & vbCrLf & _
                      groupCodeQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      increaseMonthQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      "C.ubARContractActive = 1" & vbCrLf & _
                      "and" & vbCrLf & _
                      "C.ufARGuardingServices <> 0" & vbCrLf & _
                      "Order By 'Debtor Account Code'"

            End If

        Else

            qry = "select" & vbCrLf & _
                    "*" & vbCrLf & _
                    "from" & vbCrLf & _
                    "Client" & vbCrLf & _
                    "where" & vbCrLf & _
                    "Account = '999'"

        End If

        Return dbConn.get_datatable(qry)

    End Function

    'Step 1 - '#New Contract Fee - ADD NEW FUNCTION  - PANIC APP
    Private Function getClientContractPanicAppFeesDT(ByVal groupCodeQryLine As String, ByVal itemCodeQryLine As String, ByVal increaseMonthQryLine As String) As DataTable

        'Declare Variables
        Dim qry As String = ""


        If itemCodeQryLine.Contains("#APP") = True Then 'Step 2 - '#New Contract Fee - Change Code

            If increaseMonthQryLine = "All" Then

                'Step 3 - '#New Contract Fee - Add New Contract Fee Field to Script and remove the copied one (CHECK EVERYWHERE FOR COPIED FEE)
                qry = "SELECT" & vbCrLf & _
                                        "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                                    "C.Account AS 'Customer Code'," & vbCrLf & _
                                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                                    "C.Name as 'Name'," & vbCrLf & _
                                      "C.Title AS 'Contract Title'," & vbCrLf & _
                                      "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                                      "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                                      "a.Code as 'Group Code'," & vbCrLf & _
                                      "'#APP - Contract Fee' as 'ItemCode'," & vbCrLf & _
                                     "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                                      "C.ufARBluePanicApp 'Current Value'" & vbCrLf & _
                                      "FROM" & vbCrLf & _
                                      "Client C" & vbCrLf & _
                                      "INNER JOIN" & vbCrLf & _
                                      "Areas A" & vbCrLf & _
                                      "ON" & vbCrLf & _
                                      "C.iAreasID = A.idAreas" & vbCrLf & _
                                      "where" & vbCrLf & _
                                      groupCodeQryLine & vbCrLf & _
                                      "AND" & vbCrLf & _
                                      "C.ubARContractActive = 1" & vbCrLf & _
                                      "and" & vbCrLf & _
                                      "C.ufARBluePanicApp <> 0" & vbCrLf & _
                                      "Order By 'Debtor Account Code'"

            Else 'Step 4 - '#New Contract Fee - Add New Contract Fee Field to Script and remove the copied one (CHECK EVERYWHERE FOR COPIED FEE)

                qry = "SELECT" & vbCrLf & _
                        "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                    "C.Account AS 'Customer Code'," & vbCrLf & _
                    "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account + 'D') AS 'Debtor Account Code'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                    "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                    "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                    "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                    "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                    "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                    "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                    "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                    "C.Name as 'Name'," & vbCrLf & _
                      "C.Title AS 'Contract Title'," & vbCrLf & _
                      "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                      "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                      "a.Code as 'Group Code'," & vbCrLf & _
                      "'#APP - Contract Fee' as 'ItemCode'," & vbCrLf & _
                      "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month', " & vbCrLf & _
                    "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code', " & vbCrLf & _
                    "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID  " & vbCrLf & _
                    "    = (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee', " & vbCrLf & _
                    "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'," & vbCrLf & _
                      "C.ufARBluePanicApp 'Current Value'" & vbCrLf & _
                      "FROM" & vbCrLf & _
                      "Client C" & vbCrLf & _
                      "INNER JOIN" & vbCrLf & _
                      "Areas A" & vbCrLf & _
                      "ON" & vbCrLf & _
                      "C.iAreasID = A.idAreas" & vbCrLf & _
                      "where" & vbCrLf & _
                      groupCodeQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      increaseMonthQryLine & vbCrLf & _
                      "AND" & vbCrLf & _
                      "C.ubARContractActive = 1" & vbCrLf & _
                      "and" & vbCrLf & _
                      "C.ufARBluePanicApp <> 0" & vbCrLf & _
                      "Order By 'Debtor Account Code'"

            End If

        Else

            qry = "select" & vbCrLf & _
                    "*" & vbCrLf & _
                    "from" & vbCrLf & _
                    "Client" & vbCrLf & _
                    "where" & vbCrLf & _
                    "Account = '999'"

        End If

        Return dbConn.get_datatable(qry)

    End Function

End Class
