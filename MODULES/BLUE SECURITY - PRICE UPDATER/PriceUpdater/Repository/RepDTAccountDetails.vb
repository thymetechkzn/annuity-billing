﻿Public Class RepDTAccountDetails

    Public Function loadAccountDetails(ByVal dgvAccDetails As DataGridView) As List(Of DTAccountDetails)

        'Declare Variables
        Dim lstAccDetails As New List(Of DTAccountDetails)

        For Each row As DataGridViewRow In dgvAccDetails.Rows

            Dim accountDetail As New DTAccountDetails

            'Loading Account Code from Datagridview
            accountDetail.accountCode = row.Cells("Customer Code").Value.ToString()
            accountDetail.isUpdated = False

            lstAccDetails.Add(accountDetail)

        Next

        Return lstAccDetails

    End Function

End Class
