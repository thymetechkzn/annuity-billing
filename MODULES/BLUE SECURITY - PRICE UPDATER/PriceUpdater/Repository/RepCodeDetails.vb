﻿Public Class RepCodeDetails

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    'LOAD INCREASE MONTHS
    Public Function loadIncreaseMonths() As List(Of Codes.IncreaseMonthsDetails)

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable
        Dim lstIncreaseMonth As New List(Of Codes.IncreaseMonthsDetails)

        'QUERY: 'Gets all distinct increase months from 
        qry = "select" & vbCrLf & _
               "ulARContractIncMonth as 'IncreaseMonths'" & vbCrLf & _
               "from client" & vbCrLf & _
               "where ulARContractIncMonth <> ''" & vbCrLf & _
               "group by ulARContractIncMonth" & vbCrLf & _
               "order by DATEPART(mm,CAST((ulARContractIncMonth) + ' 1900' AS DATETIME)) asc"

        dt = dbConn.get_datatable(qry)

        'Loading Increase Months
        For Each row As DataRow In dt.Rows

            Dim itemCode As New Codes.IncreaseMonthsDetails

            itemCode.IncreaseMonthName = row("IncreaseMonths")

            lstIncreaseMonth.Add(itemCode)

        Next


        Return lstIncreaseMonth

    End Function
    'LOAD GROUP CODES
    Public Function loadGroupCodes(ByVal increaseMonth As String) As List(Of Codes.GroupCodesDetails)

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable
        Dim lstGroupCodes As New List(Of Codes.GroupCodesDetails)

        'QUERY: Gets all the codes and their description from the Areas table
        If increaseMonth = "All" Then

            qry = "select" & vbCrLf & _
               "code as 'GroupCode'," & vbCrLf & _
               "Description as 'GroupDescription'" & vbCrLf & _
               "from" & vbCrLf & _
               "Areas" & vbCrLf & _
               "order by code"

        Else

            qry = "select" & vbCrLf & _
                           "code as 'GroupCode'," & vbCrLf & _
                           "Description as 'GroupDescription'" & vbCrLf & _
                           "from" & vbCrLf & _
                           "Areas" & vbCrLf & _
                           "where" & vbCrLf & _
                           "idAreas in" & vbCrLf & _
                           "			(select" & vbCrLf & _
                           "				iAreasID " & vbCrLf & _
                           "				from" & vbCrLf & _
                           "				Client" & vbCrLf & _
                           "				where" & vbCrLf & _
                           "				ulARContractIncMonth = '" & increaseMonth & "')" & vbCrLf & _
                           "order by code"

        End If


        dt = dbConn.get_datatable(qry)

        'Loading Group Codes their and Group Descriptions
        For Each row As DataRow In dt.Rows

            Dim groupCode As New Codes.GroupCodesDetails

            groupCode.GroupCode = row("GroupCode")
            groupCode.GroupDescription = row("GroupDescription")

            lstGroupCodes.Add(groupCode)

        Next

        Return lstGroupCodes

    End Function
    'LOAD ITEM CODES
    Public Function loadItemCodes(ByVal increaseMonth As String) As List(Of Codes.ItemCodesDetails)


        '#New Contract Fee

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable
        Dim lstItemCodes As New List(Of Codes.ItemCodesDetails)

        'Declare Contract Fees
        Dim hasServiceFee As Boolean = False
        Dim hasSMS As Boolean = False
        Dim hasReports As Boolean = False
        Dim hasSurveilance As Boolean = False
        Dim hasAngel As Boolean = False
        Dim hasGuarding As Boolean = False ' Step 1 #New Contract Fee
        Dim hasPanicApp As Boolean = False ' Trishani 20161019 Step 1 #New Contract Fee


        'QUERY: Gets all items codes and their description from the StkItem table where the Codes are prefixed with a "#" and belong to an item group prefixed 
        '"CON-"
        If increaseMonth = "All" Then

            qry = "Select" & vbCrLf & _
                            "Code as 'ItemCode'," & vbCrLf & _
                            "Description_1 as 'ItemDescription'" & vbCrLf & _
                            "from" & vbCrLf & _
                            "stkItem si" & vbCrLf & _
                            "inner join" & vbCrLf & _
                            "_rtblStockLinks sl" & vbCrLf & _
                            "on si.StockLink = sl.iStockID " & vbCrLf & _
                            "where" & vbCrLf & _
                            "Code like '#%'" & vbCrLf & _
                            "and" & vbCrLf & _
                            "ItemGroup like 'CON-%'" & vbCrLf & _
                            "group by si.Code , si.Description_1"

        Else

            qry = "Select" & vbCrLf & _
                            "Code as 'ItemCode'," & vbCrLf & _
                            "Description_1 as 'ItemDescription'" & vbCrLf & _
                            "from" & vbCrLf & _
                            "stkItem si" & vbCrLf & _
                            "inner join" & vbCrLf & _
                            "_rtblStockLinks sl" & vbCrLf & _
                            "on si.StockLink = sl.iStockID " & vbCrLf & _
                            "where" & vbCrLf & _
                            "Code like '#%'" & vbCrLf & _
                            "and" & vbCrLf & _
                            "ItemGroup like 'CON-%'" & vbCrLf & _
                            "and" & vbCrLf & _
                            "sl.iDCLink in" & vbCrLf & _
                            "			(select" & vbCrLf & _
                            "			 DCLink" & vbCrLf & _
                            "			 from" & vbCrLf & _
                            "			 Client" & vbCrLf & _
                            "			 where" & vbCrLf & _
                            "            ulARContractIncMonth = '" & increaseMonth & "')" & vbCrLf & _
                            "group by si.Code , si.Description_1"

        End If


        dt = dbConn.get_datatable(qry)

        'Loading Item Codes and their Item Descriptions
        For Each row As DataRow In dt.Rows

            Dim itemCode As New Codes.ItemCodesDetails

            'CONTRACT PRICE UPDATE PURPOSES
            If (row("ItemCode")).Equals("#SERFEE") Then

                hasServiceFee = True

            ElseIf (row("ItemCode")).Equals("#OCSMS") Then

                hasSMS = True

            ElseIf (row("ItemCode")).Equals("#MONREP") Then

                hasReports = True

            ElseIf (row("ItemCode")).Equals("#REMSUR") Then

                hasSurveilance = True

            ElseIf (row("ItemCode")).Equals("#ANGELS") Then

                hasAngel = True

            ElseIf (row("ItemCode")).Equals("#GUARD") Then 'Step 2 #New Contract Fee

                hasGuarding = True

            ElseIf (row("ItemCode")).Equals("#APP") Then ' Trishani 20161019 Step 2 #New Contract Fee

                hasPanicApp = True

            End If

            itemCode.ItemCode = row("ItemCode")
            itemCode.ItemDescription = row("ItemDescription")

            lstItemCodes.Add(itemCode)

        Next

        'Add Contract Fees
        If hasServiceFee = False Then

            Dim contractCode As New Codes.ItemCodesDetails

            contractCode.ItemCode = "#SERFEE"
            contractCode.ItemDescription = "Service Fees"
            lstItemCodes.Add(contractCode)

        End If

        If hasSMS = False Then

            Dim contractCode As New Codes.ItemCodesDetails

            contractCode.ItemCode = "#OCSMS"
            contractCode.ItemDescription = "Open/Close SMS"
            lstItemCodes.Add(contractCode)

        End If

        If hasReports = False Then

            Dim contractCode As New Codes.ItemCodesDetails

            contractCode.ItemCode = "#MONREP"
            contractCode.ItemDescription = "Activity Reports"
            lstItemCodes.Add(contractCode)

        End If

        If hasSurveilance = False Then

            Dim contractCode As New Codes.ItemCodesDetails

            contractCode.ItemCode = "#REMSUR"
            contractCode.ItemDescription = "Remote Surveillance Fees"
            lstItemCodes.Add(contractCode)


        End If

        If hasAngel = False Then

            Dim contractCode As New Codes.ItemCodesDetails

            contractCode.ItemCode = "#ANGELS"
            contractCode.ItemDescription = "Blue Angels Charitable Trust"
            lstItemCodes.Add(contractCode)

        End If

        If hasGuarding = False Then 'Step 3 #New Contract Fee

            Dim contractCode As New Codes.ItemCodesDetails

            contractCode.ItemCode = "#GUARD" 'GET FROM stkItem
            contractCode.ItemDescription = "Guarding Fees" 'GET FROM stkItem
            lstItemCodes.Add(contractCode)

        End If

        If hasPanicApp = False Then ' Trishani 20161019 Step 3 #New Contract Fee

            Dim contractCode As New Codes.ItemCodesDetails

            contractCode.ItemCode = "#APP" 'GET FROM stkItem
            contractCode.ItemDescription = "Blue Panic App" 'GET FROM stkItem
            lstItemCodes.Add(contractCode)

        End If

        Return lstItemCodes

    End Function

End Class
