﻿Imports System.IO
Imports System.Data
Imports System.Data.OleDb

Public Class frmPriceUpdater


    Dim dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)
    Dim isImport As Boolean = False
    Dim SelectedPath As String
    Dim selectMonth As String
    Dim clientDT As New DataTable
    Dim dgvClient As New DataGridView
    Dim totalRows As String

    Sub New(ByVal dBInstance As String, ByVal dBName As String, _
        ByVal dBUser As String, ByVal dBPassword As String, ByVal evoCommon As String)

        ' This call is required by the designer.
        InitializeComponent()

        My.Settings.DBInstance = dBInstance
        My.Settings.DBName = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.EvolutionCommon = evoCommon

        dbConn = New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    '------------------------AUTO GENERATED METHODS------------------------
    Private Sub frmIncreases_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        refreshAll()
        loadIncreaseMonths()

    End Sub
    Private Sub cboIncreaseMonth_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboIncreaseMonth.SelectedIndexChanged

        'Declare Variables
        Dim increaseMonnth As String = cboIncreaseMonth.SelectedItem.ToString()

        clbGroupCodes.Items.Clear()
        clbItemCodes.Items.Clear()

        loadGroupCode(increaseMonnth)
        loadItemCodes(increaseMonnth)

        TableLayoutPanel7.Visible = True
        btnProcessSelec.Enabled = True

    End Sub
    Private Sub clbGroupCodes_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clbGroupCodes.SelectedIndexChanged


        'Declare Variable
        Dim index As Integer = clbGroupCodes.Items.Count - 1
        Dim count As Integer = 0

        If index <> -1 Then


            For Each grpCode As Object In clbGroupCodes.CheckedItems

                count += 1

                If (grpCode.ToString()).Equals("Select All") Then

                    For numChecked As Integer = 0 To clbGroupCodes.Items.Count - 1

                        clbGroupCodes.SetItemChecked(numChecked, True)

                    Next

                    Exit For

                End If

            Next
        End If


    End Sub
    Private Sub clbItemCodes_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles clbItemCodes.SelectedIndexChanged

        'Declare Variable
        Dim index As Integer = clbItemCodes.Items.Count - 1
        Dim count As Integer = 0

        If index <> -1 Then


            For Each itemCode As Object In clbItemCodes.CheckedItems

                count += 1

                If (itemCode.ToString()).Equals("Select All") Then

                    For numChecked As Integer = 0 To clbItemCodes.Items.Count - 1

                        clbItemCodes.SetItemChecked(numChecked, True)

                    Next

                    Exit For

                End If

            Next
        End If


    End Sub
    Private Sub btnProcessSelec_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessSelec.Click

        'Enable Controls
        btnProcessSelec.Enabled = False
        btnPercentageIncrease.Enabled = True
        numTxtPercentage.Enabled = True
        ExportExcellDocumentToolStripMenuItem.Enabled = True
        clbGroupCodes.Enabled = False
        clbItemCodes.Enabled = False
        cboIncreaseMonth.Enabled = False
        selectMonth = cboIncreaseMonth.SelectedItem.ToString()

        BackgroundWorker1.RunWorkerAsync()
        Timer1.Enabled = True
        Timer1.Start()


    End Sub
    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

        loadDataGridView()

        If Me.InvokeRequired Then
            Timer1.Stop()
            Timer1.Enabled = False
            Me.Invoke(New MethodInvoker(AddressOf loadDGV))
            Me.Invoke(New MethodInvoker(AddressOf clearProgressbar))
        Else
            MsgBox("The data has been retrieved.")
        End If


    End Sub
    Private Sub loadDGV() Handles BackgroundWorker1.RunWorkerCompleted

        dgvClients.DataSource = clientDT
        lblTotalAccounts.Text = "Total Number of Accounts: " & totalRows.ToString()

    End Sub
    Private Sub clearProgressbar() Handles BackgroundWorker1.RunWorkerCompleted

        pbPriceUpdater.Value = pbPriceUpdater.Maximum

    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If pbPriceUpdater.Value >= pbPriceUpdater.Maximum Then
            pbPriceUpdater.Value = 0
        Else
            pbPriceUpdater.Value += 1
        End If

    End Sub

    

    Private Sub btnPercentageIncrease_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPercentageIncrease.Click

        calculateNewPriceByPercentage()

        'Set Variable to notify update method that the datagridview column index should be 18
        isImport = False

        'Enable process button
        btnProcNewPriceUpdates.Enabled = True

        'Disable the other option to update the prices
        ExportExcellDocumentToolStripMenuItem.Enabled = False

    End Sub
    Private Sub ExportExcellDocumentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportExcellDocumentToolStripMenuItem.Click

        'Add Extra Columns to Datagridview before export for updating
        dgvClients.Columns.Add("Increase Value", "Increase Value")
        dgvClients.Columns.Add("New Value", "New Value")

        exportToExcel()

        'Disable the other option to update the prices
        btnPercentageIncrease.Enabled = False
        numTxtPercentage.Enabled = False
        ExportExcellDocumentToolStripMenuItem.Enabled = False
        ImportExcelDocumentToolStripMenuItem.Enabled = True

    End Sub
    Private Sub ImportExcelDocumentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportExcelDocumentToolStripMenuItem.Click

        'Declare Variables
        Dim numAccounts As Integer = 0
        Dim totalRows As Integer

        resetDGV()
        dgvClients.DataSource = importToExcel()

        'Enable/disable Controls
        If dgvClients.RowCount >= 1 Then

            ImportExcelDocumentToolStripMenuItem.Enabled = False
            btnProcNewPriceUpdates.Enabled = True

            'Set Variable to notify update method that the datagridview column index should be 17
            isImport = True

            'Display total rows in DataGridView

            For index As Integer = 0 To dgvClients.RowCount

                totalRows = index

            Next

        End If

        lblTotalAccounts.Text = "Total Number of Accounts: " & totalRows.ToString()


    End Sub
    Private Sub btnProcNewPriceUpdates_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcNewPriceUpdates.Click

        'Declare Variables
        Dim msgResult As Integer

        'Since the document has already been exported to Excel, there is no need to create a new document of it.
        msgResult = MessageBox.Show("Are you sure that you want to update these prices", "Price Updater", MessageBoxButtons.YesNo)

        If msgResult = DialogResult.Yes Then

            If isImport = False Then
                exportToExcel()
            End If

            dgvClient = dgvClients

            BackgroundWorker2.RunWorkerAsync()
            Timer2.Enabled = True
            Timer2.Start()

            pbPriceUpdater.Value = 0

        End If

    End Sub
    Private Sub BackgroundWorker2_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork

        updatePrice()

        If Me.InvokeRequired Then
            Timer2.Stop()
            Timer2.Enabled = False
            Me.Invoke(New MethodInvoker(AddressOf refreshAll))
            Me.Invoke(New MethodInvoker(AddressOf clearProgressbar))
            MsgBox("The Update is complete!")
            'pbRRun.Value = 0
        Else
            MsgBox("The Update is complete!")
            pbPriceUpdater.Value = 0
        End If

    End Sub
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick

    End Sub


    Private Sub btnRefresh_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefresh.Click
        refreshAll()
    End Sub
    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click

        Me.Close()

    End Sub
    '------------LOADING GROUP CODES, ITEM CODES & INCREASE MONTHS------------
    Private Sub loadIncreaseMonths()

        'Declare Variable
        Dim codes As New List(Of Codes.IncreaseMonthsDetails)
        Dim repIncreaseMonths As New RepCodeDetails

        'Call Method that loads Increase Months
        codes = repIncreaseMonths.loadIncreaseMonths()

        'Populate the Increase Months into checklist boxes

        cboIncreaseMonth.Items.Add("All")

        For Each increaseMonth As Codes.IncreaseMonthsDetails In codes

            cboIncreaseMonth.Items.Add(increaseMonth.IncreaseMonthName)

        Next

    End Sub
    Private Sub loadGroupCode(ByVal incrMonth As String)

        'Declare Variables
        Dim codes As New List(Of Codes.GroupCodesDetails)
        Dim repGroupCode As New RepCodeDetails

        'Calls Method that loads Group Codes
        codes = repGroupCode.loadGroupCodes(incrMonth)

        'Populates the Group Codes into checklist boxes
        clbGroupCodes.Items.Add("Select All")

        For Each grpCode As Codes.GroupCodesDetails In codes

            clbGroupCodes.Items.Add(grpCode.GroupCode & "-" & grpCode.GroupDescription)

        Next

    End Sub
    Private Sub loadItemCodes(ByVal incrMonth As String)

        'Declare Variables
        Dim codes As New List(Of Codes.ItemCodesDetails)
        Dim repItemCode As New RepCodeDetails

        'Call Method that loads Items Codes
        codes = repItemCode.loadItemCodes(incrMonth)

        'Populate the Item Codes into checklist boxes
        clbItemCodes.Items.Add("Select All")

        For Each itemcode As Codes.ItemCodesDetails In codes

            clbItemCodes.Items.Add(itemcode.ItemCode & "-" & itemcode.ItemDescription)

        Next

    End Sub

    '------------RETRIEVE GROUP CODES, ITEM CODES & INCREASE MONTHS SELECTED AND LOAD DATAGRIDVIEW------------
    Private Sub loadDataGridView()

        'Declare Variables
        Dim repClientDetails As New RepClients

        ' clientDT.Merge(repClientDetails.getClientAdhocDT(getGroupCodeLine(), getItemCodeLine, getIncreaseMonthLine()), False)
        clientDT = repClientDetails.getClientAdhocDT(getGroupCodeLine(), getItemCodeLine, getIncreaseMonthLine())

        'Display total rows in DataGridView
        If clientDT.Rows.Count > 0 Then

            For index As Integer = 0 To clientDT.Rows.Count

                totalRows = index

            Next

        End If

    End Sub
    Private Function getGroupCodeLine() As String

        'Declare Variables
        Dim arrGroupCode As New ArrayList
        Dim grpLine As String = "a.Code in ('" 'Setting a line for the where clause in the query to get the client details
        Dim grpAddition As String
        Dim num As Integer = 0
        Dim spacePos As Integer
        Dim gCode As String

        'Retrieving which checkboxes were selected from the Group Codes checkbox list
        For grpCode As Integer = 0 To clbGroupCodes.CheckedItems.Count - 1

            spacePos = (clbGroupCodes.CheckedItems(grpCode)).ToString().IndexOf("-")

            If spacePos > 0 Then

                gCode = (clbGroupCodes.CheckedItems(grpCode)).ToString().Substring(0, spacePos).Trim()
            Else
                gCode = (clbGroupCodes.CheckedItems(grpCode)).ToString()

            End If


            arrGroupCode.Add(gCode)

        Next

        'Creating the line for the query
        If arrGroupCode.Count > 0 Then

            For Each aGrpCode In arrGroupCode
                num += 1

                If num <> arrGroupCode.Count Then
                    grpAddition = aGrpCode + "','"
                Else
                    grpAddition = aGrpCode + "')"
                End If

                grpLine = grpLine + grpAddition
            Next
        Else

            grpLine = "--"

        End If



            Return grpLine

    End Function
    Private Function getItemCodeLine() As String

        'Declare Variables
        Dim arrStkCode As New ArrayList
        Dim stkLine As String = "si.Code in ('" 'Setting a line for the where clause in the query to get the client details
        Dim stkAddition As String
        Dim num As Integer = 0
        Dim spacePos As Integer
        Dim sCode As String

        'Retrieving which checkboxes were selected from the Item Codes checkbox list
        For itemCode As Integer = 0 To clbItemCodes.CheckedItems.Count - 1

            spacePos = (clbItemCodes.CheckedItems(itemCode)).ToString().IndexOf("-")

            If spacePos > 0 Then

                sCode = (clbItemCodes.CheckedItems(itemCode)).ToString().Substring(0, spacePos).Trim()
            Else

                sCode = (clbItemCodes.CheckedItems(itemCode)).ToString()

            End If


            arrStkCode.Add(sCode)

        Next

        'Creating the line for the query
        If arrStkCode.Count > 0 Then

            For Each aStkCode In arrStkCode
                num += 1

                If num <> arrStkCode.Count Then
                    stkAddition = aStkCode + "','"
                Else
                    stkAddition = aStkCode + "')"
                End If

                stkLine = stkLine + stkAddition

            Next

        Else

            stkLine = "--"

        End If


        Return stkLine

    End Function
    Private Function getIncreaseMonthLine() As String

        'Declare Varibales
        Dim arrInclMth As New ArrayList
        Dim inclMthLine As String = "c.ulARContractIncMonth = '" 'Setting a line for the where clause in the query to get the client details
        Dim num As Integer = 0

        'Creating the line for the query
        If selectMonth = "All" Then

            inclMthLine = selectMonth

        Else

            inclMthLine = inclMthLine & selectMonth & "'"

        End If


        Return inclMthLine

    End Function

    '------------CALCULATE NEW PRICE BY PERCENTAGE------------
    Public Sub calculateNewPriceByPercentage()

        'Declare Variables
        Dim Percentage As Double
        Dim curValue As Decimal
        Dim lstClients As New List(Of Client)

        Percentage = numTxtPercentage.Value

        'If Columns do not exist, create columns
        If Not (dgvClients.Columns.Contains("% Increase") = True) Then

            dgvClients.Columns.Add("% Increase", "% Increase")
            dgvClients.Columns.Add("Increase Value", "Increase Value")
            dgvClients.Columns.Add("New Value", "New Value")

        End If

            For Each row As DataGridViewRow In dgvClients.Rows

                curValue = row.Cells.Item("Current Value").Value

                row.Cells.Item("% Increase").Value = Percentage.ToString() + "%"

                row.Cells.Item("Increase Value").Value = Math.Round((curValue * (Percentage / 100)), 2, MidpointRounding.AwayFromZero)
                row.Cells.Item("New Value").Value = Math.Round(((curValue * (Percentage / 100)) + curValue), 2, MidpointRounding.AwayFromZero)

            Next

    End Sub

    '--------------IMPORT TO EXCEL--------------
    Public Function importToExcel() As DataTable

        Dim fileName As String
        Dim name As String
        Dim dt As New DataTable
        Dim conString As String
        Dim selStr As String

        OpenFileDialog1.Title = "Please Select file to import"
        'OpenFileDialog1.InitialDirectory = Environment.SpecialFolder.MyDocuments

        Try
            If OpenFileDialog1.ShowDialog() = DialogResult.OK Then

                SelectedPath = IO.Path.GetDirectoryName(OpenFileDialog1.InitialDirectory + OpenFileDialog1.FileName)
                fileName = IO.Path.GetFileName(OpenFileDialog1.InitialDirectory + OpenFileDialog1.FileName)
                name = IO.Path.GetFileNameWithoutExtension(OpenFileDialog1.InitialDirectory + OpenFileDialog1.FileName)
                dt = New DataTable(name)

                conString = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""text;HDR={1};FMT=Delimited"";", SelectedPath, "Yes")
                selStr = "Select * From [" & fileName & "]"

                Using da As New Data.OleDb.OleDbDataAdapter(selStr, conString)

                    da.Fill(dt)
                    da.Dispose()

                End Using

                Return dt

            End If
        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        Return dt

    End Function

    '------------CALCULATE NEW PRICE BY PERCENTAGE & EXPORT TO EXCEL------------
    Private Sub exportToExcel()

        'Declare Variables
        Dim repExportToExcel As New RepExcel
        Dim fileName As String
        Dim f As FolderBrowserDialog = New FolderBrowserDialog

        'set properties
        f.Description = "Please select file Destination"
        f.RootFolder = Environment.SpecialFolder.Desktop

        Try
            If f.ShowDialog() = DialogResult.OK Then

                fileName = InputBox("Please Enter a file name", "File Name", "PRICE UPDATER - " & (System.DateTime.Today).ToString("ddMMMyyyy") & _
                                     "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm tt"))

                SelectedPath = f.SelectedPath.ToString

                repExportToExcel.ExportData(dgvClients, fileName, SelectedPath)

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try


    End Sub
    Private Sub updatePrice()

        'Declare Varaibles
        Dim qry As String = ""
        Dim cellIndex As Integer
        Dim accDetails As New List(Of DTAccountDetails)
        Dim repAccDetail As New RepDTAccountDetails
        Dim repUpdatePrice As New RepUpdatePrices
        Dim repWriteUpdateLog As New RepUpdateErrorLog
        Dim updated As Boolean = False
        Dim code As String
        'Log
        Dim logger As New LogFile(SelectedPath, "Price Updater Log " & (System.DateTime.Today).ToString("dd-MM-yyyy"))
        Dim strLog As String = ""
        'Call Method that loads the account codes and default update status for Update Log
        accDetails = repAccDetail.loadAccountDetails(dgvClient)

        'Check if the update was done by a percentage update or by excel
        If isImport = True Then
            cellIndex = 22
        Else
            cellIndex = 23
        End If

        Try


            'Go through and retrieve the data in the datagridview for updating the client's prices
            For Each row As DataGridViewRow In dgvClient.Rows

                code = row.Cells("Customer Code").Value.ToString()
                'Go through list of accounts retrieved from the datagridview for update log purposes
                For Each accDetail As DTAccountDetails In accDetails

                    updated = False

                    'Checks if account code in the list of account codes is equal to account code in datagridview for update log purposes
                    If accDetail.accountCode.Equals(row.Cells("Customer Code").Value.ToString()) Then

                        If (row.Cells("Item Code").Value.ToString()).Contains("#SERFEE -") = True Then 'Contract Service Fee

                            'Pulls through as True/False - Indicates whether the update was a success
                            updated = repUpdatePrice.updateContractServiceFee(row.Cells("New Value").Value.ToString(), row.Cells("Customer Code").Value.ToString())

                        ElseIf (row.Cells("Item Code").Value.ToString()).Contains("#OCSMS -") = True Then 'Contract SMS Fess

                            updated = repUpdatePrice.updateContractSMSFee(row.Cells("New Value").Value.ToString(), row.Cells("Customer Code").Value.ToString())

                        ElseIf (row.Cells("Item Code").Value.ToString()).Contains("#REMSUR -") = True Then 'Contract Surveilance Fee

                            updated = repUpdatePrice.updateContractSurveilanceFee(row.Cells("New Value").Value.ToString(), row.Cells("Customer Code").Value.ToString())

                        ElseIf (row.Cells("Item Code").Value.ToString()).Contains("#GUARD -") = True Then 'Contract Guarding Fee

                            updated = repUpdatePrice.updateContractGuardFee(row.Cells("New Value").Value.ToString(), row.Cells("Customer Code").Value.ToString())

                        ElseIf (row.Cells("Item Code").Value.ToString()).Contains("#ANGELS -") = True Then 'Contract Angels Fee

                            updated = repUpdatePrice.updateContractAngelsFee(row.Cells("New Value").Value.ToString(), row.Cells("Customer Code").Value.ToString())

                        ElseIf (row.Cells("Item Code").Value.ToString()).Contains("#MONREP -") = True Then 'Contract SMS Fess

                            updated = repUpdatePrice.updateContractReportFee(row.Cells("New Value").Value.ToString(), row.Cells("Customer Code").Value.ToString())

                        ElseIf (row.Cells("Item Code").Value.ToString()).Contains("#APP -") = True Then 'Contract Panic App Fee

                            updated = repUpdatePrice.updateContractPanicAppFee(row.Cells("New Value").Value.ToString(), row.Cells("Customer Code").Value.ToString())

                        Else 'Adhoc Fee

                            updated = repUpdatePrice.updateAdhocFee(row.Cells("New Value").Value.ToString(), row.Cells("Customer Code").Value.ToString(), _
                                                                    row.Cells("Item Code").Value.ToString())

                        End If

                        If updated = True Then
                            strLog &= "Account : " & accDetail.accountCode & "; Item Code: " & row.Cells("Item Code").Value.ToString() & "; was updated." & vbCrLf
                        Else
                            strLog &= "Account : " & accDetail.accountCode & "; Item Code: " & row.Cells("Item Code").Value.ToString() & "; was not updated." & vbCrLf
                        End If

                    End If
                Next
            Next

            logger.CreateLog(strLog)

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

        'repWriteUpdateLog.createAndWriteToFile(accDetails)
        logger.CreateLog("The Update is complete")

    End Sub

    '------------REFRESH ALL CONTROLS------------
    Public Sub refreshAll()

        For numChecked As Integer = 0 To clbGroupCodes.Items.Count - 1

            clbGroupCodes.SetItemChecked(numChecked, False)

        Next

        For numChecked As Integer = 0 To clbItemCodes.Items.Count - 1

            clbItemCodes.SetItemChecked(numChecked, False)

        Next

        'Set Components
        btnProcessSelec.Enabled = True
        btnPercentageIncrease.Enabled = False
        numTxtPercentage.Value = 0.0
        numTxtPercentage.Enabled = False
        ExportExcellDocumentToolStripMenuItem.Enabled = False
        ImportExcelDocumentToolStripMenuItem.Enabled = True 'Taken Out Due to Spec Change???
        clbGroupCodes.Enabled = True
        cboIncreaseMonth.Enabled = True
        clbItemCodes.Enabled = True
        btnProcNewPriceUpdates.Enabled = False
        lblTotalAccounts.Text = ""
        dgvClients.Columns.Clear()

        resetDGV()

    End Sub
    Private Sub resetDGV()

        dgvClients.CancelEdit()
        dgvClients.Columns.Clear()
        dgvClients.DataSource = Nothing

    End Sub

End Class
