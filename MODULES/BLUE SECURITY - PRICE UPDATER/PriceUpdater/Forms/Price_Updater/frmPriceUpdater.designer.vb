﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPriceUpdater
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmPriceUpdater))
        Me.mnuFile = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportExcellDocumentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ImportExcelDocumentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.dgvClients = New System.Windows.Forms.DataGridView()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.btnProcNewPriceUpdates = New System.Windows.Forms.Button()
        Me.clbGroupCodes = New System.Windows.Forms.CheckedListBox()
        Me.clbItemCodes = New System.Windows.Forms.CheckedListBox()
        Me.lblGroupCodes = New System.Windows.Forms.Label()
        Me.lblItemCodes = New System.Windows.Forms.Label()
        Me.btnPercentageIncrease = New System.Windows.Forms.Button()
        Me.btnRefresh = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.numTxtPercentage = New System.Windows.Forms.NumericUpDown()
        Me.btnProcessSelec = New System.Windows.Forms.Button()
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel7 = New System.Windows.Forms.TableLayoutPanel()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.lblIncreaseMonth = New System.Windows.Forms.Label()
        Me.cboIncreaseMonth = New System.Windows.Forms.ComboBox()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnViewReport = New System.Windows.Forms.Button()
        Me.lblTotalAccounts = New System.Windows.Forms.Label()
        Me.pbPriceUpdater = New System.Windows.Forms.ProgressBar()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.mnuFile.SuspendLayout()
        CType(Me.dgvClients, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel1.SuspendLayout()
        CType(Me.numTxtPercentage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel6.SuspendLayout()
        Me.TableLayoutPanel7.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'mnuFile
        '
        Me.mnuFile.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.mnuFile.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.mnuFile.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ExportToolStripMenuItem})
        Me.mnuFile.Location = New System.Drawing.Point(0, 0)
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.mnuFile.Size = New System.Drawing.Size(820, 24)
        Me.mnuFile.TabIndex = 0
        Me.mnuFile.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ExportToolStripMenuItem
        '
        Me.ExportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportExcellDocumentToolStripMenuItem, Me.ImportExcelDocumentToolStripMenuItem})
        Me.ExportToolStripMenuItem.Name = "ExportToolStripMenuItem"
        Me.ExportToolStripMenuItem.Size = New System.Drawing.Size(87, 20)
        Me.ExportToolStripMenuItem.Text = "File Transfer"
        '
        'ExportExcellDocumentToolStripMenuItem
        '
        Me.ExportExcellDocumentToolStripMenuItem.Name = "ExportExcellDocumentToolStripMenuItem"
        Me.ExportExcellDocumentToolStripMenuItem.Size = New System.Drawing.Size(302, 22)
        Me.ExportExcellDocumentToolStripMenuItem.Text = "Generate Increase Report / Export to Excel"
        '
        'ImportExcelDocumentToolStripMenuItem
        '
        Me.ImportExcelDocumentToolStripMenuItem.Name = "ImportExcelDocumentToolStripMenuItem"
        Me.ImportExcelDocumentToolStripMenuItem.Size = New System.Drawing.Size(302, 22)
        Me.ImportExcelDocumentToolStripMenuItem.Text = "Import from Excel"
        '
        'dgvClients
        '
        Me.dgvClients.AllowUserToAddRows = False
        Me.dgvClients.AllowUserToDeleteRows = False
        Me.dgvClients.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClients.Location = New System.Drawing.Point(3, 3)
        Me.dgvClients.Name = "dgvClients"
        Me.dgvClients.ReadOnly = True
        Me.dgvClients.Size = New System.Drawing.Size(799, 270)
        Me.dgvClients.TabIndex = 1
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'btnProcNewPriceUpdates
        '
        Me.btnProcNewPriceUpdates.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnProcNewPriceUpdates.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcNewPriceUpdates.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcNewPriceUpdates.Location = New System.Drawing.Point(9, 565)
        Me.btnProcNewPriceUpdates.Name = "btnProcNewPriceUpdates"
        Me.btnProcNewPriceUpdates.Size = New System.Drawing.Size(203, 31)
        Me.btnProcNewPriceUpdates.TabIndex = 3
        Me.btnProcNewPriceUpdates.Text = "Process Updates"
        Me.btnProcNewPriceUpdates.UseVisualStyleBackColor = False
        '
        'clbGroupCodes
        '
        Me.clbGroupCodes.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.clbGroupCodes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.clbGroupCodes.FormattingEnabled = True
        Me.clbGroupCodes.Location = New System.Drawing.Point(3, 25)
        Me.clbGroupCodes.Name = "clbGroupCodes"
        Me.clbGroupCodes.Size = New System.Drawing.Size(319, 89)
        Me.clbGroupCodes.TabIndex = 4
        '
        'clbItemCodes
        '
        Me.clbItemCodes.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.clbItemCodes.Dock = System.Windows.Forms.DockStyle.Fill
        Me.clbItemCodes.FormattingEnabled = True
        Me.clbItemCodes.Location = New System.Drawing.Point(328, 25)
        Me.clbItemCodes.Name = "clbItemCodes"
        Me.clbItemCodes.Size = New System.Drawing.Size(314, 89)
        Me.clbItemCodes.TabIndex = 5
        '
        'lblGroupCodes
        '
        Me.lblGroupCodes.AutoSize = True
        Me.lblGroupCodes.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGroupCodes.Location = New System.Drawing.Point(3, 0)
        Me.lblGroupCodes.Name = "lblGroupCodes"
        Me.lblGroupCodes.Size = New System.Drawing.Size(87, 18)
        Me.lblGroupCodes.TabIndex = 5
        Me.lblGroupCodes.Text = "Group Codes"
        '
        'lblItemCodes
        '
        Me.lblItemCodes.AutoSize = True
        Me.lblItemCodes.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblItemCodes.Location = New System.Drawing.Point(328, 0)
        Me.lblItemCodes.Name = "lblItemCodes"
        Me.lblItemCodes.Size = New System.Drawing.Size(78, 18)
        Me.lblItemCodes.TabIndex = 6
        Me.lblItemCodes.Text = "Item Codes"
        '
        'btnPercentageIncrease
        '
        Me.btnPercentageIncrease.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnPercentageIncrease.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPercentageIncrease.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPercentageIncrease.Location = New System.Drawing.Point(71, 3)
        Me.btnPercentageIncrease.Name = "btnPercentageIncrease"
        Me.btnPercentageIncrease.Size = New System.Drawing.Size(149, 23)
        Me.btnPercentageIncrease.TabIndex = 16
        Me.btnPercentageIncrease.Text = "Apply % Price Update"
        Me.btnPercentageIncrease.UseVisualStyleBackColor = False
        '
        'btnRefresh
        '
        Me.btnRefresh.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRefresh.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefresh.Location = New System.Drawing.Point(618, 566)
        Me.btnRefresh.Name = "btnRefresh"
        Me.btnRefresh.Size = New System.Drawing.Size(193, 31)
        Me.btnRefresh.TabIndex = 4
        Me.btnRefresh.Text = "Refresh"
        Me.btnRefresh.UseVisualStyleBackColor = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 2
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.80357!))
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.19643!))
        Me.TableLayoutPanel1.Controls.Add(Me.btnPercentageIncrease, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.numTxtPercentage, 0, 0)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(588, 253)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 1
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(224, 29)
        Me.TableLayoutPanel1.TabIndex = 21
        '
        'numTxtPercentage
        '
        Me.numTxtPercentage.DecimalPlaces = 1
        Me.numTxtPercentage.Location = New System.Drawing.Point(3, 3)
        Me.numTxtPercentage.Maximum = New Decimal(New Integer() {99, 0, 0, 0})
        Me.numTxtPercentage.Minimum = New Decimal(New Integer() {99, 0, 0, -2147483648})
        Me.numTxtPercentage.Name = "numTxtPercentage"
        Me.numTxtPercentage.Size = New System.Drawing.Size(62, 23)
        Me.numTxtPercentage.TabIndex = 26
        '
        'btnProcessSelec
        '
        Me.btnProcessSelec.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnProcessSelec.Enabled = False
        Me.btnProcessSelec.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcessSelec.Location = New System.Drawing.Point(3, 3)
        Me.btnProcessSelec.Name = "btnProcessSelec"
        Me.btnProcessSelec.Size = New System.Drawing.Size(199, 28)
        Me.btnProcessSelec.TabIndex = 22
        Me.btnProcessSelec.Text = "Process Selections"
        Me.btnProcessSelec.UseVisualStyleBackColor = False
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 1
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.dgvClients, 0, 0)
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(7, 282)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(805, 276)
        Me.TableLayoutPanel6.TabIndex = 24
        '
        'TableLayoutPanel7
        '
        Me.TableLayoutPanel7.ColumnCount = 2
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel7.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 320.0!))
        Me.TableLayoutPanel7.Controls.Add(Me.clbItemCodes, 1, 1)
        Me.TableLayoutPanel7.Controls.Add(Me.clbGroupCodes, 0, 1)
        Me.TableLayoutPanel7.Controls.Add(Me.lblItemCodes, 1, 0)
        Me.TableLayoutPanel7.Controls.Add(Me.lblGroupCodes, 0, 0)
        Me.TableLayoutPanel7.Location = New System.Drawing.Point(88, 98)
        Me.TableLayoutPanel7.Name = "TableLayoutPanel7"
        Me.TableLayoutPanel7.RowCount = 2
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 19.64286!))
        Me.TableLayoutPanel7.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 80.35714!))
        Me.TableLayoutPanel7.Size = New System.Drawing.Size(645, 117)
        Me.TableLayoutPanel7.TabIndex = 25
        Me.TableLayoutPanel7.Visible = False
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 1
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.btnProcessSelec, 0, 0)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(8, 226)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(205, 34)
        Me.TableLayoutPanel5.TabIndex = 23
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 618)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(196, 15)
        Me.Label1.TabIndex = 26
        Me.Label1.Text = "© Copyright 2014 Ritzy IT (Pty) LTD"
        '
        'lblIncreaseMonth
        '
        Me.lblIncreaseMonth.AutoSize = True
        Me.lblIncreaseMonth.Font = New System.Drawing.Font("Calibri", 11.25!)
        Me.lblIncreaseMonth.Location = New System.Drawing.Point(85, 53)
        Me.lblIncreaseMonth.Name = "lblIncreaseMonth"
        Me.lblIncreaseMonth.Size = New System.Drawing.Size(104, 18)
        Me.lblIncreaseMonth.TabIndex = 27
        Me.lblIncreaseMonth.Text = "Increase Month"
        '
        'cboIncreaseMonth
        '
        Me.cboIncreaseMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncreaseMonth.FormattingEnabled = True
        Me.cboIncreaseMonth.Location = New System.Drawing.Point(206, 49)
        Me.cboIncreaseMonth.Name = "cboIncreaseMonth"
        Me.cboIncreaseMonth.Size = New System.Drawing.Size(204, 23)
        Me.cboIncreaseMonth.TabIndex = 28
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 1
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Controls.Add(Me.btnViewReport, 0, 0)
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(219, 226)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(205, 34)
        Me.TableLayoutPanel2.TabIndex = 29
        Me.TableLayoutPanel2.Visible = False
        '
        'btnViewReport
        '
        Me.btnViewReport.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnViewReport.Enabled = False
        Me.btnViewReport.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnViewReport.Location = New System.Drawing.Point(3, 3)
        Me.btnViewReport.Name = "btnViewReport"
        Me.btnViewReport.Size = New System.Drawing.Size(199, 28)
        Me.btnViewReport.TabIndex = 22
        Me.btnViewReport.Text = "View Report"
        Me.btnViewReport.UseVisualStyleBackColor = False
        Me.btnViewReport.Visible = False
        '
        'lblTotalAccounts
        '
        Me.lblTotalAccounts.AutoSize = True
        Me.lblTotalAccounts.Location = New System.Drawing.Point(427, 582)
        Me.lblTotalAccounts.Name = "lblTotalAccounts"
        Me.lblTotalAccounts.Size = New System.Drawing.Size(0, 15)
        Me.lblTotalAccounts.TabIndex = 30
        '
        'pbPriceUpdater
        '
        Me.pbPriceUpdater.Location = New System.Drawing.Point(225, 574)
        Me.pbPriceUpdater.Name = "pbPriceUpdater"
        Me.pbPriceUpdater.Size = New System.Drawing.Size(196, 23)
        Me.pbPriceUpdater.TabIndex = 31
        '
        'BackgroundWorker1
        '
        '
        'Timer1
        '
        '
        'BackgroundWorker2
        '
        '
        'Timer2
        '
        '
        'frmPriceUpdater
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(820, 639)
        Me.Controls.Add(Me.pbPriceUpdater)
        Me.Controls.Add(Me.lblTotalAccounts)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Controls.Add(Me.cboIncreaseMonth)
        Me.Controls.Add(Me.lblIncreaseMonth)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TableLayoutPanel7)
        Me.Controls.Add(Me.TableLayoutPanel6)
        Me.Controls.Add(Me.TableLayoutPanel5)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.btnRefresh)
        Me.Controls.Add(Me.btnProcNewPriceUpdates)
        Me.Controls.Add(Me.mnuFile)
        Me.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.mnuFile
        Me.Name = "frmPriceUpdater"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Price Updater"
        Me.mnuFile.ResumeLayout(False)
        Me.mnuFile.PerformLayout()
        CType(Me.dgvClients, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel1.ResumeLayout(False)
        CType(Me.numTxtPercentage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel6.ResumeLayout(False)
        Me.TableLayoutPanel7.ResumeLayout(False)
        Me.TableLayoutPanel7.PerformLayout()
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents mnuFile As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents dgvClients As System.Windows.Forms.DataGridView
    Friend WithEvents OpenFileDialog1 As System.Windows.Forms.OpenFileDialog
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents btnProcNewPriceUpdates As System.Windows.Forms.Button
    Friend WithEvents clbGroupCodes As System.Windows.Forms.CheckedListBox
    Friend WithEvents clbItemCodes As System.Windows.Forms.CheckedListBox
    Friend WithEvents lblGroupCodes As System.Windows.Forms.Label
    Friend WithEvents lblItemCodes As System.Windows.Forms.Label
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportExcellDocumentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btnPercentageIncrease As System.Windows.Forms.Button
    Friend WithEvents btnRefresh As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnProcessSelec As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel7 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents ImportExcelDocumentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents numTxtPercentage As System.Windows.Forms.NumericUpDown
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents lblIncreaseMonth As System.Windows.Forms.Label
    Friend WithEvents cboIncreaseMonth As System.Windows.Forms.ComboBox
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents btnViewReport As System.Windows.Forms.Button
    Friend WithEvents lblTotalAccounts As System.Windows.Forms.Label
    Friend WithEvents pbPriceUpdater As System.Windows.Forms.ProgressBar
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Timer2 As System.Windows.Forms.Timer

End Class
