﻿'Monthly Service
Public Class ServiceContract

    Public Property ServiceFee As Double
    Public Property ServiceDescription As String = "Service Fees"
    Public Property SurveilanceFee As Double
    Public Property SurveilanceDescription As String = "Remote Surveillance Fees"
    Public Property ReportFee As Double
    Public Property ReportDescription As String = "Activity Reports"
    Public Property SMSFee As Double
    Public Property SMSDescription As String = "Open/Close SMS"
    Public Property AngelFee As Double
    Public Property AngelDescription As String = "Blue Angels Charitable Trust"
    'NEW
    Public Property GuardingFees As Double
    Public Property GuardingDescription As String = "Guarding Fees"
    'NEW 2016-10-12
    Public Property BluePanicAppFees As Double
    Public Property BluePanicAppDescription As String = "Blue Panic App"

End Class

'Annual Radio Licience
Public Class RadioContract

    Public Property RadioFeeMonth As String
    Public Property RadioFee As Double
    Public Property RadioDescription As String

End Class

'Extra Service Items
Public Class AdHocContract

    Public Property ItemCode As String
    Public Property ItemDescription As String
    Public Property ItemPrice As Double

End Class
