﻿Public Class Contract

    Public Property InvoiceDate As String
    Public Property Description As String
    Public Property isGroupInvoice As Boolean
    Public Property isThridParty As Boolean
    Public Property isSpecialProjects As Boolean
    Public Property PaymentFrequency As Integer
    Public Property FirstBillDate As Date
    Public Property NextBillDate As Date
    Public Property ContractEndDate As Date

    Public Property ServiceContract As New ServiceContract
    Public Property RadioContract As New RadioContract
    Public Property AdhocContract As New List(Of AdHocContract)

End Class


