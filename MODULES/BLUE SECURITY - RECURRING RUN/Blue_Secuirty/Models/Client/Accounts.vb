﻿Public Class Accounts

    Public Property CustomerID As Integer
    Public Property CustomerCode As String
    Public Property Name As String
    Public Property ContractActive As Boolean
    Public Property ContractSuspended As Boolean
    Public Property Physical1 As String
    Public Property Physical2 As String
    Public Property Physical3 As String
    Public Property Physical4 As String
    Public Property Physical5 As String
    Public Property PhysicalPC As String
    Public Property Post1 As String
    Public Property Post2 As String
    Public Property Post3 As String
    Public Property Post4 As String
    Public Property Post5 As String
    Public Property PostPC As String

    Public Property Contract As New Contract

End Class
