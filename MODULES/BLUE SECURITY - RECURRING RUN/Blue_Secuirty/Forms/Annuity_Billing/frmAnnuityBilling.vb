﻿Imports Pastel.Evolution
Imports System.IO

Public Class frmAnnuityBill

    Dim customers As New List(Of customer)
    Dim runMonth As String
    Dim path As String
    Dim radioRun As Boolean = False
    Dim log As New LogFile("LogFile", "Sample")

    Sub New(ByVal dBInstance As String, ByVal dBName As String, _
           ByVal dBUser As String, ByVal dBPassword As String, ByVal evoCommon As String)

        ' This call is required by the designer.
        InitializeComponent()

        My.Settings.DBInstance = dBInstance
        My.Settings.DBName = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.EvolutionCommon = evoCommon

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    '--------AUTO GENERATED METHODS (FORM EVENTS)
    Private Sub frmAnnuityBill_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load


        log.CreateLog("Application opened")


        'Declare Variables
        Dim repRunMonth As New repGetInvoiceDatails
        Dim dr As DataRow

        For Each dr In repRunMonth.getRunMonths.Rows

            DDLMonths.Items.Add(dr("RUN MONTH"))

        Next

    End Sub
    Private Sub DDLMonths_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DDLMonths.SelectedIndexChanged

        btnGetData.Enabled = True
        btnProcessRadio.Enabled = True
        runMonth = DDLMonths.SelectedItem.ToString()

    End Sub

    Private Sub btnGetData_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGetData.Click '***New***

        DDLMonths.Enabled = False
        btnProcessRadio.Enabled = False
        btnGetData.Enabled = False

        radioRun = False

        Dim fileDialog As FolderBrowserDialog = New FolderBrowserDialog

        fileDialog.Description = "Please select a path to save the Montyly Invoicing Report Logs"
        fileDialog.RootFolder = Environment.SpecialFolder.Desktop
        Try

            If fileDialog.ShowDialog() = DialogResult.OK Then

                path = fileDialog.SelectedPath.ToString()

                If Not Directory.Exists(path) Then

                    Directory.CreateDirectory(path)

                End If

            End If

        Catch ex As Exception

        End Try

        BackgroundWorker1.RunWorkerAsync()
        Timer1.Enabled = True
        Timer1.Start()

    End Sub

    Private Sub btnProcessRadio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessRadio.Click

        DDLMonths.Enabled = False
        btnGetData.Enabled = False
        btnProcessRadio.Enabled = False

        radioRun = True

        Dim fileDialog As FolderBrowserDialog = New FolderBrowserDialog

        fileDialog.Description = "Please select a path to save the Radio Licence Invoicing Report Logs"
        fileDialog.RootFolder = Environment.SpecialFolder.Desktop
        Try

            If fileDialog.ShowDialog() = DialogResult.OK Then

                path = fileDialog.SelectedPath.ToString()

                If Not Directory.Exists(path) Then

                    Directory.CreateDirectory(path)

                End If

            End If

        Catch ex As Exception

        End Try

        BackgroundWorker2.RunWorkerAsync()
        Timer2.Enabled = True
        Timer2.Start()

    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

        'Retrieval
        Dim repInvoiceDetails As New repGetInvoiceDatails
        Dim repUpdateSuspended As New repUpdates

        'Post
        Dim PastelInterface As New repPastelInterface
        Dim insertNextRunDetails As New repInsert
        Dim updateRunStatus As New repUpdates

        Dim invFirstDate As String = repInvoiceDetails.getInvFirstDate(runMonth)

        'Unsuspend Accounts
        repUpdateSuspended.unSuspendTempAccounts(invFirstDate)

        'Create Sales Orders
        Dim invLastDate As String = repInvoiceDetails.getInvLastDate(runMonth)
        Dim invNextFirstDate As String = (Date.Parse(insertNextRunDetails.setInvLastDate(runMonth)).AddDays(1)).ToString()

        'calls method which creates invoices in pastel evo
        retrieveData("N/A")

        updateRunStatus.unblockPeriod("0", (Date.Parse(invNextFirstDate)).AddDays(-1).ToString("yyyy-MM-dd"))

        'generate invoices for all customer!! 
        PastelInterface.ProcessSalesOrders(customers, runMonth, radioRun, invLastDate, invFirstDate, invNextFirstDate, path)

        updateRunStatus.unblockPeriod("1", (Date.Parse(invNextFirstDate)).AddDays(-1).ToString("yyyy-MM-dd"))

        insertNextRunDetails.nextRunMonthDetails(runMonth)
        updateRunStatus.updateRunStatus(runMonth)

        If Me.InvokeRequired Then
            Timer1.Stop()
            Timer1.Enabled = False
            Me.Invoke(New MethodInvoker(AddressOf clearProgressbar))
            Me.Invoke(New MethodInvoker(AddressOf displsayMonthlyResults))
            'exportMonthlyInvoicesReportToExcel()
            'exportMonthlyContractFeeBreakdownToExcel()
            MsgBox(runMonth & " Monthly Invoice Run has completed. Invoices have been created.")
            ' MsgBox("The data has been retrieved.")
        Else
            MsgBox("The data has been retrieved.")
            ' btnProcessInvoices.Enabled = True
            pbRRun.Value = 0
        End If

    End Sub
    Private Sub BackgroundWorker2_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork

        'Retrieval
        Dim repInvoiceDetails As New repGetInvoiceDatails
        Dim repUpdateSuspended As New repUpdates

        'Post
        Dim PastelInterface As New repPastelInterface
        Dim insertNextRunDetails As New repInsert
        Dim updateRunStatus As New repUpdates

        Dim invFirstDate As String = repInvoiceDetails.getInvFirstDate(runMonth)

        'Unsuspend Accounts
        repUpdateSuspended.unSuspendTempAccounts(invFirstDate)

        'Create Sales Orders
        Dim radioRun As String = repInvoiceDetails.getRadioRun(runMonth)
        Dim radioDescription As String = repInvoiceDetails.getRadioDescription(runMonth)
        Dim invLastDate As String = repInvoiceDetails.getInvLastDate(runMonth)
        Dim invNextFirstDate As String = (Date.Parse(insertNextRunDetails.setInvLastDate(runMonth)).AddDays(1)).AddMonths(-1).ToString()

        'calls method which creates invoices in pastel evo
        retrieveData(radioRun)

        updateRunStatus.unblockPeriod("0", (Date.Parse(invNextFirstDate)).AddDays(-1).ToString("yyyy-MM-dd"))

        'generate invoices for all customer!! 
        PastelInterface.ProcessRadioSalesOrders(customers, runMonth, radioRun, radioDescription, invLastDate, invFirstDate, path)

        updateRunStatus.unblockPeriod("1", (Date.Parse(invNextFirstDate)).AddDays(-1).ToString("yyyy-MM-dd"))

        If Me.InvokeRequired Then
            Timer1.Stop()
            Timer1.Enabled = False
            Me.Invoke(New MethodInvoker(AddressOf clearProgressbar))
            Me.Invoke(New MethodInvoker(AddressOf displsayRadioLicenceResults))
            'exportRadioInvoicesReportToExcel()
            'exportRadioContractFeeBreakdownToExcel()
            MsgBox(runMonth & " Radio Licence Invoice Run has completed. Invoices have been created.")
            ' MsgBox("The data has been retrieved.")
        Else
            MsgBox("The data has been retrieved.")
            ' btnProcessInvoices.Enabled = True
            pbRRun.Value = 0
        End If

    End Sub

    Private Sub clearProgressbar() Handles BackgroundWorker1.RunWorkerCompleted

        pbRRun.Value = pbRRun.Maximum

    End Sub
    Private Sub displsayRadioLicenceResults() Handles BackgroundWorker1.RunWorkerCompleted

        'Results
        Dim repResults As New repGetStats

        ' lblMonthlyResults.Text = "Total number of Monthly Invoices: " & repResults.monthlyInvoices(runMonth)
        lblRadioResults.Text = "Total number of Radio Invoices: " & repResults.radioInvoices(runMonth)

        pbRRun.Value = pbRRun.Maximum

    End Sub
    'NEW - BY SHWETA
    Private Sub displsayMonthlyResults() Handles BackgroundWorker1.RunWorkerCompleted

        'Results
        Dim repResults As New repGetStats

        lblMonthlyResults.Text = "Total number of Monthly Invoices: " & repResults.monthlyInvoices(runMonth)
        'lblRadioResults.Text = "Total number of Radio Invoices: " & repResults.radioInvoices(runMonth)

        pbRRun.Value = pbRRun.Maximum

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick


        If pbRRun.Value >= pbRRun.Maximum Then
            pbRRun.Value = 0
        Else
            pbRRun.Value += 1
        End If

    End Sub

    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick

        If pbRRun.Value >= pbRRun.Maximum Then
            pbRRun.Value = 0
        Else
            pbRRun.Value += 1
        End If

    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click

        log.CreateLog("Form closed")
        Me.Close()
    End Sub

    Private Sub retrieveData(ByVal radioRunMonth As String)

        'Decalare objects
        'Dim customers As New List(Of customer)
        Dim customerLoader As New RepClients
        Dim contractLoader As New RepContracts
        'NEW WITH RADIO LICENCE SEPERATING
        Dim extraQry As String = ""


        If radioRun = True Then

            extraQry = "AND (C.ulARRadioLicFeeBillMonth = '" & radioRunMonth & "' or CL.ulARRadioLicFeeBillMonth = '" & radioRunMonth & "')"
            'set customers using customer repository
            customers = customerLoader.getPastelCustomers(extraQry)

        Else

            'set customers using customer repository
            customers = customerLoader.getPastelCustomers(extraQry)

        End If

        'load contract info into customers!! - using repository
        customers = contractLoader.loadContracts(customers, radioRun)


    End Sub

    Private Sub exportRadioInvoicesReportToExcel()

        'Declare Variables
        Dim rep As New repGetStats
        Dim fileName As String
        'Dim f As FolderBrowserDialog = New FolderBrowserDialog

        ''set properties
        'f.Description = "Please select file Destination"
        'f.RootFolder = Environment.SpecialFolder.Desktop

        Try

            fileName = InputBox("Please Enter a file name", "File Name", "Radio Licence - " & (System.DateTime.Today).ToString("ddMMMyyyy") & _
                                 "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm tt"))

            rep.ExportRadioInvoicesReportData(runMonth, fileName, path)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try


    End Sub

    Private Sub exportMonthlyInvoicesReportToExcel()

        'Declare Variables
        Dim rep As New repGetStats
        Dim fileName As String
        'Dim f As FolderBrowserDialog = New FolderBrowserDialog

        ''set properties
        'f.Description = "Please select file Destination"
        'f.RootFolder = Environment.SpecialFolder.Desktop

        Try

            fileName = InputBox("Please Enter a file name", "File Name", "Monthly Invoices - " & (System.DateTime.Today).ToString("ddMMMyyyy") & _
                                 "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm tt"))

            rep.ExportMonthlyInvoicesReportData(runMonth, fileName, path)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try


    End Sub

    Private Sub exportMonthlyContractFeeBreakdownToExcel()

        'Declare Variables
        Dim rep As New repGetStats
        Dim fileName As String
        'Dim f As FolderBrowserDialog = New FolderBrowserDialog

        ''set properties
        'f.Description = "Please select file Destination"
        'f.RootFolder = Environment.SpecialFolder.Desktop

        Try

            fileName = InputBox("Please Enter a file name", "File Name", "Monthly Contract Fee Breakdown - " & (System.DateTime.Today).ToString("ddMMMyyyy") & _
                                 "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm tt"))

            rep.ExportMonthlyContractFeeBreakdownData(runMonth, fileName, path)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try


    End Sub

    Private Sub exportRadioContractFeeBreakdownToExcel()

        'Declare Variables
        Dim rep As New repGetStats
        Dim fileName As String
        'Dim f As FolderBrowserDialog = New FolderBrowserDialog

        ''set properties
        'f.Description = "Please select file Destination"
        'f.RootFolder = Environment.SpecialFolder.Desktop

        Try

            fileName = InputBox("Please Enter a file name", "File Name", "Radio Licence Fee Breakdown - " & (System.DateTime.Today).ToString("ddMMMyyyy") & _
                                 "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm tt"))

            rep.ExportRadioContractFeeBreakdownData(runMonth, fileName, path)

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try


    End Sub

End Class

