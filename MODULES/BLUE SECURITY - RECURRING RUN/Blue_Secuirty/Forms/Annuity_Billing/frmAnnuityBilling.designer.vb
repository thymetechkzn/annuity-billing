﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmAnnuityBill
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmAnnuityBill))
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConnectionToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SettingsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.btnGetData = New System.Windows.Forms.Button()
        Me.DDLMonths = New System.Windows.Forms.ComboBox()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.lblSelectMonth = New System.Windows.Forms.Label()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnProcessRadio = New System.Windows.Forms.Button()
        Me.pbRRun = New System.Windows.Forms.ProgressBar()
        Me.lblCopyright = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.lblMonthlyResults = New System.Windows.Forms.Label()
        Me.lblRadioResults = New System.Windows.Forms.Label()
        Me.MenuStrip1.SuspendLayout()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.MenuStrip1.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ConnectionToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Padding = New System.Windows.Forms.Padding(8, 3, 0, 3)
        Me.MenuStrip1.Size = New System.Drawing.Size(559, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(40, 18)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(93, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ConnectionToolStripMenuItem
        '
        Me.ConnectionToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.SettingsToolStripMenuItem})
        Me.ConnectionToolStripMenuItem.Name = "ConnectionToolStripMenuItem"
        Me.ConnectionToolStripMenuItem.Size = New System.Drawing.Size(79, 18)
        Me.ConnectionToolStripMenuItem.Text = "Connection"
        Me.ConnectionToolStripMenuItem.Visible = False
        '
        'SettingsToolStripMenuItem
        '
        Me.SettingsToolStripMenuItem.Name = "SettingsToolStripMenuItem"
        Me.SettingsToolStripMenuItem.Size = New System.Drawing.Size(116, 22)
        Me.SettingsToolStripMenuItem.Text = "Settings"
        '
        'btnGetData
        '
        Me.btnGetData.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnGetData.Enabled = False
        Me.btnGetData.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGetData.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGetData.Location = New System.Drawing.Point(445, 5)
        Me.btnGetData.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnGetData.Name = "btnGetData"
        Me.btnGetData.Size = New System.Drawing.Size(96, 69)
        Me.btnGetData.TabIndex = 3
        Me.btnGetData.Text = "Process " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Monthly " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Billing"
        Me.btnGetData.UseVisualStyleBackColor = False
        '
        'DDLMonths
        '
        Me.DDLMonths.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.DDLMonths.FormattingEnabled = True
        Me.DDLMonths.Location = New System.Drawing.Point(3, 29)
        Me.DDLMonths.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.DDLMonths.Name = "DDLMonths"
        Me.DDLMonths.Size = New System.Drawing.Size(337, 27)
        Me.DDLMonths.TabIndex = 1
        '
        'BackgroundWorker1
        '
        '
        'lblSelectMonth
        '
        Me.lblSelectMonth.AutoSize = True
        Me.lblSelectMonth.Font = New System.Drawing.Font("Calibri", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSelectMonth.Location = New System.Drawing.Point(3, 0)
        Me.lblSelectMonth.Name = "lblSelectMonth"
        Me.lblSelectMonth.Size = New System.Drawing.Size(230, 23)
        Me.lblSelectMonth.TabIndex = 6
        Me.lblSelectMonth.Text = "Select Billing Month and Year"
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.lblSelectMonth, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.DDLMonths, 0, 1)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(5, 6)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 2
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(343, 63)
        Me.TableLayoutPanel1.TabIndex = 7
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel1.Controls.Add(Me.btnProcessRadio)
        Me.Panel1.Controls.Add(Me.TableLayoutPanel1)
        Me.Panel1.Controls.Add(Me.btnGetData)
        Me.Panel1.Location = New System.Drawing.Point(7, 27)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(546, 77)
        Me.Panel1.TabIndex = 8
        '
        'btnProcessRadio
        '
        Me.btnProcessRadio.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnProcessRadio.Enabled = False
        Me.btnProcessRadio.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcessRadio.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnProcessRadio.Location = New System.Drawing.Point(354, 5)
        Me.btnProcessRadio.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.btnProcessRadio.Name = "btnProcessRadio"
        Me.btnProcessRadio.Size = New System.Drawing.Size(89, 69)
        Me.btnProcessRadio.TabIndex = 2
        Me.btnProcessRadio.Text = "Process Radio Licences"
        Me.btnProcessRadio.UseVisualStyleBackColor = False
        Me.btnProcessRadio.Visible = False
        '
        'pbRRun
        '
        Me.pbRRun.Location = New System.Drawing.Point(362, 109)
        Me.pbRRun.Name = "pbRRun"
        Me.pbRRun.Size = New System.Drawing.Size(191, 23)
        Me.pbRRun.TabIndex = 8
        '
        'lblCopyright
        '
        Me.lblCopyright.AutoSize = True
        Me.lblCopyright.Font = New System.Drawing.Font("Calibri", 9.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCopyright.Location = New System.Drawing.Point(177, 179)
        Me.lblCopyright.Name = "lblCopyright"
        Me.lblCopyright.Size = New System.Drawing.Size(196, 15)
        Me.lblCopyright.TabIndex = 9
        Me.lblCopyright.Text = "© Copyright 2014 Ritzy IT (Pty) LTD"
        '
        'Timer1
        '
        '
        'Timer2
        '
        '
        'BackgroundWorker2
        '
        '
        'lblMonthlyResults
        '
        Me.lblMonthlyResults.AutoSize = True
        Me.lblMonthlyResults.Location = New System.Drawing.Point(12, 113)
        Me.lblMonthlyResults.Name = "lblMonthlyResults"
        Me.lblMonthlyResults.Size = New System.Drawing.Size(0, 19)
        Me.lblMonthlyResults.TabIndex = 10
        '
        'lblRadioResults
        '
        Me.lblRadioResults.AutoSize = True
        Me.lblRadioResults.Location = New System.Drawing.Point(9, 144)
        Me.lblRadioResults.Name = "lblRadioResults"
        Me.lblRadioResults.Size = New System.Drawing.Size(0, 19)
        Me.lblRadioResults.TabIndex = 11
        '
        'frmAnnuityBill
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 19.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = True
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(559, 204)
        Me.Controls.Add(Me.lblRadioResults)
        Me.Controls.Add(Me.lblMonthlyResults)
        Me.Controls.Add(Me.lblCopyright)
        Me.Controls.Add(Me.pbRRun)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmAnnuityBill"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Annuity Billing"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents btnGetData As System.Windows.Forms.Button
    Friend WithEvents DDLMonths As System.Windows.Forms.ComboBox
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblSelectMonth As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents pbRRun As System.Windows.Forms.ProgressBar
    Friend WithEvents lblCopyright As System.Windows.Forms.Label
    Friend WithEvents ConnectionToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents SettingsToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Timer2 As System.Windows.Forms.Timer
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents lblMonthlyResults As System.Windows.Forms.Label
    Friend WithEvents lblRadioResults As System.Windows.Forms.Label
    Friend WithEvents btnProcessRadio As System.Windows.Forms.Button

End Class
