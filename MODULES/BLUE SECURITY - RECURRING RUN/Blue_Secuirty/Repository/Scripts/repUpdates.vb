﻿Public Class repUpdates

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Sub unSuspendTempAccounts(ByVal dateRange As String)

        Dim qry As String

        qry = "UPDATE" & vbCrLf & _
                "Client" & vbCrLf & _
                "SET " & vbCrLf & _
                "ubARSuspendService = 0    " & vbCrLf & _
                "WHERE" & vbCrLf & _
                "ubARSuspendService = 1" & vbCrLf & _
                "AND" & vbCrLf & _
                "CONVERT(DATE, udARSuspendReactivateDate) <= '" & dateRange & "'" & vbCrLf & _
                "and ulARSuspendType = 'Temporary'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()
        dbConn.CloseConnection()

    End Sub

    Public Sub unblockPeriod(ByVal blockType As String, ByVal periodDate As String)


        Dim qry As String

        qry = "UPDATE _etblPeriod" & vbCrLf & _
                "SET bBlocked = '" & blockType & "'" & vbCrLf & _
                "WHERE dPeriodDate ='" & periodDate & " 00:00:00'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()
        dbConn.CloseConnection()

    End Sub

    Public Sub updateNextBillDate(ByVal accountCode As String, ByVal nextDate As String)


        Dim qry As String

        qry = "UPDATE Client" & vbCrLf & _
            "SET udARContractNextBillDate = '" & nextDate & "'" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "Account = '" & accountCode & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()
        dbConn.CloseConnection()

    End Sub

    Public Sub updateRunStatus(ByVal curRunMonth As String)

        Dim qry As String

        qry = "UPDATE" & vbCrLf & _
                "runMonth" & vbCrLf & _
                "SET isCompleted = 1" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "invDescriptionSuffix = '" & curRunMonth & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()
        dbConn.CloseConnection()

    End Sub

End Class
