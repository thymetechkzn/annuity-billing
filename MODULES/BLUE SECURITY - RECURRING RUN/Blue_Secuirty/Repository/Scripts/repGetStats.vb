﻿Public Class repGetStats

    Dim Conn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function monthlyInvoices(ByVal runMonth As String) As Integer

        'Declare
        Dim qry As String
        Dim numMonthlyInvoices As Integer

        qry = "SELECT" & vbCrLf & _
                "COUNT(*) AS 'NumberOfMonthlyInv'" & vbCrLf & _
                "FROM" & vbCrLf & _
                "InvNUm" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "Message2 LIKE '%" & runMonth & "_INVOICE%'"

        Conn.CloseConnection()

        Conn.setSqlString(qry)
        Conn.createNewConnection()

        numMonthlyInvoices = Conn.get_value("NumberOfMonthlyInv")

        Return numMonthlyInvoices

    End Function

    Public Function radioInvoices(ByVal runMonth As String) As Integer

        'Declare
        Dim qry As String
        Dim numRadioInvoices As Integer

        qry = "SELECT" & vbCrLf & _
                "COUNT(*) AS 'NumberOfRadioInv'" & vbCrLf & _
                "FROM" & vbCrLf & _
                "InvNUm" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "Message2 LIKE '%" & runMonth & "_RADIO%'"

        Conn.CloseConnection()

        Conn.setSqlString(qry)
        Conn.createNewConnection()

        numRadioInvoices = Conn.get_value("NumberOfRadioInv")

        Return numRadioInvoices

    End Function
    'Monthly Recurring Report
    Public Function MonthlyInvoicesReport(ByVal runMonth As String) As DataTable

        'Declare
        Dim qry As String
        Dim dt As New DataTable

        qry = "SELECT" & vbCrLf & _
                "(SELECT C.Account FROM Client C WHERE C.DClink = I.AccountID) AS 'Account Code'," & vbCrLf & _
                "(SELECT C.Name FROM Client C WHERE C.DClink = I.AccountID) AS 'Account Name'," & vbCrLf & _
                "I.InvNumber AS 'Invoice Number'," & vbCrLf & _
                "I.Description," & vbCrLf & _
                "I.InvDate AS 'Invoice Date'," & vbCrLf & _
                "I.InvTotIncl  AS 'Monthly Recurring Fee (Incl)'" & vbCrLf & _
                "FROM" & vbCrLf & _
                "InvNum I" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "I.Message2 LIKE '%" & runMonth & "_I%'" & vbCrLf & _
                "ORDER BY 'Account Code'" & vbCrLf


        Conn.CloseConnection()
        dt = Conn.get_datatable(qry)
        Conn.createNewConnection()

        Return dt

    End Function

    Public Function RadioInvoicesReport(ByVal runMonth As String) As DataTable
        'Declare
        Dim qry As String
        Dim dt As New DataTable

        qry = "SELECT" & vbCrLf & _
                "(SELECT C.Account FROM Client C WHERE C.DClink = I.AccountID) AS 'Account Code'," & vbCrLf & _
                "(SELECT C.Name FROM Client C WHERE C.DClink = I.AccountID) AS 'Account Name'," & vbCrLf & _
                "I.InvNumber AS 'Invoice Number'," & vbCrLf & _
                "I.Description," & vbCrLf & _
                "I.InvDate AS 'Invoice Date'," & vbCrLf & _
                "I.InvTotIncl  AS 'Monthly Recurring Fee (Incl)'" & vbCrLf & _
                "FROM" & vbCrLf & _
                "InvNum I" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "I.Message2 LIKE '%" & runMonth & "_R%'" & vbCrLf & _
                "ORDER BY 'Account Code'" & vbCrLf



        Conn.CloseConnection()
        dt = Conn.get_datatable(qry)
        Conn.createNewConnection()

        Return dt


    End Function
    ' Contract Fee Breakdown- Consolidated per invoice with Nominal Codes
    Public Function MonthlyContractFeeBreakdown(ByVal runMonth As String) As DataTable

        'Declare
        Dim qry As String
        Dim dt As New DataTable

        qry = "SELECT" & vbCrLf & _
                "I.InvNumber," & vbCrLf & _
                "I.InvDate," & vbCrLf & _
                "I.OrderNum," & vbCrLf & _
                "--L.fQuantityLineTotIncl," & vbCrLf & _
                "--(select Code  from StkItem where StockLink = L.iStockCodeID) as 'ITEM'," & vbCrLf & _
                "(SELECT A.Master_Sub_Account FROM Accounts A WHERE A.AccountLink = G.AccountLink) AS 'NominalCode'," & vbCrLf & _
                "(SELECT A.Description FROM Accounts A WHERE A.AccountLink = G.AccountLink) AS 'NominalDescription'," & vbCrLf & _
                "G.Debit AS 'Debit'," & vbCrLf & _
                "G.Credit AS 'Credit'" & vbCrLf & _
                "FROM" & vbCrLf & _
                "InvNum I" & vbCrLf & _
                "INNER JOIN" & vbCrLf & _
                "PostGL G" & vbCrLf & _
                "ON" & vbCrLf & _
                "I.InvNumber = G.Reference" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "I.Message2 LIKE '%" & runMonth & "_I%'" & vbCrLf & _
                "AND" & vbCrLf & _
                "G.UserName = 'SDK'" & vbCrLf & _
                "ORDER BY I.InvNumber" & vbCrLf



        Conn.CloseConnection()
        dt = Conn.get_datatable(qry)
        Conn.createNewConnection()

        Return dt

    End Function

    Public Function RadioContractFeeBreakdown(ByVal runMonth As String) As DataTable

        'Declare
        Dim qry As String
        Dim dt As New DataTable

        qry = "SELECT" & vbCrLf & _
                "I.InvNumber," & vbCrLf & _
                "I.InvDate," & vbCrLf & _
                "I.OrderNum," & vbCrLf & _
                "--L.fQuantityLineTotIncl," & vbCrLf & _
                "--(select Code  from StkItem where StockLink = L.iStockCodeID) as 'ITEM'," & vbCrLf & _
                "(SELECT A.Master_Sub_Account FROM Accounts A WHERE A.AccountLink = G.AccountLink) AS 'NominalCode'," & vbCrLf & _
                "(SELECT A.Description FROM Accounts A WHERE A.AccountLink = G.AccountLink) AS 'NominalDescription'," & vbCrLf & _
                "G.Debit AS 'Debit'," & vbCrLf & _
                "G.Credit AS 'Credit'" & vbCrLf & _
                "FROM" & vbCrLf & _
                "InvNum I" & vbCrLf & _
                "INNER JOIN" & vbCrLf & _
                "PostGL G" & vbCrLf & _
                "ON" & vbCrLf & _
                "I.InvNumber = G.Reference" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "I.Message2 LIKE '%" & runMonth & "_R%'" & vbCrLf & _
                "AND" & vbCrLf & _
                "G.UserName = 'SDK'" & vbCrLf & _
                "ORDER BY I.InvNumber" & vbCrLf



        Conn.CloseConnection()
        dt = Conn.get_datatable(qry)
        Conn.createNewConnection()

        Return dt

    End Function

    Public Sub ExportMonthlyInvoicesReportData(ByVal runMonth As String, ByVal filename As String, ByVal selectedPath As String) 'This Method converts Datagridview Tables into Cells and columns of an Excel Sheet

        Try
            System.Threading.Thread.CurrentThread.CurrentCulture = _
            System.Globalization.CultureInfo.CreateSpecificCulture("en-UK")
            Dim oExcel As Microsoft.Office.Interop.Excel.Application
            Dim oBook As Microsoft.Office.Interop.Excel.Workbook
            Dim oSheet As Microsoft.Office.Interop.Excel.Worksheet
            oExcel = CreateObject("Excel.Application")
            oBook = oExcel.Workbooks.Add(Type.Missing)
            oSheet = oBook.Worksheets(1)

            Dim dc As New DataColumn
            Dim dr As DataRow


            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0
            ' Dim runMonth As String = ""
            Dim dgvExcelExport As New DataTable

            Dim DT As New DataTable

            DT = MonthlyInvoicesReport(runMonth)

            For Each dc In DT.Columns

                colIndex = colIndex + 1
                oSheet.Cells(1, colIndex) = dc.ColumnName

            Next

            For Each dr In DT.Rows

                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In DT.Columns

                    colIndex = colIndex + 1
                    oSheet.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName).ToString()

                Next

            Next

            'Set final path
            Dim FileExtension As String = ".XLS"

            filename = filename + FileExtension
            '"PRICE UPDATER - " + (System.DateTime.Today).ToString("ddMMMyyyy") + FileExtension

            Dim finalPath = selectedPath + "\" + filename
            oSheet.Columns.AutoFit()
            'Save file in final path
            oBook.SaveAs(finalPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, _
            Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, _
            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing)

            'Release the objects
            ReleaseObject(oSheet)
            oBook.Close(False, Type.Missing, Type.Missing)
            ReleaseObject(oBook)
            oExcel.Quit()
            ReleaseObject(oExcel)
            'Some time Office application does not quit after automation: 
            'so i am calling GC.Collect method.
            GC.Collect()

            MessageBox.Show("Export done successfully!")

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try
    End Sub

    Public Sub ExportRadioInvoicesReportData(ByVal runMonth As String, ByVal filename As String, ByVal selectedPath As String) 'This Method converts Datagridview Tables into Cells and columns of an Excel Sheet

        'Declare and instantiate variables
        'Dim SelectedPath As String
        'Dim fileName As String
        'Dim f As FolderBrowserDialog = New FolderBrowserDialog

        'set properties
        'f.Description = "Please select file Destination"
        'f.RootFolder = Environment.SpecialFolder.MyDocuments

        Try
            'If f.ShowDialog() = DialogResult.OK Then

            '    fileName = InputBox("Please Enter a file name", "File Name", "PRICE UPDATER - " & (System.DateTime.Today).ToString("ddMMMyyyy") & _
            '                         "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm"))

            '    SelectedPath = f.SelectedPath.ToString
            'This section help you if your language is not English.
            System.Threading.Thread.CurrentThread.CurrentCulture = _
            System.Globalization.CultureInfo.CreateSpecificCulture("en-UK")
            Dim oExcel As Microsoft.Office.Interop.Excel.Application
            Dim oBook As Microsoft.Office.Interop.Excel.Workbook
            Dim oSheet As Microsoft.Office.Interop.Excel.Worksheet
            oExcel = CreateObject("Excel.Application")
            oBook = oExcel.Workbooks.Add(Type.Missing)
            oSheet = oBook.Worksheets(1)

            'Dim dc As DataGridViewColumn  '***********TEST UNCOMMENT WHEN DONE***********SHWETA We'll see
            'Dim dr As DataGridViewRow '***********TEST UNCOMMENT WHEN DONE***********SHWETA We'll see
            Dim dc As New DataColumn
            Dim dr As DataRow


            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0
            ' Dim runMonth As String = ""
            Dim dgvExcelExport As New DataTable

            'Style Columns
            'Dim style As Microsoft.Office.Interop.Excel.Style = oSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
            'style.Font.Bold = True
            'style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightBlue)


            'TESTING
            'dgvExcelExport = MonthlyInvoicesReport(runMonth)
            Dim DT As New DataTable

            DT = RadioInvoicesReport(runMonth)

            For Each dc In DT.Columns

                colIndex = colIndex + 1
                oSheet.Cells(1, colIndex) = dc.ColumnName

            Next

            For Each dr In DT.Rows

                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In DT.Columns

                    colIndex = colIndex + 1
                    oSheet.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName).ToString()

                Next

            Next

            ''Export the Columns to excel file
            'For Each dc In dgvExcelExport.Columns
            '    colIndex = colIndex + 1
            '    oSheet.Cells(1, colIndex) = dc.Name
            '    ' oSheet.Cells(1, colIndex).Style = "NewStyle"
            'Next

            ''Export the rows to excel file
            'For Each dr In dgvExcelExport.Rows
            '    rowIndex = rowIndex + 1
            '    colIndex = 0
            '    For Each dc In dgvExcelExport.Columns
            '        colIndex = colIndex + 1
            '        oSheet.Cells(rowIndex + 1, colIndex) = dr.Cells(dc.Name).Value
            '    Next
            'Next

            'TESTING

            'Set final path
            Dim FileExtension As String = ".XLS"

            filename = filename + FileExtension
            '"PRICE UPDATER - " + (System.DateTime.Today).ToString("ddMMMyyyy") + FileExtension

            Dim finalPath = selectedPath + "\" + filename
            oSheet.Columns.AutoFit()
            'Save file in final path
            oBook.SaveAs(finalPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, _
            Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, _
            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing)

            'Release the objects
            ReleaseObject(oSheet)
            oBook.Close(False, Type.Missing, Type.Missing)
            ReleaseObject(oBook)
            oExcel.Quit()
            ReleaseObject(oExcel)
            'Some time Office application does not quit after automation: 
            'so i am calling GC.Collect method.
            GC.Collect()

            MessageBox.Show("Export done successfully!")

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try
    End Sub

    Public Sub ExportMonthlyContractFeeBreakdownData(ByVal runMonth As String, ByVal filename As String, ByVal selectedPath As String) 'This Method converts Datagridview Tables into Cells and columns of an Excel Sheet

        'Declare and instantiate variables
        'Dim SelectedPath As String
        'Dim fileName As String
        'Dim f As FolderBrowserDialog = New FolderBrowserDialog

        'set properties
        'f.Description = "Please select file Destination"
        'f.RootFolder = Environment.SpecialFolder.MyDocuments

        Try
            'If f.ShowDialog() = DialogResult.OK Then

            '    fileName = InputBox("Please Enter a file name", "File Name", "PRICE UPDATER - " & (System.DateTime.Today).ToString("ddMMMyyyy") & _
            '                         "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm"))

            '    SelectedPath = f.SelectedPath.ToString
            'This section help you if your language is not English.
            System.Threading.Thread.CurrentThread.CurrentCulture = _
            System.Globalization.CultureInfo.CreateSpecificCulture("en-UK")
            Dim oExcel As Microsoft.Office.Interop.Excel.Application
            Dim oBook As Microsoft.Office.Interop.Excel.Workbook
            Dim oSheet As Microsoft.Office.Interop.Excel.Worksheet
            oExcel = CreateObject("Excel.Application")
            oBook = oExcel.Workbooks.Add(Type.Missing)
            oSheet = oBook.Worksheets(1)

            'Dim dc As DataGridViewColumn  '***********TEST UNCOMMENT WHEN DONE***********SHWETA We'll see
            'Dim dr As DataGridViewRow '***********TEST UNCOMMENT WHEN DONE***********SHWETA We'll see
            Dim dc As New DataColumn
            Dim dr As DataRow


            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0
            ' Dim runMonth As String = ""
            Dim dgvExcelExport As New DataTable

            'Style Columns
            'Dim style As Microsoft.Office.Interop.Excel.Style = oSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
            'style.Font.Bold = True
            'style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightBlue)


            'TESTING
            'dgvExcelExport = MonthlyInvoicesReport(runMonth)
            Dim DT As New DataTable

            DT = MonthlyContractFeeBreakdown(runMonth)

            For Each dc In DT.Columns

                colIndex = colIndex + 1
                oSheet.Cells(1, colIndex) = dc.ColumnName

            Next

            For Each dr In DT.Rows

                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In DT.Columns

                    colIndex = colIndex + 1
                    oSheet.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName).ToString()

                Next

            Next

            ''Export the Columns to excel file
            'For Each dc In dgvExcelExport.Columns
            '    colIndex = colIndex + 1
            '    oSheet.Cells(1, colIndex) = dc.Name
            '    ' oSheet.Cells(1, colIndex).Style = "NewStyle"
            'Next

            ''Export the rows to excel file
            'For Each dr In dgvExcelExport.Rows
            '    rowIndex = rowIndex + 1
            '    colIndex = 0
            '    For Each dc In dgvExcelExport.Columns
            '        colIndex = colIndex + 1
            '        oSheet.Cells(rowIndex + 1, colIndex) = dr.Cells(dc.Name).Value
            '    Next
            'Next

            'TESTING




            'Set final path
            Dim FileExtension As String = ".XLS"

            filename = filename + FileExtension
            '"PRICE UPDATER - " + (System.DateTime.Today).ToString("ddMMMyyyy") + FileExtension

            Dim finalPath = selectedPath + "\" + filename
            oSheet.Columns.AutoFit()
            'Save file in final path
            oBook.SaveAs(finalPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, _
            Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, _
            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing)

            'Release the objects
            ReleaseObject(oSheet)
            oBook.Close(False, Type.Missing, Type.Missing)
            ReleaseObject(oBook)
            oExcel.Quit()
            ReleaseObject(oExcel)
            'Some time Office application does not quit after automation: 
            'so i am calling GC.Collect method.
            GC.Collect()

            MessageBox.Show("Export done successfully!")

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try
    End Sub

    Public Sub ExportRadioContractFeeBreakdownData(ByVal runMonth As String, ByVal filename As String, ByVal selectedPath As String) 'This Method converts Datagridview Tables into Cells and columns of an Excel Sheet

        'Declare and instantiate variables
        'Dim SelectedPath As String
        'Dim fileName As String
        'Dim f As FolderBrowserDialog = New FolderBrowserDialog

        'set properties
        'f.Description = "Please select file Destination"
        'f.RootFolder = Environment.SpecialFolder.MyDocuments

        Try
            'If f.ShowDialog() = DialogResult.OK Then

            '    fileName = InputBox("Please Enter a file name", "File Name", "PRICE UPDATER - " & (System.DateTime.Today).ToString("ddMMMyyyy") & _
            '                         "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm"))

            '    SelectedPath = f.SelectedPath.ToString
            'This section help you if your language is not English.
            System.Threading.Thread.CurrentThread.CurrentCulture = _
            System.Globalization.CultureInfo.CreateSpecificCulture("en-UK")
            Dim oExcel As Microsoft.Office.Interop.Excel.Application
            Dim oBook As Microsoft.Office.Interop.Excel.Workbook
            Dim oSheet As Microsoft.Office.Interop.Excel.Worksheet
            oExcel = CreateObject("Excel.Application")
            oBook = oExcel.Workbooks.Add(Type.Missing)
            oSheet = oBook.Worksheets(1)

            'Dim dc As DataGridViewColumn  '***********TEST UNCOMMENT WHEN DONE***********SHWETA We'll see
            'Dim dr As DataGridViewRow '***********TEST UNCOMMENT WHEN DONE***********SHWETA We'll see
            Dim dc As New DataColumn
            Dim dr As DataRow


            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0
            ' Dim runMonth As String = ""
            Dim dgvExcelExport As New DataTable

            'Style Columns
            'Dim style As Microsoft.Office.Interop.Excel.Style = oSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
            'style.Font.Bold = True
            'style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightBlue)


            'TESTING
            'dgvExcelExport = MonthlyInvoicesReport(runMonth)
            Dim DT As New DataTable

            DT = RadioContractFeeBreakdown(runMonth)

            For Each dc In DT.Columns

                colIndex = colIndex + 1
                oSheet.Cells(1, colIndex) = dc.ColumnName

            Next

            For Each dr In DT.Rows

                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In DT.Columns

                    colIndex = colIndex + 1
                    oSheet.Cells(rowIndex + 1, colIndex) = dr(dc.ColumnName).ToString()

                Next

            Next

            ''Export the Columns to excel file
            'For Each dc In dgvExcelExport.Columns
            '    colIndex = colIndex + 1
            '    oSheet.Cells(1, colIndex) = dc.Name
            '    ' oSheet.Cells(1, colIndex).Style = "NewStyle"
            'Next

            ''Export the rows to excel file
            'For Each dr In dgvExcelExport.Rows
            '    rowIndex = rowIndex + 1
            '    colIndex = 0
            '    For Each dc In dgvExcelExport.Columns
            '        colIndex = colIndex + 1
            '        oSheet.Cells(rowIndex + 1, colIndex) = dr.Cells(dc.Name).Value
            '    Next
            'Next

            'TESTING




            'Set final path
            Dim FileExtension As String = ".XLS"

            filename = filename + FileExtension
            '"PRICE UPDATER - " + (System.DateTime.Today).ToString("ddMMMyyyy") + FileExtension

            Dim finalPath = selectedPath + "\" + filename
            oSheet.Columns.AutoFit()
            'Save file in final path
            oBook.SaveAs(finalPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, _
            Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, _
            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing)

            'Release the objects
            ReleaseObject(oSheet)
            oBook.Close(False, Type.Missing, Type.Missing)
            ReleaseObject(oBook)
            oExcel.Quit()
            ReleaseObject(oExcel)
            'Some time Office application does not quit after automation: 
            'so i am calling GC.Collect method.
            GC.Collect()

            MessageBox.Show("Export done successfully!")

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try
    End Sub

    Private Sub ReleaseObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try

    End Sub

End Class
