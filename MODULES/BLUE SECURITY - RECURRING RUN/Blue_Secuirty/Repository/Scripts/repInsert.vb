﻿Public Class repInsert

    Dim Conn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Sub nextRunMonthDetails(ByVal curRunMonth As String)

        'Declare Varaibles
        Dim invDescriptionSuffix As String = setInvDescriptionSuffix(curRunMonth)
        Dim radioDescription As String = curRunMonth
        Dim radioMonth As String = setRadioMonth(curRunMonth)
        Dim invLastDate As String = setInvLastDate(curRunMonth)
        Dim invFirstDate As String = (Date.Parse(invLastDate).AddDays(1)).ToString()
        Dim qry As String

        If isSecondRun(curRunMonth) Then


        Else

            qry = "INSERT INTO [" & My.Settings.DBName & "].[dbo].runMonth" & vbCrLf & _
                           "(invLastDate, invFirstDate,invDescriptionSuffix, radioDescriptionSuffix, radioMonth, isCompleted)" & vbCrLf & _
                           "VALUES" & vbCrLf & _
                           "('" & invLastDate & "', '" & invFirstDate & "', '" & invDescriptionSuffix & "', '" & radioDescription & "', '" & radioMonth & "', 0)"

            Conn.setSqlString(qry)
            Conn.createNewConnection()
            Conn.CloseConnection()

        End If

    End Sub

    Private Function setInvDescriptionSuffix(ByVal curRunMonth As String) As String

        'Declare Variables
        Dim intMonth As Integer
        Dim invDate As Date
        Dim invDescription As String
        Dim index As Integer
        Dim year As Integer

        index = curRunMonth.IndexOf(" ")
        year = Convert.ToInt32(curRunMonth.Substring(index).Trim())

        'Get Month Number from Month Name
        intMonth = getMonthNum(curRunMonth)

        If intMonth = 12 Then
            intMonth = 0
            year = year + 1
        End If

        invDate = New Date(2000, intMonth + 1, 1)


        invDescription = invDate.ToString("MMMM") & " " & year

        Return invDescription

    End Function

    Private Function getMonthNum(ByVal curRunMonth As String) As Integer

        'Declare Variables
        Dim qry As String
        Dim intMonth As Integer

        qry = "SELECT" & vbCrLf & _
                "DATEPART(MM, '" & curRunMonth & "') AS 'CurrentRunMonthNum'"

        Conn.setSqlString(qry)
        Conn.createNewConnection()

        intMonth = Convert.ToInt32(Conn.get_value("CurrentRunMonthNum"))

        Return intMonth

    End Function

    Private Function setRadioMonth(ByVal curRunMonth As String) As String

        'Declare Variables
        Dim index As Integer
        Dim radioMonth As String

        index = curRunMonth.IndexOf(" ")
        radioMonth = (curRunMonth.Substring(0, index).Trim()).ToUpper()

        Return radioMonth

    End Function

    Public Function setInvLastDate(ByVal curRunMonth As String) As String

        'Declare Variables
        Dim firstdate As Date
        Dim lastDate As Date
        Dim index As Integer
        Dim year As Integer
        Dim intMonth As Integer

        index = curRunMonth.IndexOf(" ")
        year = Convert.ToInt32(curRunMonth.Substring(index).Trim())
        intMonth = getMonthNum(curRunMonth)

        firstdate = New Date(year, intMonth, 1)
        lastDate = firstdate.AddMonths(1).AddDays(-1)

        Return lastDate.ToString()

    End Function

    Private Function isSecondRun(ByVal curRunMonth As String) As Boolean

        'Declare Variables
        Dim qry As String
        Dim isComplete As String
        qry = "SELECT" & vbCrLf & _
            "R.isCompleted" & vbCrLf & _
            "FROM" & vbCrLf & _
            "runMonth R" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "R.invDescriptionSuffix = '" & curRunMonth & "'"

        Conn.setSqlString(qry)
        Conn.createNewConnection()

        isComplete = Conn.get_value("isCompleted")

        Return isComplete

    End Function

End Class
