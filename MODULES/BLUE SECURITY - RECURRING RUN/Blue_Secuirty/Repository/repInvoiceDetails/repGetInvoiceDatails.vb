﻿Public Class repGetInvoiceDatails

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function getRunMonths() As DataTable

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable

        qry = "SELECT" & vbCrLf & _
                "R.invDescriptionSuffix AS 'RUN MONTH'" & vbCrLf & _
                "FROM" & vbCrLf & _
                "runMonth R" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "R.isCompleted = 0" & vbCrLf & _
                "ORDER BY R.monthID"

        dt = dbConn.get_datatable(qry)

        Return dt

    End Function

    Public Function getRadioRun(ByVal runMonth As String) As String

        'Declare Variables
        Dim qry As String
        Dim radioRun As String
        qry = "SELECT" & vbCrLf & _
            "R.radioMonth" & vbCrLf & _
            "FROM" & vbCrLf & _
            "runMonth R" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "R.invDescriptionSuffix = '" & runMonth & "'" & vbCrLf & _
            "ORDER BY R.monthID"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        radioRun = dbConn.get_value("radioMonth")

        Return radioRun

    End Function

    Public Function getRadioDescription(ByVal runMonth As String) As String

        'Declare Variables
        Dim qry As String
        Dim radioDescription As String
        qry = "SELECT" & vbCrLf & _
            "R.radioDescriptionSuffix" & vbCrLf & _
            "FROM" & vbCrLf & _
            "runMonth R" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "R.invDescriptionSuffix = '" & runMonth & "'" & vbCrLf & _
            "ORDER BY R.monthID"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        radioDescription = dbConn.get_value("radioDescriptionSuffix")

        Return radioDescription

    End Function

    Public Function getInvLastDate(ByVal runMonth As String) As String

        'Declare Variables
        Dim qry As String
        Dim invLastDate As String
        qry = "SELECT" & vbCrLf & _
                "R.invLastDate" & vbCrLf & _
                "FROM" & vbCrLf & _
                "runMonth R" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "R.invDescriptionSuffix = '" & runMonth & "'" & vbCrLf & _
                "ORDER BY R.monthID"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        invLastDate = dbConn.get_value("invLastDate")

        Return invLastDate

    End Function

    Public Function getInvFirstDate(ByVal runMonth As String) As String

        'Declare Variables
        Dim qry As String
        Dim invFirstDate As String
        qry = "SELECT" & vbCrLf & _
                "R.invFirstDate" & vbCrLf & _
                "FROM" & vbCrLf & _
                "runMonth R" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "R.invDescriptionSuffix = '" & runMonth & "'" & vbCrLf & _
                "ORDER BY R.monthID"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        invFirstDate = dbConn.get_value("invFirstDate")

        Return invFirstDate

    End Function

End Class
