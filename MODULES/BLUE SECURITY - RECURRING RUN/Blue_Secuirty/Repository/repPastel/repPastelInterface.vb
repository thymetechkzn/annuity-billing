﻿Imports Pastel.Evolution

Public Class repPastelInterface

    'CREATE CONNECTION TO PASTEL
    Private Function CreateConnection() As Boolean

        Dim Success As Boolean = False

        Try

            DatabaseContext.CreateCommonDBConnection(My.Settings.DBInstance, My.Settings.EvolutionCommon, My.Settings.DBUser, My.Settings.DBPassword, False)
            DatabaseContext.SetLicense("DE09110073", "4730608")
            DatabaseContext.CreateConnection(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword, False)

            Success = True

        Catch ex As Exception
            MsgBox("Connection Failed. Please check connection settings.")
            Success = False
        End Try

        Return Success

    End Function
    'MONTH
    Public Sub ProcessSalesOrders(ByVal Customers As List(Of customer), ByVal runMonth As String, ByVal radioRun As String _
                                  , ByVal invLastDate As String, ByVal invFirstDate As String, ByVal nextInvFirstDate As String, ByVal filePath As String)

        '-----do standard invoice here(for recurring)-----
        'open pastel sdk connection
        Dim logger As New LogFile(filePath, "Invoice Report " & (System.DateTime.Today).ToString("dd-MM-yyyy") & " RECRUN_" & runMonth.ToUpper() & "_INVOICE")
        Dim updateDate As New repUpdates
        Dim strLog As String = ""
        Dim masterInvoiceDate As String
        Dim previousInvoiceDate As String

        logger.CreateLog("Attempt to open connection")

        If CreateConnection() Then

            logger.CreateLog("Connection successful")

            logger.CreateLog("RECRUN_" & runMonth.ToUpper() & "_INVOICE")

            logger.CreateLog("Initiate invoice generation")

            For Each customer As customer In Customers

                Try

                    Dim SOLast As New SalesOrder()
                    Dim SOFirst As New SalesOrder()
                    Dim AD As New Address

                    'Gets the Master Account's InvoiceDate
                    masterInvoiceDate = customer.ContractInvoiceDay

                    'For Last Day of the Month Accounts - Set Pastel Invoice Header Details
                    SOLast.InvoiceDate = invLastDate
                    SOLast.MessageLine2 = "RECRUN_" & runMonth.ToUpper() & "_INVOICE"
                    SOLast.Customer = New Pastel.Evolution.Customer(customer.CustomerID)
                    'How will I sort the grouped
                    SOLast.Description = customer.CustomerCode & " " & runMonth

                    ''NEW
                    If customer.CustomerCode = "BEC000143" Then

                        SOFirst.ExternalOrderNo = "4919295706/707"
                        SOLast.ExternalOrderNo = "4919295706/707"

                    End If

                    'Input Physical Address into Invoice Line - Last Day of Month Invoice Date
                    AD.Line1 = customer.Physical1
                    AD.Line2 = customer.Physical2
                    AD.Line3 = customer.Physical3
                    AD.Line4 = customer.Physical4
                    AD.Line5 = customer.Physical5
                    AD.Line6 = customer.PhysicalPC
                    AD.PostalCode = customer.PhysicalPC
                    SOLast.DeliverTo = AD.Condense

                    'Input Postal Address into Invoice Line - Last Day of Month Invoice Date
                    AD.Line1 = customer.Post1
                    AD.Line2 = customer.Post2
                    AD.Line3 = customer.Post3
                    AD.Line4 = customer.Post4
                    AD.Line5 = customer.Post5
                    AD.Line6 = customer.PostPC
                    AD.PostalCode = customer.PostPC
                    SOLast.InvoiceTo = AD.Condense

                    'For First Day o the Month Accounts - Set Pastel Invoice Header Details
                    SOFirst.InvoiceDate = invFirstDate
                    SOFirst.MessageLine2 = "RECRUN_" & runMonth.ToUpper() & "_INVOICE"
                    SOFirst.Customer = New Pastel.Evolution.Customer(customer.CustomerID)
                    SOFirst.Description = customer.CustomerCode & " " & runMonth

                    'Input Physical Address into Invoice Line - First Day of Month Invoice Date
                    AD.Line1 = customer.Physical1
                    AD.Line2 = customer.Physical2
                    AD.Line3 = customer.Physical3
                    AD.Line4 = customer.Physical4
                    AD.Line5 = customer.Physical5
                    AD.Line6 = customer.PhysicalPC
                    AD.PostalCode = customer.PhysicalPC
                    SOFirst.DeliverTo = AD.Condense

                    'Input Postal Address into Invoice Line - Last Day of Month Invoice Date
                    AD.Line1 = customer.Post1
                    AD.Line2 = customer.Post2
                    AD.Line3 = customer.Post3
                    AD.Line4 = customer.Post4
                    AD.Line5 = customer.Post5
                    AD.Line6 = customer.PostPC
                    AD.PostalCode = customer.PostPC
                    SOFirst.InvoiceTo = AD.Condense

                    'set pastel invoice line details per account 
                    ''***********ACOUNTS HOLD CONTRACTS INFORMATION

                    Dim lstODLast As New List(Of OrderDetail)
                    Dim lstODFirst As New List(Of OrderDetail)

                    For Each account As Accounts In customer.Accounts

                        previousInvoiceDate = account.Contract.InvoiceDate

                        If account.Contract.ContractEndDate < invFirstDate Then ' Trish  NEW END

                            If account.ContractActive = True Then

                                If account.Contract.PaymentFrequency = 1 And _
                                    (account.Contract.FirstBillDate <= invFirstDate Or account.Contract.NextBillDate <= invFirstDate) And _
                                    account.ContractActive = True And
                                    account.ContractSuspended = 0 Then

                                    'If InvoiceDate is Last Day of the Month
                                    If account.Contract.InvoiceDate.Equals("Last Day of Month") Then

                                        lstODLast.AddRange(GetAdhocSOLines(account, runMonth, radioRun, invLastDate, invFirstDate))
                                        lstODLast.AddRange(GetServiceContractSOLines(account, runMonth, radioRun, invLastDate, invFirstDate))

                                        'If InvoiceDate is First Day of the Next Month
                                    ElseIf account.Contract.InvoiceDate.Equals("1st Day of Next Month") Then

                                        lstODFirst.AddRange(GetAdhocSOLines(account, runMonth, radioRun, invLastDate, invFirstDate))
                                        lstODFirst.AddRange(GetServiceContractSOLines(account, runMonth, radioRun, invLastDate, invFirstDate))

                                    Else

                                        'Master Account isn't Blank/ NULL
                                        If masterInvoiceDate.Equals("Last Day of Month") Then

                                            lstODLast.AddRange(GetAdhocSOLines(account, runMonth, radioRun, invLastDate, invFirstDate))
                                            lstODLast.AddRange(GetServiceContractSOLines(account, runMonth, radioRun, invLastDate, invFirstDate))

                                        ElseIf masterInvoiceDate.Equals("1st Day of Next Month") Then

                                            lstODFirst.AddRange(GetAdhocSOLines(account, runMonth, radioRun, invLastDate, invFirstDate))
                                            lstODFirst.AddRange(GetServiceContractSOLines(account, runMonth, radioRun, invLastDate, invFirstDate))

                                        Else

                                            strLog &= "Account did not get invoiced : " & account.CustomerCode & ": No invoice date specified" & vbCrLf

                                        End If
                                    End If

                                    updateDate.updateNextBillDate(account.CustomerCode, nextInvFirstDate)

                                End If
                            End If
                        End If
                    Next

                    'Last Day of the Month Invoice Date
                    For Each OD As OrderDetail In lstODLast
                        SOLast.Detail.Add(OD)
                    Next
                    'Last Day of the Month Invoice Date
                    If SOLast.Detail.Count > 0 Then
                        strLog &= "Invoice Created for customer:" & customer.CustomerCode & ": Dated last" & vbCrLf
                        SOLast.Process()
                        updateDate.updateNextBillDate(customer.CustomerCode, nextInvFirstDate)
                    End If

                    'First Day of the Month Invoice Date
                    For Each OD As OrderDetail In lstODFirst
                        SOFirst.Detail.Add(OD)

                    Next
                    'First Day of the Month Invoice Date
                    If SOFirst.Detail.Count > 0 Then
                        SOFirst.Process()
                        strLog &= "Invoice Created for customer:" & customer.CustomerCode & ": Dated first" & vbCrLf
                        updateDate.updateNextBillDate(customer.CustomerCode, nextInvFirstDate)
                    End If

                    'Dispose Objects
                    SOLast = Nothing
                    SOFirst = Nothing

                Catch ex As Exception
                    strLog &= "Account did not get invoiced : " & customer.CustomerCode & ": " & ex.Message & vbCrLf
                End Try

            Next

            logger.CreateLog(strLog)
        End If

        logger.CreateLog("Invoice generation complete")

    End Sub
    'RADIO
    Public Sub ProcessRadioSalesOrders(ByVal Customers As List(Of customer), ByVal runMonth As String, ByVal radioRun As String, _
                                       ByVal radioDescription As String, ByVal invLastDate As String, ByVal invFirstDate As String, _
                                       ByVal filePath As String)

        'Radio Licence Fee Invoice Date are always at the last day of the month

        'open pastel sdk connection
        CreateConnection()
        Dim amount As Integer = 0
        Dim logger As New LogFile(filePath, "RadioLicenceInvoice " & (System.DateTime.Today).ToString("dd-MM-yyyy") & " RECRUN_" & runMonth.ToUpper() & "_RADIO")
        Dim strLog As String = ""

        logger.CreateLog("RECRUN_" & runMonth.ToUpper() & "_RADIO")

        'NEW****
        invFirstDate = Convert.ToDateTime(invFirstDate).AddMonths(-1)

        For Each customer As customer In Customers

            'Declare pastel sales order
            Dim SO As New SalesOrder()
            Dim AD As New Address

            'set pastel invoice header details
            SO.InvoiceDate = invFirstDate
            SO.Customer = New Pastel.Evolution.Customer(customer.CustomerID)
            SO.Description = customer.CustomerCode & " " & radioDescription
            SO.MessageLine2 = "RECRUN_" & runMonth.ToUpper() & "_RADIO"
            AD.Line1 = customer.Physical1
            AD.Line2 = customer.Physical2
            AD.Line3 = customer.Physical3
            AD.Line4 = customer.Physical4
            AD.Line5 = customer.Physical5
            AD.Line6 = customer.PhysicalPC
            AD.PostalCode = customer.PhysicalPC
            SO.DeliverTo = AD.Condense

            AD.Line1 = customer.Post1
            AD.Line2 = customer.Post2
            AD.Line3 = customer.Post3
            AD.Line4 = customer.Post4
            AD.Line5 = customer.Post5
            AD.Line6 = customer.PostPC
            AD.PostalCode = customer.PostPC
            SO.InvoiceTo = AD.Condense

            '***
            Try
                For Each od As OrderDetail In GetRadioSOLines(customer.Accounts, runMonth, radioRun, radioDescription, invLastDate, invFirstDate)

                    SO.Detail.Add(od)

                Next

                If SO.Detail.Count > 0 Then

                    SO.Process()
                    SO = Nothing

                    strLog &= ("RADIO LICENSE GENERATED FOR: " & customer.CustomerCode) & vbCrLf
                End If
            Catch ex As Exception
                strLog &= "Account did not get invoiced : " & customer.CustomerCode & ": " & ex.Message & vbCrLf
            End Try
        Next

        logger.CreateLog(strLog)

    End Sub

    '-------------------------------SALES ORDER LINES-------------------------------
    Private Function GetServiceContractSOLines(ByVal account As Accounts, ByVal runMonth As String, ByVal radioRun As String _
                                  , ByVal invLastDate As String, ByVal invFirstDate As String) As List(Of OrderDetail)

        'Declare Variables
        Dim lstOD As New List(Of OrderDetail)

        'Angel Fees - 1
        If account.Contract.ServiceContract.AngelFee > 0 Then

            Dim OD As New OrderDetail
            OD.InventoryItem = New InventoryItem(6)
            OD.Quantity = 1
            OD.ToProcess = 1

            'Set Description for ThirdParty / Group Invoice
            If account.Contract.isGroupInvoice = True Or account.Contract.isThridParty = True Or account.Contract.isSpecialProjects = True Then

                OD.Description = account.Contract.Description & " " & runMonth

            Else

                OD.Description = account.Contract.ServiceContract.AngelDescription & " " & runMonth

            End If

            OD.UnitSellingPrice = account.Contract.ServiceContract.AngelFee

            OD.TaxType = New TaxRate(6)
            'ADD INVOICE LINE TO LIST OF ORDER DETAILS
            lstOD.Add(OD)
            'DISPOSE OF OBJECT!!!
            OD = Nothing

        End If
        '******************************************************************************
        'Report Fees - 2
        If account.Contract.ServiceContract.ReportFee > 0 Then

            Dim OD As New OrderDetail
            OD.InventoryItem = New InventoryItem(4)
            OD.Quantity = 1
            OD.ToProcess = 1

            'Set Description for ThirdParty / Group Invoice
            If account.Contract.isGroupInvoice = True Or account.Contract.isThridParty = True Or account.Contract.isSpecialProjects = True Then

                OD.Description = account.Contract.Description & " " & runMonth

            Else

                OD.Description = account.Contract.ServiceContract.ReportDescription & " " & runMonth

            End If

            OD.UnitSellingPrice = (account.Contract.ServiceContract.ReportFee / 1.14)
            OD.TaxType = New TaxRate(1)
            'ADD INVOICE LINE TO LIST OF ORDER DETAILS
            lstOD.Add(OD)
            'DISPOSE OF OBJECT!!!
            OD = Nothing

        End If
        '******************************************************************************
        'Service Fees - 3
        If account.Contract.ServiceContract.ServiceFee > 0 Then

            Dim OD As New OrderDetail
            OD.InventoryItem = New InventoryItem(2)
            OD.Quantity = 1
            OD.ToProcess = 1

            'Set Description for ThirdParty / Group Invoice
            If account.Contract.isGroupInvoice = True Or account.Contract.isThridParty = True Or account.Contract.isSpecialProjects = True Then

                OD.Description = account.Contract.Description & " " & runMonth

            Else

                OD.Description = account.Contract.ServiceContract.ServiceDescription & " " & runMonth

            End If

            OD.UnitSellingPrice = (account.Contract.ServiceContract.ServiceFee / 1.14)
            OD.TaxType = New TaxRate(1)
            'ADD INVOICE LINE TO LIST OF ORDER DETAILS
            lstOD.Add(OD)
            'DISPOSE OF OBJECT!!!
            OD = Nothing

        End If
        '******************************************************************************
        'SMS Fees - 4
        If account.Contract.ServiceContract.SMSFee > 0 Then

            Dim OD As New OrderDetail
            OD.InventoryItem = New InventoryItem(3)
            OD.Quantity = 1
            OD.ToProcess = 1

            'Set Description for ThirdParty / Group Invoice
            If account.Contract.isGroupInvoice = True Or account.Contract.isThridParty = True Or account.Contract.isSpecialProjects = True Then

                OD.Description = account.Contract.Description & " " & runMonth

            Else

                OD.Description = account.Contract.ServiceContract.SMSDescription & " " & runMonth

            End If

            OD.UnitSellingPrice = (account.Contract.ServiceContract.SMSFee / 1.14)
            OD.TaxType = New TaxRate(1)
            'ADD INVOICE LINE TO LIST OF ORDER DETAILS
            lstOD.Add(OD)
            'DISPOSE OF OBJECT!!!
            OD = Nothing

        End If
        '******************************************************************************
        'Surveilance Fees - 5
        If account.Contract.ServiceContract.SurveilanceFee > 0 Then

            Dim OD As New OrderDetail
            OD.InventoryItem = New InventoryItem(5)
            OD.Quantity = 1
            OD.ToProcess = 1

            'Set Description for ThirdParty / Group Invoice
            If account.Contract.isGroupInvoice = True Or account.Contract.isThridParty = True Or account.Contract.isSpecialProjects = True Then

                OD.Description = account.Contract.Description & " " & runMonth

            Else

                OD.Description = account.Contract.ServiceContract.SurveilanceDescription & " " & runMonth

            End If
            ' OD.Description = account.Contract.ServiceContract.SurveilanceDescription & " May 2014"

            OD.UnitSellingPrice = (account.Contract.ServiceContract.SurveilanceFee / 1.14)
            OD.TaxType = New TaxRate(1)
            'ADD INVOICE LINE TO LIST OF ORDER DETAILS
            lstOD.Add(OD)
            'DISPOSE OF OBJECT!!!
            OD = Nothing

        End If
        '******************************************************************************
        'Guarding Fees - 6
        If account.Contract.ServiceContract.GuardingFees > 0 Then

            Dim OD As New OrderDetail
            OD.InventoryItem = New InventoryItem(8)
            OD.Quantity = 1
            OD.ToProcess = 1

            'Set Description for ThirdParty / Group Invoice
            If account.Contract.isGroupInvoice = True Or account.Contract.isThridParty = True Or account.Contract.isSpecialProjects = True Then

                OD.Description = account.Contract.Description & " " & runMonth

            Else

                OD.Description = account.Contract.ServiceContract.GuardingDescription & " " & runMonth

            End If

            OD.UnitSellingPrice = (account.Contract.ServiceContract.GuardingFees / 1.14)
            OD.TaxType = New TaxRate(1)
            'ADD INVOICE LINE TO LIST OF ORDER DETAILS
            lstOD.Add(OD)
            'DISPOSE OF OBJECT!!!
            OD = Nothing
        End If
        '******************************************************************************
        'Blue Panic App Fees - 5
        If account.Contract.ServiceContract.BluePanicAppFees > 0 Then

            Dim OD As New OrderDetail
            OD.InventoryItem = New InventoryItem(3657)
            OD.Quantity = 1
            OD.ToProcess = 1

            'Set Description for ThirdParty / Group Invoice
            If account.Contract.isGroupInvoice = True Or account.Contract.isThridParty = True Or account.Contract.isSpecialProjects = True Then

                OD.Description = account.Contract.Description & " " & runMonth

            Else

                OD.Description = account.Contract.ServiceContract.BluePanicAppDescription & " " & runMonth

            End If
            ' OD.Description = account.Contract.ServiceContract.SurveilanceDescription & " May 2014"

            OD.UnitSellingPrice = (account.Contract.ServiceContract.BluePanicAppFees / 1.14)
            OD.TaxType = New TaxRate(1)
            'ADD INVOICE LINE TO LIST OF ORDER DETAILS
            lstOD.Add(OD)
            'DISPOSE OF OBJECT!!!
            OD = Nothing

        End If
        '******************************************************************************
        Return lstOD

    End Function

    Private Function GetAdhocSOLines(ByVal account As Accounts, ByVal runMonth As String, ByVal radioRun As String _
                                  , ByVal invLastDate As String, ByVal invFirstDate As String) As List(Of OrderDetail)

        'Declare Variables
        Dim lstOD As New List(Of OrderDetail)

        'ADD ADHOC LINES PER ACCOUNT
        For Each ADHOC As AdHocContract In account.Contract.AdhocContract

            Dim OD As New OrderDetail

            OD = New OrderDetail

            OD.InventoryItem = New InventoryItem(ADHOC.ItemCode)
            OD.Quantity = 1
            OD.ToProcess = 1

            'Set Description for ThirdParty / Group Invoice
            If account.Contract.isGroupInvoice = True Or account.Contract.isThridParty = True Or account.Contract.isSpecialProjects = True Then

                OD.Description = account.Contract.Description & " " & runMonth

            Else

                OD.Description = ADHOC.ItemDescription & " " & runMonth

            End If

            'OD.Description = ADHOC.ItemDescription '****& "For May 2014"
            OD.UnitSellingPrice = (ADHOC.ItemPrice / 1.14)
            OD.TaxType = New TaxRate(1)

            lstOD.Add(OD)
            'always dispose
            OD = Nothing

        Next

        Return lstOD

    End Function

    Private Function GetRadioSOLines(ByVal lstAccounts As List(Of Accounts), ByVal runMonth As String, ByVal radioRun As String, ByVal radioDescription As String _
                                  , ByVal invLastDate As String, ByVal invFirstDate As String) As List(Of OrderDetail)

        Dim lstOrderDetails As New List(Of OrderDetail)

        For Each account As Accounts In lstAccounts

            If account.Contract.ContractEndDate < invFirstDate Then ' Trish  NEW END

                'account.Contract.PaymentFrequency = 1 And _

                If (account.Contract.FirstBillDate <= invFirstDate Or account.Contract.NextBillDate <= invFirstDate) And _
                                    account.ContractActive = True And
                                    account.ContractSuspended = 0 Then

                    'CHECK IF ITS THE MONTH TO BE INVOICED FOR RADIO
                    If account.Contract.RadioContract.RadioFeeMonth.ToUpper.Equals(radioRun) And account.Contract.RadioContract.RadioFee > 0 Then

                        'ADD LINE FOR RADIO
                        Dim OD As New OrderDetail()
                        OD.InventoryItem = New InventoryItem(1)
                        OD.Quantity = 1
                        OD.ToProcess = 1

                        OD.Description = "Network Administration Fee"

                        '"Annual Admin and Radio Network Maintenance Fee"

                        '***************************************************HAS BEEN REMOVED DUE TO CHANGE IN LEGISLATION WITH ICASA
                        'account.Contract.RadioContract.RadioDescription & " March " & account.CustomerCode

                        'Set Description for ThirdParty / Group Invoice
                        'If account.Contract.isGroupInvoice = True Or account.Contract.isThridParty = True Or account.Contract.isSpecialProjects = True Then

                        '    OD.Description = account.Contract.Description

                        'Else

                        '    OD.Description = account.Contract.RadioContract.RadioDescription & " " & radioDescription

                        'End If
                        ' OD.Description = account.Contract.Description
                        '***************************************************

                        OD.UnitSellingPrice = account.Contract.RadioContract.RadioFee
                        OD.TaxType = New TaxRate(1)

                        lstOrderDetails.Add(OD)
                        OD = Nothing

                    End If

                End If

            End If

        Next

        Return lstOrderDetails

    End Function

End Class
