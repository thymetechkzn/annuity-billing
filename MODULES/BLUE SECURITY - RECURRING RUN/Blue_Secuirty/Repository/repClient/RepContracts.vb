﻿Public Class RepContracts

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function loadContracts(ByVal customers As List(Of customer), ByVal isRadioRun As Boolean) As List(Of customer)

        For Each customer As customer In customers

            For Each account As Accounts In customer.Accounts

                account.Contract = loadContract(account, isRadioRun)

            Next

            Debug.WriteLine(customer.CustomerID)

        Next

        Return customers

    End Function

    '------------------------LOAD CONTRACTS------------------------
    Private Function loadContract(ByVal account As Accounts, ByVal isRadioRun As Boolean) As Contract

        Dim contract As Contract

        'Load Header details into contract
        contract = loadContractHeader(account)
        'Load Line details into contract
        If isRadioRun = True Then

            contract.RadioContract = loadRadioContract(account)

        Else

            contract.ServiceContract = loadServiceContract(account)
            contract.AdhocContract = loadAdhocContract(account)

        End If


        Return contract

    End Function

    '------------------------LOAD HEADER DETAILS FOR INVOICE------------------------
    Private Function loadContractHeader(ByVal account As Accounts) As Contract

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable
        Dim contract As New Contract

        'QUERY: Get Header Details
        qry = "select" & vbCrLf & _
            "ISNULL(C.ucARContractDesc, '') as 'CustomerDescription'," & vbCrLf & _
            "ISNULL(C.ubARGroupInvoice, 0)as 'isGroupInvoice'," & vbCrLf & _
            "ISNULL(C.ubARThirdParty, 0)as 'isThirdParty'," & vbCrLf & _
             "ISNULL(C.ubARSpecialProject, 0)as 'isSpecialProjects'," & vbCrLf & _
            "C.ulARInvoiceDate as 'CustomerInvoiceDate'," & vbCrLf & _
            "ISNULL(C.ulARPaymentFreq, 0) as 'CustomerPaymentFrequency'," & vbCrLf & _
            "CONVERT(DATE, ISNULL(C.udARContractFirstBillDate, '')) as 'FirstBillDate'," & vbCrLf & _
            "CONVERT(DATE, ISNULL(C.udARContractNextBillDate, '')) as 'NextBillDate'," & vbCrLf & _
            "CONVERT(DATE, ISNULL(C.udARContractEndDate, '')) as 'ContractEndDate'" & vbCrLf & _
            "FROM " & vbCrLf & _
            "Client C" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "C.DCLink =  " & account.CustomerID & vbCrLf & _
            "ORDER BY C.DCLink"

        dt = dbConn.get_datatable(qry)

        For Each row As DataRow In dt.Rows

            'Check if Generic Invoice Description is Null

            contract.Description = row("CustomerDescription")
            contract.isGroupInvoice = row("isGroupInvoice")
            contract.isThridParty = row("isThirdParty")
            contract.isThridParty = row("isSpecialProjects")
            contract.InvoiceDate = row("CustomerInvoiceDate")
            If (row("CustomerPaymentFrequency")).Equals("") Then

                contract.PaymentFrequency = 0

            Else
                contract.PaymentFrequency = row("CustomerPaymentFrequency")

            End If
            contract.FirstBillDate = row("FirstBillDate")
            contract.NextBillDate = row("NextBillDate")
            contract.ContractEndDate = row("ContractEndDate")

        Next

        Return contract

    End Function
    '------------------------LOAD LINE DETAILS FOR INVOICE------------------------
    Private Function loadServiceContract(ByVal account As Accounts) As ServiceContract

        'Declare Variable
        Dim qry As String
        Dim dt As DataTable
        Dim serviceContract As New ServiceContract

        'QUERY: Get Service Contract Fees
        qry = "select" & vbCrLf & _
                "C.ufARMonthlyServFeeValue as 'CustomerServiceFee'," & vbCrLf & _
                "C.ufAROpenCloseSMSValue as 'CustomerSMSFee'," & vbCrLf & _
                "C.ufARMonthlyReportsValue as 'CustomerReportFee'," & vbCrLf & _
                "C.ufARRemoteSurvValue as 'CustomerSurveilanceFee'," & vbCrLf & _
                "C.ufARBlueAngelsTrustValue as 'CustomerAngelsFee'," & vbCrLf & _
                "C.ufARGuardingServices AS 'CustomerGuardingFees'," & vbCrLf & _
                "C.ufARBluePanicApp AS 'CustomerBluePanicAppFees'" & vbCrLf & _
                "FROM " & vbCrLf & _
                "Client C" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "C.DCLink =  " & account.CustomerID & vbCrLf & _
                "ORDER BY C.DCLink"

        dt = dbConn.get_datatable(qry)

        For Each row As DataRow In dt.Rows

            serviceContract.ServiceFee = row("CustomerServiceFee")
            serviceContract.SMSFee = row("CustomerSMSFee")
            serviceContract.ReportFee = row("CustomerReportFee")
            serviceContract.SurveilanceFee = row("CustomerSurveilanceFee")
            serviceContract.AngelFee = row("CustomerAngelsFee")
            'NEW
            serviceContract.GuardingFees = row("CustomerGuardingFees")
            'NEW 2016-10-12
            serviceContract.BluePanicAppFees = row("CustomerBluePanicAppFees")
        Next

        Return serviceContract

    End Function
    Private Function loadRadioContract(ByVal account As Accounts) As RadioContract

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable
        Dim radioContract As New RadioContract

        'QUERY: Get Radio Contract Fees
        qry = "select" & vbCrLf & _
                "ISNULL(ulARRadioLicFeeBillMonth, '') as 'CustomerRadioFeeMonth'," & vbCrLf & _
                "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID " & vbCrLf & _
                "=" & vbCrLf & _
                "(Select" & vbCrLf & _
                "IDPriceListName " & vbCrLf & _
                "from" & vbCrLf & _
                "_etblPriceListName " & vbCrLf & _
                "where" & vbCrLf & _
                "cName = ulARRadioLicFeePriceList)), 0) as 'CustomerRadioFee'," & vbCrLf & _
                "(select Description_1  from StkItem where StockLink = 1) as 'CustomerRadioDescription'" & vbCrLf & _
                "from" & vbCrLf & _
                "Client" & vbCrLf & _
                "where DCLink = " & account.CustomerID

        dt = dbConn.get_datatable(qry)

        For Each row As DataRow In dt.Rows

            radioContract.RadioFeeMonth = row("CustomerRadioFeeMonth")
            radioContract.RadioFee = row("CustomerRadioFee")
            radioContract.RadioDescription = row("CustomerRadioDescription")
        Next

        Return radioContract

    End Function
    Private Function loadAdhocContract(ByVal account As Accounts) As List(Of AdHocContract)

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable
        Dim lstAdhocContract As New List(Of AdHocContract)

        If account.CustomerCode = "ETS000007-1" Then
            Debug.WriteLine(account.CustomerCode)
        End If

        'QUERY: Get Adhoc Contract Fees
        qry = "SELECT" & vbCrLf & _
            "SI.Code as 'ItemCode'," & vbCrLf & _
            "SI.Description_1 as 'AdHocDescription'," & vbCrLf & _
            "SL.cProductReference as 'AdHocFee'" & vbCrLf & _
            "FROM" & vbCrLf & _
            "_rtblStockLinks SL" & vbCrLf & _
            "inner join" & vbCrLf & _
            "StkItem SI" & vbCrLf & _
            "ON" & vbCrLf & _
            "SL.iStockID = SI.StockLink" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "cModule = 'AR'" & vbCrLf & _
            "and" & vbCrLf & _
                "iDCLink = " & account.CustomerID

        Debug.WriteLine(qry)

        dt = dbConn.get_datatable(qry)

        'Load Adhoc Items
        For Each row As DataRow In dt.Rows

            Dim adhocItemDetails As New AdHocContract

            adhocItemDetails.ItemCode = row("ItemCode")
            adhocItemDetails.ItemDescription = row("AdHocDescription")

            If IsNumeric(row("AdHocFee")) Then
                adhocItemDetails.ItemPrice = row("AdHocFee")
            Else
                adhocItemDetails.ItemPrice = 0
            End If

            Debug.WriteLine(adhocItemDetails.ItemDescription) '****REMOVE WHEN DONE**** TESTING

            lstAdhocContract.Add(adhocItemDetails)

        Next

        Return lstAdhocContract

    End Function

End Class
