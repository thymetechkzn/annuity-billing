﻿Public Class RepClients

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function getPastelCustomers(ByVal extraQ As String) As List(Of customer)

        Dim lstCustomers As New List(Of customer)
        Dim qry As String
        Dim dt As DataTable
        Dim customer As New customer

        qry = "select" & vbCrLf & _
                "-- MASTER ACCOUNT INFO" & vbCrLf & _
                "C.DCLink as 'CustomerID'," & vbCrLf & _
                "C.physical1," & vbCrLf & _
                "C.physical2," & vbCrLf & _
                "C.physical3," & vbCrLf & _
                "C.physical4," & vbCrLf & _
                "C.physical5," & vbCrLf & _
                "C.physicalPC," & vbCrLf & _
                "C.post1," & vbCrLf & _
                "C.post2," & vbCrLf & _
                "C.post3," & vbCrLf & _
                "C.post4," & vbCrLf & _
                "C.post5," & vbCrLf & _
                "C.postPC," & vbCrLf & _
                "C.Account as 'CustomerCode'," & vbCrLf & _
                "C.Name as 'CustomerName'," & vbCrLf & _
                "C.ubARContractActive as 'CustomerActive'," & vbCrLf & _
                "C.ubARSuspendService as 'CustomerSuspended'," & vbCrLf & _
                "C.ulARInvoiceDate as 'DefaultInvoiceDate'," & vbCrLf & _
                "-- SUB ACCOUNT INFO" & vbCrLf & _
                "CL.DCLink as 'AccountID'," & vbCrLf & _
                "ISNULL(CL.Account, '') as 'AccountCode'," & vbCrLf & _
                "ISNULL(CL.Name, '') as 'AccountName'," & vbCrLf & _
                "ISNULL(CL.ubARContractActive, 0) as 'AccountActive'," & vbCrLf & _
                "ISNULL(CL.ubARSuspendService, 1) as 'AccountSuspended'" & vbCrLf & _
                "FROM " & vbCrLf & _
                "Client C" & vbCrLf & _
                "LEFT JOIN" & vbCrLf & _
                "CLIENT CL" & vbCrLf & _
                "ON C.DCLink = CL.MainAccLink " & vbCrLf & _
                "Where " & vbCrLf & _
                "(C.MainAccLink = 0 OR C.MainAccLink is null)" & vbCrLf & _
                "and" & vbCrLf & _
                "(C.ubARSuspendService = 0 " & vbCrLf & _
                "or" & vbCrLf & _
                "CL.ubARSuspendService = 0)" & vbCrLf & _
                "order by C.DCLink"

        dt = dbConn.get_datatable(qry)

        For Each row As DataRow In dt.Rows

            'If row("CustomerCode").Equals("REB000019") Then
            '    Debug.WriteLine("")
            'End If

            Try

                If customer.CustomerID = row("CustomerID") Then ' Is same customer just add account

                    Dim account As New Accounts

                    'Set Customer Properties
                    account.CustomerID = row("AccountID")
                    account.CustomerCode = row("AccountCode")
                    account.Name = row("AccountName")
                    account.ContractActive = row("AccountActive")
                    account.ContractSuspended = row("AccountSuspended")
                    account.Physical1 = row("Physical1")
                    account.Physical2 = row("Physical2")
                    account.Physical3 = row("Physical3")
                    account.Physical4 = row("Physical4")
                    account.Physical5 = row("Physical5")
                    account.PhysicalPC = row("PhysicalPC")
                    account.Post1 = row("Post1")
                    account.Post2 = row("Post2")
                    account.Post3 = row("Post3")
                    account.Post4 = row("Post4")
                    account.Post5 = row("Post5")
                    account.PostPC = row("PostPC")

                    customer.Accounts.Add(account)

                Else

                    'Add Account to Collection
                    lstCustomers.Add(customer)

                    'Dispose of old object and re-create it
                    customer = Nothing
                    customer = New customer

                    'Set Customer Properties
                    customer.CustomerID = row("CustomerID")
                    customer.CustomerCode = row("CustomerCode")
                    customer.Name = row("CustomerName")
                    customer.ContractActive = row("CustomerActive")
                    customer.ContractSuspended = row("CustomerSuspended")
                    customer.ContractInvoiceDay = row("DefaultInvoiceDate")
                    customer.Physical1 = row("Physical1")
                    customer.Physical2 = row("Physical2")
                    customer.Physical3 = row("Physical3")
                    customer.Physical4 = row("Physical4")
                    customer.Physical5 = row("Physical5")
                    customer.PhysicalPC = row("PhysicalPC")
                    customer.Post1 = row("Post1")
                    customer.Post2 = row("Post2")
                    customer.Post3 = row("Post3")
                    customer.Post4 = row("Post4")
                    customer.Post5 = row("Post5")
                    customer.PostPC = row("PostPC")
                    'Add Account/s

                    Dim account As New Accounts


                    'Set First Account Properties
                    account.CustomerID = row("CustomerID")
                    account.CustomerCode = row("CustomerCode")
                    account.Name = row("CustomerName")
                    account.ContractActive = row("CustomerActive")
                    account.ContractSuspended = row("CustomerSuspended")
                    account.Physical1 = row("Physical1")
                    account.Physical2 = row("Physical2")
                    account.Physical3 = row("Physical3")
                    account.Physical4 = row("Physical4")
                    account.Physical5 = row("Physical5")
                    account.PhysicalPC = row("PhysicalPC")
                    account.Post1 = row("Post1")
                    account.Post2 = row("Post2")
                    account.Post3 = row("Post3")
                    account.Post4 = row("Post4")
                    account.Post5 = row("Post5")
                    account.PostPC = row("PostPC")

                    customer.Accounts.Add(account)

                    'Set Second Account if applicable
                    If Not IsDBNull(row("AccountID")) Then

                        account = Nothing
                        account = New Accounts

                        account.CustomerID = row("AccountID")
                        account.CustomerCode = row("AccountCode")
                        account.Name = row("AccountName")
                        account.ContractActive = row("AccountActive")
                        account.ContractSuspended = row("AccountSuspended")
                        account.Physical1 = row("Physical1")
                        account.Physical2 = row("Physical2")
                        account.Physical3 = row("Physical3")
                        account.Physical4 = row("Physical4")
                        account.Physical5 = row("Physical5")
                        account.PhysicalPC = row("PhysicalPC")
                        account.Post1 = row("Post1")
                        account.Post2 = row("Post2")
                        account.Post3 = row("Post3")
                        account.Post4 = row("Post4")
                        account.Post5 = row("Post5")
                        account.PostPC = row("PostPC")

                        customer.Accounts.Add(account)

                    End If

                End If


            Catch ex As Exception

                MsgBox(ex.Message)

            End Try

        Next

        lstCustomers.RemoveAt(0)


        Return lstCustomers

    End Function

End Class
