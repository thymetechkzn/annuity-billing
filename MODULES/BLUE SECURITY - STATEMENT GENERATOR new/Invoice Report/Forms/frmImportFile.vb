﻿
Imports System.IO

Public Class frmImportFile

    Dim LstOrders As New List(Of Order)
    Dim EmailFolderPath As String
    Dim PostFolderPath As String
    Private RunDate As String
    Dim frmMain As frmStatementRepMain

    Sub New(ByVal dBInstance As String, ByVal dBName As String, _
           ByVal dBUser As String, ByVal dBPassword As String, ByVal evoCommon As String, ByVal frmMainMenu As frmStatementRepMain)

        ' This call is required by the designer.
        InitializeComponent()

        My.Settings.DBServer = dBInstance
        My.Settings.PASCompany = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.PASCommon = evoCommon
        Me.frmMain = frmMainMenu
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    

    Private Sub CloseProcessing() Handles BackgroundWorker1.RunWorkerCompleted
        frmProcessing.Close()
    End Sub

    Private Sub btnGenerate_Click(sender As System.Object, e As System.EventArgs) Handles btnGenerate.Click

        Dim Repository As New SQLInterface
        Dim LoadInfo As New LoadOrders
        Dim PeriodRunDate As String = ""

        Dim FolderBrowser As New FolderBrowserDialog

        If FolderBrowser.ShowDialog = Windows.Forms.DialogResult.OK Then

            frmStatementRepMain.WindowState = FormWindowState.Minimized

            EmailFolderPath = FolderBrowser.SelectedPath & "\Invoice " & MonthName(Today.Month, False) & "\Email Statement\"
            PostFolderPath = FolderBrowser.SelectedPath & "\Invoice " & MonthName(Today.Month, False) & "\Post Statement\"

            If Not Directory.Exists(EmailFolderPath) Then

                Directory.CreateDirectory(EmailFolderPath)

            End If

            If Not Directory.Exists(PostFolderPath) Then

                Directory.CreateDirectory(PostFolderPath)

            End If

            LstOrders = Repository.GetOrderNums()

            BackgroundWorker1.RunWorkerAsync()
            Timer1.Enabled = True
            Timer1.Start()

        End If

    End Sub
    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim LoadInfo As New LoadOrders

        LoadInfo.LoadOrderInfo(LstOrders, EmailFolderPath, PostFolderPath, Convert.ToDateTime(DateTimePicker1.Text).Date, Convert.ToDateTime(DateTimePicker2.Text).Date)

        LstOrders.Clear()
        LstOrders = Nothing

        If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf clearProgressbar))
            Me.Invoke(New MethodInvoker(AddressOf CloseProcessing))
            'MsgBox("Generation complete.")
            MessageBox.Show(MsgBox("Generation complete."))
        Else
            Me.Close()
            MsgBox("Generation complete.")
        End If

    End Sub
    Private Sub clearProgressbar() Handles BackgroundWorker1.RunWorkerCompleted

        pbStatementGenerator.Value = pbStatementGenerator.Maximum

    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick


        If pbStatementGenerator.Value >= pbStatementGenerator.Maximum Then
            pbStatementGenerator.Value = 0
        Else
            pbStatementGenerator.Value += 1
        End If

    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.frmMain.Close()
    End Sub

    Private Sub frmImportFile_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        MsgBox("Hello2")


    End Sub

    Private Sub frmImportFile_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing

        MsgBox("Hello")

    End Sub


    Private Sub frmImportFile_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub

    Private Sub EmailSettingsToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EmailSettingsToolStripMenuItem.Click

        Dim frmNewSettings As New frmEmailStatementSettings
        frmNewSettings.ShowDialog()

    End Sub
End Class