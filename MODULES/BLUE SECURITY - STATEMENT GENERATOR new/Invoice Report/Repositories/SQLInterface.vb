﻿Public Class SQLInterface

    Dim DBCon As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function GetOrderNums() As List(Of Order) '*

        Dim lstOrders As New List(Of Order)
        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim LastDate As String = ""
        Dim FirstDate As String = ""

        'SQLQuery = "select Distinct(Client.DCLink) from Client  " & vbCrLf & _
        '            "inner join  " & vbCrLf & _
        '            "PostAR  " & vbCrLf & _
        '            " on  " & vbCrLf & _
        '            "PostAR.AccountLink = Client.DCLink " & vbCrLf & _
        '            "WHERE" & vbCrLf & _
        '            "POSTAR.AccountLink IN" & vbCrLf & _
        '            "(" & vbCrLf & _
        '            "SELECT S.DCLINK FROM StatementFix S" & vbCrLf & _
        '            ")" & vbCrLf & _
        '            "order by Client.DCLink"

        SQLQuery = "select Distinct(Client.DCLink) from Client " & vbCrLf & _
                    "inner join " & vbCrLf & _
                    "PostAR " & vbCrLf & _
                    "on " & vbCrLf & _
                    "PostAR.AccountLink = Client.DCLink" & vbCrLf & _
                    "order by Client.DCLink"

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Dim OrderHeader As New Order

            OrderHeader.AccountCode = Row("DCLink")

            lstOrders.Add(OrderHeader)

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return lstOrders

    End Function

    Public Function GetCompanyDetails() As Company

        Dim BlueComp As New Company
        Dim SQLQuery As String = ""
        Dim DT As New DataTable

        SQLQuery = "select" & vbCrLf & _
                    "PhAddress1," & vbCrLf & _
                    "PhAddress2," & vbCrLf & _
                    "PhAddress3," & vbCrLf & _
                    "PHPostalCode," & vbCrLf & _
                    "PoAddress1," & vbCrLf & _
                    "POAddress2," & vbCrLf & _
                    "POAddress3," & vbCrLf & _
                    "POPostalCode," & vbCrLf & _
                    "Telephone1," & vbCrLf & _
                    "Fax," & vbCrLf & _
                    "BankName," & vbCrLf & _
                    "BankAccount," & vbCrLf & _
                    "BranchCode" & vbCrLf & _
                    "from entities"

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            BlueComp.Physical1 = Row("PhAddress1")
            BlueComp.Physical2 = Row("PhAddress2")
            BlueComp.PhysicalPostCode = Row("PHPostalCode")
            BlueComp.Post1 = Row("PoAddress1")
            BlueComp.Post2 = Row("POAddress2")
            BlueComp.PostPostCode = Row("POPostalCode")
            BlueComp.TelephoneNumber = Row("Telephone1")
            BlueComp.FaxNumber = Row("Fax")
            'BlueComp.VATno = Row("Tax_Number")'Not Needed
            'BlueComp.Registration = Row("Registration") 'Not Needed
            BlueComp.BankName = Row("BankName")
            BlueComp.BankAccount = Row("BankAccount")
            BlueComp.BranchCode = Row("BranchCode")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return BlueComp

    End Function

    Public Function getAccountCode(ByVal dcLink As Integer) As String

        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim accountCode As String = ""

        SQLQuery = "select " & vbCrLf & _
                    "C.Account" & vbCrLf & _
                    "FROM Client C " & vbCrLf & _
                    "WHERE C. dclink = " & dcLink

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            accountCode = Row("Account")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return accountCode

    End Function
    Public Function getAccName(ByVal dcLink As Integer) As String

        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim accName As String = ""

        SQLQuery = "select " & vbCrLf & _
                    "C.Name" & vbCrLf & _
                    "FROM Client C" & vbCrLf & _
                    "WHERE C.dclink = " & dcLink

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            accName = Row("Name")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return accName

    End Function

    Public Function getPost1(ByVal dcLink As Integer) As String

        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim Post1 As String = ""

        SQLQuery = "select " & vbCrLf & _
                    "C.Post1" & vbCrLf & _
                    "FROM Client C" & vbCrLf & _
                    "WHERE C.dclink = " & dcLink

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Post1 = Row("Post1")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return Post1

    End Function

    Public Function getPost2(ByVal dcLink As Integer) As String

        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim Post2 As String = ""

        SQLQuery = "select " & vbCrLf & _
                    "C.Post2" & vbCrLf & _
                    "FROM Client C" & vbCrLf & _
                    "WHERE C.dclink = " & dcLink

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Post2 = Row("Post2")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return Post2

    End Function

    Public Function getPost3(ByVal dcLink As Integer) As String

        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim Post3 As String = ""

        SQLQuery = "select " & vbCrLf & _
                    "C.Post3" & vbCrLf & _
                    "FROM Client C" & vbCrLf & _
                    "WHERE C.dclink = " & dcLink

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Post3 = Row("Post3")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return Post3

    End Function

    Public Function getPost4(ByVal dcLink As Integer) As String

        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim Post4 As String = ""

        SQLQuery = "select " & vbCrLf & _
                    "C.Post4" & vbCrLf & _
                    "FROM Client C" & vbCrLf & _
                    "WHERE C.dclink = " & dcLink

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Post4 = Row("Post4")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return Post4

    End Function
    Public Function getPost5(ByVal dcLink As Integer) As String

        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim Post5 As String = ""

        SQLQuery = "select " & vbCrLf & _
                    "C.Post5" & vbCrLf & _
                    "FROM Client C" & vbCrLf & _
                    "WHERE C.dclink = " & dcLink

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Post5 = Row("Post5")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return Post5

    End Function

    Public Function getPostPC(ByVal dcLink As Integer) As String

        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim PostPC As String = ""

        SQLQuery = "select " & vbCrLf & _
                    "C.PostPC" & vbCrLf & _
                    "FROM Client C" & vbCrLf & _
                    "WHERE C.dclink = " & dcLink

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            PostPC = Row("PostPC")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return PostPC

    End Function
End Class
