﻿Public Class Order

    Public Property AccountCode As String
    Public Property TransactionDate As String
    Public Property InvoiceType As String
    Public Property OurReference As String
    Public Property OrderNumber As String
    Public Property InvoiceNumber As String
    Public Property OrderID As Integer
    Public Property InvoicePost1 As String
    Public Property InvoicePost2 As String
    Public Property InvoicePost3 As String
    Public Property InvoicePost4 As String
    Public Property InvoicePost5 As String
    Public Property InvoicePost6 As String
    Public Property InvoicePhysical1 As String
    Public Property InvoicePhysical2 As String
    Public Property InvoicePhysical3 As String
    Public Property InvoicePhysical4 As String
    Public Property InvoicePhysical5 As String
    Public Property InvoicePhysical6 As String
    Public Property SalesRepCode As String

    Public Property TotalExclusive As String
    Public Property TotalTax As String
    Public Property TotalInclusive As String
    Public Property TotalDiscount As String


End Class
