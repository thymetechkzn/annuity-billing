﻿Namespace Details

    Public Class Order

        Public Property TransactionDate As String
        Public Property InvoiceType As String
        Public Property OurReference As String
        Public Property TotalExclusive As String
        Public Property TotalInclusive As String
        Public Property TotalTax As String
        Public Property TotalDiscount As String
        Public Property InvoicePost1 As String
        Public Property InvoicePost2 As String
        Public Property InvoicePost3 As String
        Public Property InvoicePost4 As String
        Public Property InvoicePost5 As String
        Public Property InvoicePost6 As String
        Public Property CustomerCode As String
        Public Property CustomerName As String
        Public Property InvoicePhysical1 As String
        Public Property InvoicePhysical2 As String
        Public Property InvoicePhysical3 As String
        Public Property InvoicePhysical4 As String
        Public Property InvoicePhysical5 As String
        Public Property InvoicePhysical6 As String
        Public Property CustomerTaxNumber As String
        Public Property Email As String
        Public Property OrderNo As String
        Public Property SalesRepCode As String
        Public Property OrderItems As New List(Of OrderItems)

        Private InvoiceID As String

        Dim Dbcon As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)

        Public Sub New(ByVal OrderNumber As String)
            LoadOrderDetails(OrderNumber)
            LoadOrderItemDetails(InvoiceID)
        End Sub

        Private Sub LoadOrderDetails(ByVal OrderNumber As String)
            Try

                Dim dt As DataTable
                Dim strSQLQuery As String

                strSQLQuery = "select" & vbCrLf & _
                                "I.InvNumber ," & vbCrLf & _
                                "I.InvDate," & vbCrLf & _
                                "I.Description ," & vbCrLf & _
                                "I.InvTotExcl ," & vbCrLf & _
                                "I.InvTotIncl , " & vbCrLf & _
                                "I.InvTotTax , " & vbCrLf & _
                                "I.InvDiscAmnt ," & vbCrLf & _
                                "I.PAddress1," & vbCrLf & _
                                "I.PAddress2," & vbCrLf & _
                                "I.PAddress3," & vbCrLf & _
                                "I.PAddress4," & vbCrLf & _
                                "I.PAddress5," & vbCrLf & _
                                "I.PAddress6," & vbCrLf & _
                                "I.Address1," & vbCrLf & _
                                "I.Address2," & vbCrLf & _
                                "I.Address3," & vbCrLf & _
                                "I.Address4," & vbCrLf & _
                                "I.Address5," & vbCrLf & _
                                "I.Address6," & vbCrLf & _
                                "I.OrderNum," & vbCrLf & _
                                "C.Account ," & vbCrLf & _
                                "C.Name," & vbCrLf & _
                                "C.Post1 ," & vbCrLf & _
                                "C.Post2 ," & vbCrLf & _
                                "c.PostPC ," & vbCrLf & _
                                "c.Tax_Number ," & vbCrLf & _
                                "c.EMail," & vbCrLf & _
                                "ISNULL(S.Code, '') AS Code," & vbCrLf & _
                                "I.AutoIndex" & vbCrLf & _
                                "from InvNum I" & vbCrLf & _
                                "inner join Client C" & vbCrLf & _
                                "on C.DCLink = I.AccountID" & vbCrLf & _
                                "left join SalesRep S" & vbCrLf & _
                                "on C.repid = s.idsalesrep" & vbCrLf & _
                                "where I.orderNum = '" & OrderNumber & "'" & vbCrLf & _
                                "Order by I.OrderNum"

                dt = Dbcon.get_datatable(strSQLQuery)

                For Each row As DataRow In dt.Rows

                    OrderNo = row("OrderNum")
                    InvoiceType = row("InvNumber")
                    TransactionDate = row("InvDate")
                    OurReference = row("Description")
                    TotalExclusive = row("InvTotExcl")
                    TotalInclusive = row("InvTotIncl")
                    TotalTax = row("InvTotTax")
                    TotalDiscount = row("InvDiscAmnt")
                    InvoicePost1 = row("PAddress1")
                    InvoicePost2 = row("PAddress2")
                    InvoicePost3 = row("PAddress3")
                    InvoicePost4 = row("PAddress4")
                    InvoicePost5 = row("PAddress5")
                    InvoicePost6 = row("PAddress6")
                    InvoicePhysical1 = row("Address1")
                    InvoicePhysical2 = row("Address2")
                    InvoicePhysical3 = row("Address3")
                    InvoicePhysical4 = row("Address4")
                    InvoicePhysical5 = row("Address5")
                    InvoicePhysical6 = row("Address6")
                    CustomerCode = row("Account")
                    CustomerName = row("Name")
                    CustomerTaxNumber = row("Tax_Number")
                    InvoiceID = row("AutoIndex")
                    Email = row("Email")
                    SalesRepCode = row("Code")

                Next

                dt.Clear()
                dt.Dispose()
                dt = Nothing

            Catch ex As Exception

            End Try

        End Sub

        Private Sub LoadOrderItemDetails(ByVal InvoiceID As Integer)
            Dim dt As DataTable
            Dim strSQLQuery As String
            
            strSQLQuery = "select " & vbCrLf & _
                            "S.Code," & vbCrLf & _
                            "B.cDescription , " & vbCrLf & _
                            "B.fUnitPriceExcl , " & vbCrLf & _
                            "b.fQtyProcessed , " & vbCrLf & _
                            "b.fQtyProcessedLineTaxAmount , " & vbCrLf & _
                            "b.fQuantityLineTotExcl, " & vbCrLf & _
                            "b.fQtyProcessedLineTotIncl" & vbCrLf & _
                            "from _btblInvoiceLines B" & vbCrLf & _
                            "inner join" & vbCrLf & _
                            "Stkitem S" & vbCrLf & _
                            "on" & vbCrLf & _
                            "S.StockLink = B.iStockCodeID" & vbCrLf & _
                            "where iInvoiceID = '" & InvoiceID & "'" & vbCrLf & _
                            "order by idInvoiceLines "

            dt = Dbcon.get_datatable(strSQLQuery)

            For Each row In dt.Rows

                Dim Od As New OrderItems

                Od.ItemCode = row("Code")
                Od.ItemDescription = row("cDescription")
                Od.Price = row("fUnitPriceExcl")
                Od.Quantity = row("fQtyProcessed")
                Od.Tax = row("fQtyProcessedLineTaxAmount")
                Od.TotalInclusive = row("fQtyProcessedLineTotIncl")
                Od.TotalLineExcl = row("fQuantityLineTotExcl")

                OrderItems.Add(Od)
            Next

            dt.Clear()
            dt.Dispose()
            dt = Nothing

        End Sub

    End Class
End Namespace