﻿Public Class frmStatementRepMain

    Dim objFrmImport As frmImportFile
    Dim objFrmInvoiceReporter As frmInvoiceReporter
    Sub New(ByVal dBInstance As String, _
            ByVal dBName As String, _
            ByVal dBUser As String, _
            ByVal dBPassword As String, _
            ByVal evoCommon As String, _
            ByVal EmailStatSubject As String, _
            ByVal EmailStatBody As String, _
            ByVal EmailStatTestMode As Boolean, _
            ByVal EmailStatTestAddress As String)

        'This call is required by the designer.
        InitializeComponent()

        My.Settings.DBServer = dBInstance
        My.Settings.PASCompany = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.PASCommon = evoCommon

        My.Settings.DBServer = dBInstance
        My.Settings.PASCompany = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.PASCommon = evoCommon
        My.Settings.EmailSubject = EmailStatSubject
        My.Settings.EmailBody = EmailStatBody
        My.Settings.EmailTestMode = EmailStatTestMode
        My.Settings.EmailTestAddress = EmailStatTestAddress

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub


    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadImportScreen()
    End Sub

    Private Sub LoadImportScreen()

        '   objFrmImport = New frmImportFile(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword, My.Settings.PASCommon)
        objFrmImport.TopLevel = False
        objFrmImport.TopMost = False
        objFrmImport.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        objFrmImport.Dock = DockStyle.Fill
        pnFormContainer.Controls.Add(objFrmImport)
        objFrmImport.Show()

    End Sub
    Private Sub ShowImportScreen()
        objFrmImport.Show()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub


End Class