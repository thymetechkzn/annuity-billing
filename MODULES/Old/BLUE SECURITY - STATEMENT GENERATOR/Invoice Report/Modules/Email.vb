﻿Imports System.Reflection
Imports Microsoft.Office.Interop
Imports System.IO

Public Class Email

    Dim newMail As Outlook.MailItem
    Dim tempApp As Outlook.Application

    Public Sub SendEmail(ByVal ToAddress As String, _
                           ByVal Subject As String, _
                           ByVal AttachmentPath As String, _
                           ByVal Body As String, _
                           ByVal InvoiceNumber As String, _
                           ByVal CustomerCode As String, _
                           ByVal CustomerName As String, _
                           ByVal fileName As String)

        Dim logger As New LogFile(Path.GetDirectoryName(AttachmentPath), fileName)

        Try

            tempApp = CreateObject("Outlook.Application")
            newMail = tempApp.CreateItem(0)

            With newMail
                If My.Settings.EmailTestMode Then
                    .To = My.Settings.EmailTestAddress
                    '.CC = CCAddress
                    .Subject = My.Settings.EmailSubject & " " & CustomerCode & " - " & CustomerName
                    .Body = My.Settings.EmailBody.ToString
                    '.HTMLBody = My.Settings.EmailBody
                    .Attachments.Add(AttachmentPath)
                    Debug.WriteLine("EMAIL SENT TO " & .To & " FOR :" & ToAddress)
                    .Send()
                Else
                    .To = ToAddress
                    '.CC = CCAddress
                    .Subject = My.Settings.EmailSubject & " " & CustomerCode & " - " & CustomerName
                    .Body = My.Settings.EmailBody.ToString
                    '.HTMLBody = My.Settings.EmailBody
                    .Attachments.Add(AttachmentPath)
                    Debug.WriteLine("EMAIL SENT TO " & .To & " FOR :" & ToAddress)
                    .Send()
                End If

            End With

            logger.CreateLog("An email for; " & CustomerCode & "; has been created. Email address; " & ToAddress)

            'dispose of objects
            newMail = Nothing
            tempApp = Nothing

        Catch ex As Exception
            logger.CreateLog("An email for; " & CustomerCode & "; has not been created. Email address; " & ToAddress)
        End Try

    End Sub

End Class