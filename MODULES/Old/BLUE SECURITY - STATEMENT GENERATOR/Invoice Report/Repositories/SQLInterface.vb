﻿Public Class SQLInterface

    Dim DBCon As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function GetOrderNums() As List(Of Order)

        Dim lstOrders As New List(Of Order)
        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim LastDate As String = ""
        Dim FirstDate As String = ""

        SQLQuery = "select Distinct(Client.DCLink) from Client" & vbCrLf & _
                    "inner join" & vbCrLf & _
                    "PostAR" & vbCrLf & _
                    "on" & vbCrLf & _
                    "PostAR.AccountLink = Client.DCLink" & vbCrLf & _
                    "order by Client.DCLink"

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Dim OrderHeader As New Order

            OrderHeader.AccountCode = Row("DCLink")

            lstOrders.Add(OrderHeader)

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return lstOrders

    End Function

    Public Function GetCompanyDetails() As Company

        Dim BlueComp As New Company
        Dim SQLQuery As String = ""
        Dim DT As New DataTable

        SQLQuery = "select" & vbCrLf & _
                    "PhAddress1," & vbCrLf & _
                    "PhAddress2," & vbCrLf & _
                    "PhAddress3," & vbCrLf & _
                    "PHPostalCode," & vbCrLf & _
                    "PoAddress1," & vbCrLf & _
                    "POAddress2," & vbCrLf & _
                    "POAddress3," & vbCrLf & _
                    "POPostalCode," & vbCrLf & _
                    "Telephone1," & vbCrLf & _
                    "Fax," & vbCrLf & _
                    "Tax_Number," & vbCrLf & _
                    "Registration," & vbCrLf & _
                    "BankName," & vbCrLf & _
                    "BankAccount," & vbCrLf & _
                    "BranchCode" & vbCrLf & _
                    "from entities"

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            BlueComp.Physical1 = Row("PhAddress1")
            BlueComp.Physical2 = Row("PhAddress2")
            BlueComp.PhysicalPostCode = Row("PHPostalCode")
            BlueComp.Post1 = Row("PoAddress1")
            BlueComp.Post2 = Row("POAddress2")
            BlueComp.PostPostCode = Row("POPostalCode")
            BlueComp.TelephoneNumber = Row("Telephone1")
            BlueComp.FaxNumber = Row("Fax")
            BlueComp.VATno = Row("Tax_Number")
            BlueComp.Registration = Row("Registration")
            BlueComp.BankName = Row("BankName")
            BlueComp.BankAccount = Row("BankAccount")
            BlueComp.BranchCode = Row("BranchCode")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return BlueComp

    End Function

End Class
