﻿Imports System.IO

Public Class LogFile

    Private fd As String = ""
    Private name As String = ""

    Public Sub New(ByVal fd As String, ByVal Name As String)

        If Not Directory.Exists(fd) Then
            Directory.CreateDirectory(fd)
        End If

        Me.fd = fd
        Me.name = Name

    End Sub

    Public Sub CreateLog(ByVal Log As String)
        Dim fw As StreamWriter

        fw = File.AppendText(fd & "\" & name & ".txt")

        fw.WriteLine(Log)
        fw.Close()
        fw.Dispose()
    End Sub

End Class
