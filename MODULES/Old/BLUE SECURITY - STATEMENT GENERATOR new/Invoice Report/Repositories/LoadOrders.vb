﻿Imports Pastel.Evolution

Public Class LoadOrders

    Dim Conn As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)

    Public Sub LoadOrderInfo(ByVal lstOrder As List(Of Order), ByVal EmailFolderPath As String, ByVal PostFolderPath As String, ByVal FromDate As String, ByVal ToDate As String)


        For Each OrderHeader As Order In lstOrder

            If CheckPreferredCorrespondence(OrderHeader.AccountCode) Then

                If CheckPreferredReport(OrderHeader.AccountCode) Then

                    Dim frmInvoiceRep As New frmInvoiceReporter(OrderHeader.AccountCode, EmailFolderPath, FromDate, ToDate)

                    frmInvoiceRep = Nothing

                Else

                    Dim frmInvoiceSimpleRep As New frmInvoiceSimpleReporter(OrderHeader.AccountCode, EmailFolderPath, FromDate, ToDate)

                    frmInvoiceSimpleRep = Nothing

                End If
            End If

        Next

        For Each OrderHeader As Order In lstOrder

            If OrderHeader.AccountCode = "176" Then
                Debug.WriteLine("")
            End If

            If CheckPreferredCorrespondencePost(OrderHeader.AccountCode) Then

                Dim frmStatementPost As New frmInvoicePostReporter(OrderHeader.AccountCode, PostFolderPath, FromDate, ToDate)

                frmStatementPost = Nothing

            End If

        Next

    End Sub

    Public Function CheckInvoiceRequired(ByVal AccountCode As String) As Boolean '*

        Dim SQLQuery As String
        Dim Required As Boolean

        SQLQuery = "select ubARInvoiceRequired from Client" & vbCrLf & _
                    "where DCLink = '" & AccountCode & "'"

        Conn.setSqlString(SQLQuery)
        Conn.createNewConnection()
        Required = Conn.get_value("ubARInvoiceRequired")

        Return Required

    End Function

    Private Function CheckPreferredCorrespondence(ByVal AccountId As String) As Boolean '*

        Dim SQLQuery As String
        Dim strValue As String
        Dim blnValue As Boolean = False

        SQLQuery = "select ulARPrefCorr from client" & vbCrLf & _
                    "where dclink = " & AccountId

        Conn.setSqlString(SQLQuery)
        Conn.createNewConnection()
        strValue = Conn.get_value("ulARPrefCorr")
        Conn.CloseConnection()

        If strValue.Equals("EMAIL") Then

            blnValue = True

        Else

            blnValue = False

        End If

        Return blnValue

    End Function
    Private Function CheckPreferredCorrespondencePost(ByVal AccountId As String) As Boolean '*

        Dim SQLQuery As String
        Dim strValue As String
        Dim blnValue As Boolean = False

        SQLQuery = "select ulARPrefCorr from client" & vbCrLf & _
                    "where dclink = " & AccountId

        Conn.setSqlString(SQLQuery)
        Conn.createNewConnection()
        strValue = Conn.get_value("ulARPrefCorr")
        Conn.CloseConnection()

        If strValue.Equals("POST") Then

            blnValue = True

        Else

            blnValue = False

        End If

        Return blnValue

    End Function
    Private Function CheckPreferredReport(ByVal AccountCode As String) As Boolean '*

        Dim SQLQuery As String
        Dim PrefCorrespondance As String
        Dim IsNormal As Boolean = True

        SQLQuery = "select ulARPreferredReport from Client" & vbCrLf & _
                    "where DCLink = '" & AccountCode & "'"

        Conn.setSqlString(SQLQuery)
        Conn.createNewConnection()
        PrefCorrespondance = Conn.get_value("ulARPreferredReport")
        Conn.CloseConnection()

        If PrefCorrespondance.Equals("Normal") Then

            IsNormal = True

        Else

            IsNormal = False

        End If

        Return IsNormal

    End Function

End Class
