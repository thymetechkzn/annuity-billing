﻿Public Class InvCustomer

    Public Property Code As String
    Public Property Name As String
    Public Property TaxNumber As String
    Public Property Email As String
    Public Property SendEmail As Boolean
    Public Property RepCode As String

    Dim SQLCon As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)
    Dim AccountID As String

    Public Sub New(ByVal AccountID As String)

        Me.AccountID = AccountID

        getCustomerDetails()

    End Sub

    Public Sub getCustomerDetails()

        Dim SQLQuery As String
        Dim DT As New DataTable

        SQLQuery = "select EMail from client" & vbCrLf & _
                    "where DCLink = " & AccountID

        DT = SQLCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Email = Row("Email")

        Next

    End Sub

End Class
