﻿Public Class Company

    Public Property TelephoneNumber As String
    Public Property FaxNumber As String
    Public Property VATno As String
    Public Property BankName As String
    Public Property BankAccount As String
    Public Property BranchCode As String
    Public Property AccountName As String
    Public Property Post1 As String
    Public Property Post2 As String
    Public Property PostPostCode As String
    Public Property Physical1 As String
    Public Property Physical2 As String
    Public Property PhysicalPostCode As String
    Public Property Registration As String

End Class
