﻿Public Class Invoice

    Public Property Company As New Company
    Public Property Order As New Order
    Public Property OrderLines As New List(Of OrderLine)

    Public Property Statement As New Details.Statement
    Public Property StatementLine As New Details.Statement_line

End Class
