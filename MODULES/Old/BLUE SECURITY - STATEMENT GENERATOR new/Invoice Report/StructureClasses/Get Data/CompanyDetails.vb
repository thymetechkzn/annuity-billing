﻿
Namespace Details
    Public Class Company

        Public Property Name As String
        Public Property PostalAddress1 As String
        Public Property PostalAddress2 As String
        Public Property PostalAddress3 As String
        Public Property PostalPostCode As String
        Public Property Telephone1 As String
        Public Property Telephone2 As String
        Public Property Fax As String
        Public Property Tax_Number As String
        Public Property Registration As String
        Public Property BankName As String
        Public Property BankAccount As String
        Public Property BranchCode As String
        Public Property AccountName As String
        Public Property BranchName As String
        Public Property Physical1 As String
        Public Property Physical2 As String
        Public Property physical3 As String
        Public Property physicalPostCode As String

        Dim Dbcon As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)

        Public Sub New()
            LoadDetails()
        End Sub
        Public Sub New(ByVal CompanyID As Integer)
            LoadDetails(CompanyID)
        End Sub

        Private Sub LoadDetails(Optional ByVal id As Integer = 1)
            Try


                Dim dt As DataTable
                Dim strSQLQuery As String

                strSQLQuery = "select" & vbCrLf & _
                            "Name," & vbCrLf & _
                            "PhAddress1," & vbCrLf & _
                            "PhAddress2," & vbCrLf & _
                            "PhAddress3," & vbCrLf & _
                            "PHPostalCode," & vbCrLf & _
                            "PoAddress1," & vbCrLf & _
                            "POAddress2," & vbCrLf & _
                            "POAddress3," & vbCrLf & _
                            "POPostalCode," & vbCrLf & _
                            "Telephone1," & vbCrLf & _
                            "Telephone2," & vbCrLf & _
                            "Fax," & vbCrLf & _
                            "Tax_Number," & vbCrLf & _
                            "Registration," & vbCrLf & _
                            "BankName," & vbCrLf & _
                            "BankAccount," & vbCrLf & _
                            "BranchCode," & vbCrLf & _
                            "cAccountName" & vbCrLf & _
                            "from entities" & vbCrLf & _
                            "WHERE Entities.idEntities = 1"

                dt = Dbcon.get_datatable(strSQLQuery)

                For Each row As DataRow In dt.Rows
                    Name = row("Name")
                    PostalAddress1 = row("POAddress1")
                    PostalAddress2 = row("POAddress2")
                    PostalAddress3 = row("POAddress3")
                    Telephone1 = row("Telephone1")
                    Telephone2 = row("Telephone2")
                    Fax = row("Fax")
                    Tax_Number = row("Tax_Number")
                    BankName = row("BankName")
                    BankAccount = row("BankAccount")
                    BranchCode = row("BranchCode")
                    AccountName = row("cAccountName")
                    'BranchName = row("cBranchName")
                    PostalPostCode = row("POPostalCode")
                    Physical1 = row("PhAddress1")
                    Physical2 = row("PhAddress2")
                    physical3 = row("PhAddress3")
                    physicalPostCode = row("PHPostalCode")
                    Registration = row("Registration")

                Next

                dt.Clear()
                dt.Dispose()
                dt = Nothing

            Catch ex As Exception

            End Try

        End Sub

    End Class

End Namespace
