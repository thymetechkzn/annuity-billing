﻿Imports System.Globalization

Namespace Details

    Public Class Statement

        Public Property FromDate As String
        Public Property ToDate As String
        Public Property AccountID As String
        Public Property Current As String = "0.00"
        Public Property Amount_Due As String = "0.00"
        Public Property dbAmount_Due As Double = 0.0
        Public Property Customer_Code As String
        Public Property CustomerName As String
        Public Property CustomerEmail As String

        Public Property Days_30 As String = "0.00"
        Public Property Days_60 As String = "0.00"
        Public Property Days_90 As String = "0.00"
        Public Property Days_120 As String = "0.00"
        Public Property Days_150 As String = "0.00"
        Public Property Days_180 As String = "0.00"

        Public Property dbl180 As Double = 0
        Public Property dbl150 As Double = 0
        Public Property dbl120 As Double = 0
        Public Property dbl90 As Double = 0
        Public Property dbl60 As Double = 0
        Public Property dbl30 As Double = 0

        Public Property isPositive_Days_30 As Boolean = True
        Public Property isPositive_Days_60 As Boolean = True
        Public Property isPositive_Days_90 As Boolean = True
        Public Property isPositive_Days_120 As Boolean = True
        Public Property isPositive_Days_150 As Boolean = True
        Public Property isPositive_Days_180 As Boolean = True

        Public Property Post1 As String
        Public Property Post2 As String
        Public Property Post3 As String
        Public Property Post4 As String
        Public Property Post5 As String
        Public Property PostPC As String
        Public Property StatementDate As String
        Public Property lstStamentLines As New List(Of Statement_line)

        Dim SQLCon As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)

        Public Sub New()

        End Sub

        Public Sub New(ByVal AccountID As String, ByVal FromDate As String, ByVal ToDate As String)

            Me.AccountID = AccountID 'AccountID
            Me.FromDate = FromDate
            Me.ToDate = ToDate

            RefreshView()

            LoadStatement(AccountID)

        End Sub

        Private Sub LoadStatement(ByVal AccountID As String)

            Dim SQLQuery As String
            Dim DT As New DataTable

            'SQLQuery = "select ART.TXDate," & vbCrLf & _
            '            "ART.Reference," & vbCrLf & _
            '            "ART.Description," & vbCrLf & _
            '            "ART.Debit," & vbCrLf & _
            '            "ART.Credit," & vbCrLf & _
            '             "ART.Outstanding," & vbCrLf & _
            '            "C.Account," & vbCrLf & _
            '            "C.EMail," & vbCrLf & _
            '            "C.Post1," & vbCrLf & _
            '            "C.Post2," & vbCrLf & _
            '            "C.Post3," & vbCrLf & _
            '            "C.Post4," & vbCrLf & _
            '            "C.Post5," & vbCrLf & _
            '            "C.PostPC," & vbCrLf & _
            '            "C.Name," & vbCrLf & _
            '            "C.EMail" & vbCrLf & _
            '            "from _bvARTransactionsFull ART" & vbCrLf & _
            '            "INNER JOIN" & vbCrLf & _
            '            "Client C" & vbCrLf & _
            '            "ON" & vbCrLf & _
            '            "ART.AccountLink = DCLink" & vbCrLf & _
            '            "where AccountLink in" & vbCrLf & _
            '            "(select DCLink from Client" & vbCrLf & _
            '            "where dclink = " & AccountID & ")" & vbCrLf & _
            '            "AND" & vbCrLf & _
            '            "TxDate >= '" & FromDate & "'" & vbCrLf & _
            '            "AND" & vbCrLf & _
            '            "TxDate <= '" & ToDate & "'" & vbCrLf & _
            '            "AND" & vbCrLf & _
            '            "C.iClassID <> 10" & vbCrLf & _
            '            "AND" & vbCrLf & _
            '            "C.iClassID <> 11" & vbCrLf & _
            '            "AND" & vbCrLf & _
            '            "C.iClassID <> 14" & vbCrLf & _
            '            "AND" & vbCrLf & _
            '            "C.iClassID <> 16" & vbCrLf & _
            '            "AND" & vbCrLf & _
            '            "C.iClassID <> 29" & vbCrLf & _
            '            "order by TxDate"

            SQLQuery = "WITH Master_CTE (DCLink,Account,EMail,Post1,Post2,Post3,Post4,Post5,PostPC,Name)" & vbCrLf & _
                    "AS" & vbCrLf & _
                    "(" & vbCrLf & _
                    "    SELECT	" & vbCrLf & _
                    "						C.DCLink,			" & vbCrLf & _
                    "						C.Account," & vbCrLf & _
                    "                        C.EMail," & vbCrLf & _
                    "                        C.Post1," & vbCrLf & _
                    "                        C.Post2," & vbCrLf & _
                    "                        C.Post3," & vbCrLf & _
                    "                        C.Post4," & vbCrLf & _
                    "                        C.Post5," & vbCrLf & _
                    "                        C.PostPC," & vbCrLf & _
                    "                        C.Name" & vbCrLf & _
                    "                       " & vbCrLf & _
                    "    FROM Client C" & vbCrLf & _
                    "    WHERE DCBalance > 10" & vbCrLf & _
                    ")," & vbCrLf & _
                    "Transaction_CTE (AccountLink,TXDate,Reference,Description,Debit,Credit,Outstanding)" & vbCrLf & _
                    "AS" & vbCrLf & _
                    "(" & vbCrLf & _
                    "SELECT" & vbCrLf & _
                    "ART.AccountLink," & vbCrLf & _
                    "						ISNULL(ART.TXDate,'')," & vbCrLf & _
                    "                        ISNULL(ART.Reference, '')," & vbCrLf & _
                    "                        ISNULL(ART.Description,'')," & vbCrLf & _
                    "                        ISNULL(ART.Debit,'')," & vbCrLf & _
                    "                        ISNULL(ART.Credit,'')," & vbCrLf & _
                    "                        ISNULL(ART.Outstanding,'')" & vbCrLf & _
                    "                         			" & vbCrLf & _
                    "FROM  " & vbCrLf & _
                    "   _bvARTransactionsFull ART" & vbCrLf & _
                    "   where Cast(ART.TXDate as DATE)>='" & ToDate & "'" & vbCrLf & _
                    "   and Cast(ART.TXDate as DATE)<='" & FromDate & "'" & vbCrLf & _
                    ")" & vbCrLf & _
                    "Select " & vbCrLf & _
                                        "ISNULL(TXDate,'') AS 'TXDate'," & vbCrLf & _
                    "					ISNULL(Reference,'') AS 'Reference'," & vbCrLf & _
                    "                    ISNULL(Description,'') AS 'Description'," & vbCrLf & _
                    "                    ISNULL(Debit,'') AS 'Debit'," & vbCrLf & _
                    "                    ISNULL(Credit,'') AS 'Credit'," & vbCrLf & _
                    "                    ISNULL(Outstanding,'') AS 'Outstanding'," & vbCrLf & _
                    "                    Account," & vbCrLf & _
                    "                    EMail," & vbCrLf & _
                    "                    Post1," & vbCrLf & _
                    "                    Post2," & vbCrLf & _
                    "                    Post3," & vbCrLf & _
                    "                    Post4," & vbCrLf & _
                    "                    Post5," & vbCrLf & _
                    "                    PostPC," & vbCrLf & _
                    "                    Name," & vbCrLf & _
                    "                    EMail   " & vbCrLf & _
                    "FROM" & vbCrLf & _
                    "Master_CTE" & vbCrLf & _
                    "LEFT OUTER JOIN" & vbCrLf & _
                    "Transaction_CTE" & vbCrLf & _
                    "ON  Master_CTE.DCLink = Transaction_CTE.AccountLink" & vbCrLf & _
                    "WHERE" & vbCrLf & _
                    "Master_CTE.DCLink = " & AccountID

            DT = SQLCon.get_datatable(SQLQuery)

            lstStamentLines.Add(BalanceBroughtForward(FromDate))
            If lstStamentLines.Item(0).Credit = Nothing And lstStamentLines.Item(0).Debit = Nothing Then
                lstStamentLines.Clear()
            End If
            getDaysAmount()

            For Each Row As DataRow In DT.Rows

                Dim stLine As New Statement_line
                Dim StatementLineBalance As Double

                stLine.stDate = Row("TxDate")
                stLine.Reference = Row("Reference")
                stLine.Description = Row("Description")
                stLine.Debit = Row("debit")
                stLine.Credit = Row("Credit")
                stLine.Outstanding = Row("Outstanding")
                Customer_Code = Row("Account")
                CustomerName = Row("Name")
                Post1 = Row("Post1")
                Post2 = Row("Post2")
                Post3 = Row("Post3")
                Post4 = Row("Post4")
                Post5 = Row("Post5")
                PostPC = Row("PostPC")
                CustomerEmail = Row("Email")

                StatementDate = Date.Now.Date

                StatementLineBalance += (Convert.ToDouble(Row("Debit")) - Convert.ToDouble(Row("Credit")))
                stLine.Balance = StatementLineBalance.ToString("#.00")

                lstStamentLines.Add(stLine)

            Next

            Dim DBLBalance As Double

            If AccountID = 11 Then
                Debug.WriteLine("")
            End If

            For Each STLine As Statement_line In lstStamentLines



                If STLine.Description.Equals("Balance Brought Forward") Then

                    DBLBalance += (Convert.ToDouble(STLine.Debit) + Convert.ToDouble(STLine.Credit))
                    STLine.Balance = DBLBalance.ToString("#.00")

                Else

                    DBLBalance += (STLine.Debit - STLine.Credit)
                    STLine.Balance = DBLBalance.ToString("#.00")

                End If

                '***
                If Customer_Code = "ABB000068 " Then

                    Debug.WriteLine("")
                End If
                '***

                Amount_Due = DBLBalance
                dbAmount_Due = DBLBalance
            Next

            Dim IndexList As Integer = lstStamentLines.Count - 1

            If lstStamentLines.Count > 1 Then
                If Amount_Due.Contains("(") Or Amount_Due.Contains("-") Then

                    Amount_Due = "0.00"

                ElseIf Convert.ToDouble(lstStamentLines(IndexList).Balance) < 1 Then

                    Amount_Due = "0.00"

                End If

            End If

            DT.Clear()
            DT = Nothing

        End Sub
        Private Function BalanceBroughtForward(ByVal FromDate As String) As Statement_line

            Dim SQLQuery As String
            Dim DT As New DataTable
            Dim STLine As New Statement_line
            Dim dblDebitCredit As Double
            Dim dblOutstanding As Double

            SQLQuery = "select (DEBIT - CREDIT) AS DCOutstanding, Outstanding from postar" & vbCrLf & _
                        "where AccountLink = " & AccountID & vbCrLf & _
                        "AND" & vbCrLf & _
                        "TxDate < '" & FromDate & "'"

            'DEBIT - CREDIT

            DT = SQLCon.get_datatable(SQLQuery)

            For Each Row As DataRow In DT.Rows

                dblDebitCredit += Row("DCOutstanding")
                dblOutstanding += Row("Outstanding")

                If dblDebitCredit > 0 Then

                    STLine.stDate = FromDate
                    STLine.Reference = ""
                    STLine.Description = "Balance Brought Forward"
                    STLine.Debit = dblDebitCredit
                    STLine.Credit = "0"
                    STLine.Balance = dblDebitCredit
                    STLine.Outstanding = dblOutstanding

                ElseIf dblDebitCredit <= 0 Then

                    STLine.stDate = FromDate
                    STLine.Reference = ""
                    STLine.Description = "Balance Brought Forward"
                    STLine.Credit = dblDebitCredit
                    STLine.Debit = "0"
                    STLine.Balance = dblDebitCredit
                    STLine.Outstanding = dblOutstanding

                End If

            Next

            DT.Clear()
            DT = Nothing

            Return STLine

        End Function
        Private Sub getDaysAmount()

            Dim AmountDue As Double = 0
            Dim dblCurrent As Double = 0
            Dim SQLQuery As String
            Dim DT As New DataTable

            SQLQuery = "select ART.TXDate," & vbCrLf & _
                        "ART.Debit," & vbCrLf & _
                        "ART.Credit," & vbCrLf & _
                        "ART.cAllocs," & vbCrLf & _
                        "ART.Outstanding" & vbCrLf & _
                        "from _bvARTransactionsFull ART" & vbCrLf & _
                        "INNER JOIN" & vbCrLf & _
                        "Client C" & vbCrLf & _
                        "ON" & vbCrLf & _
                        "ART.AccountLink = DCLink" & vbCrLf & _
                        "where AccountLink in" & vbCrLf & _
                        "(select DCLink from Client" & vbCrLf & _
                        "where dclink = " & AccountID & ")" & vbCrLf & _
                        "AND" & vbCrLf & _
                        "ART.TxDate <= '" & ToDate & "'" & vbCrLf & _
                        "order by TxDate"

            DT = SQLCon.get_datatable(SQLQuery)

            If AccountID = 35676 Then
                Debug.WriteLine("")
            End If

            For Each Row As DataRow In DT.Rows

                'CALCULATING THE DAYS OVER DUE
                Dim TodayYear As Integer = Convert.ToDateTime(ToDate).Year
                Dim TransactionYear As Integer = Convert.ToDateTime(Row("TxDate")).Year

                Dim TodayMonth As Integer = Convert.ToDateTime(ToDate).Month
                Dim TransactionDate As Integer = Convert.ToDateTime(Row("TxDate")).Month

                Dim YearDifference As Integer = ((TodayYear - TransactionYear) * 12)

                Dim TotalMonths As Integer = ((YearDifference - TransactionDate) + TodayMonth)

                Dim TotalDays As Integer = (TotalMonths * 30)

                Dim Outstanding As Double = Row("Outstanding")

                '   AmountDue += Outstanding.ToString("#.00")

                'CHECKING THE CATEGORY THE AMOUNT FALLS UNDER
                If TotalDays >= 180 Then

                    dbl180 += Outstanding

                    If dbl180 >= 0 Then

                        Days_180 = dbl180.ToString("#.00")
                        'dbDays_180 = dbl180
                        isPositive_Days_180 = True

                    Else

                        ' dbl180 = Outstanding * -1
                        'Days_180 = "(" & dbl180 & ")"
                        Days_180 = dbl180.ToString("#.00")
                        'dbDays_180 = dbl180
                        isPositive_Days_180 = False

                    End If

                ElseIf TotalDays < 180 And TotalDays >= 150 Then

                    dbl150 += Outstanding

                    If dbl150 >= 0 Then

                        Days_150 = dbl150.ToString("#.00")
                        'dbDays_150 = dbl150
                        isPositive_Days_150 = True

                    Else

                        ' dbl150 = Outstanding * -1
                        'Days_150 = "(" & dbl150 & ")"
                        Days_150 = dbl150.ToString("#.00")
                        '' dbDays_150 = dbl150
                        isPositive_Days_150 = False

                    End If

                ElseIf TotalDays < 150 And TotalDays >= 120 Then

                    dbl120 += Outstanding

                    If dbl120 >= 0 Then

                        Days_120 = dbl120.ToString("#.00")
                        ' dbDays_120 = dbl120
                        isPositive_Days_120 = True

                    Else

                        'dbl120 = Outstanding * -1
                        'Days_120 = "(" & dbl120 & ")"
                        Days_120 = dbl120.ToString("#.00") '"(" & dbl120 & ")"
                        ' dbDays_120 = dbl120
                        isPositive_Days_120 = False

                    End If

                ElseIf TotalDays < 120 And TotalDays >= 90 Then

                    dbl90 += Outstanding

                    If dbl90 >= 0 Then

                        Days_90 = dbl90.ToString("#.00")
                        ' dbDays_90 = dbl90
                        isPositive_Days_90 = True

                    Else

                        'dbl90 = Outstanding * -1
                        ' Days_90 = "(" & dbl90 & ")"
                        Days_90 = dbl90.ToString("#.00")
                        'dbDays_90 = dbl90
                        isPositive_Days_90 = False

                    End If

                ElseIf TotalDays < 90 And TotalDays >= 60 Then

                    dbl60 += Outstanding

                    If dbl60 >= 0 Then

                        Days_60 = dbl60.ToString("#.00")
                        ' dbDays_60 = dbl60
                        isPositive_Days_60 = True

                    Else

                        ' dbl60 = Outstanding * -1
                        'Days_60 = "(" & dbl60 & ")"
                        Days_60 = dbl60.ToString("#.00")
                        'dbDays_60 = dbl60
                        isPositive_Days_60 = False

                    End If

                ElseIf TotalDays < 60 And TotalDays >= 30 Then

                    dbl30 += Outstanding

                    If dbl30 >= 0 Then

                        Days_30 = dbl30.ToString("#.00")
                        ' dbDays_30 = Days_30
                        isPositive_Days_30 = True

                    Else

                        If AccountID = 22 Then
                            Debug.WriteLine("")
                        End If
                        ' dbl30 = Outstanding * -1
                        'Days_30 = "(" & dbl30 & ")"
                        Days_30 = dbl30.ToString("#.00")
                        ' dbDays_30 = Days_30
                        isPositive_Days_30 = False

                    End If

                ElseIf TotalDays < 30 Then

                    dblCurrent += Outstanding

                    If dblCurrent >= 0 Then

                        Current = dblCurrent.ToString("#.00")

                    Else

                        ' dblCurrent = Outstanding * -1
                        'Current = "(" & dblCurrent & ")"
                        Current = dblCurrent.ToString("#.00") '"(" & dblCurrent & ")

                    End If

                End If

            Next

            '  Amount_Due = AmountDue.ToString()

            DT.Clear()
            DT = Nothing

        End Sub

        Private Sub RefreshView()

            Dim SQLQuery As String = "sp_refreshview '_bvARTransactionsFull'"

            SQLCon.setSqlString(SQLQuery)
            SQLCon.createNewConnection()
            SQLCon.CloseConnection()

        End Sub

    End Class

    Public Class Statement_line

        Public Property stDate As String
        Public Property Reference As String
        Public Property Description As String
        Public Property Outstanding As Double
        Public Property Debit As String
        Public Property Credit As String
        Public Property Balance As String
        Public Property Customer_Code As String

    End Class

End Namespace