﻿Public Class frmProcessing

    Private Sub frmProcessing_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        Timer1.Enabled = True
        Timer1.Start()
    End Sub

    Private Sub Timer1_Tick(sender As System.Object, e As System.EventArgs) Handles Timer1.Tick
        If ProgressBar.Value >= ProgressBar.Maximum Then
            ProgressBar.Value = 0
        Else
            ProgressBar.Value += 1
        End If
    End Sub

End Class