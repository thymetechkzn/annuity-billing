﻿Imports Pastel.Evolution
Imports System.IO
Imports Microsoft.Reporting.WinForms

Public Class frmInvoiceSimpleReporter

    Private AccountID As String
    Private FilePath As String
    Private fileName As String
    Private SIMNumber As String
    Private FromDate As String
    Private ToDate As String

    Public Sub New(ByVal AccountID As String, ByVal FilePath As String, ByVal FromDate As String, ByVal ToDate As String)

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.
        'SAVE PASSED VARIABLES
        Me.AccountID = AccountID
        Me.FilePath = FilePath
        Me.FromDate = FromDate
        Me.ToDate = ToDate

        LoadInvoiceDetails()

        AccountID = Nothing
        FilePath = Nothing

        Me.Close()

    End Sub

    Private Sub LoadInvoiceDetails()

        Dim invoice As New Invoice
        Dim comp As New Details.Company
        Dim cust As New InvCustomer(AccountID)
        'Dim order As New Details.Order(OrderNumber)
        Dim Stat As New Details.Statement(AccountID, FromDate, ToDate)
        Dim StatLines As New List(Of Details.Statement_line)
        Dim Emailer As New Email

        Try
            'CHECKS IF BLUE IS OWING MONEY TO CLIENT
            'IF BLUE IS OWING MONEY TO CLIENT DO NOT DO SAVE AND EMAIL
            'IF BLUE IS NOT OWING MONEY TO CLIENT DO SAVE AND EMAIL
            If Not Stat.Amount_Due.Equals("0.00") And Convert.ToDouble(Stat.Amount_Due) > 10 Then

                If Stat.lstStamentLines.Count > 0 Then

                    If Not Stat.Customer_Code = Nothing Then

                        invoice.Statement.Amount_Due = Strings.FormatNumber(Stat.Amount_Due, 2, TriState.True, TriState.True, TriState.True).ToString
                        invoice.Statement.Current = Strings.FormatNumber(Stat.Current, 2, TriState.True, TriState.True, TriState.True).ToString
                        invoice.Statement.Customer_Code = Stat.Customer_Code
                        invoice.Statement.CustomerName = Stat.CustomerName
                        invoice.Statement.Days_180 = Stat.Days_180.Replace("-", "")
                        invoice.Statement.Days_180 = Strings.FormatNumber(invoice.Statement.Days_180, 2, TriState.True, TriState.True, TriState.True).ToString
                        invoice.Statement.Days_150 = Stat.Days_150.Replace("-", "")
                        invoice.Statement.Days_150 = Strings.FormatNumber(invoice.Statement.Days_150, 2, TriState.True, TriState.True, TriState.True).ToString
                        invoice.Statement.Days_120 = Stat.Days_120.Replace("-", "")
                        invoice.Statement.Days_120 = Strings.FormatNumber(invoice.Statement.Days_120, 2, TriState.True, TriState.True, TriState.True).ToString
                        invoice.Statement.Days_90 = Stat.Days_90.Replace("-", "")
                        invoice.Statement.Days_90 = Strings.FormatNumber(invoice.Statement.Days_90, 2, TriState.True, TriState.True, TriState.True).ToString
                        invoice.Statement.Days_60 = Stat.Days_60.Replace("-", "")
                        invoice.Statement.Days_60 = Strings.FormatNumber(invoice.Statement.Days_60, 2, TriState.True, TriState.True, TriState.True).ToString
                        invoice.Statement.Days_30 = Stat.Days_30.Replace("-", "")
                        invoice.Statement.Days_30 = Strings.FormatNumber(invoice.Statement.Days_30, 2, TriState.True, TriState.True, TriState.True).ToString
                        invoice.Statement.Post1 = Stat.Post1
                        invoice.Statement.Post2 = Stat.Post2
                        invoice.Statement.Post3 = Stat.Post3
                        invoice.Statement.Post4 = Stat.Post4
                        invoice.Statement.Post5 = Stat.Post5
                        invoice.Statement.PostPC = Stat.PostPC
                        invoice.Statement.StatementDate = Stat.StatementDate.Replace("-", "/")
                        invoice.Statement.StatementDate = ToDate.Replace("-", "/")

                        For Each StatLine As Details.Statement_line In Stat.lstStamentLines

                            Dim NewStatLine As New Details.Statement_line

                            NewStatLine.Balance = Strings.FormatNumber(StatLine.Balance, 2, TriState.True, TriState.True, TriState.True).ToString

                            If NewStatLine.Balance.Contains("-") Then

                                NewStatLine.Balance.Replace("-", "")
                                NewStatLine.Balance = "(" & NewStatLine.Balance & ")"

                            End If

                            NewStatLine.Credit = Strings.FormatNumber(StatLine.Credit, 2, TriState.True, TriState.True, TriState.True).ToString
                            If NewStatLine.Credit.Equals("0.00") Then
                                NewStatLine.Credit = ""
                            End If

                            NewStatLine.Debit = Strings.FormatNumber(StatLine.Debit, 2, TriState.True, TriState.True, TriState.True).ToString
                            If NewStatLine.Debit.Equals("0.00") Then
                                NewStatLine.Debit = ""
                            End If

                            If Not (NewStatLine.Credit.Equals("") And NewStatLine.Debit.Equals("")) Then

                                NewStatLine.Description = StatLine.Description
                                NewStatLine.Reference = StatLine.Reference
                                NewStatLine.stDate = StatLine.stDate.Replace("-", "/")

                                StatLines.Add(NewStatLine)

                            End If

                        Next

                        'SETTING COMPANY DETAILS
                        invoice.Company.AccountName = comp.AccountName
                        invoice.Company.BranchCode = comp.BranchCode
                        invoice.Company.BankAccount = comp.BankAccount
                        invoice.Company.BankName = comp.BankName
                        invoice.Company.TelephoneNumber = comp.Telephone1
                        invoice.Company.VATno = comp.Tax_Number
                        invoice.Company.FaxNumber = comp.Fax
                        invoice.Company.Post1 = comp.PostalAddress1 & ", " & comp.PostalAddress2 & ", " & comp.PostalPostCode
                        invoice.Company.Physical1 = comp.Physical1 & ", " & comp.Physical2 & ", " & comp.physicalPostCode
                        invoice.Company.Registration = comp.Registration

                        'BINDING THE DATA TO THE REPORT
                        OrderBindingSource.Add(invoice.Order)
                        CompanyBindingSource.Add(invoice.Company)
                        StatementBindingSource.Add(invoice.Statement)

                        For Each stLine As Details.Statement_line In StatLines
                            Statement_lineBindingSource.Add(stLine)
                        Next

                        'SAVING THE REPORT TO PDF
                        save(Stat.Customer_Code)

                        'SENDING EMAILS AND PDF ATTACHMENTS

                        Emailer.SendEmail(cust.Email, _
                                          My.Settings.EmailStatementSubject, _
                                          FilePath, _
                                          My.Settings.EmailStatementBody, _
                                          SIMNumber, _
                                          invoice.Statement.Customer_Code, _
                                          invoice.Statement.CustomerName, fileName)

                        comp = Nothing
                        AccountID = Nothing
                        invoice = Nothing

                        CustomerBindingSource.Clear()
                        OrderBindingSource.Clear()
                        CompanyBindingSource.Clear()
                        OrderLineBindingSource.Clear()

                        CustomerBindingSource = Nothing
                        OrderBindingSource = Nothing
                        CompanyBindingSource = Nothing
                        OrderLineBindingSource = Nothing

                        Emailer = Nothing

                        FilePath = Nothing

                    End If

                End If

            Else

                Debug.WriteLine(Stat.AccountID & " Less Than 0 (" & Stat.Amount_Due & ")")

            End If

        Catch ex As Exception
            Debug.WriteLine(AccountID & " " & invoice.Statement.Customer_Code)
        End Try

    End Sub

    Public Sub save(ByVal CustomerCode As String)

        'SAVE PDF to FilePath

        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim extension As String = Nothing
        Dim byteviewer As Byte()
        Dim deviceinfo As String = "<DeviceInfo><OutputFormat>PDF</OutputFormat><PageWidth>8.5in</PageWidth><PageHeight>11in</PageHeight><MarginTop>0in</MarginTop><MarginLeft>0in</MarginLeft><MarginRight>0in</MarginRight><MarginBottom>0in</MarginBottom></DeviceInfo>"

        fileName = "Statement Reporter - Simple Email " & (System.DateTime.Today).ToString("dd-MM-yyyy")
        Dim logger As New LogFile(Path.GetDirectoryName(FilePath), fileName)
        Dim strLog As String = ""

        Try

            ReportViewer1.LocalReport.EnableHyperlinks = True

            'SAVE REPORT AT ARRAY OF BYTES
            byteviewer = ReportViewer1.LocalReport.Render("PDF", deviceinfo, mimeType, encoding, extension, streamids, warnings)

            'CREATE PDF WITH ARRAY BYTES
            Dim fs As New FileStream(FilePath & "Statement " & CustomerCode & ".pdf", FileMode.Create)
            FilePath = FilePath & "Statement " & CustomerCode & ".pdf"
            fs.Write(byteviewer, 0, byteviewer.Length)

            fs.Close()
            fs.Dispose()
            fs = Nothing

            Array.Clear(byteviewer, 0, byteviewer.Length)
            byteviewer = Nothing

            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.LocalReport.DataSources.Clear()
            ReportViewer1.LocalReport.ReleaseSandboxAppDomain()
            ReportViewer1.Reset()
            ReportViewer1.Clear()

            warnings = Nothing
            streamids = Nothing
            mimeType = Nothing
            encoding = Nothing
            extension = Nothing
            deviceinfo = Nothing

            logger.CreateLog("Statement for; " & CustomerCode & "; has been created.")

        Catch ex As Exception
            logger.CreateLog("Statement for; " & CustomerCode & "; has not been created.")
        End Try

    End Sub

End Class