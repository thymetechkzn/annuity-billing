﻿Public Class frmEmailStatementSettings

    Private EmailSubject As String
    Private EmailBody As String
    Private EmailTestMode As Boolean
    Private EmailTestAddress As String

    Public Sub New(ByVal EmailSubject As String, ByVal EmailBody As String, ByVal EmailTestMode As Boolean, ByVal EmailTestAddress As String)

        ' This call is required by the designer.
        InitializeComponent()

        Me.EmailSubject = EmailSubject
        Me.EmailBody = EmailBody
        Me.EmailTestMode = EmailTestMode
        Me.EmailTestAddress = EmailTestAddress

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnSaveEmailSettings_Click(sender As System.Object, e As System.EventArgs) Handles btnSaveEmailSettings.Click

        My.Settings.EmailStatementSubject = txtSubject.Text
        My.Settings.EmailStatementBody = txtBody.Text
        My.Settings.EmailStatementTestMode = ckbTestMode.CheckState
        My.Settings.EmailStatementTestAddress = txtTestEmailAddress.Text

        My.Settings.Save()

        MsgBox("Email Settings have been Saved")

        Me.Close()

    End Sub

    Private Sub frmEmailSettings_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        txtSubject.Text = My.Settings.EmailStatementSubject
        txtBody.Text = My.Settings.EmailStatementBody
        txtTestEmailAddress.Text = My.Settings.EmailStatementTestAddress
        If My.Settings.EmailStatementTestMode = True Then
            ckbTestMode.CheckState = CheckState.Checked
        Else
            ckbTestMode.CheckState = CheckState.Unchecked
        End If
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

End Class