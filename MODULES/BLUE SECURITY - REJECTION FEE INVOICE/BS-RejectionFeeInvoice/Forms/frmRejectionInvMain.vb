﻿Public Class frmRejectionInvMain

    Public Property LstSOHeader As New List(Of SOHeader)
    Dim path As String = ""
    Dim lstID As List(Of String)

    Sub New(ByVal dBInstance As String, ByVal dBName As String, _
           ByVal dBUser As String, ByVal dBPassword As String, ByVal evoCommon As String)

        ' This call is required by the designer.
        InitializeComponent()

        My.Settings.DBServer = dBInstance
        My.Settings.PASCompany = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.PASCommon = evoCommon

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click

        Me.Close()

    End Sub
    Private Sub ImportToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ImportToolStripMenuItem.Click

        'Clear
        LstSOHeader.Clear()
        LstSOHeader = Nothing

        Dim FileDialog As New OpenFileDialog
        Dim FilePath As String

        If FileDialog.ShowDialog = Windows.Forms.DialogResult.OK Then

            FilePath = FileDialog.FileName

            LstSOHeader = LoadSO.LoadFile(FilePath)

            btnProcessInvoices.Enabled = True

        End If

    End Sub
    Private Sub btnProcessInvoices_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessInvoices.Click

        'Path Options
        'Declare Variables
        Dim fileDialog As FolderBrowserDialog = New FolderBrowserDialog

        fileDialog.Description = "Please select a path to the Error Log"
        fileDialog.RootFolder = Environment.SpecialFolder.Desktop
        Try

            If fileDialog.ShowDialog() = DialogResult.OK Then


                path = fileDialog.SelectedPath.ToString()

                BackgroundWorker1.RunWorkerAsync()
                Timer1.Enabled = True
                Timer1.Start()

                pbBankFeeInv.Value = 0

            End If

        Catch ex As Exception

        End Try

    End Sub
    Private Sub Form1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        btnProcessInvoices.Enabled = False

    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

        'Get  dates to be invoiced
        lstID = AccountPreiodCheck.unBlockAccountingPeriods(LstSOHeader)

        If lstID.Count > 0 Then

            AccountPreiodCheck.changeGLPeriodBlock(lstID, "0")

        End If


        unHold(LstSOHeader)
        CreateSO.ProcessSO(LstSOHeader, path)
        placeOnHold(LstSOHeader)

        If lstID.Count > 0 Then

            AccountPreiodCheck.changeGLPeriodBlock(lstID, "1")

        End If


        If Me.InvokeRequired Then
            Timer1.Stop()
            Timer1.Enabled = False
            MsgBox("Process Complete")
        Else
            MsgBox("Process Complete")
            pbBankFeeInv.Value = 0
        End If

    End Sub

    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If pbBankFeeInv.Value >= pbBankFeeInv.Maximum Then
            pbBankFeeInv.Value = 0
        Else
            pbBankFeeInv.Value += 1
        End If

    End Sub
End Class
