﻿Imports Pastel.Evolution
Imports System.IO

Public Class GLDates

    Public Property GLMonth As Integer
    Public Property GLYear As Integer

End Class

Public Class SOHeader

    Public Property CustomerCode As String
    Public Property Description As String
    Public Property SODate As String
    Public Property SOLines As New SOLine

End Class
Public Class SOLine

    Public Property ItemCode As String
    Public Property ItemDescription As String
    Public Property ItemQuantity As String
    Public Property ItemAmount As String

End Class

Public Module LoadSO

    Private Property DT As New DataTable
    Private Property lstSOHeader As New List(Of SOHeader)

    Public Function LoadFile(ByVal FilePath As String) As List(Of SOHeader)

        LoadDatatable(FilePath)
        LoadSO()

        Return lstSOHeader

    End Function
    Private Sub LoadDatatable(ByVal csvFilePath As String)

        'csvFilePath = "C:\Users\ryan.hardouin.RITZYIT\Documents\New Job Card.csv"
        Try

            Dim dir As String = IO.Path.GetDirectoryName(csvFilePath)
            Dim file As String = IO.Path.GetFileName(csvFilePath)
            Dim name As String = IO.Path.GetFileNameWithoutExtension(csvFilePath)
            DT = New DataTable(name)
            Dim constring As String = String.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=""text;HDR={1};FMT=Delimited"";", dir, "Yes")
            Dim selStr As String = "Select * From [" & file & "]"

            Using da As New Data.OleDb.OleDbDataAdapter(selStr, constring)

                da.Fill(DT)
                da.Dispose()

            End Using

        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try

    End Sub
    Private Function LoadSO() As List(Of SOHeader)

        For Each Row As DataRow In DT.Rows

            Dim SOH As New SOHeader

            SOH.CustomerCode = Row("AccountCode")
            SOH.SODate = Row("InvoiceDate")
            SOH.Description = Row("InvoiceDescription")

            Dim SOL As New SOLine

            SOL.ItemAmount = Row("Amount")
            SOL.ItemCode = Row("ItemCode")
            SOL.ItemDescription = Row("ItemDescription")
            SOL.ItemQuantity = Row("Quantity")

            SOH.SOLines = SOL

            lstSOHeader.Add(SOH)

        Next

        Return lstSOHeader

    End Function

End Module

Public Module CreateSO

    Public Sub unHold(ByVal lstSOHeader As List(Of SOHeader))

        For Each SOH As SOHeader In lstSOHeader

            changeOnHoldStatus(SOH.CustomerCode, False)

        Next

    End Sub

    Public Sub ProcessSO(ByVal lstSOHeader As List(Of SOHeader), ByVal filePath As String)

        Dim ErrorList As New List(Of String)

        SDKOperations.CreateConnection()

        For Each SOH As SOHeader In lstSOHeader

            Try


                Dim PASSO As New SalesOrder
                Dim PASCust As New Customer(SOH.CustomerCode)
                Dim AD As New Address
                'Dim bIsOnHold As Boolean = False

                'bIsOnHold = isOnHold(SOH.CustomerCode)

                'If bIsOnHold Then

                '    changeOnHoldStatus(SOH.CustomerCode, False)

                'End If

                PASSO.Customer = PASCust
                PASSO.InvoiceDate = SOH.SODate

                If SOH.Description.Length > 39 Then
                    PASSO.Description = SOH.Description.Substring(0, 39)
                Else
                    PASSO.Description = SOH.Description
                End If
                PASSO.MessageLine3 = "Rejection Fee Invoice (" & DateTime.Today.ToString("MMMM") & " " & DateTime.Today.ToString("yyyy") & ")"

                AD.Line1 = PASCust.PhysicalAddress.Line1
                AD.Line2 = PASCust.PhysicalAddress.Line2
                AD.Line3 = PASCust.PhysicalAddress.Line3
                AD.Line4 = PASCust.PhysicalAddress.Line4
                AD.Line5 = PASCust.PhysicalAddress.Line5
                AD.Line6 = PASCust.PhysicalAddress.Line6
                AD.PostalCode = PASCust.PhysicalAddress.PostalCode

                PASSO.DeliverTo = AD

                AD.Line1 = PASCust.PhysicalAddress.Line1
                AD.Line2 = PASCust.PhysicalAddress.Line2
                AD.Line3 = PASCust.PhysicalAddress.Line3
                AD.Line4 = PASCust.PhysicalAddress.Line4
                AD.Line5 = PASCust.PhysicalAddress.Line5
                AD.Line6 = PASCust.PhysicalAddress.Line6
                AD.PostalCode = PASCust.PhysicalAddress.PostalCode

                PASSO.InvoiceTo = AD

                Dim OD As New OrderDetail

                PASSO.Detail.Add(OD)

                OD.GLAccount = New GLAccount(SOH.SOLines.ItemCode)
                OD.Description = SOH.SOLines.ItemDescription
                OD.Quantity = SOH.SOLines.ItemQuantity
                OD.ToProcess = SOH.SOLines.ItemQuantity
                OD.UnitSellingPrice = SOH.SOLines.ItemAmount
                OD.TaxType = New TaxRate(1)

                PASSO.Process()

                'If bIsOnHold Then

                '    changeOnHoldStatus(SOH.CustomerCode, True)

                'End If

            Catch ex As Exception

                ErrorList.Add(ex.Message & "| Account :" & SOH.CustomerCode)

            End Try

        Next

        If ErrorList.Count > 0 Then
            WriteFile(ErrorList, filePath)
        End If

    End Sub

    Public Sub placeOnHold(ByVal lstSOHeader As List(Of SOHeader))

        For Each SOH As SOHeader In lstSOHeader

            changeOnHoldStatus(SOH.CustomerCode, True)

        Next

    End Sub

    Private Sub WriteFile(ByVal Errors As List(Of String), ByVal filePath As String)

        If Not Directory.Exists(filePath & "\TextFiles\RejectionFee - Errors" & (System.DateTime.Today).ToString("dd-MMM-yyyy") & ".txt") Then

            Directory.CreateDirectory(filePath & "\TextFiles\RejectionFee - Errors" & (System.DateTime.Today).ToString("dd-MMM-yyyy") & ".txt")

        End If

        Dim SW As New StreamWriter(filePath & "\TextFiles\RejectionFee - Errors" & (System.DateTime.Today).ToString("dd-MMM-yyyy") & ".txt")

        For Each Item As String In Errors

            SW.WriteLine(Item)

        Next

        SW.Close()
        SW.Dispose()

        Process.Start(filePath & "\TextFiles\RejectionFee - Errors" & (System.DateTime.Today).ToString("dd-MMM-yyyy") & ".txt")

    End Sub

End Module

'UNBLOCKS AND BLOCKS GL ACCOUNT PERIODS
Public Module AccountPreiodCheck

    Private dbconn As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)


    'Gets IDs
    Public Function unBlockAccountingPeriods(ByVal lstSOHear As List(Of SOHeader)) As List(Of String)

        Dim index As Integer = 0
        Dim DT As New DataTable
        Dim lstID As New List(Of String)

        Dim qry As String = "SELECT" & vbCrLf & _
                            "idPeriod" & vbCrLf & _
                            "FROM" & vbCrLf & _
                            "_etblPeriod " & vbCrLf & _
                            "WHERE" & vbCrLf & _
                            "bBlocked = '1'" & vbCrLf & _
                            "AND" & vbCrLf & _
                            "("

        For Each glDate In getGLDates(getInvDates(lstSOHear))

            index = index + 1

            If index = 1 Then

                qry = qry + _
                        "(" & vbCrLf & _
                        "MONTH(dPeriodDate) = " & glDate.GLMonth & vbCrLf & _
                        "AND" & vbCrLf & _
                        "YEAR(dPeriodDate) = " & glDate.GLYear & vbCrLf & _
                        ")" & vbCrLf & _
                        ""

            Else

                qry = qry + _
                            "OR" & vbCrLf & _
                        "(" & vbCrLf & _
                        "MONTH(dPeriodDate) = " & glDate.GLMonth & vbCrLf & _
                        "AND" & vbCrLf & _
                        "YEAR(dPeriodDate) = " & glDate.GLYear & vbCrLf & _
                        ")" & vbCrLf & _
                        ""

            End If

        Next

        qry = qry + ")"

        'dbconn.createNewConnection()
        DT = dbconn.get_datatable(qry)
        dbconn.CloseConnection()

        For Each id In DT.Rows

            lstID.Add(id("idPeriod"))

        Next

        Return lstID

    End Function

    'Get dates from file - remove duplicates
    Private Function getInvDates(ByVal lstSOHear As List(Of SOHeader)) As List(Of String)

        'Declare variables
        Dim lstDates As New List(Of String)

        For Each invDate In lstSOHear

            lstDates.Add(invDate.SODate)

        Next

        lstDates = lstDates.Distinct().ToList

        Return lstDates

    End Function
    'Gets Months and Years
    Private Function getGLDates(ByVal lstFileDates As List(Of String)) As List(Of GLDates)

        Dim lstGLDate As New List(Of GLDates)
        Dim exists As Boolean = False

        For Each fileDate In lstFileDates

            Dim glDates As New GLDates

            glDates.GLMonth = Month(fileDate)
            glDates.GLYear = Year(fileDate)
            exists = False

            If lstGLDate.Count > 0 Then

                For Each addedDate In lstGLDate

                    If addedDate.GLMonth = glDates.GLMonth And addedDate.GLYear = glDates.GLYear Then

                        exists = True

                    End If

                Next

            End If

            If exists = False Then
                lstGLDate.Add(glDates)
            End If


        Next

        Return lstGLDate

    End Function


    'Updates Data
    Public Sub changeGLPeriodBlock(ByVal lstID As List(Of String), ByVal blockType As String)

        Dim qry As String = "UPDATE _etblPeriod" & vbCrLf & _
                            "SET bBlocked = '" & blockType & "'" & vbCrLf & _
                            "WHERE" & vbCrLf & _
                            "idPeriod IN ("
        Dim index As Integer = 0

        For Each id In lstID

            index = index + 1

            If index = 1 Then

                qry = qry & id

            Else

                qry = qry & "," & id

            End If

        Next

        qry = qry & ")"

        dbconn.setSqlString(qry)
        dbconn.createNewConnection()
        dbconn.CloseConnection()

    End Sub

    Public Sub changeOnHoldStatus(ByVal accCode As String, ByVal flag As Boolean)

        'Declare Variables
        Dim strQry As String = ""
        Dim iFlag As Integer = 2

        If flag Then

            iFlag = 1

        Else

            iFlag = 0

        End If

        strQry = "update client set On_Hold = " & iFlag & " where Account = '" & accCode & "'  and ubARSuspendService = 1"

        dbconn.setSqlString(strQry)
        dbconn.createNewConnection()
        dbconn.CloseConnection()

    End Sub

    'Checks
    Public Function isOnHold(ByVal accCode As String) As Boolean

        'Declare Variables
        Dim bIsOnHold As Boolean = False
        Dim strSQL As String = ""
        Dim DT As New DataTable
        Dim strOnHold As String = ""

        strSQL = "select On_Hold  from client where Account = '" & accCode & "'"

        DT = dbconn.get_datatable(strSQL)

        For Each row As DataRow In DT.Rows

            'strOnHold = row("On_Hold")

            'If strOnHold.Equals("True") Then

            bIsOnHold = row("On_Hold")

            'Else

            'bIsOnHold = False

            'End If


        Next

        Return bIsOnHold

    End Function

End Module