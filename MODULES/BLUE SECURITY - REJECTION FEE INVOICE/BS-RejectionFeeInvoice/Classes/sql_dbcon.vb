﻿Imports System.Data
Imports System.Data.SqlClient

Public Class sql_dbcon

    'sql data reader
    Public sqldr As SqlDataReader
    Dim sqlconn As New SqlConnection()
    Dim sqlcmd As New SqlCommand()

    Dim DBInstance As String
    Dim dbname As String
    Dim dbuser As String
    Dim dbpass As String
    Private strSqlQuery As String
    Private strConnectionString As String

    'INITILIZE 
    Public Sub New(ByVal database_server As String,
                   ByVal database_name As String,
                   ByVal database_user As String,
                   ByVal database_password As String)

        DBInstance = database_server
        dbname = database_name
        dbuser = database_user
        dbpass = database_password

        strConnectionString = "Data Source=" & DBInstance & ";Initial Catalog=" & dbname & ";User Id=" & dbuser & ";Password=" & dbpass & ";Connection Timeout=10;"


    End Sub

    'SET/GET SQL STRING
    Public Sub setSqlString(ByVal sqlString As String)

        strSqlQuery = sqlString

    End Sub
    Public Function serverdb_exits() As Boolean

        Dim exists As Boolean = False
        strSqlQuery = "select * from sys.tables"
        Try
            sqlconn.ConnectionString = strConnectionString
            sqlconn.Open()


            sqlcmd.Connection = sqlconn
            sqlcmd.CommandText = strSqlQuery

            '            sqldr = sqlcmd.ExecuteReader
            exists = True

            CloseConnection()
        Catch ex As Exception

            CloseConnection()
            exists = False
        End Try

        Return exists
    End Function

    'SET/GET CONNECTION STRING
    Public Sub setConnectionString(ByVal ConnectionString As String)

        strConnectionString = ConnectionString

    End Sub
    Public Function getConnectionString() As String

        Return strConnectionString

    End Function
    'CREATE/CLOSE CONNECTION
    Public Sub createNewConnection()

        Try
            sqlconn.ConnectionString = strConnectionString
            sqlconn.Open()

            sqlcmd.Connection = sqlconn
            sqlcmd.CommandText = strSqlQuery

            sqldr = sqlcmd.ExecuteReader

        Catch ex As Exception
            MsgBox(ex.Message + " " & strSqlQuery)
            CloseConnection()
        End Try


    End Sub
    Public Sub CloseConnection()
        Try

            sqlconn.Close()
            sqlconn.Dispose()
            sqlcmd.Dispose()

        Catch ex As Exception

            sqlconn.Dispose()
            sqlcmd.Dispose()

        End Try

    End Sub

    'RETURN/CLOSE SQL DR
    Public Function getSqlDr()

        Return sqldr

    End Function
    Public Sub closeSqlDr()

        sqldr.Close()

    End Sub

    'RUN SQL COMMANDS
    Public Function executeNonQuery()

        Dim sqlCon As New SqlConnection(strConnectionString)
        Dim nonquery As New SqlCommand(strSqlQuery, sqlCon)
        Dim queryMessage As String = "Failed"

        Try
            sqlCon.Open()

            nonquery.CommandText = strSqlQuery
            nonquery.ExecuteNonQuery()

            queryMessage = "Succesful"

            sqlCon.Close()
            sqlCon.Dispose()
            nonquery.Dispose()

        Catch ex As Exception
            queryMessage = "Failed"

            sqlCon.Dispose()
            nonquery.Dispose()
            CloseConnection()
            'AutoManufacture.Dispose()
            'AutoManufacture.Close()
        End Try

        Return queryMessage


    End Function

    'RETURNS VALUES
    Public Function get_value(ByVal fieldName As String) As String

        'declares variable to be returned
        Dim strValue As String
        'sets value to be returned if no data is read
        strValue = "-no value-"

        Try

            While sqldr.Read
                'returns this value if everything happens correctly and there is a value
                strValue = sqldr(fieldName)
                If IsDBNull(strValue) Then
                    strValue = ""
                End If
            End While

            CloseConnection()

        Catch ex As Exception

            strValue = "-error reading-"
            CloseConnection()

        End Try

        'sqldr.Close()

        'returns value
        Return strValue

    End Function
    Public Function get_values(ByRef fieldname As String) As ArrayList
        'declares variable to be returned
        Dim strValues As New ArrayList
        'sets value to be returned if no data is read
        Try

            While sqldr.Read
                'returns this value if everything happens correctly and there is a value
                strValues.Add(sqldr(fieldname))

            End While

            CloseConnection()

        Catch ex As Exception
            strValues.Clear()
            strValues.Add("error!")
            CloseConnection()

        End Try

        Return strValues
    End Function
    Public Function get_all() As ArrayList
        'declares variable to be returned
        Dim values As New ArrayList
        'sets value to be returned if no data is read
        Try

            While sqldr.Read
                'returns this value if everything happens correctly and there is a value


                For i As Integer = 0 To sqldr.FieldCount - 1

                    values.Add(sqldr.Item(i))
                Next

            End While

            CloseConnection()

        Catch ex As Exception
            values.Clear()
            values.Add("error!")
            CloseConnection()

        End Try

        Return values

    End Function

    'RETURN DATATABLE
    Public Function get_datatable(ByVal strSQL As String) As DataTable

        Dim dt As New DataTable
        setSqlString(strSQL)
        createNewConnection()
        dt.Load(sqldr)
        CloseConnection()
        Return dt

    End Function

    'RETURN DATASET
    Public Function Return_Dataset(ByVal strSQL As String) As DataSet

        Dim ds As New DataSet
        ds.Tables.Add(get_datatable(strSQL))
        Return ds
    End Function

End Class
