﻿Imports Pastel.Evolution

Public Class LoadOrders

    Dim Conn As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)

    Public Sub LoadOrderInfo(ByVal lstOrder As List(Of Order), ByVal EmailFolderPath As String, ByVal PostFolderPath As String)

        For Each OrderHeader As Order In lstOrder


            Try

                'CHECK IF THE CLIENT REQUIRES AN INVOICE
                '  If CheckInvoiceRequired(OrderHeader.AccountCode) Then

                'CHECKING PREFERRED CORRESPONDANCE (POST/EMAIL)

                'Preferred  = EMAIL
                If CheckPreferredCorrespondence(OrderHeader.AccountID) Then

                    If CheckPreferredReport(OrderHeader.AccountCode) Then

                        'CREATING FANCY PDFS FOR EMAILING
                        Dim frmInvoiceRep As New frmInvoiceReporter(OrderHeader.OrderNumber, EmailFolderPath)

                        frmInvoiceRep = Nothing

                    Else

                        'CREATING SIMPLE PDFS FOR EMAILING
                        Dim frmInvoiceSimpleRep As New frmInvoiceSimpleReporter(OrderHeader.OrderNumber, EmailFolderPath)

                        frmInvoiceSimpleRep = Nothing

                    End If

                End If

                '  End If

            Catch ex As Exception
                MsgBox("#4 General failure creating PDF invoice: " & ex.ToString)
            End Try

        Next

        For Each OrderHeader As Order In lstOrder

            Try

                'CHECK IF THE CLIENT REQUIRES AN INVOICE
                '  If CheckInvoiceRequired(OrderHeader.AccountCode) Then

                'Preffered  = POST
                If CheckPreferredCorrespondencePost(OrderHeader.AccountID) Then

                    'CREATE POST PDF INVOICE
                    Dim frmInvoicePost As New frmInvoicePostReporter(OrderHeader.OrderNumber, PostFolderPath)

                    frmInvoicePost = Nothing

                End If

                ' End If

            Catch ex As Exception
                MsgBox("#4 General failure creating PDF invoice: " & ex.ToString)
            End Try

        Next

    End Sub

    Public Function CheckInvoiceRequired(ByVal AccountCode As String) As Boolean

        Dim SQLQuery As String
        Dim Required As Boolean

        SQLQuery = "select ubARInvoiceRequired from Client" & vbCrLf & _
                    "where Account = '" & AccountCode & "'"

        Conn.setSqlString(SQLQuery)
        Conn.createNewConnection()
        Required = Conn.get_value("ubARInvoiceRequired")

        Return Required

    End Function
    Private Function CheckPreferredCorrespondence(ByVal AccountId As String) As Boolean

        Dim SQLQuery As String
        Dim strValue As String
        Dim blnValue As Boolean = False

        SQLQuery = "select ulARPrefCorr from client" & vbCrLf & _
                    "where dclink = " & AccountId

        Conn.setSqlString(SQLQuery)
        Conn.createNewConnection()
        strValue = Conn.get_value("ulARPrefCorr")
        Conn.CloseConnection()

        If strValue.Equals("EMAIL") Then

            blnValue = True

        Else

            blnValue = False

        End If

        Return blnValue

    End Function
    Private Function CheckPreferredCorrespondencePost(ByVal AccountId As String) As Boolean

        Dim SQLQuery As String
        Dim strValue As String
        Dim blnValue As Boolean = False

        SQLQuery = "select ulARPrefCorr from client" & vbCrLf & _
                    "where dclink = " & AccountId

        Conn.setSqlString(SQLQuery)
        Conn.createNewConnection()
        strValue = Conn.get_value("ulARPrefCorr")
        Conn.CloseConnection()

        If strValue.Equals("POST") Then

            blnValue = True

        Else

            blnValue = False

        End If

        Return blnValue

    End Function
    Private Function CheckPreferredReport(ByVal AccountCode As String) As Boolean

        Dim SQLQuery As String
        Dim PrefCorrespondance As String
        Dim IsNormal As Boolean = True

        SQLQuery = "select ulARPreferredReport from Client" & vbCrLf & _
                    "where Account = '" & AccountCode & "'"

        Conn.setSqlString(SQLQuery)
        Conn.createNewConnection()
        PrefCorrespondance = Conn.get_value("ulARPreferredReport")
        Conn.CloseConnection()

        If PrefCorrespondance.Equals("Normal") Then

            IsNormal = True

        Else

            IsNormal = False

        End If

        Return IsNormal

    End Function

End Class
