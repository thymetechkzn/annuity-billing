﻿Public Class SQLInterface

    Dim DBCon As New sql_dbcon(My.Settings.DBServer, My.Settings.PASCompany, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function GetOrderNums(ByVal DateFirst As String, ByVal InvType As String) As List(Of Order)

        Dim lstOrders As New List(Of Order)
        Dim SQLQuery As String = ""
        Dim DT As New DataTable
        Dim LastDate As String = ""
        Dim FirstDate As String = ""

        SQLQuery = "select" & vbCrLf & _
                    "OrderNum," & vbCrLf & _
                    "AutoIndex," & vbCrLf & _
                    "InvNumber," & vbCrLf & _
                    "ExtOrderNum," & vbCrLf & _
                    "C.Account," & vbCrLf & _
                    "AccountID from InvNum" & vbCrLf & _
                    "inner join" & vbCrLf & _
                    "Client C" & vbCrLf & _
                    "ON" & vbCrLf & _
                    "InvNum.AccountID = C.DcLink" & vbCrLf & _
                    "where" & vbCrLf & _
                    "(Message2 LIKE '%RECRUN_" & (MonthName(Convert.ToDateTime(DateFirst).Month)).ToUpper & " " & Convert.ToDateTime(DateFirst).Year & "_" & InvType & "%')" & vbCrLf & _
                    "ORDER BY Account ASC"


        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Dim OrderHeader As New Order

            OrderHeader.OrderNumber = Row("OrderNum")
            OrderHeader.OrderID = Row("AutoIndex")
            OrderHeader.InvoiceNumber = Row("InvNumber")
            OrderHeader.ExtOrderNumber = Row("ExtOrderNum") 'NEW PO NUMBERS
            OrderHeader.AccountCode = Row("Account")
            OrderHeader.AccountID = Row("AccountID")

            lstOrders.Add(OrderHeader)

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return lstOrders

    End Function

    Public Function GetCompanyDetails() As Company

        Dim BlueComp As New Company
        Dim SQLQuery As String = ""
        Dim DT As New DataTable

        SQLQuery = "select" & vbCrLf & _
                    "PhAddress1," & vbCrLf & _
                    "PhAddress2," & vbCrLf & _
                    "PhAddress3," & vbCrLf & _
                    "PHPostalCode," & vbCrLf & _
                    "PoAddress1," & vbCrLf & _
                    "POAddress2," & vbCrLf & _
                    "POAddress3," & vbCrLf & _
                    "POPostalCode," & vbCrLf & _
                    "Telephone1," & vbCrLf & _
                    "Fax," & vbCrLf & _
                    "BankName," & vbCrLf & _
                    "BankAccount," & vbCrLf & _
                    "BranchCode" & vbCrLf & _
                    "from entities"

        DT = DBCon.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            BlueComp.Physical1 = Row("PhAddress1")
            BlueComp.Physical2 = Row("PhAddress2")
            BlueComp.PhysicalPostCode = Row("PHPostalCode")
            BlueComp.Post1 = Row("PoAddress1")
            BlueComp.Post2 = Row("POAddress2")
            BlueComp.PostPostCode = Row("POPostalCode")
            BlueComp.TelephoneNumber = Row("Telephone1")
            BlueComp.FaxNumber = Row("Fax")
            ''BlueComp.VATno = Row("Tax_Number") 'Not Needed
            ''BlueComp.Registration = Row("Registration") 'Not needed
            BlueComp.BankName = Row("BankName")
            BlueComp.BankAccount = Row("BankAccount")
            BlueComp.BranchCode = Row("BranchCode")

        Next

        DT.Clear()
        DT.Dispose()
        DT = Nothing

        Return BlueComp

    End Function

    Public Function getDebitOrderRuns() As DataTable

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable

        qry = "SELECT TOP 1" & vbCrLf & _
                "invDescriptionSuffix " & vbCrLf & _
                "FROM" & vbCrLf & _
                "runMonth" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "isCompleted = 1" & vbCrLf & _
                "ORDER BY monthID DESC"

        'qry = "SELECT" & vbCrLf & _
        '        "invDescriptionSuffix" & vbCrLf & _
        '        "FROM" & vbCrLf & _
        '        "runMonth" & vbCrLf & _
        '        "WHERE" & vbCrLf & _
        '        "isCompleted = 1"

        dt = DBCon.get_datatable(qry)

        Return dt

    End Function

End Class
