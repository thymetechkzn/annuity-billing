﻿Public Class frmInvRepMain

    Dim objFrmImport As frmImportFile

    Sub New(ByVal dBInstance As String, _
            ByVal dBName As String, _
            ByVal dBUser As String, _
            ByVal dBPassword As String, _
            ByVal evoCommon As String, _
            ByVal EmailInvoiceSubject As String, _
            ByVal EmailInvocieBody As String, _
            ByVal EmailInvoiceTestMode As Boolean, _
            ByVal EmailInvocieTestAddress As String)

        ' This call is required by the designer.
        InitializeComponent()

        My.Settings.DBServer = dBInstance
        My.Settings.PASCompany = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.PASCommon = evoCommon
        My.Settings.EmailInvoiceSubject = EmailInvoiceSubject
        My.Settings.EmailInvoiceBody = EmailInvocieBody
        My.Settings.EmailInvoiceTestMode = EmailInvoiceTestMode
        My.Settings.EmailInvoiceTestAddress = EmailInvocieTestAddress

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub frmMain_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        LoadImportScreen()
    End Sub

    Private Sub LoadImportScreen()

        objFrmImport = New frmImportFile(My.Settings.DBServer, _
                                         My.Settings.PASCompany, _
                                         My.Settings.DBUser, _
                                         My.Settings.DBPassword, _
                                         My.Settings.PASCommon, _
                                         Me, _
                                         My.Settings.EmailInvoiceSubject, _
                                        My.Settings.EmailInvoiceBody, _
                                        My.Settings.EmailInvoiceTestMode, _
                                        My.Settings.EmailInvoiceTestAddress)

        objFrmImport.TopLevel = False
        objFrmImport.TopMost = False
        objFrmImport.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        objFrmImport.Dock = DockStyle.Fill
        pnFormContainer.Controls.Add(objFrmImport)
        objFrmImport.Show()

    End Sub
    Private Sub ShowImportScreen()
        objFrmImport.Show()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

End Class