﻿Imports Pastel.Evolution
Imports System.IO
Imports Microsoft.Reporting.WinForms

Public Class frmInvoiceReporter

    Private OrderNumber As String
    Private FilePath As String
    Private FPath As String
    Private SIMNumber As String

    Public Sub New(ByVal OrderNumber As String, ByVal FilePath As String)

        ' This call is required by the designer.
        InitializeComponent()

        Try

        ' Add any initialization after the InitializeComponent() call.
        'SAVE PASSED VARIABLES
            Me.OrderNumber = OrderNumber
            Me.FilePath = FilePath
            Me.FPath = FilePath
            LoadInvoiceDetails()

            OrderNumber = Nothing
            FilePath = Nothing

            Me.Close()

        Catch ex As Exception
            MsgBox("#3 Failed to create PDF: " & ex.ToString)
        End Try

    End Sub

    Private Sub LoadInvoiceDetails()

        Dim FileLog As New clsLog(FilePath, "Invoice Report")
        Dim invoice As New Invoice
        Dim comp As New Details.Company
        Dim order As New Details.Order(OrderNumber)

        Try

            invoice.Order.AccountCode = order.CustomerCode
            invoice.Order.TransactionDate = order.TransactionDate.Replace("-", "/")

            'SETTING ORDER HEADER
            invoice.Order.InvoiceType = order.InvoiceType
            SIMNumber = order.InvoiceType
            invoice.Order.OurReference = order.OurReference
            invoice.Order.TotalExclusive = Strings.FormatNumber(order.TotalExclusive, 2, TriState.True, TriState.True, TriState.True)
            invoice.Order.TotalTax = Strings.FormatNumber(order.TotalTax, 2, TriState.True, TriState.True, TriState.True)
            invoice.Order.TotalInclusive = Strings.FormatNumber(order.TotalInclusive, 2, TriState.True, TriState.True, TriState.True)
            invoice.Order.TotalDiscount = Strings.FormatNumber(order.TotalDiscount, 2, TriState.True, TriState.True, TriState.True)
            invoice.Order.InvoicePost1 = order.InvoicePost1
            invoice.Order.InvoicePost2 = order.InvoicePost2
            invoice.Order.InvoicePost3 = order.InvoicePost3
            invoice.Order.InvoicePost4 = order.InvoicePost4
            invoice.Order.InvoicePost5 = order.InvoicePost5
            invoice.Order.InvoicePost6 = order.InvoicePost6
            invoice.Order.InvoicePhysical1 = order.InvoicePhysical1
            invoice.Order.InvoicePhysical2 = order.InvoicePhysical2
            invoice.Order.InvoicePhysical3 = order.InvoicePhysical3
            invoice.Order.InvoicePhysical4 = order.InvoicePhysical4
            invoice.Order.InvoicePhysical5 = order.InvoicePhysical5
            invoice.Order.InvoicePhysical6 = order.InvoicePhysical6
            invoice.Order.OrderNumber = order.ExtOrderNumber 'order.OrderNo
            invoice.Order.ExtOrderNumber = order.ExtOrderNumber  'NEW PO NUMBERS
            invoice.Order.SalesRepCode = order.SalesRepCode

            'SETTING CUSTOMER DETAILS
            invoice.Customer.Code = order.CustomerCode
            invoice.Customer.Name = order.CustomerName
            invoice.Customer.TaxNumber = order.CustomerTaxNumber
            invoice.Customer.Email = order.Email

            'SETTING COMPANY DETAILS
            invoice.Company.AccountName = comp.AccountName
            invoice.Company.BranchCode = comp.BranchCode
            invoice.Company.BankAccount = comp.BankAccount
            invoice.Company.BankName = comp.BankName
            invoice.Company.TelephoneNumber = comp.Telephone1
            invoice.Company.VATno = comp.Tax_Number
            invoice.Company.FaxNumber = comp.Fax
            invoice.Company.Post1 = comp.PostalAddress1 & ", " & comp.PostalAddress2 & ", " & comp.PostalPostCode
            invoice.Company.Physical1 = comp.Physical1 & ", " & comp.Physical2 & ", " & comp.physicalPostCode
            invoice.Company.Registration = comp.Registration

        Catch ex As Exception
            FileLog.CreateLog("#5 General error occurred: " & ex.ToString & "Invoice Headers")
        End Try

        Try

            'LOOPING THROUGH ORDER DETAIL LINES
            For i As Integer = 0 To order.OrderItems.Count - 1
                'SETTING ORDER DETAIL LINES
                Dim orderItem As Details.OrderItems
                orderItem = order.OrderItems(i)
                Dim objOrderLine As New OrderLine

                objOrderLine.ItemCode = orderItem.ItemCode
                objOrderLine.ItemDescription = orderItem.ItemDescription.ToUpper
                objOrderLine.Price = Strings.FormatNumber(orderItem.Price, 2, TriState.True, TriState.True, TriState.True)
                objOrderLine.Qty = orderItem.Quantity
                objOrderLine.Tax = Strings.FormatNumber(orderItem.Tax, 2, TriState.True, TriState.True, TriState.True)
                objOrderLine.TotalInc = Strings.FormatNumber(orderItem.TotalInclusive, 2, TriState.True, TriState.True, TriState.True)
                objOrderLine.TotalLineExcl = Strings.FormatNumber(orderItem.TotalLineExcl, 2, TriState.True, TriState.True, TriState.True)

                invoice.OrderLines.Add(objOrderLine)

            Next

        Catch ex As Exception
            FileLog.CreateLog("#6 General error occurred: " & ex.ToString & "Invoice Lines")
        End Try

        Try
            'BINDING THE DATA TO THE REPORT
            CustomerBindingSource.Add(invoice.Customer)
            OrderBindingSource.Add(invoice.Order)
            CompanyBindingSource.Add(invoice.Company)

            For Each ol As OrderLine In invoice.OrderLines
                OrderLineBindingSource.Add(ol)
            Next
        Catch ex As Exception
            FileLog.CreateLog("#7 General error occurred: " & ex.ToString & "Invoice Binding")
        End Try

        'SAVING THE REPORT TO PDF
        save()


        'SENDING EMAILS AND PDF ATTACHMENTS
        Dim Emailer As New Email
        Emailer.SendEmail(invoice.Customer.Email, My.Settings.EmailInvoiceSubject, FilePath, My.Settings.EmailInvoiceBody, SIMNumber, FPath, invoice.Customer.Code, invoice.Customer.Name) 'TRISH REMOVE


        comp = Nothing
        order = Nothing
        invoice = Nothing

        CustomerBindingSource.Clear()
        OrderBindingSource.Clear()
        CompanyBindingSource.Clear()
        OrderLineBindingSource.Clear()

        CustomerBindingSource = Nothing
        OrderBindingSource = Nothing
        CompanyBindingSource = Nothing
        OrderLineBindingSource = Nothing

        '*****SPECIAL RUN APRIL 
        Emailer = Nothing
        '*****SPECIAL RUN APRIL 

        FilePath = Nothing

    End Sub

    Public Sub save()

        'SAVE PDF to FilePath

        Dim warnings As Warning() = Nothing
        Dim streamids As String() = Nothing
        Dim mimeType As String = Nothing
        Dim encoding As String = Nothing
        Dim extension As String = Nothing
        Dim byteviewer As Byte()
        Dim deviceinfo As String = "<DeviceInfo><OutputFormat>PDF</OutputFormat><PageWidth>8.5in</PageWidth><PageHeight>11in</PageHeight><MarginTop>0in</MarginTop><MarginLeft>0in</MarginLeft><MarginRight>0in</MarginRight><MarginBottom>0in</MarginBottom></DeviceInfo>"

        Try


            ReportViewer1.LocalReport.EnableHyperlinks = True

            'SAVE REPORT AT ARRAY OF BYTES
            byteviewer = ReportViewer1.LocalReport.Render("PDF", deviceinfo, mimeType, encoding, extension, streamids, warnings)

            'CREATE PDF WITH ARRAY BYTES
            Dim fs As New FileStream(FilePath & SIMNumber & ".pdf", FileMode.Create)
            FilePath = FilePath & SIMNumber & ".pdf"
            fs.Write(byteviewer, 0, byteviewer.Length)

            fs.Close()
            fs.Dispose()
            fs = Nothing

            Array.Clear(byteviewer, 0, byteviewer.Length)
            byteviewer = Nothing

            ReportViewer1.LocalReport.Refresh()
            ReportViewer1.LocalReport.DataSources.Clear()
            ReportViewer1.LocalReport.ReleaseSandboxAppDomain()
            ReportViewer1.Reset()
            ReportViewer1.Clear()

            warnings = Nothing
            streamids = Nothing
            mimeType = Nothing
            encoding = Nothing
            extension = Nothing
            deviceinfo = Nothing

        Catch ex As Exception
            MsgBox("#2 A general error occurred creating PDF's: " & ex.ToString & "(" & ex.Message & ")")
        End Try

    End Sub

    Private Sub frmInvoiceReporter_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class