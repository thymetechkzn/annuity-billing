﻿
Imports System.IO

Public Class frmImportFile

    ' INVOICE REPORT - Import File

    Dim LstOrders As New List(Of Order)
    Dim EmailFolderPath As String
    Dim PostFolderPath As String
    Dim frmMain As frmInvRepMain

    Sub New(ByVal dBInstance As String, _
            ByVal dBName As String, _
            ByVal dBUser As String, _
            ByVal dBPassword As String, _
            ByVal evoCommon As String, _
            ByVal MainMenu As frmInvRepMain, _
            ByVal EmailInvoiceSubject As String, _
            ByVal EmailInvocieBody As String, _
            ByVal EmailInvoiceTestMode As Boolean, _
            ByVal EmailInvocieTestAddress As String)

        ' This call is required by the designer.
        InitializeComponent()

        My.Settings.DBServer = dBInstance
        My.Settings.PASCompany = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.PASCommon = evoCommon
        Me.frmMain = MainMenu

        My.Settings.EmailInvoiceSubject = EmailInvoiceSubject
        My.Settings.EmailInvoiceBody = EmailInvocieBody
        My.Settings.EmailInvoiceTestMode = EmailInvoiceTestMode
        My.Settings.EmailInvoiceTestAddress = EmailInvocieTestAddress

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub BackgroundWorker1_DoWork(sender As System.Object, e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork
        Dim LoadInfo As New LoadOrders

        LoadInfo.LoadOrderInfo(LstOrders, EmailFolderPath, PostFolderPath)

        LstOrders.Clear()
        LstOrders = Nothing

            If Me.InvokeRequired Then
            Me.Invoke(New MethodInvoker(AddressOf CloseProcessing))
            MsgBox("Generation complete.")
            Else
                Me.Close()
            End If

    End Sub

    Private Sub CloseProcessing() Handles BackgroundWorker1.RunWorkerCompleted
        frmProcessing.Close()
    End Sub

    Private Sub btnGenerate_Click(sender As System.Object, e As System.EventArgs) Handles btnGenerate.Click

        Dim Repository As New SQLInterface
        Dim LoadInfo As New LoadOrders
        Dim PeriodRunDate As String = ""

        Dim FolderBrowser As New FolderBrowserDialog

        If rbRadioLic.Checked = True Then

            If FolderBrowser.ShowDialog = Windows.Forms.DialogResult.OK Then

                frmInvRepMain.WindowState = FormWindowState.Minimized

                EmailFolderPath = FolderBrowser.SelectedPath & "\Invoice " & MonthName(Today.Month, False) & "\Radio Licence Email Invoice\"
                PostFolderPath = FolderBrowser.SelectedPath & "\Invoice " & MonthName(Today.Month, False) & "\Radio Licence Post Invoice\"

                If Not Directory.Exists(EmailFolderPath) Then

                    Directory.CreateDirectory(EmailFolderPath)

                End If

                If Not Directory.Exists(PostFolderPath) Then

                    Directory.CreateDirectory(PostFolderPath)

                End If
            End If
        End If

        If rbMonthlyBilling.Checked = True Then

            If FolderBrowser.ShowDialog = Windows.Forms.DialogResult.OK Then

                frmInvRepMain.WindowState = FormWindowState.Minimized

                EmailFolderPath = FolderBrowser.SelectedPath & "\Invoice " & MonthName(Today.Month, False) & "\Monthly Billing Email Invoice\"
                PostFolderPath = FolderBrowser.SelectedPath & "\Invoice " & MonthName(Today.Month, False) & "\Monthly Billing Post Invoice\"

                If Not Directory.Exists(EmailFolderPath) Then

                    Directory.CreateDirectory(EmailFolderPath)

                End If

                If Not Directory.Exists(PostFolderPath) Then

                    Directory.CreateDirectory(PostFolderPath)

                End If

            End If

        End If

        If rbRadioLic.Checked = True Then

            Dim rad As String = "R"

            ' rbRadioLic.Text = rad

            LstOrders = Repository.GetOrderNums(ComboBox1.SelectedItem, rad)

        ElseIf rbMonthlyBilling.Checked = True Then

            Dim inv As String = "I"

            'rbMonthlyBilling.Text = inv

            LstOrders = Repository.GetOrderNums(ComboBox1.SelectedItem, inv)

        End If


        'LoadInfo.LoadOrderInfo(LstOrders, EmailFolderPath, PostFolderPath)

        'LstOrders.Clear()
        'LstOrders = Nothing

        BackgroundWorker1.RunWorkerAsync()
        frmProcessing.WindowState = FormWindowState.Normal
        frmProcessing.Show()



    End Sub

    Private Sub frmImportFile_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load

        'Dim month
        'Dim i = 1
        'Do While i <= 12
        '    month = MonthName(i, True)
        '    ComboBox1.Items.Add(month & " " & Today.Year)
        '    i = i + 1
        'Loop

        'Declare Variables
        Dim repLoadDORuns As New SQLInterface
        Dim dr As DataRow

        For Each dr In repLoadDORuns.getDebitOrderRuns().Rows

            ComboBox1.Items.Add(dr("invDescriptionSuffix"))

        Next


        btnGenerate.Enabled = False
       
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.frmMain.Close()
    End Sub

    Private Sub EmailSettingsToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles EmailSettingsToolStripMenuItem.Click

        Dim frmNewSettings As New frmEmailInvoiceSettings
        frmNewSettings.ShowDialog()

    End Sub

    Private Sub rbRadioLic_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbRadioLic.CheckedChanged

  btnGenerate.Enabled = True

    End Sub

    Private Sub rbMonthlyBilling_CheckedChanged(sender As System.Object, e As System.EventArgs) Handles rbMonthlyBilling.CheckedChanged

     
            btnGenerate.Enabled = True

    End Sub
End Class