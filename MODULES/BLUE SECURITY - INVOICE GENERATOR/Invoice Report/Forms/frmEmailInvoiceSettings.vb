﻿Public Class frmEmailInvoiceSettings

    Private EmailSubject As String
    Private EmailBody As String
    Private EmailTestMode As Boolean
    Private EmailTestAddress As String

    Private Sub btnSaveEmailSettings_Click(sender As System.Object, e As System.EventArgs) Handles btnSaveEmailSettings.Click

        My.Settings.EmailInvoiceSubject = txtSubject.Text
        My.Settings.EmailInvoiceBody = txtBody.Text
        My.Settings.EmailInvoiceTestMode = ckbTestMode.CheckState
        My.Settings.EmailInvoiceTestAddress = txtTestEmailAddress.Text

        My.Settings.Save()

        MsgBox("Email Settings have been Saved")

        Me.Close()

    End Sub

    Private Sub frmEmailSettings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtSubject.Text = My.Settings.EmailInvoiceSubject
        txtBody.Text = My.Settings.EmailInvoiceBody
        txtTestEmailAddress.Text = My.Settings.EmailInvoiceTestAddress
        If My.Settings.EmailInvoiceTestMode = True Then
            ckbTestMode.CheckState = CheckState.Checked
        Else
            ckbTestMode.CheckState = CheckState.Unchecked
        End If
  
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

End Class