﻿
Namespace Details
    Public Class OrderItems

        Public Property ItemCode As String
        Public Property ItemDescription As String
        Public Property Price As String
        Public Property Quantity As String
        Public Property Tax As String
        Public Property TotalInclusive As String
        Public Property TotalLineExcl As String

    End Class
End Namespace

