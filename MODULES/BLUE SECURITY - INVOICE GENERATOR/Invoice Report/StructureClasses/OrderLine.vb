﻿Public Class OrderLine

    Public Property ItemCode As String
    Public Property ItemDescription As String
    Public Property Qty As String
    Public Property Price As String
    Public Property Tax As String
    Public Property TotalInc As String
    Public Property TotalLineExcl As String

End Class
