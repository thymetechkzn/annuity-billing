﻿Imports System.Reflection
Imports Microsoft.Office.Interop

Public Class Email

    Dim newMail As Outlook.MailItem
    Dim tempApp As Outlook.Application

    Public Sub SendEmail(ByVal ToAddress As String, _
                           ByVal Subject As String, _
                           ByVal AttachmentPath As String, _
                           ByVal Body As String, _
                           ByVal InvoiceNumber As String, _
                           ByVal FilePath As String, _
                           ByVal AccountCode As String, _
                           ByVal AccountName As String)

        Dim logger As New LogFile(FilePath, "Emailer " & (System.DateTime.Today).ToString("dd-MM-yyyy"))
        Dim strLog As String = ""

        InvoiceNumber = InvoiceNumber.Replace("INR", "").Trim()

        Try

            tempApp = CreateObject("Outlook.Application")
            newMail = tempApp.CreateItem(0)
            With newMail
                If My.Settings.EmailInvoiceTestMode Then
                    .To = My.Settings.EmailInvoiceTestAddress
                    '.CC = CCAddress
                    .Subject = AccountName & " " & InvoiceNumber & " " & AccountCode
                    'My.Settings.EmailInvoiceSubject & " " & InvoiceNumber & " " & AccountCode
                    .Body = My.Settings.EmailInvoiceBody.ToString
                    '.HTMLBody = My.Settings.EmailBody
                    .Attachments.Add(AttachmentPath)
                    logger.CreateLog("EMAIL SENT TO " & .To & " FOR :" & ToAddress)
                    .Send()
                Else
                    .To = ToAddress
                    '.CC = CCAddress
                    .Subject = AccountName & " " & InvoiceNumber & " " & AccountCode
                    'My.Settings.EmailInvoiceSubject & " " & InvoiceNumber & " " & AccountCode
                    .Body = My.Settings.EmailInvoiceBody.ToString
                    '.HTMLBody = My.Settings.EmailBody
                    .Attachments.Add(AttachmentPath)
                    logger.CreateLog("EMAIL SENT TO " & .To & " FOR :" & ToAddress)
                    .Send()
                End If

            End With

        Catch ex As Exception

        End Try

        'dispose of objects
        newMail = Nothing
        tempApp = Nothing

    End Sub

End Class