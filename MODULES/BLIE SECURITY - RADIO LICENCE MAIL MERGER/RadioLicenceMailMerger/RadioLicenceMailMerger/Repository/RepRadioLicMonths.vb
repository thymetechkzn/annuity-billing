﻿Public Class RepRadioLicMonths

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    'LOAD INCREASE MONTHS
    Public Function loadRadioLicMonths() As DataTable

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable

        'QUERY: 'Gets all distinct increase months from 
        qry = "select" & vbCrLf & _
               "ulARRadioLicFeeBillMonth as 'IncreaseMonths'" & vbCrLf & _
               "from client" & vbCrLf & _
               "where ulARRadioLicFeeBillMonth <> ''" & vbCrLf & _
               "group by ulARRadioLicFeeBillMonth" & vbCrLf & _
               "order by DATEPART(mm,CAST((ulARRadioLicFeeBillMonth) + ' 1900' AS DATETIME)) asc"

        dt = dbConn.get_datatable(qry)

        Return dt

    End Function

End Class
