﻿Public Class RepExcel

    Public Sub ExportData(ByVal dgvExcelExport As DataGridView, ByVal filename As String, ByVal selectedPath As String) 'This Method converts Datagridview Tables into Cells and columns of an Excel Sheet

        'Declare and instantiate variables
        'Dim SelectedPath As String
        'Dim fileName As String
        'Dim f As FolderBrowserDialog = New FolderBrowserDialog

        'set properties
        'f.Description = "Please select file Destination"
        'f.RootFolder = Environment.SpecialFolder.MyDocuments

        Try
            'If f.ShowDialog() = DialogResult.OK Then

            '    fileName = InputBox("Please Enter a file name", "File Name", "PRICE UPDATER - " & (System.DateTime.Today).ToString("ddMMMyyyy") & _
            '                         "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm"))

            '    SelectedPath = f.SelectedPath.ToString
            'This section help you if your language is not English.
            System.Threading.Thread.CurrentThread.CurrentCulture = _
            System.Globalization.CultureInfo.CreateSpecificCulture("en-UK")
            Dim oExcel As Microsoft.Office.Interop.Excel.Application
            Dim oBook As Microsoft.Office.Interop.Excel.Workbook
            Dim oSheet As Microsoft.Office.Interop.Excel.Worksheet
            oExcel = CreateObject("Excel.Application")
            oBook = oExcel.Workbooks.Add(Type.Missing)
            oSheet = oBook.Worksheets(1)

            Dim dc As DataGridViewColumn
            Dim dr As DataGridViewRow
            Dim colIndex As Integer = 0
            Dim rowIndex As Integer = 0

            'Style Columns
            'Dim style As Microsoft.Office.Interop.Excel.Style = oSheet.Application.ActiveWorkbook.Styles.Add("NewStyle")
            'style.Font.Bold = True
            'style.Interior.Color = System.Drawing.ColorTranslator.ToOle(System.Drawing.Color.LightBlue)


            'Export the Columns to excel file
            For Each dc In dgvExcelExport.Columns
                colIndex = colIndex + 1
                oSheet.Cells(1, colIndex) = dc.Name
                ' oSheet.Cells(1, colIndex).Style = "NewStyle"
            Next

            'Export the rows to excel file
            For Each dr In dgvExcelExport.Rows
                rowIndex = rowIndex + 1
                colIndex = 0
                For Each dc In dgvExcelExport.Columns
                    colIndex = colIndex + 1
                    oSheet.Cells(rowIndex + 1, colIndex) = dr.Cells(dc.Name).Value
                Next
            Next

            'Set final path
            Dim FileExtension As String = ".XLS"


            fileName = fileName + FileExtension
            '"PRICE UPDATER - " + (System.DateTime.Today).ToString("ddMMMyyyy") + FileExtension


            Dim finalPath = SelectedPath + "\" + fileName
            oSheet.Columns.AutoFit()
            'Save file in final path
            oBook.SaveAs(finalPath, Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal, Type.Missing, _
            Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, _
            Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing)

            'Release the objects
            ReleaseObject(oSheet)
            oBook.Close(False, Type.Missing, Type.Missing)
            ReleaseObject(oBook)
            oExcel.Quit()
            ReleaseObject(oExcel)
            'Some time Office application does not quit after automation: 
            'so i am calling GC.Collect method.
            GC.Collect()

            MessageBox.Show("Export done successfully!")

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try
    End Sub
    Private Sub ReleaseObject(ByVal o As Object)
        Try
            While (System.Runtime.InteropServices.Marshal.ReleaseComObject(o) > 0)
            End While
        Catch
        Finally
            o = Nothing
        End Try

    End Sub

    Public Sub ImportData(ByVal dgvExcelImport As DataGridView)

        'Dim conn As System.Data.OleDb.OleDbConnection
        'Dim ds As System.Data.DataSet
        'Dim cmd As System.Data.OleDb.OleDbDataAdapter
        'Dim fileName As String
        'Dim f As FolderBrowserDialog = New FolderBrowserDialog

        ''set properties
        'f.Description = "Please Select file to import"
        'f.RootFolder = Environment.SpecialFolder.MyDocuments


        'If f.ShowDialog() = DialogResult.OK Then

        '    fileName = OpenFileDialog1.InitialDirectory + OpenFileDialog1.FileName
        '    conn = New System.Data.OleDb.OleDbConnection("provider=Microsoft.Jet.OLEDB.4.0;Data Source='" & fileName & "';Extended Properties=Excel 8.0;")
        '    cmd = New System.Data.OleDb.OleDbDataAdapter("select * from [Sheet1$]", conn)
        '    ds = New System.Data.DataSet

        '    cmd.Fill(ds)
        '    dgvExcelImport.Columns.Clear()
        '    dgvExcelImport.Refresh()
        '    dgvExcelImport.DataSource = ds.Tables(0)
        '    conn.Close()


        'End If


    End Sub


End Class
