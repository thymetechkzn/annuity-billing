﻿Public Class RepUpdatePrices

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    '------------------------UPDATE CONTRACT SERVICE FEE------------------------
    Public Function updateContractServiceFee(ByVal newPrice As Double, ByVal accountCode As String)

        'Declare Variables
        Dim qry As String = ""

        Try

            qry = "update" & vbCrLf & _
                  "Client" & vbCrLf & _
                  "set" & vbCrLf & _
                  "ufARMonthlyServFeeValue = " & newPrice & vbCrLf & _
                  "where" & vbCrLf & _
                  "Account = '" & accountCode & "'"

            dbConn.setSqlString(qry)
            dbConn.createNewConnection()
            dbConn.executeNonQuery()
            dbConn.CloseConnection()

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function
    '------------------------UPDATE CONTRACT SMS FEE------------------------
    Public Function updateContractSMSFee(ByVal newPrice As Double, ByVal accountCode As String)

        'Declare Variables
        Dim qry As String = ""

        Try

            qry = "update" & vbCrLf & _
                  "Client" & vbCrLf & _
                  "set" & vbCrLf & _
                  "ufAROpenCloseSMSValue = " & newPrice & vbCrLf & _
                  "where" & vbCrLf & _
                  "Account = '" & accountCode & "'"

            dbConn.setSqlString(qry)
            dbConn.createNewConnection()
            dbConn.executeNonQuery()
            dbConn.CloseConnection()

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function
    '------------------------UPDATE CONTRACT REPORT FEE------------------------
    Public Function updateContractReportFee(ByVal newPrice As Double, ByVal accountCode As String)

        'Declare Variables
        Dim qry As String = ""

        Try

            qry = "update" & vbCrLf & _
                  "Client" & vbCrLf & _
                  "set" & vbCrLf & _
                  "ufARMonthlyReportsValue = " & newPrice & vbCrLf & _
                  "where" & vbCrLf & _
                  "Account = '" & accountCode & "'"

            dbConn.setSqlString(qry)
            dbConn.createNewConnection()
            dbConn.executeNonQuery()
            dbConn.CloseConnection()

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function
    '------------------------UPDATE CONTRACT SURVEILANCE FEE------------------------
    Public Function updateContractSurveilanceFee(ByVal newPrice As Double, ByVal accountCode As String)

        'Declare Variables
        Dim qry As String = ""


        Try

            qry = "update" & vbCrLf & _
                                    "Client" & vbCrLf & _
                                    "set" & vbCrLf & _
                                    "ufARRemoteSurvValue = " & newPrice & vbCrLf & _
                                    "where" & vbCrLf & _
                                    "Account = '" & accountCode & "'"

            dbConn.setSqlString(qry)
            dbConn.createNewConnection()
            dbConn.executeNonQuery()
            dbConn.CloseConnection()

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function
    '------------------------UPDATE CONTRACT ANGELS FEE------------------------
    Public Function updateContractAngelsFee(ByVal newPrice As Double, ByVal accountCode As String)

        'Declare Variables
        Dim qry As String = ""

        Try

            qry = "update" & vbCrLf & _
                                   "Client" & vbCrLf & _
                                   "set" & vbCrLf & _
                                   "ufARBlueAngelsTrustValue = " & newPrice & vbCrLf & _
                                   "where" & vbCrLf & _
                                   "Account = '" & accountCode & "'"

            dbConn.setSqlString(qry)
            dbConn.createNewConnection()
            dbConn.executeNonQuery()
            dbConn.CloseConnection()

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function
    '------------------------UPDATE ADHOC FEE------------------------
    Public Function updateAdhocFee(ByVal newPrice As Double, ByVal accountCode As String, ByVal itemCode As String)

        'Declare Variables
        Dim qry As String = ""

        Try

            qry = "update" & vbCrLf & _
                  "_rtblStockLinks" & vbCrLf & _
                  "set cProductReference = " & newPrice & vbCrLf & _
                  "where" & vbCrLf & _
                  "iDCLink = " & vbCrLf & _
                            "(select" & vbCrLf & _
                            "DCLink" & vbCrLf & _
                            "from" & vbCrLf & _
                            "client" & vbCrLf & _
                            "where" & vbCrLf & _
                            "Account = '" & accountCode & "')" & vbCrLf & _
                  "and" & vbCrLf & _
                  "iStockID = " & vbCrLf & _
                            "	        (select" & vbCrLf & _
                            "	         StockLink " & vbCrLf & _
                            "	         from" & vbCrLf & _
                            "	         StkItem" & vbCrLf & _
                            "	         where" & vbCrLf & _
                            "	         Code = '" & itemCode & "')"

            dbConn.setSqlString(qry)
            dbConn.createNewConnection()
            dbConn.executeNonQuery()
            dbConn.CloseConnection()

        Catch ex As Exception
            Return False
        End Try

        Return True

    End Function


End Class
