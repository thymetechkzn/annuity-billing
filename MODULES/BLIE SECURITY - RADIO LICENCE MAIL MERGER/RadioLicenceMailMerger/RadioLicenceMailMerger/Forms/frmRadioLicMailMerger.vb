﻿Imports System.IO
Imports System.Data
Imports System.Data.OleDb

Public Class frmRadioLicMailMerger

    Dim dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)
    Dim isImport As Boolean = False
    Dim fileName As String = ""
    Dim SelectedPath As String
    Dim selectMonth As String
    Dim clientDT As New DataTable
    Dim dgvClient As New DataGridView
    Dim totalRows As String

    Sub New(ByVal dBInstance As String, ByVal dBName As String, _
       ByVal dBUser As String, ByVal dBPassword As String, ByVal evoCommon As String)

        ' This call is required by the designer.
        InitializeComponent()

        My.Settings.DBInstance = dBInstance
        My.Settings.DBName = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.EvolutionCommon = evoCommon

        dbConn = New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    '------------------------AUTO GENERATED METHODS------------------------
    Private Sub frmRadioLicMailMerger_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        refreshAll()
        loadRadioLicMonths()

    End Sub
    Private Sub btnProcessSelec_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcessSelec.Click

        'Enable Controls
        ExportExcellDocumentToolStripMenuItem.Enabled = True
        selectMonth = cboIncreaseMonth.SelectedItem.ToString()

        BackgroundWorker1.RunWorkerAsync()
        Timer1.Enabled = True
        Timer1.Start()

    End Sub
    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

        loadDataGridView()

        If Me.InvokeRequired Then
            Timer1.Stop()
            Timer1.Enabled = False
            Me.Invoke(New MethodInvoker(AddressOf loadDGV))
            Me.Invoke(New MethodInvoker(AddressOf clearProgressbar))
        Else
            MsgBox("The data has been retrieved.")
        End If

    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If pbPriceUpdater.Value >= pbPriceUpdater.Maximum Then
            pbPriceUpdater.Value = 0
        Else
            pbPriceUpdater.Value += 1
        End If

    End Sub
    Private Sub ExportExcellDocumentToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExportExcellDocumentToolStripMenuItem.Click

        'pbPriceUpdater.Value = 0

        Dim f As FolderBrowserDialog = New FolderBrowserDialog

        'set properties
        f.Description = "Please select file Destination"
        f.RootFolder = Environment.SpecialFolder.Desktop

        Try
            If f.ShowDialog() = DialogResult.OK Then

                fileName = InputBox("Please Enter a file name", "File Name", "RADIO LICENCE MAIL MERGER - " & (System.DateTime.Today).ToString("ddMMMyyyy") & _
                                     "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm"))

                SelectedPath = f.SelectedPath.ToString

            End If

            BackgroundWorker2.RunWorkerAsync()
            Timer2.Enabled = True
            Timer2.Start()

        Catch ex As Exception
            MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK)
        End Try

    End Sub
    Private Sub BackgroundWorker2_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker2.DoWork

        exportToExcel()

        If Me.InvokeRequired Then
            Timer2.Stop()
            Timer2.Enabled = False
            Me.Invoke(New MethodInvoker(AddressOf clearProgressbar))
        Else
            MsgBox("The data has been retrieved.")
        End If

    End Sub
    Private Sub Timer2_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer2.Tick

        If pbPriceUpdater.Value >= pbPriceUpdater.Maximum Then
            pbPriceUpdater.Value = 0
        Else
            pbPriceUpdater.Value += 1
        End If

    End Sub
    '------------LOADING GROUP CODES, ITEM CODES & INCREASE MONTHS------------
    Private Sub loadRadioLicMonths()

        'Declare Variable
        Dim radioMonths As New DataTable
        Dim repIncreaseMonths As New RepRadioLicMonths

        'Call Method that loads Increase Months
        radioMonths = repIncreaseMonths.loadRadioLicMonths()

        'Populate the Increase Months into checklist boxes
        For Each radioMonth As DataRow In radioMonths.Rows

            cboIncreaseMonth.Items.Add(radioMonth("IncreaseMonths"))

        Next

    End Sub
    '------------RETRIEVE GROUP CODES, ITEM CODES & INCREASE MONTHS SELECTED AND LOAD DATAGRIDVIEW------------
    Private Sub loadDataGridView() 

        'Declare Variables
        Dim repClientDetails As New RepClients

        'Load data from query into datatable
        clientDT = repClientDetails.getClients(selectMonth)

        'Display total rows in DataGridView
        If clientDT.Rows.Count > 0 Then

            For index As Integer = 0 To clientDT.Rows.Count

                totalRows = index

            Next

        End If

    End Sub
    Private Sub loadDGV() Handles BackgroundWorker1.RunWorkerCompleted

        dgvClients.DataSource = clientDT
        lblTotAcc.Text = "Total Number of Accounts: " & totalRows.ToString()

    End Sub
    Private Sub clearProgressbar() Handles BackgroundWorker1.RunWorkerCompleted

        pbPriceUpdater.Value = pbPriceUpdater.Maximum

    End Sub
    '------------CALCULATE NEW PRICE BY PERCENTAGE & EXPORT TO EXCEL------------
    Private Sub exportToExcel()

        'Declare Variables
        Dim repExportToExcel As New RepExcel

        repExportToExcel.ExportData(dgvClients, fileName, selectedPath)

    End Sub

    '------------REFRESH ALL CONTROLS------------
    Public Sub refreshAll()

        'Set Components
        btnProcessSelec.Enabled = True
        ExportExcellDocumentToolStripMenuItem.Enabled = False
        cboIncreaseMonth.Enabled = True
        lblTotalAccounts.Text = ""
        dgvClients.Columns.Clear()

        resetDGV()

    End Sub
    Private Sub resetDGV()

        dgvClients.CancelEdit()
        dgvClients.Columns.Clear()
        dgvClients.DataSource = Nothing

    End Sub
   
    Private Sub ExitToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ExitToolStripMenuItem.Click



        Me.Hide()

        Me.Close()


    End Sub
End Class