﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRadioLicMailMerger
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRadioLicMailMerger))
        Me.TableLayoutPanel6 = New System.Windows.Forms.TableLayoutPanel()
        Me.dgvClients = New System.Windows.Forms.DataGridView()
        Me.mnuFile = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExportExcellDocumentToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.lblIncreaseMonth = New System.Windows.Forms.Label()
        Me.cboIncreaseMonth = New System.Windows.Forms.ComboBox()
        Me.btnProcessSelec = New System.Windows.Forms.Button()
        Me.TableLayoutPanel5 = New System.Windows.Forms.TableLayoutPanel()
        Me.pbPriceUpdater = New System.Windows.Forms.ProgressBar()
        Me.lblTotalAccounts = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.BackgroundWorker1 = New System.ComponentModel.BackgroundWorker()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.lblTotAcc = New System.Windows.Forms.Label()
        Me.BackgroundWorker2 = New System.ComponentModel.BackgroundWorker()
        Me.Timer2 = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel6.SuspendLayout()
        CType(Me.dgvClients, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.mnuFile.SuspendLayout()
        Me.TableLayoutPanel5.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel6
        '
        Me.TableLayoutPanel6.ColumnCount = 1
        Me.TableLayoutPanel6.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Controls.Add(Me.dgvClients, 0, 0)
        Me.TableLayoutPanel6.Location = New System.Drawing.Point(12, 163)
        Me.TableLayoutPanel6.Name = "TableLayoutPanel6"
        Me.TableLayoutPanel6.RowCount = 1
        Me.TableLayoutPanel6.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel6.Size = New System.Drawing.Size(805, 298)
        Me.TableLayoutPanel6.TabIndex = 43
        '
        'dgvClients
        '
        Me.dgvClients.AllowUserToAddRows = False
        Me.dgvClients.AllowUserToDeleteRows = False
        Me.dgvClients.BackgroundColor = System.Drawing.SystemColors.Control
        Me.dgvClients.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvClients.Location = New System.Drawing.Point(3, 3)
        Me.dgvClients.Name = "dgvClients"
        Me.dgvClients.ReadOnly = True
        Me.dgvClients.Size = New System.Drawing.Size(799, 292)
        Me.dgvClients.TabIndex = 1
        '
        'mnuFile
        '
        Me.mnuFile.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.mnuFile.Font = New System.Drawing.Font("Calibri", 9.0!)
        Me.mnuFile.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.ExportToolStripMenuItem})
        Me.mnuFile.Location = New System.Drawing.Point(0, 0)
        Me.mnuFile.Name = "mnuFile"
        Me.mnuFile.Padding = New System.Windows.Forms.Padding(7, 2, 0, 2)
        Me.mnuFile.Size = New System.Drawing.Size(840, 24)
        Me.mnuFile.TabIndex = 44
        Me.mnuFile.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'ExportToolStripMenuItem
        '
        Me.ExportToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExportExcellDocumentToolStripMenuItem})
        Me.ExportToolStripMenuItem.Name = "ExportToolStripMenuItem"
        Me.ExportToolStripMenuItem.Size = New System.Drawing.Size(87, 20)
        Me.ExportToolStripMenuItem.Text = "File Transfer"
        '
        'ExportExcellDocumentToolStripMenuItem
        '
        Me.ExportExcellDocumentToolStripMenuItem.Name = "ExportExcellDocumentToolStripMenuItem"
        Me.ExportExcellDocumentToolStripMenuItem.Size = New System.Drawing.Size(302, 22)
        Me.ExportExcellDocumentToolStripMenuItem.Text = "Generate Increase Report / Export to Excel"
        '
        'lblIncreaseMonth
        '
        Me.lblIncreaseMonth.AutoSize = True
        Me.lblIncreaseMonth.Font = New System.Drawing.Font("Calibri", 11.25!)
        Me.lblIncreaseMonth.Location = New System.Drawing.Point(218, 50)
        Me.lblIncreaseMonth.Name = "lblIncreaseMonth"
        Me.lblIncreaseMonth.Size = New System.Drawing.Size(104, 18)
        Me.lblIncreaseMonth.TabIndex = 37
        Me.lblIncreaseMonth.Text = "Increase Month"
        '
        'cboIncreaseMonth
        '
        Me.cboIncreaseMonth.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboIncreaseMonth.FormattingEnabled = True
        Me.cboIncreaseMonth.Location = New System.Drawing.Point(339, 46)
        Me.cboIncreaseMonth.Name = "cboIncreaseMonth"
        Me.cboIncreaseMonth.Size = New System.Drawing.Size(204, 21)
        Me.cboIncreaseMonth.TabIndex = 38
        '
        'btnProcessSelec
        '
        Me.btnProcessSelec.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnProcessSelec.Enabled = False
        Me.btnProcessSelec.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnProcessSelec.Location = New System.Drawing.Point(3, 3)
        Me.btnProcessSelec.Name = "btnProcessSelec"
        Me.btnProcessSelec.Size = New System.Drawing.Size(199, 28)
        Me.btnProcessSelec.TabIndex = 22
        Me.btnProcessSelec.Text = "Process Selections"
        Me.btnProcessSelec.UseVisualStyleBackColor = False
        '
        'TableLayoutPanel5
        '
        Me.TableLayoutPanel5.ColumnCount = 1
        Me.TableLayoutPanel5.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Controls.Add(Me.btnProcessSelec, 0, 0)
        Me.TableLayoutPanel5.Location = New System.Drawing.Point(15, 116)
        Me.TableLayoutPanel5.Name = "TableLayoutPanel5"
        Me.TableLayoutPanel5.RowCount = 1
        Me.TableLayoutPanel5.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel5.Size = New System.Drawing.Size(205, 34)
        Me.TableLayoutPanel5.TabIndex = 40
        '
        'pbPriceUpdater
        '
        Me.pbPriceUpdater.Location = New System.Drawing.Point(243, 127)
        Me.pbPriceUpdater.Name = "pbPriceUpdater"
        Me.pbPriceUpdater.Size = New System.Drawing.Size(196, 23)
        Me.pbPriceUpdater.TabIndex = 41
        '
        'lblTotalAccounts
        '
        Me.lblTotalAccounts.AutoSize = True
        Me.lblTotalAccounts.Location = New System.Drawing.Point(516, 116)
        Me.lblTotalAccounts.Name = "lblTotalAccounts"
        Me.lblTotalAccounts.Size = New System.Drawing.Size(0, 13)
        Me.lblTotalAccounts.TabIndex = 42
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(9, 476)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(177, 13)
        Me.Label1.TabIndex = 45
        Me.Label1.Text = "© Copyright 2015 Ritzy IT (Pty) LTD"
        '
        'BackgroundWorker1
        '
        '
        'Timer1
        '
        '
        'lblTotAcc
        '
        Me.lblTotAcc.AutoSize = True
        Me.lblTotAcc.Location = New System.Drawing.Point(446, 136)
        Me.lblTotAcc.Name = "lblTotAcc"
        Me.lblTotAcc.Size = New System.Drawing.Size(0, 13)
        Me.lblTotAcc.TabIndex = 46
        '
        'BackgroundWorker2
        '
        '
        'Timer2
        '
        '
        'frmRadioLicMailMerger
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(840, 498)
        Me.Controls.Add(Me.lblTotAcc)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.mnuFile)
        Me.Controls.Add(Me.TableLayoutPanel6)
        Me.Controls.Add(Me.lblTotalAccounts)
        Me.Controls.Add(Me.pbPriceUpdater)
        Me.Controls.Add(Me.TableLayoutPanel5)
        Me.Controls.Add(Me.cboIncreaseMonth)
        Me.Controls.Add(Me.lblIncreaseMonth)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmRadioLicMailMerger"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Radio Licence Mail Merger"
        Me.TableLayoutPanel6.ResumeLayout(False)
        CType(Me.dgvClients, System.ComponentModel.ISupportInitialize).EndInit()
        Me.mnuFile.ResumeLayout(False)
        Me.mnuFile.PerformLayout()
        Me.TableLayoutPanel5.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents TableLayoutPanel6 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents dgvClients As System.Windows.Forms.DataGridView
    Friend WithEvents mnuFile As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExportExcellDocumentToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents lblIncreaseMonth As System.Windows.Forms.Label
    Friend WithEvents cboIncreaseMonth As System.Windows.Forms.ComboBox
    Friend WithEvents btnProcessSelec As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel5 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents pbPriceUpdater As System.Windows.Forms.ProgressBar
    Friend WithEvents lblTotalAccounts As System.Windows.Forms.Label
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents BackgroundWorker1 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents lblTotAcc As System.Windows.Forms.Label
    Friend WithEvents BackgroundWorker2 As System.ComponentModel.BackgroundWorker
    Friend WithEvents Timer2 As System.Windows.Forms.Timer

End Class
