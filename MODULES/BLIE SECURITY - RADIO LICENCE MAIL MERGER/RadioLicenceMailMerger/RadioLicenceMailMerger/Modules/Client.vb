﻿Public Class Client

    'Contract Details
    Public Property ContractNo As String
    Public Property AccountCode As String
    'Debtor Details
    Public Property DebitorAccountCode As String
    Public Property DebitorFirstName As String
    'More Details
    Public Property CrossReference As String
    'Address
    Public Property PostalAddress1 As String
    Public Property PostalAddress2 As String
    Public Property PostalAddress3 As String
    Public Property PostalAddress4 As String
    Public Property PostalCode As String
    'Contract Details
    Public Property ContractTitle As String
    Public Property ContractFirstName As String
    Public Property ContractSurname As String
    'Group & Item Codes
    Public Property GroupCode As String
    Public Property ItemCode As String
    'Prices
    Public Property CurrentPrice As Double
    Public Property PercentageUpdate As Double
    Public Property NewValueAdded As Double
    Public Property NewPrice As Double

End Class
