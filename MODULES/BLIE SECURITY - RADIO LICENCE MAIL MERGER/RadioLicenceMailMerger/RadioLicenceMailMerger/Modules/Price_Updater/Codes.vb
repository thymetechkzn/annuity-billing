﻿Public Class Codes

    Public Class GroupCodesDetails

        Public Property GroupCode As String
        Public Property GroupDescription As String

    End Class

    Public Class ItemCodesDetails

        Public Property ItemCode As String
        Public Property ItemDescription As String

    End Class

    Public Class IncreaseMonthsDetails

        Public Property IncreaseMonthName As String

    End Class

    Public Property GroupCode As List(Of GroupCodesDetails)
    Public Property ItemCode As List(Of ItemCodesDetails)
    Public Property IncreaseMonth As List(Of IncreaseMonthsDetails)

End Class
