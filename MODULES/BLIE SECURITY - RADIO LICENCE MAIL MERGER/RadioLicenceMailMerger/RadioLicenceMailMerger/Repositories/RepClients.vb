﻿Public Class RepClients

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)


    Public Function getClients(ByVal radioLicMonths As String) As DataTable

        'Declare Variables
        Dim qry As String = ""


        qry = "SELECT" & vbCrLf & _
                "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
                "C.Account AS 'Customer Code'," & vbCrLf & _
                "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
                "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account) AS 'Debtor Account Code'," & vbCrLf & _
                "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
                "C.ulARDebtorType AS 'Debtor Type'," & vbCrLf & _
                "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
                "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
                "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
                "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
                "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
                "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
                "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
                "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
                "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
                "C.Name as 'Name'," & vbCrLf & _
                "C.Title AS 'Contract Title'," & vbCrLf & _
                "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
                "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
                "a.Code as 'Group Code'," & vbCrLf & _
                "ISNULL(ulARRadioLicFeeBillMonth, '') as 'Customer Radio Fee Month'," & vbCrLf & _
                "ulARRadioLicFeePriceList AS 'Customer Radio Licence Fee Price Code'," & vbCrLf & _
                "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID " & vbCrLf & _
                "	= (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) as 'Customer Radio Licence Fee'," & vbCrLf & _
                "(select Description_1  from StkItem where StockLink = 1) as 'Customer Radio Description'" & vbCrLf & _
                "FROM" & vbCrLf & _
                "Client C" & vbCrLf & _
                "INNER JOIN" & vbCrLf & _
                "Areas A" & vbCrLf & _
                "ON" & vbCrLf & _
                "C.iAreasID = A.idAreas" & vbCrLf & _
                "where" & vbCrLf & _
                "C.ulARRadioLicFeeBillMonth = '" & radioLicMonths & "'" & vbCrLf & _
                "and" & vbCrLf & _
                "C.ubARContractActive = 1" & vbCrLf & _
                "and" & vbCrLf & _
                "ISNULL((select fExclPrice from _etblPriceListPrices where iStockID = 1 and iPriceListNameID " & vbCrLf & _
                "	= (Select IDPriceListName from _etblPriceListName where cName = ulARRadioLicFeePriceList)), 0) <> 0" & vbCrLf & _
                "Order By 'Debtor Account Code'"


        '"SELECT" & vbCrLf & _
        '                "C.ucARContractNo AS 'Contract No'," & vbCrLf & _
        '                "C.Account AS 'Customer Code'," & vbCrLf & _
        '                "case when C.ubARSuspendService = 1 then 'Yes' else 'No' end as 'Is Suspended'," & vbCrLf & _
        '                "ISNULL((SELECT DEB.Account FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Account) AS 'Debtor Account Code'," & vbCrLf & _
        '                "ISNULL((SELECT DEB.Name FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Name) AS 'Debtor Name'," & vbCrLf & _
        '                "C.ucARCrossRef AS 'Cross Reference'," & vbCrLf & _
        '                "C.udARContractFirstBillDate AS 'Contract Start Date'," & vbCrLf & _
        '                "C.ulARContractIncMonth AS 'Increase Month'," & vbCrLf & _
        '                "ISNULL((SELECT DEB.EMail FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.EMail) AS 'Email Address'," & vbCrLf & _
        '                "ISNULL((SELECT DEB.Post1 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post1) AS 'Postal Address 1'," & vbCrLf & _
        '                "ISNULL((SELECT DEB.Post2 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post2) AS 'Postal Address 2'," & vbCrLf & _
        '                "ISNULL((SELECT DEB.Post3 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post3) AS 'Postal Address 3'," & vbCrLf & _
        '                "ISNULL((SELECT DEB.Post4 FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.Post4) AS 'Postal Address 4'," & vbCrLf & _
        '                "ISNULL((SELECT DEB.PostPC FROM Client DEB WHERE C.MainAccLink = DEB.DCLink), C.PostPC) AS 'Postal Code'," & vbCrLf & _
        '                "C.Name as 'Name'," & vbCrLf & _
        '                "C.Title AS 'Contract Title'," & vbCrLf & _
        '                "C.ucAROrigContactFirstname AS 'Contract First Name'," & vbCrLf & _
        '                "C.ucAROrigContactSurname AS 'Contract Surname'," & vbCrLf & _
        '                "a.Code as 'Group Code'," & vbCrLf & _
        '                "'#SERFEE - Contract Fee' as 'ItemCode'," & vbCrLf & _
        '                "C.ufARMonthlyServFeeValue 'Current Value'" & vbCrLf & _
        '                "FROM" & vbCrLf & _
        '                "Client C" & vbCrLf & _
        '                "INNER JOIN" & vbCrLf & _
        '                "Areas A" & vbCrLf & _
        '                "ON" & vbCrLf & _
        '                "C.iAreasID = A.idAreas" & vbCrLf & _
        '                 "where" & vbCrLf & _
        '                 "C.ulARRadioLicFeeBillMonth = '" & radioLicMonths & "'" & vbCrLf & _
        '                  "and" & vbCrLf & _
        '                  "C.ubARContractActive = 1" & vbCrLf & _
        '                  "and" & vbCrLf & _
        '                  "C.ufARMonthlyServFeeValue <> 0" & vbCrLf & _
        '                "Order By 'Debtor Account Code'"

        Return dbConn.get_datatable(qry)

    End Function


End Class
