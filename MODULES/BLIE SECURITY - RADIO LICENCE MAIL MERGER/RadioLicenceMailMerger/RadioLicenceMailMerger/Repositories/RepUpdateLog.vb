﻿Imports System.IO
Public Class RepUpdateErrorLog

    Public Sub createAndWriteToFile(ByVal accDetails As List(Of DTAccountDetails))

        'Declare Variables
        Dim fileName As String = ""
        Dim path As String = ""
        Dim f As FolderBrowserDialog = New FolderBrowserDialog
        Dim numError As Integer = 0
        '
        f.Description = "Please select a file destination for the Error Log"
        f.RootFolder = Environment.SpecialFolder.MyDocuments

        Try

            If f.ShowDialog() = DialogResult.OK Then

                path = f.SelectedPath.ToString()

                'Get File Name prvided by User
                fileName = InputBox("Please Enter a file name", "File Name", "UPDATE LOG - " & (System.DateTime.Today).ToString("dd MMM yyyy") & _
                                    "-" & (System.DateTime.Now).ToString("HH") & "-" & (System.DateTime.Now).ToString("mm"))
                'Open Writer
                Dim dOWriter As New StreamWriter(path & "\" & fileName & ".XLS")

                'Write Header
                dOWriter.WriteLine("***********************PRICE UPDATER ERROR LOG***********************")

                'Write Lines
                For Each account As DTAccountDetails In accDetails

                    'If there is an error, write account code to file
                    If account.isUpdated = False Then
                        numError += 1
                        dOWriter.WriteLine(account.accountCode)
                    End If
                Next

                'If there are no errors that ocurred
                If numError = 0 Then

                    dOWriter.WriteLine("No Errors Occurred! All accounts were updated!")

                End If

                'Close Writer
                dOWriter.Close()

            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try




    End Sub

End Class
