﻿Imports System.IO
Imports System.Data
Imports System.Data.OleDb
Public Class frmDebitOrderGen

    'PLEASE CREATE A SETTING AND SETTINGS PAGE TO HOLD THIS INFORMATION
    Dim Conn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Dim intRunMonth As Integer
    Dim strRunMonth As String
    Dim DoRun As String
    Dim recRunName As String
    Dim dtReleaseDate As Date
    Dim strReleaseDate As String
    Dim path As String
    Dim DOfileName As String
    Sub New(ByVal dBInstance As String, ByVal dBName As String, ByVal dBCashManName As String, _
            ByVal dBUser As String, ByVal dBPassword As String, ByVal evoCommon As String)

        ' This call is required by the designer.
        InitializeComponent()

        My.Settings.DBInstance = dBInstance
        My.Settings.DBName = dBName
        My.Settings.DBUser = dBUser
        My.Settings.DBPassword = dBPassword
        My.Settings.EvolutionCommon = evoCommon
        My.Settings.CashInstance = dBInstance
        My.Settings.CashDBName = dBCashManName
        My.Settings.CashUser = dBUser
        My.Settings.CashPassword = dBPassword

        Conn = New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub frmDebitOrderGen_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        loadDebitOrderRuns()

    End Sub

    Private Sub cboDebitOrderRun_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboDebitOrderRun.SelectedIndexChanged

        'Declare Variables
        Dim repGetData As New repGetGUIData

        strRunMonth = cboDebitOrderRun.SelectedItem.ToString()

        intRunMonth = repGetData.getRunMonth(strRunMonth)

        dgvRun.DataSource = repGetData.loadDatagridview(cboDebitOrderRun.SelectedItem.ToString())


        dgvRun.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvRun.MultiSelect = False
        dgvRun.Rows(0).ReadOnly = True



    End Sub

    Private Sub dgvRun_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvRun.CellClick

        'Declare variables
        Dim getReleaseDate As New repSetDateRange

        'Enable Button
        btnProcess.Enabled = True

        'DoRun = dgvRun.Rows(e.RowIndex).Cells(0).Value
        recRunName = dgvRun.Rows(e.RowIndex).Cells(1).Value
        DoRun = dgvRun.Rows(e.RowIndex).Cells(0).Value


        dtReleaseDate = Date.Parse(getReleaseDate.getNoteDateLessThanOrEqual(DoRun, intRunMonth, True))


        If dtReleaseDate.DayOfWeek = DayOfWeek.Sunday Then

            dtReleaseDate = dtReleaseDate.AddDays(1)

        End If

        txtReleaseDate.Text = dtReleaseDate.ToString("yyyy-MM-dd")

    End Sub
    
    
    'CALLS 'MAIN METHOD'
    Private Sub btnProcess_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnProcess.Click

        'Declare Variables
        Dim fileDialog As FolderBrowserDialog = New FolderBrowserDialog

        pbDORun.Value = 0

        fileDialog.Description = "Please select a path to save the debit order file"
        fileDialog.RootFolder = Environment.SpecialFolder.Desktop

        Try

            If fileDialog.ShowDialog() = DialogResult.OK Then

                'Get Path
                path = fileDialog.SelectedPath.ToString()

                If Not Directory.Exists(path) Then

                    Directory.CreateDirectory(path)

                End If

                'Get release date
                strReleaseDate = InputBox("The release date for " & DoRun & " is, " & dtReleaseDate.ToString("yyyy-MM-dd") & "." & vbCrLf _
                                         & "Please Change if necessary.", "Release Date", dtReleaseDate.ToString("yyyy-MM-dd"))

                txtReleaseDate.Text = strReleaseDate

                strReleaseDate = Date.Parse(strReleaseDate).ToString("yyMMdd")

                'Get File Name
                DOfileName = InputBox("Please Enter a name for the debit order file.", "Debit Order File Name", "Debit Order File - " & DoRun & " " & txtReleaseDate.Text)

                txtPath.Text = path & "\" & DOfileName & ".txt"

                BackgroundWorker1.RunWorkerAsync()
                Timer1.Enabled = True
                Timer1.Start()


            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    'SUB PROCESS METHODS...

    'THIS IS THE MAIN METHOD THAT CALLS ALL SUB METHODS TO DO WORK
    Private Sub DoProcess(ByVal path As String, ByVal DOfileName As String)

        'Declare Variables
        Dim lstCheckedClients As New List(Of clsBankDetails)
        Dim lstClients As New List(Of clsBankDetails)
        Dim loadCashManAccs As New repLoadCashMangerAccounts
        Dim loadInvoiceAccs As New repLoadInvoiceAccounts
        Dim loadJobCardAccs As New repLoadJobCardAccounts
        Dim releaseDate As New repSetDateRange

        'PLEASE CHECK THIS CODE WORK 100%

        '        ALSO -- DO ANY OF THE METHODS BELOW CHECK FOR AN INVOICE OR JOB CARD CREATED AND FLAGGED TO BE PAYED BY D/O ???
        '   PLEASE CHECK

        lstCheckedClients.AddRange(loadCashManAccs.GetCashManagerContracts(DoRun))
        lstCheckedClients.AddRange(loadCashManAccs.GetCashManagerAdHoc(DoRun, intRunMonth))


        lstClients.AddRange(loadCashManAccs.CheckDoAccounts(lstCheckedClients, DoRun, intRunMonth))


        lstClients.AddRange(loadInvoiceAccs.GetContractClients(DoRun, recRunName))
        lstClients.AddRange(loadJobCardAccs.GetJobCardsDOAccounts(DoRun, intRunMonth, recRunName))
        lstClients.AddRange(loadJobCardAccs.GetJobCardsNonDOAccounts(DoRun, intRunMonth))
        lstClients.AddRange(loadInvoiceAccs.GetFlaggedInvoices(DoRun, intRunMonth, recRunName))
        lstClients.AddRange(loadInvoiceAccs.GetFlaggedInvoicesNonContract(DoRun, intRunMonth, recRunName))
        'WRITE D/O INFORMATION TO FILE 

        'DO CHECKS FOR ACCOUNTS
        '*******************************************************************************************************

        '*****PLEASE DOUBLE CHECK THE VALUES DON'T CHANGE EVERY RUN

        modExportToExcelDO.ExportData(lstClients, strReleaseDate, path, DOfileName)

    End Sub
    'Populate Drop Down
    Private Sub loadDebitOrderRuns()

        'Declare Variables
        Dim repLoadDORuns As New repGetGUIData
        Dim dr As DataRow

        For Each dr In repLoadDORuns.getDebitOrderRuns().Rows

            cboDebitOrderRun.Items.Add(dr("invDescriptionSuffix"))

        Next

        cboDebitOrderRun.SelectedIndex = 0

    End Sub

    Private Sub BackgroundWorker1_DoWork(ByVal sender As System.Object, ByVal e As System.ComponentModel.DoWorkEventArgs) Handles BackgroundWorker1.DoWork

        DoProcess(path, DOfileName)

        If Me.InvokeRequired Then
            Timer1.Stop()
            Timer1.Enabled = False
            Me.Invoke(New MethodInvoker(AddressOf clearProgressbar))
            'MsgBox("Process Complete")
            MessageBox.Show("Process Complete")
            'pbDORun.Value = 0
        Else

            pbDORun.Value = 0

        End If

    End Sub

    Private Sub clearProgressbar() Handles BackgroundWorker1.RunWorkerCompleted

        pbDORun.Value = pbDORun.Maximum

    End Sub
    Private Sub Timer1_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Timer1.Tick

        If pbDORun.Value >= pbDORun.Maximum Then

            pbDORun.Value = 0

        Else

            pbDORun.Value += 1

        End If

    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click

        Me.Close()

    End Sub

    Private Sub ViewErrorLogToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ViewErrorLogToolStripMenuItem.Click

        System.Diagnostics.Process.Start(txtPath.Text & "\Error Log - " & strReleaseDate & ".txt")

    End Sub


End Class