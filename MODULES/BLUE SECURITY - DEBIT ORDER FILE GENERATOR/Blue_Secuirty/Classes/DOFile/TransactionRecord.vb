﻿Public Class TransactionRecord

    Public Property RecordID As String = "10" 'A -2
    Public Property UserBranch As String = "223626" 'B -6
    Public Property UserNominatedAccNo As String = "62154045423" 'C -11
    Public Property Filler1 As String = "B457" 'D -4
    Public Property SequnceNo As String = "000001" 'E -6
    Public Property HomingRecipientBranch As String = "250655" 'F -6
    Public Property HomingrecipientAccNo As String = "62370800859" 'G -11
    Public Property TypeOfAccount As String = "1" 'H
    Public Property Amount As String = "00000022329" 'I - 11
    Public Property Filler2 As String = "13102521" 'J - 8
    Public Property TaxCode As String = "0" 'K
    Public Property Filler3 As String = "00" 'L
    Public Property Filler4 As String = "0" 'M

    Public Property AbbreviatedUserName As String = "OOOOOOOOOO" 'O (Not N) - 10
    Public Property Collections As String = "NNNNNNNNNNNNNNNNNNNN" 'N - 20


    Public Property HomingRecipientAccName As String = "PPPPPPPPPPPPPPP" 'P - 15
    Public Property Filler6 As String = "QQQQQQQQQQQQQQQ" 'Q - 15
    Public Property NonStandardAccNo As String = "00000000000000000000" 'R - 20
    Public Property Filler7 As String = "                35            " 'S

    Public Function RecordValues() As String

        Dim strValues As String = ""

        strValues &= RecordID
        strValues &= UserBranch
        strValues &= UserNominatedAccNo
        strValues &= Filler1
        strValues &= SequnceNo
        strValues &= HomingRecipientBranch
        strValues &= HomingrecipientAccNo
        strValues &= TypeOfAccount
        strValues &= Amount
        strValues &= Filler2
        strValues &= TaxCode
        strValues &= Filler3
        strValues &= Filler4

        strValues &= AbbreviatedUserName
        strValues &= Collections

        strValues &= HomingRecipientAccName
        strValues &= Filler6
        strValues &= NonStandardAccNo
        strValues &= Filler7

        Return strValues

    End Function

End Class
