﻿Public Class MasterDebtor

    Public Property DCLink As Integer
    Public Property Account As String
    Public Property Name As String
    Public Property BranchCode As String
    Public Property BankAccNumber As String
    Public Property BankAccType As String
    Public Property Amount As Double
    Public Property Processed As Boolean = False
End Class

