﻿Public Class clsBankDetails

    Public Property Account As String
    Public Property BankAccountNumber As String
    Public Property AccountType As String
    Public Property BranchCode As String
    Public Property ClientName As String
    Public Property Amount As Decimal
    Public Property ActualAmount As Decimal
    Public Property Processed As Boolean = False
    Public Property RefNo As String = "0"

    Public Property MasterAcc As Integer


End Class