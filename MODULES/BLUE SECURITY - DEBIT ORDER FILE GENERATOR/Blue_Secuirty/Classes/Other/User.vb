﻿Public Class User

    Public Property UserID As Integer
    Public Property Username As String
    Public Property Password As String
    Public Property DebitOrderAccess As Boolean = False
    Public Property AnuityBillAcccess As Boolean = False
    Public Property PriceUpdateAccess As Boolean = False
    Public Property UserAdminAccess As Boolean = False

End Class
