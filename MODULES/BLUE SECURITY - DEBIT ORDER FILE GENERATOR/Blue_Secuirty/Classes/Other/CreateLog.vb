﻿Imports System.IO

Public Class CreateLog

    Public Sub CreateLogFile(ByVal path As String, ByVal Log As String)

        Dim fw As New StreamWriter(path)

        fw.Write(Log)

        fw.Close()
        fw.Dispose()

    End Sub

End Class
