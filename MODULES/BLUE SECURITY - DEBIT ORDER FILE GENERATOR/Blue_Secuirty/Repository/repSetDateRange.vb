﻿Public Class repSetDateRange

    'PLEASE CREATE A SETTING AND SETTINGS PAGE TO HOLD THIS INFORMATION
    Dim Conn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function getNoteMonthDayGreaterThan(ByVal DORunConfig As String, ByVal intRunMonth As Integer) As String

        'Declare Variables
        Dim currentConfig As String = ""
        Dim previousConfig As String = ""
        Dim intGreaterThanDay As Integer = 0
        Dim intGreaterThanMonth As Integer = intRunMonth
        Dim intYear As Integer = Date.Today.Year
        Dim sqlQry As String
        Dim DT As DataTable

        sqlQry = "SELECT" & vbCrLf & _
                "DISTINCT(C.ulARDORunDate) as 'DOConfig'" & vbCrLf & _
                "FROM" & vbCrLf & _
                "[" & My.Settings.DBName & "].dbo.Client C" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "C.ulARDORunDate LIKE '%DO%'" & vbCrLf & _
                "ORDER BY DOConfig"

        DT = Conn.get_datatable(sqlQry)

        'Set Month - If NOT DO15
        If Not (DORunConfig.Equals("DO15") Or DORunConfig.Equals("DO07")) Then

            If intRunMonth = 1 Then
                intRunMonth = 12
            Else
                intRunMonth = intRunMonth - 1
            End If
            intGreaterThanMonth = intRunMonth
        End If

        'If intRunMonth = 1 Then

        '    intRunMonth = 13

        'End If

        'Get Last Row for Default previousConfig Value
        previousConfig = Date.DaysInMonth(Today.Year, intRunMonth) '*********MONTH IS HARD CODED + YEAR*********

        For Each Row As DataRow In DT.Rows

            currentConfig = Row("DOConfig").ToString().Substring(0, 4).Trim()

            'Check if Config is equal to the DO Run Config
            If currentConfig.Equals(DORunConfig) Then

                'Set Previous DO Run Config
                If DORunConfig.Equals("DO31") Then
                    intGreaterThanDay = (Convert.ToInt32(Date.DaysInMonth(Today.Year, intRunMonth)) - 1)
                Else
                    intGreaterThanDay = Convert.ToInt32(previousConfig)
                    If DORunConfig.Equals("DO01") Then
                        '
                        'If intRunMonth = 1 Then

                            intYear = Date.Today.Year

                        'End If

                        intGreaterThanDay = (Convert.ToInt32(Date.DaysInMonth(Today.Year, intRunMonth)))
                    End If
                End If

            End If

            previousConfig = currentConfig.Substring(2, 2).Trim()

        Next

        Return intYear & "-" & intGreaterThanMonth.ToString() & "-" & intGreaterThanDay.ToString()

    End Function

    Public Function getNoteDateLessThanOrEqual(ByVal DORunConfig As String, ByVal intRunMonth As Integer, ByVal isReleaseDate As Boolean) As String

        'Delcare Variables
        Dim NoteDateLessEqual As String = ""


        If Not (DORunConfig.Equals("DO15") Or DORunConfig.Equals("DO01") Or DORunConfig.Equals("DO07")) Then

            If intRunMonth = 1 Then

                intRunMonth = 12
            Else
                intRunMonth = intRunMonth - 1

            End If

        End If

        If DORunConfig.Equals("DO30") Then

            NoteDateLessEqual = Today.Year & "-" & intRunMonth & "-" & (Convert.ToInt32(Date.DaysInMonth(Today.Year, intRunMonth)) - 1).ToString()

        ElseIf DORunConfig.Equals("DO31") Then

            NoteDateLessEqual = Today.Year & "-" & intRunMonth & "-" & Date.DaysInMonth(Today.Year, intRunMonth)

        ElseIf DORunConfig.Equals("DO01") Then

            If intRunMonth = 1 Then

                NoteDateLessEqual = (Convert.ToInt32(Today.Year) + 1).ToString() & "-" & 1 & "-" & DORunConfig.Substring(2, 2) '*********HARDCODE*********

            Else

                NoteDateLessEqual = Today.Year & "-" & intRunMonth & "-" & DORunConfig.Substring(2, 2)

            End If

        Else

            If intRunMonth = 13 Then
                If isReleaseDate = True Then
                    intRunMonth = 1
                    NoteDateLessEqual = Today.Year & "-" & intRunMonth & "-" & Convert.ToInt32(DORunConfig.Substring(2, 2))
                Else
                    intRunMonth = intRunMonth - 1
                    NoteDateLessEqual = Today.Year & "-" & intRunMonth & "-" & Convert.ToInt32(DORunConfig.Substring(2, 2))
                End If
            Else
                NoteDateLessEqual = Today.Year & "-" & intRunMonth & "-" & Convert.ToInt32(DORunConfig.Substring(2, 2))
            End If


        End If


        Return NoteDateLessEqual

    End Function

End Class
