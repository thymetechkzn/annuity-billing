﻿Public Class repLoadJobCardAccounts

    'PLEASE CREATE A SETTING AND SETTINGS PAGE TO HOLD THIS INFORMATION
    Dim Conn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    'Job Cards
    Public Function GetJobCardsDOAccounts(ByVal DOrun As String, ByVal intRunMonth As Integer, ByVal recRunMonthName As String) As List(Of clsBankDetails)

        Dim lstBankDets As New List(Of clsBankDetails)
        Dim SQLQuery As String
        Dim DT As New DataTable
        Dim accountFilter As String = ""
        Dim getDateRange As New repSetDateRange
        Dim index As Integer
        Dim year As String
        Dim intRunMonth2 As Integer
        Dim yearGreater As String = ""

        index = recRunMonthName.IndexOf(" ")
        year = recRunMonthName.Substring(index).Trim()
        yearGreater = year

        If intRunMonth = 1 Then

            intRunMonth2 = intRunMonth
            intRunMonth = 13
            yearGreater = year - 1

        End If

        Dim DOConfig As String

        Dim lastDayMonthGreater As Integer
        Dim lastDayMonthLess As Integer

        Dim noteDateGreater As String
        Dim noteDateLessThan As String

        DOConfig = (DOrun.Replace("DO", ""))

        lastDayMonthGreater = Date.DaysInMonth(yearGreater, (intRunMonth - 1))

        If DOConfig.Equals("31") Then

            If lastDayMonthGreater = 30 Then

                noteDateGreater = yearGreater & "-" & (intRunMonth - 1).ToString() & "-" & lastDayMonthGreater.ToString

            Else

                noteDateGreater = yearGreater & "-" & (intRunMonth - 1).ToString() & "-" & DOConfig

            End If

        Else

            noteDateGreater = yearGreater & "-" & (intRunMonth - 1).ToString() & "-" & DOConfig

        End If

        'getDateRange.getNoteMonthDayGreaterThan(DOrun, intRunMonth)  '*********FIND OUT IF THIS WILL BE A PROBLEM - FORMAT*********
        If intRunMonth = 13 Then

            intRunMonth = 1
        End If

        lastDayMonthLess = Date.DaysInMonth(year, (intRunMonth))

        If DOConfig.Equals("31") Then

            If lastDayMonthLess = 30 Then

                noteDateLessThan = year & "-" & (intRunMonth).ToString() & "-" & lastDayMonthLess

            Else

                noteDateLessThan = year & "-" & (intRunMonth).ToString() & "-" & DOConfig

            End If
        Else

            noteDateLessThan = year & "-" & (intRunMonth).ToString() & "-" & DOConfig

        End If

        SQLQuery = "SELECT" & vbCrLf &
                    "ISNULL((SELECT D.Account FROM [" & My.Settings.DBName & "].dbo.Client D WHERE C.MainAccLink = D.DCLink), C.Account) AS 'Account'," & vbCrLf &
                    "ISNULL((SELECT D.DCLink FROM  [" & My.Settings.DBName & "].dbo.Client D WHERE C.MainAccLink = D.DCLink), C.DCLink) AS 'DCLink'," & vbCrLf &
                    "ISNULL((SELECT D.Name FROM [" & My.Settings.DBName & "].dbo.Client D WHERE C.MainAccLink = D.DCLink), C.Name) AS 'Name'," & vbCrLf &
                    "--BANKING DETAILS" & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL(C.BRANCHCODE,'')" & vbCrLf &
                    "WHEN '' THEN '000000'" & vbCrLf &
                    "ELSE C.BranchCode" & vbCrLf &
                    "END" & vbCrLf &
                    ")as 'BRANCHCODE' ," & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL(C.BANKACCNUM, '')" & vbCrLf &
                    "WHEN '' THEN '00000000000'" & vbCrLf &
                    "ELSE C.BANKACCNUM" & vbCrLf &
                    "END" & vbCrLf &
                    ") as 'BANKACCNUM'," & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL(C.BANKACCTYPE, '')" & vbCrLf &
                    "WHEN '' THEN 'Current/Cheque Account'" & vbCrLf &
                    "ELSE C.BANKACCTYPE" & vbCrLf &
                    "END" & vbCrLf &
                    ") as 'BANKACCTYPE'," & vbCrLf &
                    "JN.InvTotIncl AS 'Amount'" & vbCrLf &
                    "FROM" & vbCrLf &
                    "[" & My.Settings.DBName & "].dbo.Client C" & vbCrLf &
                    "INNER JOIN" & vbCrLf &
                    "[" & My.Settings.DBName & "].dbo._btblJCMaster  JC" & vbCrLf &
                    "ON" & vbCrLf &
                    "C.DCLInk = JC.iClientId" & vbCrLf &
                    "INNER JOIN" & vbCrLf &
                    "[" & My.Settings.DBName & "].dbo.JobNum JN" & vbCrLf &
                    "ON" & vbCrLf &
                    "JC.IDJCMaster = JN.iJCMasterID" & vbCrLf &
                    "WHERE" & vbCrLf &
                    "JC.ubJCInclDO = 1" & vbCrLf &
                    "AND" & vbCrLf &
                    "C.ulardORunDate LIKE '%" & DOrun & "%'" & vbCrLf &
                    "AND" & vbCrLf &
                    "C.ulARPaymentMethod = 'D/O'" & vbCrLf &
                    "AND" & vbCrLf &
                    "(CONVERT(DATE, JC.dInvDate) > '" & noteDateGreater & "' AND CONVERT(DATE, JC.dInvDate) <= '" & noteDateLessThan & "')" & vbCrLf &
                    "AND" & vbCrLf &
                    "JN.DocType = 1"

        DT = Conn.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Dim BankDets As New clsBankDetails

            BankDets.Account = Row("ACCOUNT")
            BankDets.AccountType = Row("BANKACCTYPE")
            BankDets.Amount = Row("Amount")
            BankDets.BankAccountNumber = Row("BANKACCNUM")
            BankDets.BranchCode = Row("BRANCHCODE")
            BankDets.ClientName = Row("NAME")
            BankDets.MasterAcc = Row("DCLINK")

            lstBankDets.Add(BankDets)

        Next

        ''**********TEST**********
        'Dim totalAmount As Decimal = 0.0

        'For Each amount As clsBankDetails In lstBankDets

        '    totalAmount = totalAmount + amount.Amount

        'Next

        'MsgBox("Flagger Job Cards - Contract: R" & totalAmount)
        ''**********TEST**********

        Return lstBankDets

    End Function
    Public Function GetJobCardsNonDOAccounts(ByVal DOrun As String, ByVal intRunMonth As Integer) As List(Of clsBankDetails)

        'Declare Variables
        Dim lstBankDets As New List(Of clsBankDetails)
        Dim SQLQuery As String
        Dim DT As New DataTable
        Dim accountFilter As String = ""
        Dim getDateRange As New repSetDateRange

        Dim noteDateGreater As String = getDateRange.getNoteMonthDayGreaterThan(DOrun, intRunMonth)  '*********FIND OUT IF THIS WILL BE A PROBLEM - FORMAT*********
        Dim noteDateLessThan As String = getDateRange.getNoteDateLessThanOrEqual(DOrun, intRunMonth, False)

        SQLQuery = "SELECT" & vbCrLf &
                    "ISNULL((SELECT D.Account FROM  [" & My.Settings.DBName & "].dbo.Client D WHERE C.MainAccLink = D.DCLink), C.Account) AS 'Account'," & vbCrLf &
                    "ISNULL((SELECT D.DCLink FROM  [" & My.Settings.DBName & "].dbo.Client D WHERE C.MainAccLink = D.DCLink), C.DCLink) AS 'DCLink'," & vbCrLf &
                    "ISNULL((SELECT D.Name FROM  [" & My.Settings.DBName & "].dbo.Client D WHERE C.MainAccLink = D.DCLink), C.Name) AS 'Name'," & vbCrLf &
                    "--BANKING DETAILS" & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL(C.BRANCHCODE,'')" & vbCrLf &
                    "WHEN '' THEN '000000'" & vbCrLf &
                    "ELSE C.BranchCode" & vbCrLf &
                    "END" & vbCrLf &
                    ")as 'BRANCHCODE' ," & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL(C.BANKACCNUM, '')" & vbCrLf &
                    "WHEN '' THEN '00000000000'" & vbCrLf &
                    "ELSE C.BANKACCNUM" & vbCrLf &
                    "END" & vbCrLf &
                    ") as 'BANKACCNUM'," & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL(C.BANKACCTYPE, '')" & vbCrLf &
                    "WHEN '' THEN 'Current/Cheque Account'" & vbCrLf &
                    "ELSE C.BANKACCTYPE" & vbCrLf &
                    "END" & vbCrLf &
                    ") as 'BANKACCTYPE'," & vbCrLf &
                    "JN.InvTotIncl AS 'Amount'" & vbCrLf &
                    "FROM" & vbCrLf &
                    "[" & My.Settings.DBName & "].dbo.Client C" & vbCrLf &
                    "INNER JOIN" & vbCrLf &
                    "[" & My.Settings.DBName & "].dbo._btblJCMaster  JC" & vbCrLf &
                    "ON" & vbCrLf &
                    "C.DCLInk = JC.iClientId" & vbCrLf &
                    "INNER JOIN" & vbCrLf &
                    "[" & My.Settings.DBName & "].dbo.JobNum JN" & vbCrLf &
                    "ON" & vbCrLf &
                    "JC.IDJCMaster = JN.iJCMasterID" & vbCrLf &
                    "WHERE" & vbCrLf &
                    "JC.ubJCInclDO = 1" & vbCrLf &
                    "AND" & vbCrLf &
                    "C.ulARPaymentMethod <> 'D/O'" & vbCrLf &
                    "AND" & vbCrLf &
                    "(CONVERT(DATE, JC.dInvDate) > '" & noteDateGreater & "' AND CONVERT(DATE, JC.dInvDate) <= '" & noteDateLessThan & "')" & vbCrLf &
                    "AND" & vbCrLf &
                    "JN.DocType = 1"

        DT = Conn.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Dim BankDets As New clsBankDetails

            BankDets.Account = Row("ACCOUNT")
            BankDets.AccountType = Row("BANKACCTYPE")
            BankDets.Amount = Row("Amount")
            BankDets.BankAccountNumber = Row("BANKACCNUM")
            BankDets.BranchCode = Row("BRANCHCODE")
            BankDets.ClientName = Row("NAME")
            BankDets.MasterAcc = Row("DCLINK")

            lstBankDets.Add(BankDets)

        Next

        ''**********TEST**********
        'Dim totalAmount As Decimal = 0.0

        'For Each amount As clsBankDetails In lstBankDets

        '    totalAmount = totalAmount + amount.Amount

        'Next

        'MsgBox("Flagged Job Cards - Non-Contract: R" & totalAmount)
        ''**********TEST**********

        Return lstBankDets

    End Function

End Class
