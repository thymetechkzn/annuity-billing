﻿Public Class repGetGUIData

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function getDebitOrderRuns() As DataTable

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable

        qry = "SELECT TOP 1" & vbCrLf & _
                "invDescriptionSuffix " & vbCrLf & _
                "FROM" & vbCrLf & _
                "runMonth" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "isCompleted = 1" & vbCrLf & _
                "ORDER BY monthID DESC"

        'qry = "SELECT" & vbCrLf & _
        '        "invDescriptionSuffix" & vbCrLf & _
        '        "FROM" & vbCrLf & _
        '        "runMonth" & vbCrLf & _
        '        "WHERE" & vbCrLf & _
        '        "isCompleted = 1"

        dt = dbConn.get_datatable(qry)

        Return dt

    End Function

    Public Function getRunMonth(ByVal selectedDORun As String) As Integer

        'Declare Variables
        Dim runMonth As Integer = 0
        Dim qry As String

        'qry = "SELECT DATEPART(MM,'June 2016' + ' 01') AS 'RunMonth'"


        Try
            qry = "SELECT" & vbCrLf & _
                            "DISTINCT(DATEPART(MM,'" & selectedDORun & "' + ' 01')) AS 'RunMonth'" & vbCrLf & _
                            "FROM" & vbCrLf & _
                            "InvNum" & vbCrLf & _
                            "WHERE" & vbCrLf & _
                            "Message2 LIKE '%" & selectedDORun & "%'" & vbCrLf & _
                            "AND" & vbCrLf & _
                            "Message2 NOT LIKE '%RADIO%'"

            dbConn.setSqlString(qry)
            dbConn.createNewConnection()

            runMonth = Convert.ToInt32(dbConn.get_value("RunMonth"))
        Catch ex As Exception

            getRunMonth(selectedDORun)

        End Try

        Return runMonth

    End Function

    Public Function loadDatagridview(ByVal selectedDORun As String) As DataTable

        'Declare Variables
        Dim qry As String
        Dim dt As DataTable

        qry = "SELECT" & vbCrLf & _
            "DISTINCT(SUBSTRING(C.ulARDORunDate, 0, 5)) AS 'Debit Order Configuration'," & vbCrLf & _
            "(SELECT DISTINCT(REPLACE(I.Message2, '_INVOICE', '')) FROM InvNum I WHERE I.Message2 LIKE '%" & selectedDORun & "%' AND Message2 NOT LIKE '%RADIO%') AS 'Batch Reference'" & vbCrLf & _
            "FROM" & vbCrLf & _
            "Client C" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "C.ulARDORunDate LIKE '%DO%'" & vbCrLf & _
            "ORDER BY [Debit Order Configuration]"

        dt = dbConn.get_datatable(qry)

        Return dt

    End Function

End Class
