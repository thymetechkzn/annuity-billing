﻿Public Class repLoadCashMangerAccounts

    'PLEASE CREATE A SETTING AND SETTINGS PAGE TO HOLD THIS INFORMATION
    Dim Conn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function GetCashManagerContracts(ByVal DORun As String) As List(Of clsBankDetails)

        Dim lstBankLines As New List(Of clsBankDetails)
        Dim SQlQuery As String
        Dim accountFilter As String = ""
        Dim Dt As New DataTable

        Try

            'Actual Amount:  "INVN.Amount as 'Amt'," & vbCrLf & 
            SQlQuery = "SELECT" & vbCrLf & _
                        "INVN.RefNo as 'ReferenceNumber'," & vbCrLf & _
                         "INVN.Amount as 'Amt'," & vbCrLf &
                        "--AMOUNT" & vbCrLf & _
                        "(" & vbCrLf & _
                        "INVN.Amount /" & vbCrLf & _
                        "(" & vbCrLf & _
                        "SELECT Convert(int, substring(N.Followup, 3,3)" & vbCrLf & _
                        ") " & vbCrLf & _
                        "FROM [" & My.Settings.CashDBName & "].dbo.invoicenotes N " & vbCrLf & _
                        "WHERE " & vbCrLf & _
                        "N.Followup LIKE '%DO%' " & vbCrLf & _
                        "AND " & vbCrLf & _
                        "N.Followup <> 'NDO' " & vbCrLf & _
                        "AND " & vbCrLf & _
                        "INVN.PKID = N.PKID)" & vbCrLf & _
                        ") AS 'Amount', " & vbCrLf & _
                        "--ACCOUNT DETAILS" & vbCrLf & _
                        "(" & vbCrLf & _
                        "CASE ISNULL((SELECT MainAccLink FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                        "	WHEN '' THEN  C.DCLink " & vbCrLf & _
                        "	ELSE (SELECT MainAccLink FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink)" & vbCrLf & _
                        "END" & vbCrLf & _
                        ") AS 'DCLINK'," & vbCrLf & _
                        "(" & vbCrLf & _
                        "CASE ISNULL((SELECT MainAccLink FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                        "	WHEN '' THEN  C.Account " & vbCrLf & _
                        "	ELSE (SELECT Account FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf & _
                        "END" & vbCrLf & _
                        ") AS 'Account'," & vbCrLf & _
                        "(" & vbCrLf & _
                        "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                        "	WHEN '' THEN  C.Name" & vbCrLf & _
                        "	ELSE (SELECT Name FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf & _
                        "END" & vbCrLf & _
                        ") AS 'Name'," & vbCrLf & _
                        "--BANKING DETAILS" & vbCrLf & _
                        "(" & vbCrLf & _
                        "CASE ISNULL((SELECT MainAccLink FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                        "	--MAIN ACCOUNT" & vbCrLf & _
                        "	WHEN '' THEN  " & vbCrLf & _
                        "	(" & vbCrLf & _
                        "		CASE ISNULL(C.BRANCHCODE,'')" & vbCrLf & _
                        "			WHEN '' THEN '000000'" & vbCrLf & _
                        "			ELSE C.BranchCode" & vbCrLf & _
                        "		END" & vbCrLf & _
                        "	)" & vbCrLf & _
                        "	--LINKED ACCOUNT" & vbCrLf & _
                        "	ELSE" & vbCrLf & _
                        "	(" & vbCrLf & _
                        "		CASE ISNULL((SELECT BRANCHCODE FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink),'')" & vbCrLf & _
                        "			WHEN '' THEN '000000'" & vbCrLf & _
                        "			ELSE (SELECT BRANCHCODE FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf & _
                        "		END" & vbCrLf & _
                        "	)" & vbCrLf & _
                        "END" & vbCrLf & _
                        ") AS 'BRANCHCODE'," & vbCrLf & _
                        "(" & vbCrLf & _
                        "CASE ISNULL((SELECT MainAccLink FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                        "	--MAIN ACCOUNT" & vbCrLf & _
                        "	WHEN '' THEN  " & vbCrLf & _
                        "	(" & vbCrLf & _
                        "		CASE ISNULL(C.BANKACCNUM,'')" & vbCrLf & _
                        "			WHEN '' THEN '00000000000'" & vbCrLf & _
                        "			ELSE C.BANKACCNUM" & vbCrLf & _
                        "		END" & vbCrLf & _
                        "	)" & vbCrLf & _
                        "	--LINKED ACCOUNT" & vbCrLf & _
                        "	ELSE" & vbCrLf & _
                        "	(" & vbCrLf & _
                        "		CASE ISNULL((SELECT BANKACCNUM FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink),'')" & vbCrLf & _
                        "			WHEN '' THEN '00000000000'" & vbCrLf & _
                        "			ELSE (SELECT BANKACCNUM FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf & _
                        "		END" & vbCrLf & _
                        "	)" & vbCrLf & _
                        "END" & vbCrLf & _
                        ") AS 'BANKACCNUM'," & vbCrLf & _
                        "(" & vbCrLf & _
                        "CASE ISNULL((SELECT MainAccLink FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                        "	--MAIN ACCOUNT" & vbCrLf & _
                        "	WHEN '' THEN  " & vbCrLf & _
                        "	(" & vbCrLf & _
                        "		CASE ISNULL(C.BANKACCTYPE,'')" & vbCrLf & _
                        "			WHEN '' THEN 'Current/Cheque Account'" & vbCrLf & _
                        "			ELSE C.BANKACCTYPE" & vbCrLf & _
                        "		END" & vbCrLf & _
                        "	)" & vbCrLf & _
                        "	--LINKED ACCOUNT" & vbCrLf & _
                        "	ELSE" & vbCrLf & _
                        "	(" & vbCrLf & _
                        "		CASE ISNULL((SELECT BANKACCTYPE FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink),'')" & vbCrLf & _
                        "			WHEN '' THEN 'Current/Cheque Account'" & vbCrLf & _
                        "			ELSE (SELECT BANKACCTYPE FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf & _
                        "		END" & vbCrLf & _
                        "	)" & vbCrLf & _
                        "END" & vbCrLf & _
                        ") AS 'BANKACCTYPE'" & vbCrLf & _
                        "FROM" & vbCrLf & _
                        "[" & My.Settings.CashDBName & "].dbo.TempDebitHeader TDH" & vbCrLf & _
                        "INNER JOIN" & vbCrLf & _
                        "[" & My.Settings.CashDBName & "].dbo.invoicenotes INVN" & vbCrLf & _
                        "ON" & vbCrLf & _
                        "TDH.Reference = INVN.RefNo" & vbCrLf & _
                        "Inner join" & vbCrLf & _
                        "[" & My.Settings.DBName & "].dbo.Client C" & vbCrLf & _
                        "ON" & vbCrLf & _
                        "TDH.Account = c.Account" & vbCrLf & _
                        "WHERE" & vbCrLf & _
                        "INVN.FollowUp  like '%DO%'" & vbCrLf & _
                        "AND" & vbCrLf & _
                        "ulARDORunDate like '%" & DORun & "%'" & vbCrLf & _
                        "AND ReasonCode = 'DO'" & vbCrLf & _
                        "AND" & vbCrLf & _
                        "ubARCONtractActive = 1 ORDER BY Account"

            Dt = Conn.get_datatable(SQlQuery)

            For Each Row As DataRow In Dt.Rows

                Dim BankDets As New clsBankDetails


                BankDets.Account = Row("Account")
                BankDets.AccountType = Row("BankAccType")
                BankDets.Amount = Row("Amount")
                BankDets.ActualAmount = Row("Amt")
                BankDets.BankAccountNumber = Row("BankAccNum")
                BankDets.BranchCode = Row("BranchCode")
                BankDets.ClientName = Row("Name")
                BankDets.MasterAcc = Row("DCLink")
                BankDets.RefNo = Row("ReferenceNumber")

                lstBankLines.Add(BankDets)

            Next

        Catch ex As Exception

            GetCashManagerContracts(DORun)

        End Try

        Return lstBankLines

    End Function
    Public Function GetCashManagerAdHoc(ByVal DOrun As String, ByVal intRunMonth As Integer) As List(Of clsBankDetails)

        Dim lstBankDets As New List(Of clsBankDetails)
        Dim SQLQuery As String
        Dim DT As New DataTable
        Dim accountFilter As String = ""
        Dim getDateRange As New repSetDateRange

        Dim noteDateGreater As String = getDateRange.getNoteMonthDayGreaterThan(DOrun, intRunMonth)  '*********FIND OUT IF THIS WILL BE A PROBLEM - FORMAT*********
        Dim noteDateLessThan As String = getDateRange.getNoteDateLessThanOrEqual(DOrun, intRunMonth, False)


        'Actual Amount:  "INVN.Amount as 'Amt'," & vbCrLf & 
        SQLQuery = "SELECT" & vbCrLf &
                    "INVN.RefNo as 'ReferenceNumber'," & vbCrLf &
                    "INVN.Amount as 'Amt'," & vbCrLf & 
                    "--AMOUNT" & vbCrLf &
                    "--(" & vbCrLf &
                    "INVN.Amount /Convert(int, substring(INVN.Followup, 3,3)) AS 'Amount', " & vbCrLf &
                    "--ACCOUNT DETAILS" & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf &
                    "	WHEN '' THEN  C.DCLink " & vbCrLf &
                    "	ELSE (SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink)" & vbCrLf &
                    "END" & vbCrLf &
                    ") AS 'DCLINK'," & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf &
                    "	WHEN '' THEN  C.Account " & vbCrLf &
                    "	ELSE (SELECT Account FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf &
                    "END" & vbCrLf &
                    ") AS 'Account'," & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf &
                    "	WHEN '' THEN  C.Name" & vbCrLf &
                    "	ELSE (SELECT Name FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf &
                    "END" & vbCrLf &
                    ") AS 'Name'," & vbCrLf &
                    "--BANKING DETAILS" & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf &
                    "	--MAIN ACCOUNT" & vbCrLf &
                    "	WHEN '' THEN  " & vbCrLf &
                    "	(" & vbCrLf &
                    "		CASE ISNULL(C.BRANCHCODE,'')" & vbCrLf &
                    "			WHEN '' THEN '000000'" & vbCrLf &
                    "			ELSE C.BranchCode" & vbCrLf &
                    "		END" & vbCrLf &
                    "	)" & vbCrLf &
                    "	--LINKED ACCOUNT" & vbCrLf &
                    "	ELSE" & vbCrLf &
                    "	(" & vbCrLf &
                    "		CASE ISNULL((SELECT BRANCHCODE FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink),'')" & vbCrLf &
                    "			WHEN '' THEN '000000'" & vbCrLf &
                    "			ELSE (SELECT BRANCHCODE FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf &
                    "		END" & vbCrLf &
                    "	)" & vbCrLf &
                    "END" & vbCrLf &
                    ") AS 'BRANCHCODE'," & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf &
                    "	--MAIN ACCOUNT" & vbCrLf &
                    "	WHEN '' THEN  " & vbCrLf &
                    "	(" & vbCrLf &
                    "		CASE ISNULL(C.BANKACCNUM,'')" & vbCrLf &
                    "			WHEN '' THEN '00000000000'" & vbCrLf &
                    "			ELSE C.BANKACCNUM" & vbCrLf &
                    "		END" & vbCrLf &
                    "	)" & vbCrLf &
                    "	--LINKED ACCOUNT" & vbCrLf &
                    "	ELSE" & vbCrLf &
                    "	(" & vbCrLf &
                    "		CASE ISNULL((SELECT BANKACCNUM FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink),'')" & vbCrLf &
                    "			WHEN '' THEN '00000000000'" & vbCrLf &
                    "			ELSE (SELECT BANKACCNUM FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf &
                    "		END" & vbCrLf &
                    "	)" & vbCrLf &
                    "END" & vbCrLf &
                    ") AS 'BANKACCNUM'," & vbCrLf &
                    "(" & vbCrLf &
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf &
                    "	--MAIN ACCOUNT" & vbCrLf &
                    "	WHEN '' THEN  " & vbCrLf &
                    "	(" & vbCrLf &
                    "		CASE ISNULL(C.BANKACCTYPE,'')" & vbCrLf &
                    "			WHEN '' THEN 'Current/Cheque Account'" & vbCrLf &
                    "			ELSE C.BANKACCTYPE" & vbCrLf &
                    "		END" & vbCrLf &
                    "	)" & vbCrLf &
                    "	--LINKED ACCOUNT" & vbCrLf &
                    "	ELSE" & vbCrLf &
                    "	(" & vbCrLf &
                    "		CASE ISNULL((SELECT BANKACCTYPE FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink),'')" & vbCrLf &
                    "			WHEN '' THEN 'Current/Cheque Account'" & vbCrLf &
                    "			ELSE (SELECT BANKACCTYPE FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf &
                    "		END" & vbCrLf &
                    "	)" & vbCrLf &
                    "END" & vbCrLf &
                    ") AS 'BankAccType'" & vbCrLf &
                    "FROM " & vbCrLf &
                    "[" & My.Settings.CashDBName & "].[dbo].TempDebitHeader TDH" & vbCrLf &
                    "INNER JOIN" & vbCrLf &
                    "[" & My.Settings.CashDBName & "].[dbo].invoicenotes INVN" & vbCrLf &
                    "ON" & vbCrLf &
                    "TDH.Reference = INVN.RefNo" & vbCrLf &
                    "INNER JOIN" & vbCrLf &
                    "[" & My.Settings.DBName & "].dbo.Client C" & vbCrLf &
                    "ON" & vbCrLf &
                    "TDH.Account = C.Account" & vbCrLf &
                    "WHERE" & vbCrLf &
                    "C.ubARContractActive = 0" & vbCrLf &
                    "AND" & vbCrLf &
                    "INVN.ReasonCode = 'DO'" & vbCrLf &
                    "AND" & vbCrLf &
                    "INVN.FollowUp LIKE '%DO%'" & vbCrLf &
                    "AND" & vbCrLf &
                    "CONVERT(DATE,INVN.NoteDate) > '" & noteDateGreater & "'" & vbCrLf &
                    "AND" & vbCrLf &
                    "CONVERT(DATE,INVN.NoteDate) <= '" & noteDateLessThan & "'" & vbCrLf &
                    "ORDER BY Account"

        DT = Conn.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Dim BankDets As New clsBankDetails

            BankDets.Account = Row("Account")
            BankDets.AccountType = Row("BankAccType")
            BankDets.Amount = Row("Amount")
            BankDets.ActualAmount = Row("Amt")
            BankDets.BankAccountNumber = Row("BankAccNum")
            BankDets.BranchCode = Row("BranchCode")
            BankDets.ClientName = Row("Name")
            BankDets.MasterAcc = Row("DCLink")
            BankDets.RefNo = Row("ReferenceNumber")

            lstBankDets.Add(BankDets)

        Next

        Return lstBankDets

    End Function

    Public Function CheckDoAccounts(ByVal lstCheckedClients As List(Of clsBankDetails), ByVal DORunConfig As String, ByVal intRunMonth As Integer) As List(Of clsBankDetails)

        'Declare Variables
        Dim SQLQuery As String
        Dim PTPDate As Date
        Dim RefNo As String = ""
        Dim totalAmount As Double = 0
        Dim FollowUpAmount As Integer
        Dim FollowUp As String = ""
        Dim DOConfig As String = ""
        Dim DT As New DataTable
        Dim lstNewClients As New List(Of clsBankDetails)
        Dim getDateRange As New repSetDateRange

        Dim countUpdate As Integer = 0
        Dim countInsert As Integer = 0
        Dim countDelete As Integer = 0


        Dim noteDateGreater As String = getDateRange.getNoteMonthDayGreaterThan(DORunConfig, intRunMonth)  '*********FIND OUT IF THIS WILL BE A PROBLEM - FORMAT*********
        Dim noteDateLessThan As String = getDateRange.getNoteDateLessThanOrEqual(DORunConfig, intRunMonth, False)

        Try

            SQLQuery = "delete from [" & My.Settings.DBName & "].[dbo].CashManagerRecords where SUBSTRING(followUp,3,3) = ActionTaken"

            Conn.setSqlString(SQLQuery)
            Conn.createNewConnection()
            Conn.CloseConnection()

            For Each Client As clsBankDetails In lstCheckedClients

                SQLQuery = "select CONVERT(Date, NoteDate) AS NoteDate, RefNo, FollowUp, Amount from [" & My.Settings.CashDBName & "].[dbo].InvoiceNotes" & vbCrLf & _
                        "where RefNo in" & vbCrLf & _
                        "(select Reference from [" & My.Settings.CashDBName & "].[dbo].TempDebitHeader" & vbCrLf & _
                        "where Account = '" & Client.Account & "' AND RefNo =" & Client.RefNo & ")"

                DT = Conn.get_datatable(SQLQuery)

                For Each Row As DataRow In DT.Rows

                    PTPDate = Row("NoteDate")
                    RefNo = Row("RefNo")
                    FollowUp = Row("FollowUp")
                    'Client.Amount = Row("Amount")
                    ''totalAmount = Row("Amount")

                Next

                DT.Dispose()

                SQLQuery = "select FollowUp, ActionTaken, DOConfig, isnull(Amount,0) as 'Amount'  from [" & My.Settings.DBName & "].[dbo].CashManagerRecords" & vbCrLf & _
                            "where RefNo = '" & RefNo & "'" & vbCrLf & _
                            "AND" & vbCrLf & _
                            "AccountCode = '" & Client.Account & "'"

                DT = Conn.get_datatable(SQLQuery)

                If DT.Rows.Count > 0 Then

                    If DT.Rows(0).Item("FollowUp").Equals("DO1") Then
                        FollowUpAmount = 1
                    ElseIf DT.Rows(0).Item("FollowUp").Equals("DO3") Then
                        FollowUpAmount = 3
                    ElseIf DT.Rows(0).Item("FollowUp").Equals("DO6") Then
                        FollowUpAmount = 6
                    End If

                    DOConfig = DT.Rows(0).Item("DOConfig")
                    Client.Amount = (DT.Rows(0).Item("Amount")) / FollowUpAmount

                    If DT.Rows(0).Item("ActionTaken") < FollowUpAmount Then

                        If DOConfig = DORunConfig Then

                            SQLQuery = "UPDATE [" & My.Settings.DBName & "].[dbo].CashManagerRecords" & vbCrLf & _
                                                                "SET ActionTaken = ActionTaken + 1," & vbCrLf & _
                                                                "DateModified = CAST(GETDATE() AS DATE)" & vbCrLf & _
                                                                "WHERE" & vbCrLf & _
                                                                "Accountcode = '" & Client.Account & "'" & vbCrLf & _
                                                                "AND" & vbCrLf & _
                                                                "RefNo = " & RefNo

                            Conn.setSqlString(SQLQuery)
                            Conn.createNewConnection()
                            Conn.CloseConnection()
                            lstNewClients.Add(Client)

                            countUpdate += 1

                        End If

                        'ElseIf FollowUpAmount = DT.Rows(0).Item("ActionTaken") Then

                        '    SQLQuery = "DELETE [" & My.Settings.DBName & "].[dbo].CashManagerRecords" & vbCrLf & _
                        '                "WHERE" & vbCrLf & _
                        '                "Accountcode = '" & Client.Account & "'" & vbCrLf & _
                        '                "AND" & vbCrLf & _
                        '                "RefNo = " & RefNo

                        '    Conn.setSqlString(SQLQuery)
                        '    Conn.createNewConnection()
                        '    Conn.CloseConnection()

                        '    countDelete += 1

                    End If

                Else

                    If PTPDate > noteDateGreater And PTPDate <= noteDateLessThan Then

                        SQLQuery = "INSERT INTO [" & My.Settings.DBName & "].[dbo].CashManagerRecords(RefNo, AccountCode, FollowUp, ActionTaken, DateCreated, DateModified, DOConfig, Amount)" & vbCrLf & _
                                    "VALUES" & vbCrLf & _
                                    "('" & RefNo & "' , '" & Client.Account & "', '" & FollowUp & "', 1, CAST(GETDATE() AS DATE),CAST(GETDATE() AS DATE),'" & DORunConfig & "', " & Client.ActualAmount & ")"

                        Conn.setSqlString(SQLQuery)
                        Conn.createNewConnection()
                        Conn.CloseConnection()

                        lstNewClients.Add(Client)

                        countInsert += 1

                    End If

                End If

                Debug.WriteLine(Client.Account)

            Next

        Catch ex As Exception

            MsgBox(ex.Message)

        End Try


        Debug.WriteLine("Update: " & countUpdate & " Insert: " & countInsert & " Delete: " & countDelete)

        ''**********TEST**********
        'Dim totalAmount As Decimal = 0.0

        'For Each amount As clsBankDetails In lstNewClients

        '    totalAmount = totalAmount + amount.Amount

        'Next

        'MsgBox("Cash Manager: R" & totalAmount)

        ''**********TEST**********

        lstNewClients.AddRange(getLostCashManagerRecords(DORunConfig))

        Return lstNewClients

    End Function

    Public Function getLostCashManagerRecords(ByVal doConfig As String) As List(Of clsBankDetails)

        Dim lstClients As New List(Of clsBankDetails)
        Dim SQLQuery As String
        Dim DT As New DataTable

        SQLQuery = "select  " & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                    "	WHEN '' THEN  C.DCLink " & vbCrLf & _
                    "	ELSE (SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink)" & vbCrLf & _
                    "END" & vbCrLf & _
                    ") AS 'DCLINK'," & vbCrLf & _
                    "" & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                    "	WHEN '' THEN  C.Account " & vbCrLf & _
                    "	ELSE (SELECT Account FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf & _
                    "END" & vbCrLf & _
                    ") AS 'Account'," & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL((SELECT MainAccLink FROM[" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                    "	WHEN '' THEN  C.Name" & vbCrLf & _
                    "	ELSE (SELECT Name FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf & _
                    "END" & vbCrLf & _
                    ") AS 'Name'," & vbCrLf & _
                    "--BANKING DETAILS" & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                    "	--MAIN ACCOUNT" & vbCrLf & _
                    "	WHEN '' THEN  " & vbCrLf & _
                    "	(" & vbCrLf & _
                    "		CASE ISNULL(C.BRANCHCODE,'')" & vbCrLf & _
                    "			WHEN '' THEN '000000'" & vbCrLf & _
                    "			ELSE C.BranchCode" & vbCrLf & _
                    "		END" & vbCrLf & _
                    "	)" & vbCrLf & _
                    "	--LINKED ACCOUNT" & vbCrLf & _
                    "	ELSE" & vbCrLf & _
                    "	(" & vbCrLf & _
                    "		CASE ISNULL((SELECT BRANCHCODE FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink),'')" & vbCrLf & _
                    "			WHEN '' THEN '000000'" & vbCrLf & _
                    "			ELSE (SELECT BRANCHCODE FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf & _
                    "		END" & vbCrLf & _
                    "	)" & vbCrLf & _
                    "END" & vbCrLf & _
                    ") AS 'BRANCHCODE'," & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                    "	--MAIN ACCOUNT" & vbCrLf & _
                    "	WHEN '' THEN  " & vbCrLf & _
                    "	(" & vbCrLf & _
                    "		CASE ISNULL(C.BANKACCNUM,'')" & vbCrLf & _
                    "			WHEN '' THEN '00000000000'" & vbCrLf & _
                    "			ELSE C.BANKACCNUM" & vbCrLf & _
                    "		END" & vbCrLf & _
                    "	)" & vbCrLf & _
                    "	--LINKED ACCOUNT" & vbCrLf & _
                    "	ELSE" & vbCrLf & _
                    "	(" & vbCrLf & _
                    "		CASE ISNULL((SELECT BANKACCNUM FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink),'')" & vbCrLf & _
                    "			WHEN '' THEN '00000000000'" & vbCrLf & _
                    "			ELSE (SELECT BANKACCNUM FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf & _
                    "		END" & vbCrLf & _
                    "	)" & vbCrLf & _
                    "END" & vbCrLf & _
                    ") AS 'BANKACCNUM'," & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL((SELECT MainAccLink FROM [" & My.Settings.DBName & "].dbo.Client WHERE DCLink = C.DCLink), '')" & vbCrLf & _
                    "	--MAIN ACCOUNT" & vbCrLf & _
                    "	WHEN '' THEN  " & vbCrLf & _
                    "	(" & vbCrLf & _
                    "		CASE ISNULL(C.BANKACCTYPE,'')" & vbCrLf & _
                    "			WHEN '' THEN 'Current/Cheque Account'" & vbCrLf & _
                    "			ELSE C.BANKACCTYPE" & vbCrLf & _
                    "		END" & vbCrLf & _
                    "	)" & vbCrLf & _
                    "	--LINKED ACCOUNT" & vbCrLf & _
                    "	ELSE" & vbCrLf & _
                    "	(" & vbCrLf & _
                    "		CASE ISNULL((SELECT BANKACCTYPE FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink),'')" & vbCrLf & _
                    "			WHEN '' THEN 'Current/Cheque Account'" & vbCrLf & _
                    "			ELSE (SELECT BANKACCTYPE FROM  [" & My.Settings.DBName & "].dbo.Client WHERE DCLink  = C.MainAccLink)" & vbCrLf & _
                    "		END" & vbCrLf & _
                    "	)" & vbCrLf & _
                    "END" & vbCrLf & _
                    ") AS 'BANKACCTYPE'," & vbCrLf & _
                    "Amount / ( SUBSTRING(followUp,3,3)) AS 'Amount'," & vbCrLf & _
                    "R.RefNo AS 'ReferenceNumber'" & vbCrLf & _
                    "from " & vbCrLf & _
                    "[BLUE SECURITY].dbo.CashManagerRecords R" & vbCrLf & _
                    "INNER JOIN" & vbCrLf & _
                    "[BLUE SECURITY].dbo.Client C  " & vbCrLf & _
                    "ON" & vbCrLf & _
                    "R.AccountCode = C.Account " & vbCrLf & _
                    "where " & vbCrLf & _
                    "R.DOConfig = '" & doConfig & "'" & vbCrLf & _
                    "and " & vbCrLf & _
                    "R.DateModified <> CAST(GETDATE() AS DATE)" & vbCrLf & _
                    "and" & vbCrLf & _
                    " SUBSTRING(followUp,3,3) <> ActionTaken " & vbCrLf

        DT = Conn.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Dim BankDets As New clsBankDetails


            BankDets.Account = Row("Account")
            BankDets.AccountType = Row("BankAccType")
            BankDets.Amount = Row("Amount")
            BankDets.BankAccountNumber = Row("BankAccNum")
            BankDets.BranchCode = Row("BranchCode")
            BankDets.ClientName = Row("Name")
            BankDets.MasterAcc = Row("DCLink")
            BankDets.RefNo = Row("ReferenceNumber")

            SQLQuery = "UPDATE [" & My.Settings.DBName & "].[dbo].CashManagerRecords" & vbCrLf & _
                                           "SET ActionTaken = ActionTaken + 1," & vbCrLf & _
                                           "DateModified = CAST(GETDATE() AS DATE)" & vbCrLf & _
                                           "WHERE" & vbCrLf & _
                                           "Accountcode = '" & BankDets.Account & "'" & vbCrLf & _
                                           "AND" & vbCrLf & _
                                           "RefNo = " & BankDets.RefNo

            Conn.setSqlString(SQLQuery)
            Conn.createNewConnection()
            Conn.CloseConnection()

            lstClients.Add(BankDets)

        Next

        Return lstClients

    End Function

End Class
