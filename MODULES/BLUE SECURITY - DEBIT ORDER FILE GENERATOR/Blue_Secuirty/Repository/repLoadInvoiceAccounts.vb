﻿Public Class repLoadInvoiceAccounts

    'PLEASE CREATE A SETTING AND SETTINGS PAGE TO HOLD THIS INFORMATION
    Dim Conn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)


    Public Function GetContractClients(ByVal DORun As String, ByVal recRunMonthName As String) As List(Of clsBankDetails)

        Dim BankLines As New List(Of clsBankDetails)
        Dim Clients As New List(Of MasterDebtor)

        Dim qry As String = "SELECT" & vbCrLf & _
                            "C.DCLINK," & vbCrLf & _
                            "C.ACCOUNT," & vbCrLf & _
                            "C.NAME," & vbCrLf & _
                            "--BANKING DETAILS" & vbCrLf & _
                            "(" & vbCrLf & _
                            "CASE ISNULL(C.BRANCHCODE,'')" & vbCrLf & _
                            "	WHEN '' THEN '000000'" & vbCrLf & _
                            "	ELSE C.BranchCode" & vbCrLf & _
                            "END" & vbCrLf & _
                            ")as 'BRANCHCODE' ," & vbCrLf & _
                            "(" & vbCrLf & _
                            "CASE ISNULL(C.BANKACCNUM, '')" & vbCrLf & _
                            "	WHEN '' THEN '00000000000'" & vbCrLf & _
                            "	ELSE C.BANKACCNUM" & vbCrLf & _
                            "END" & vbCrLf & _
                            ") as 'BANKACCNUM'," & vbCrLf & _
                            "(" & vbCrLf & _
                            "CASE ISNULL(C.BANKACCTYPE, '')" & vbCrLf & _
                            "	WHEN '' THEN 'Current/Cheque Account'" & vbCrLf & _
                            "	ELSE C.BANKACCTYPE" & vbCrLf & _
                            "END" & vbCrLf & _
                            ") as 'BANKACCTYPE'," & vbCrLf & _
                            "--Amount" & vbCrLf & _
                            "C.DCBalance," & vbCrLf & _
                            "I.ORDTOTINCL AS 'AMOUNT'" & vbCrLf & _
                            "FROM" & vbCrLf & _
                            "[" & My.Settings.DBName & "].dbo.Client C" & vbCrLf & _
                            "INNER JOIN" & vbCrLf & _
                            "[" & My.Settings.DBName & "].dbo.InvNum I" & vbCrLf & _
                            "ON" & vbCrLf & _
                            "C.DClink = I.AccountID" & vbCrLf & _
                            "WHERE" & vbCrLf & _
                            "(C.MainAccLink Is null or C.MainAccLink = 0)" & vbCrLf & _
                             "AND (C.ulARDORunDate like '%" & DORun & "%')" & vbCrLf & _
                            "AND ulARPaymentMethod = 'D/O'" & vbCrLf & _
                            "AND ( MESSAGE2 LIKE '%" & recRunMonthName & "%')" & vbCrLf & _
                            "ORDER BY C.Account asc"

        '"(I.ORDTOTINCL - C.DCBalance) AS 'AMOUNT'" & vbCrLf & _

        'LOAD ITEMS INTO COLLECTION
        For Each Row As DataRow In Conn.get_datatable(qry).Rows

            Dim Client As New MasterDebtor

            If Convert.ToInt32(Row("Amount")) > 0 Then

                Client.DCLink = Row("DCLINK")
                Client.Account = Row("ACCOUNT")
                Client.Name = Row("NAME")
                Client.BranchCode = Row("BRANCHCODE")
                Client.BankAccNumber = Row("BANKACCNUM")
                Client.BankAccType = Row("BANKACCTYPE")
                Client.Amount = Row("Amount")

                If Client.Account = "DON000255" Then
                    Debug.WriteLine("")
                End If

                Clients.Add(Client)

            End If



        Next

        'CREATE BANK DETAILS REQUIRED FOR GENERATION OF BANK FILE... IN PROCESS, IF 
        'CUSTOMER HAS MORE THAN ONE INVOICE, IT FINDS THAT CUSTOMER AND TOTALS THE AMOUNTS
        'AND FLAGS CUSTOMER SO THEY ARE NOT RE-PROCESSED

        For i As Integer = 0 To Clients.Count - 1
            'CHECK CLIENTS / INVOICE VALUE HASN'T BEEN USED
            If Not Clients(i).Processed Then

                'SET DEFAULT BANK DETAILS
                Dim BankLine As New clsBankDetails

                BankLine.Account = Clients(i).Account
                BankLine.AccountType = Clients(i).BankAccType
                BankLine.Amount = Clients(i).Amount
                BankLine.BankAccountNumber = Clients(i).BankAccNumber
                BankLine.BranchCode = Clients(i).BranchCode
                BankLine.ClientName = Clients(i).Name
                BankLine.MasterAcc = Clients(i).DCLink

                'LOOK THROUGH THE REST OF CLIENT (EXCLUDING THIS AND PREVIOUSLY PROCESSED)
                For x As Integer = i + 1 To Clients.Count - 1

                    'SAME ACCOUNT CODE --- ADD AMOUNT AND FLAG CLIENT 'PROCESSED'
                    If Clients(x).Account.Equals(Clients(i).Account) Then
                        'adds amount of same account/different invoice
                        BankLine.Amount += Clients(x).Amount
                        'FLAG THE CLIENT -SO HE DOESN"T GET PROCESSED AGAIN
                        Clients(x).Processed = True
                    End If

                Next

                'ADD THE BANK LINE TO THE COLLECTION (DECLARED AT TOP OF THIS METHOD)
                BankLines.Add(BankLine)

            End If

        Next

        ''**********TEST**********
        'Dim totalAmount As Decimal = 0.0

        'For Each amount As clsBankDetails In BankLines

        '    totalAmount = totalAmount + amount.Amount

        'Next

        'MsgBox("Annuity Billing: R" & totalAmount)
        ''**********TEST**********

        Return BankLines

    End Function

    'Flagged Invoices
    Public Function GetFlaggedInvoices(ByVal DOrun As String, ByVal intRunMonth As Integer, ByVal recRunMonthName As String) As List(Of clsBankDetails)

        Dim lstBankDets As New List(Of clsBankDetails)
        Dim SQLQuery As String
        Dim DT As New DataTable
        Dim accountFilter As String = ""
        Dim index As Integer
        Dim year As String
        Dim getDateRange As New repSetDateRange
        Dim intRunMonth2 As Integer
        Dim yearGreater As String = ""

        index = recRunMonthName.IndexOf(" ")
        year = recRunMonthName.Substring(index).Trim()
        yearGreater = year


        Dim DOConfig As String

        Dim lastDayMonthGreater As Integer
        Dim lastDayMonthLess As Integer

        Dim noteDateGreater As String
        Dim noteDateLessThan As String

        If intRunMonth = 1 Then

            intRunMonth2 = intRunMonth
            intRunMonth = 13
            yearGreater = year - 1

        End If

        DOConfig = (DOrun.Replace("DO", ""))

        lastDayMonthGreater = Date.DaysInMonth(yearGreater, (intRunMonth - 1))

        If DOConfig.Equals("31") Then

            If lastDayMonthGreater = 30 Then

                noteDateGreater = yearGreater & "-" & (intRunMonth - 1).ToString() & "-" & lastDayMonthGreater

            Else

                noteDateGreater = yearGreater & "-" & (intRunMonth - 1).ToString() & "-" & DOConfig

            End If

        Else

            noteDateGreater = yearGreater & "-" & (intRunMonth - 1).ToString() & "-" & DOConfig

        End If

        'TRISH getDateRange.getNoteMonthDayGreaterThan(DOrun, intRunMonth)  '*********FIND OUT IF THIS WILL BE A PROBLEM - FORMAT*********
        If intRunMonth = 13 Then

            intRunMonth = 1
        End If

        lastDayMonthLess = Date.DaysInMonth(year, (intRunMonth))

        If DOConfig.Equals("31") Then

            If lastDayMonthLess = 30 Then

                noteDateLessThan = year & "-" & (intRunMonth).ToString() & "-" & lastDayMonthLess

            Else

                noteDateLessThan = year & "-" & (intRunMonth).ToString() & "-" & DOConfig

            End If

        Else

            noteDateLessThan = year & "-" & (intRunMonth).ToString() & "-" & DOConfig

        End If



        'getDateRange.getNoteDateLessThanOrEqual(DOrun, intRunMonth)



        SQLQuery = "SELECT" & vbCrLf & _
                    "C.DCLINK," & vbCrLf & _
                    "C.ACCOUNT," & vbCrLf & _
                    "C.NAME," & vbCrLf & _
                    "--BANKING DETAILS" & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL(C.BRANCHCODE,'')" & vbCrLf & _
                    "	WHEN '' THEN '000000'" & vbCrLf & _
                    "	ELSE C.BranchCode" & vbCrLf & _
                    "END" & vbCrLf & _
                    ")as 'BRANCHCODE' ," & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL(C.BANKACCNUM, '')" & vbCrLf & _
                    "	WHEN '' THEN '00000000000'" & vbCrLf & _
                    "	ELSE C.BANKACCNUM" & vbCrLf & _
                    "END" & vbCrLf & _
                    ") as 'BANKACCNUM'," & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL(C.BANKACCTYPE, '')" & vbCrLf & _
                    "	WHEN '' THEN 'Current/Cheque Account'" & vbCrLf & _
                    "	ELSE C.BANKACCTYPE" & vbCrLf & _
                    "END" & vbCrLf & _
                    ") as 'BANKACCTYPE'," & vbCrLf & _
                    "--Amount" & vbCrLf & _
                    "I.ORDTOTINCL" & vbCrLf & _
                    "FROM" & vbCrLf & _
                    "Client C" & vbCrLf & _
                    "INNER JOIN" & vbCrLf & _
                    "InvNum I" & vbCrLf & _
                    "ON" & vbCrLf & _
                    "C.DClink = I.AccountID" & vbCrLf & _
                    "WHERE" & vbCrLf & _
                    "(C.MainAccLink Is null OR C.MainAccLink = 0)" & vbCrLf & _
                    "AND (C.ulARDORunDate like '%" & DOrun & "%')" & vbCrLf & _
                    "AND ulARPaymentMethod = 'D/O'" & vbCrLf & _
                    "AND" & vbCrLf & _
                    "(" & vbCrLf & _
                    "I.ubIDInvInclDO = 1" & vbCrLf & _
                    "OR" & vbCrLf & _
                    "I.ubIDSOrdInclDO = 1" & vbCrLf & _
                    "OR" & vbCrLf & _
                    "I.ubIDCrnInclDO = 1" & vbCrLf & _
                    ")" & vbCrLf & _
                    "AND (CONVERT(DATE, I.InvDate)  > '" & noteDateGreater & "' AND CONVERT(DATE, I.InvDate) <= '" & noteDateLessThan & "')" & vbCrLf & _
                    "ORDER BY C.Account asc"

        DT = Conn.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Dim BankDets As New clsBankDetails

            BankDets.Account = Row("Account")
            BankDets.AccountType = Row("Bank Account Type")
            BankDets.Amount = Row("Amount")
            BankDets.BankAccountNumber = Row("Bank Account Number")
            BankDets.BranchCode = Row("Branch Code")
            BankDets.ClientName = Row("Name")
            BankDets.MasterAcc = Row("DCLink")

            lstBankDets.Add(BankDets)

        Next

        ''**********TEST**********
        'Dim totalAmount As Decimal = 0.0

        'For Each amount As clsBankDetails In lstBankDets

        '    totalAmount = totalAmount + amount.Amount

        'Next

        'MsgBox("Flagged Invoices - Contract: R" & totalAmount)
        ''**********TEST**********

        Return lstBankDets

    End Function

    Public Function GetFlaggedInvoicesNonContract(ByVal DOrun As String, ByVal intRunMonth As Integer, ByVal recRunMonthName As String) As List(Of clsBankDetails)

        Dim lstBankDets As New List(Of clsBankDetails)
        Dim SQLQuery As String
        Dim DT As New DataTable
        Dim accountFilter As String = ""
        Dim getDateRange As New repSetDateRange

        Dim noteDateGreater As String = getDateRange.getNoteMonthDayGreaterThan(DOrun, intRunMonth)  '*********FIND OUT IF THIS WILL BE A PROBLEM - FORMAT*********
        Dim noteDateLessThan As String = getDateRange.getNoteDateLessThanOrEqual(DOrun, intRunMonth, False)

        SQLQuery = "SELECT" & vbCrLf & _
                    "C.DCLINK," & vbCrLf & _
                    "C.ACCOUNT," & vbCrLf & _
                    "C.NAME," & vbCrLf & _
                    "--BANKING DETAILS" & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL(C.BRANCHCODE,'')" & vbCrLf & _
                    "	WHEN '' THEN '000000'" & vbCrLf & _
                    "	ELSE C.BranchCode" & vbCrLf & _
                    "END" & vbCrLf & _
                    ")as 'BRANCHCODE' ," & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL(C.BANKACCNUM, '')" & vbCrLf & _
                    "	WHEN '' THEN '00000000000'" & vbCrLf & _
                    "	ELSE C.BANKACCNUM" & vbCrLf & _
                    "END" & vbCrLf & _
                    ") as 'BANKACCNUM'," & vbCrLf & _
                    "(" & vbCrLf & _
                    "CASE ISNULL(C.BANKACCTYPE, '')" & vbCrLf & _
                    "	WHEN '' THEN 'Current/Cheque Account'" & vbCrLf & _
                    "	ELSE C.BANKACCTYPE" & vbCrLf & _
                    "END" & vbCrLf & _
                    ") as 'BANKACCTYPE'," & vbCrLf & _
                    "--Amount" & vbCrLf & _
                    "I.ORDTOTINCL" & vbCrLf & _
                    "FROM" & vbCrLf & _
                    "Client C" & vbCrLf & _
                    "INNER JOIN" & vbCrLf & _
                    "InvNum I" & vbCrLf & _
                    "ON" & vbCrLf & _
                    "C.DClink = I.AccountID" & vbCrLf & _
                    "WHERE" & vbCrLf & _
                    "(C.MainAccLink Is null OR C.MainAccLink = 0)" & vbCrLf & _
                    "AND ulARPaymentMethod <> 'D/O'" & vbCrLf & _
                    "AND" & vbCrLf & _
                    "(" & vbCrLf & _
                    "I.ubIDInvInclDO = 1" & vbCrLf & _
                    "OR" & vbCrLf & _
                    "I.ubIDSOrdInclDO = 1" & vbCrLf & _
                    "OR" & vbCrLf & _
                    "I.ubIDCrnInclDO = 1" & vbCrLf & _
                    ")" & vbCrLf & _
                    "AND (CONVERT(DATE, I.InvDate)  > '" & noteDateGreater & "' AND CONVERT(DATE, I.InvDate) <= '" & noteDateLessThan & "')" & vbCrLf & _
                    "ORDER BY C.Account asc"

        DT = Conn.get_datatable(SQLQuery)

        For Each Row As DataRow In DT.Rows

            Dim BankDets As New clsBankDetails

            BankDets.Account = Row("Account")
            BankDets.AccountType = Row("Bank Account Type")
            BankDets.Amount = Row("Amount")
            BankDets.BankAccountNumber = Row("Bank Account Number")
            BankDets.BranchCode = Row("Branch Code")
            BankDets.ClientName = Row("Name")
            BankDets.MasterAcc = Row("DCLink")

            lstBankDets.Add(BankDets)

        Next

        ''**********TEST**********
        'Dim totalAmount As Decimal = 0.0

        'For Each amount As clsBankDetails In lstBankDets

        '    totalAmount = totalAmount + amount.Amount

        'Next

        'MsgBox("Flagger Invocies - Non-Contract: R" & totalAmount)
        ''**********TEST**********

        Return lstBankDets

    End Function
End Class
