﻿Imports Pastel.Evolution
Imports System.IO


Public Module SDKOperations

    'Create Connection

    Public Property ServerName As String = My.Settings.DBInstance
    Public Property Login As String = My.Settings.DBUser
    Public Property Password As String = My.Settings.DBPassword
    Public Property CompanyName As String = My.Settings.DBName
    Public Property CommonName As String = My.Settings.EvolutionCommon

    Public Function CreateConnection()

        Try

            Dim ServerName As String = My.Settings.DBInstance
            Dim Login As String = My.Settings.DBUser
            Dim Password As String = My.Settings.DBPassword
            Dim CompanyName As String = My.Settings.DBName
            Dim CommonName As String = My.Settings.EvolutionCommon

            DatabaseContext.CreateCommonDBConnection(ServerName, CommonName, Login, Password, False)
            DatabaseContext.SetLicense("DE09110073", "4730608")
            DatabaseContext.CreateConnection(ServerName, CompanyName, Login, Password, False)

        Catch ex As Exception
            Return False
        End Try
        Return True
    End Function

End Module
