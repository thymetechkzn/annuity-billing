﻿Imports System.IO

Module modExportToExcelDO

    Dim count As Integer = 0


    Public Function GetDOContent(ByVal lstClient As List(Of clsBankDetails))

        Dim DOContent As New DOFile
        Dim Val As Decimal = 0.0

        For Each BankLine As clsBankDetails In lstClient

            If BankLine.Account = "BTS001125" Then
                Debug.WriteLine("")
            End If
            Dim TRecord As New TransactionRecord

            '---set record values
            TRecord.HomingRecipientBranch = getBranchCode(BankLine.BranchCode) 'F
            TRecord.HomingrecipientAccNo = getBankAcc(BankLine.BankAccountNumber, BankLine.Account) 'G
            TRecord.TypeOfAccount = accType(BankLine.AccountType) 'H
            TRecord.Amount = getAmount(BankLine.Amount) 'I
            Val += BankLine.Amount
            TRecord.Filler2 = Date.Today.ToString("yyMMdd") & "00" 'J

            TRecord.AbbreviatedUserName = "BLUESECURI" 'getAbbreviatedUserName(BankLine.ClientName) 'O (Now N)

            TRecord.Collections = getUserRef(BankLine.Account) 'N 

            TRecord.HomingRecipientAccName = getFillerP(BankLine.ClientName) 'P
            TRecord.Filler6 = getFillerQ(BankLine.ClientName) 'Q



            '----add record to collection
            DOContent.TRRecords.Add(TRecord)

        Next

        Debug.WriteLine(Val.ToString)

        Return DOContent

    End Function

    Public Sub GenerateExport(ByVal DOContent As DOFile, ByVal releaseDate As String, _
                              ByVal path As String, ByVal DOfileName As String)

        If Not Directory.Exists(path) Then

            Directory.CreateDirectory(path)

        End If

        'OPEN WRITER
        Dim DOWrite As New StreamWriter(path & "\" & DOfileName & ".txt")

        'WRITE HEADERS
        DOWrite.WriteLine(DOContent.TRHeader.InstallationHeader)
        DOWrite.WriteLine(DOContent.TRHeader.UserHeader)

        'WRITE LINES
        For Each Line As TransactionRecord In DOContent.TRRecords
            DOWrite.WriteLine(Line.RecordValues)
        Next

        'WRITE FOOTER
        DOWrite.WriteLine("5200000000000000000000000000000000000000000000000000000000" + releaseDate + "00000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000") 'DOContent.TRFooter.ContraRecord)
        DOWrite.WriteLine(DOContent.TRFooter.UserTrailerRecord)
        DOWrite.WriteLine(DOContent.TRFooter.InstallationTrailRecord)

        'CLOSE WRITE
        DOWrite.Close()


    End Sub

    Public Sub ExportData(ByVal lstClient As List(Of clsBankDetails), ByVal releaseDate As String, _
                          ByVal path As String, ByVal DOfileName As String) 'This Method converts Datagridview Tables into Cells and columns of an Excell Sheet

        Dim DOContent As DOFile
        'Error Log
        Dim logger As New CreateLog()
        Dim strLog As String = ""

        'Dim copyClient As List(Of clsBankDetails) = lstClient

        Dim newList As New List(Of clsBankDetails)
        Dim accNames As New ArrayList
        Dim index As Integer = 0


        For i As Integer = 0 To lstClient.Count - 1

            If lstClient(i).Account.Equals("BTS001125") Then
                Debug.WriteLine("")
            End If

            If accNames.Contains(lstClient(i).Account) Then

                index = accNames.IndexOf(lstClient(i).Account)
                newList(index).Amount += lstClient(i).Amount

            Else

                Dim newAcc As New clsBankDetails
                newAcc.Amount = lstClient(i).Amount
                newAcc.Account = lstClient(i).Account
                newAcc.AccountType = lstClient(i).AccountType
                newAcc.BankAccountNumber = lstClient(i).BankAccountNumber
                newAcc.BranchCode = lstClient(i).BranchCode
                newAcc.ClientName = lstClient(i).ClientName

                ' newAcc = copyClient(i)
                If newAcc.BankAccountNumber.Equals("") Or newAcc.BranchCode.Equals("") Or newAcc.AccountType.Equals("") Then

                    strLog &= "Error with account, there are blank banking details: " & newAcc.Account & vbCrLf

                ElseIf newAcc.BankAccountNumber.Equals("00000000000") Or newAcc.BranchCode.Equals("000000") Then

                    strLog &= "Error with account, there are blank banking details: " & newAcc.Account & vbCrLf

                ElseIf Not IsNumeric(newAcc.BankAccountNumber) Then

                    strLog &= "Error with account, the bank account number is not a number: " & newAcc.Account & vbCrLf

                Else

                    accNames.Add(lstClient(i).Account)
                    newList.Add(newAcc)

                End If

            End If

        Next

        DOContent = GetDOContent(newList)

        GenerateExport(DOContent, releaseDate, path, DOfileName)

        logger.CreateLogFile(path & "\Error Log - " & releaseDate & ".txt", strLog)

    End Sub


    '---FUNCTIONS TO HELP WITH CREATION OF DO FILE

    Private Function accType(ByVal accountType As String)

        Dim accTypeNum As String = ""

        If accountType.Equals("Transmission Account") Then

            accTypeNum = "1"

        ElseIf accountType.Equals("Current/Cheque Account") Then

            accTypeNum = "1"

        ElseIf accountType.Equals("Savings") Then

            accTypeNum = "2"

        ElseIf accountType.Equals("Savings Account") Then

            accTypeNum = "2"

        End If

        Return accTypeNum

    End Function

    Private Function getAmount(ByVal amount As Double) As String

        Dim spacePos As Integer
        Dim rands As String = ""
        Dim cents As String = ""
        Dim strAmount As String = ""

        spacePos = (amount.ToString()).IndexOf(".")

        If spacePos < 0 Then

            rands = amount
            cents = "00"
            strAmount = rands & cents

        Else

            rands = (amount.ToString()).Substring(0, spacePos).Trim()
            cents = (amount.ToString()).Substring(spacePos + 1).Trim()
            'if cents amount = 1 in length
            If cents.Length = 1 Then
                cents &= 0
            ElseIf cents.Length > 2 Then
                cents = cents.Substring(0, 2)
            End If
            strAmount = rands & cents

        End If

        For i As Integer = strAmount.Length To 10

            strAmount = "0" & strAmount

        Next

        Return strAmount

    End Function

    Private Function getBankAcc(ByVal AccountNumber As String, ByVal accountCode As String)

        Dim strBankAcc As String = ""

        AccountNumber = AccountNumber.Trim()

        If AccountNumber.Length > 11 Then
            'reverse account number
            Dim tmpAcc As String = ReverseString(AccountNumber)
            'select first 11 characters
            tmpAcc = tmpAcc.Substring(0, 11)
            'undo reverse and update account
            AccountNumber = ReverseString(tmpAcc)

        End If

        For i As Integer = AccountNumber.Length To 10
            strBankAcc = "0" & strBankAcc
        Next

        strBankAcc &= AccountNumber

        Return strBankAcc.Trim()

    End Function

    Private Function getBranchCode(ByVal BranchCode As String)

        Dim strBCode As String = BranchCode.Trim()
        BranchCode = BranchCode.Trim()

        If (BranchCode.Trim()).Length > 6 Then

            strBCode = strBCode.Substring(0, 6)

        End If

        For i As Integer = (BranchCode.Trim()).Length To 5
            strBCode = "0" & strBCode
        Next

        BranchCode = strBCode

        Return BranchCode


    End Function

    Private Function ReverseString(ByVal S As String)
        Dim newS As String = ""
        Dim count As Integer = S.Length - 1

        While count >= 0
            newS &= S.Chars(count)
            count -= 1
        End While

        Return newS

    End Function

    Private Function getAbbreviatedUserName(ByVal AccountName As String) As String

        Dim abbreviatedUserName As String
        Dim nameLength As Integer
        Dim NameLengthDiff As Integer
        Dim index As Integer
        Dim newName As String = ""
        Dim space As String = " "

        abbreviatedUserName = AccountName.Replace(" ", "")

        nameLength = abbreviatedUserName.Length

        If nameLength > 10 Then

            abbreviatedUserName = abbreviatedUserName.Substring(0, 10)

        End If

        nameLength = abbreviatedUserName.Length

        If nameLength < 10 Then

            NameLengthDiff = 10 - nameLength

            Do While index < NameLengthDiff

                newName += space

                index += 1

            Loop

        End If

        newName = abbreviatedUserName + newName

        Return newName

    End Function

    Private Function getUserRef(ByVal AccountCode As String) As String

        Dim AccCode As String
        Dim CodeLength As Integer = 0
        Dim CodeLengthDiff As Integer = 0
        Dim UserRef As String = ""
        Dim space As String = " "
        Dim index As Integer = 1

        AccCode = AccountCode
        CodeLength = AccCode.Length - 1

        If CodeLength <= 14 Then

            CodeLengthDiff = 14 - CodeLength

            Do While index < CodeLengthDiff

                UserRef += space

                index += 1

            Loop

        End If

        UserRef = AccCode + UserRef + Date.Now.ToString("yyddMM")

        Return UserRef

    End Function

    Private Function getFillerO(ByVal AccountCode As String) As String

        Dim FillerO As String = Date.Today.ToString("MMMM")
        FillerO = FillerO.Substring(0, 3)

        FillerO &= " " & AccountCode.Substring(0, 6)

        Return FillerO

    End Function

    Private Function getFillerP(ByVal AccountHolder As String) As String

        Dim FillerP As String = ""

        AccountHolder = AccountHolder.Replace(",", "")
        AccountHolder = RTrim(LTrim(AccountHolder))
        AccountHolder = AccountHolder.Replace(ChrW(9), "")

        For i As Integer = 0 To 14

            If i > AccountHolder.Length - 1 Then
                FillerP &= " "
            Else
                FillerP &= AccountHolder.Chars(i).ToString
            End If

        Next

        Return FillerP

    End Function

    Private Function getFillerQ(ByVal AccountHolder As String) As String

        Dim FillerQ As String = ""

        AccountHolder = AccountHolder.Replace(",", "")
        AccountHolder = RTrim(LTrim(AccountHolder))
        AccountHolder = AccountHolder.Replace(ChrW(9), "")

        For i As Integer = 15 To 29

            If i > AccountHolder.Length - 1 Then
                FillerQ &= " "
            Else
                FillerQ &= AccountHolder.Chars(i).ToString
            End If

        Next

        Return FillerQ

    End Function

End Module
