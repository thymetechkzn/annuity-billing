﻿Public Class frmUserAdmin

    Dim dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)
    Dim RowIndex As Integer

    Public Event editUser(ByVal userID As String, ByVal userName As String, ByVal password As String, _
                          ByVal isRecBill As Boolean, ByVal isDORun As Boolean, ByVal isPriceUpd As Boolean, _
                           ByVal isReceipting As Boolean, ByVal isRejections As Boolean, ByVal isStatements As Boolean, _
                          ByVal isAdmin As Boolean, ByVal frmName As String)

    Public Event addUser(ByVal frmName As String)
    Dim isAdmin As Boolean
    Dim uName As String
    Dim pWord As String

    Sub New(ByVal isAdmin As Boolean, ByVal userName As String, ByVal password As String)

        ' This call is required by the designer.
        InitializeComponent()

        Me.isAdmin = isAdmin
        Me.uName = userName
        Me.pWord = password

        If isAdmin = True Then

            btnDelete.Enabled = True
            btnAddUser.Enabled = True

        Else

            btnDelete.Enabled = False
            btnAddUser.Enabled = False

        End If

        ' Add any initialization after the InitializeComponent() call.

    End Sub
    Sub New()

        ' This call is required by the designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub frmUserAdmin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If isAdmin = True Then

            loadAdminDGV()

        Else

            loadUserDGV()

        End If

    End Sub

    Private Sub loadAdminDGV()

        Dim qry As String = "SELECT" & vbCrLf & _
                            "userID AS 'User ID'," & vbCrLf & _
                            "name AS 'Name'," & vbCrLf & _
                            "username As 'Username'," & vbCrLf & _
                            "isRecurRun As 'Recurring Run'," & vbCrLf & _
                            "isDORun AS 'Debit Order Generator'," & vbCrLf & _
                            "isReceiptingGen As 'Receipting Generator'," & vbCrLf & _
                            "isStatementGen AS 'Statement Generator'," & vbCrLf & _
                            "isRejectionGen As 'Rejection Generator'," & vbCrLf & _
                            "isPriceUpdater as 'Price Updater'," & vbCrLf & _
                            "isAdmin AS 'Administrator'" & vbCrLf & _
                            "FROM" & vbCrLf & _
                            "userAdmin" & vbCrLf & _
                            "ORDER BY isAdmin desc, Name"

        Dim ds As New DataSet

        ds = dbconn.Return_Dataset(qry)
        dgvUserAdmin.DataSource = ds.Tables(0)

        dgvUserAdmin.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvUserAdmin.MultiSelect = False
        dgvUserAdmin.Rows(0).ReadOnly = True


    End Sub
    Private Sub loadUserDGV()

        Dim qry As String = "SELECT" & vbCrLf & _
                            "userID AS 'User ID'," & vbCrLf & _
                            "name AS 'Name'," & vbCrLf & _
                            "[Password] as 'Password'," & vbCrLf & _
                            "username As 'Username'," & vbCrLf & _
                            "isRecurRun As 'Recurring Run'," & vbCrLf & _
                            "isDORun AS 'Debit Order Generator'," & vbCrLf & _
                            "isReceiptingGen As 'Receipting Generator'," & vbCrLf & _
                            "isStatementGen AS 'Statement Generator'," & vbCrLf & _
                            "isRejectionGen As 'Rejection Generator'," & vbCrLf & _
                            "isPriceUpdater as 'Price Updater'," & vbCrLf & _
                            "isAdmin AS 'Administrator'" & vbCrLf & _
                            "FROM" & vbCrLf & _
                            "userAdmin" & vbCrLf & _
                            "WHERE" & vbCrLf & _
                            "username = '" & uName & "'" & vbCrLf & _
                            "AND" & vbCrLf & _
                            "password = '" & pWord & "'"

        Dim ds As New DataSet

        ds = dbConn.Return_Dataset(qry)
        dgvUserAdmin.DataSource = ds.Tables(0)

        dgvUserAdmin.SelectionMode = DataGridViewSelectionMode.FullRowSelect
        dgvUserAdmin.MultiSelect = False
        dgvUserAdmin.Rows(0).ReadOnly = True


    End Sub

    Private Function getPassword(ByVal name As String) As String

        Dim password As String

        Dim qry As String = "SELECT" & vbCrLf & _
                            "[Password] as 'Password'" & vbCrLf & _
                            "FROM" & vbCrLf & _
                            "userAdmin" & vbCrLf & _
                            "WHERE" & vbCrLf & _
                            "name = '" & name & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        password = dbConn.get_value("Password")

        Return password

    End Function

    Private Sub dgvUserAdmin_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvUserAdmin.CellClick

        RowIndex = e.RowIndex

    End Sub

    Private Sub btnAddUser_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAddUser.Click

        Dim frmAddUser As New frmEditAddUser("Add User")

        Me.Hide()
        frmAddUser.ShowDialog()
        Me.Show()

        dgvUserAdmin.CancelEdit()
        dgvUserAdmin.Columns.Clear()
        dgvUserAdmin.DataSource = Nothing

        If isAdmin = True Then

            loadAdminDGV()

        Else

            loadUserDGV()

        End If

    End Sub

    Private Sub btnDelete_Click(sender As System.Object, e As System.EventArgs) Handles btnDelete.Click

        'Decalre Variables
        Dim UserID As String
        Dim Query As String
        Dim msgResult As Integer

        'Prompt User if he/she would like to delete the selected user
        msgResult = MessageBox.Show("Are you sure that you want to delete the following user: " & vbCrLf & _
                                    dgvUserAdmin.Rows(RowIndex).Cells("Name").Value _
                                    , "Delete User", MessageBoxButtons.YesNo)

        If msgResult = DialogResult.Yes Then

            UserID = dgvUserAdmin.Rows(RowIndex).Cells(0).Value

            Query = "delete from useradmin" & vbCrLf & _
                    "where userid = " & UserID

            dbConn.setSqlString(Query)
            dbConn.createNewConnection()
            dbConn.CloseConnection()

        End If

        dgvUserAdmin.CancelEdit()
        dgvUserAdmin.Columns.Clear()
        dgvUserAdmin.DataSource = Nothing

        If isAdmin = True Then

            loadAdminDGV()

        Else

            loadUserDGV()

        End If


    End Sub

    Private Sub dgvUserAdmin_CellContentDoubleClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvUserAdmin.CellContentDoubleClick

        Dim userID As String
        Dim name As String
        Dim userName As String
        Dim password As String
        Dim isPriceUpd As Boolean
        Dim isRecBill As Boolean
        Dim isDORun As Boolean
        Dim receipting As Boolean
        Dim rejection As Boolean
        Dim statementGen As Boolean
        Dim administrator As Boolean
        Dim formName As String
        If isAdmin = True Then

            formName = "Edit User"

        Else

            formName = "Edit Login Details"

        End If


        userID = dgvUserAdmin.Rows(e.RowIndex).Cells("User ID").Value()
        name = dgvUserAdmin.Rows(e.RowIndex).Cells("Name").Value()
        userName = dgvUserAdmin.Rows(e.RowIndex).Cells("Username").Value()
        password = getPassword(name)
        isRecBill = dgvUserAdmin.Rows(e.RowIndex).Cells("Recurring Run").Value()
        isDORun = dgvUserAdmin.Rows(e.RowIndex).Cells("Debit Order Generator").Value()
        receipting = dgvUserAdmin.Rows(e.RowIndex).Cells("Receipting Generator").Value()
        statementGen = dgvUserAdmin.Rows(e.RowIndex).Cells("Statement Generator").Value()
        rejection = dgvUserAdmin.Rows(e.RowIndex).Cells("Rejection Generator").Value()
        isPriceUpd = dgvUserAdmin.Rows(e.RowIndex).Cells("Price Updater").Value()
        administrator = dgvUserAdmin.Rows(e.RowIndex).Cells("Administrator").Value()

        'RaiseEvent editUser(userID, userName, password, isRecBill, isDORun, isPriceUpd, receipting, rejection, statementGen,
        '                    administrator, formName)

        Dim frmEditUser As New frmEditAddUser(userID, name, userName, password, isRecBill, isDORun, isPriceUpd, receipting, rejection, statementGen,
                            administrator, formName, isAdmin)

        Me.Hide()
        frmEditUser.ShowDialog()
        Me.Show()
    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

End Class