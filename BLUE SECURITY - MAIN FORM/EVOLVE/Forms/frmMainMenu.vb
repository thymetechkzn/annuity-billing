﻿Imports DebitOrderFileGenerator
Imports PriceUpdater
Imports RadioLicenceMailMerger
Imports RecurringRunInvoiceGenerator
Imports Payment_Allocations
Imports RejectionFeeInvoice
Imports StatementReporter
Imports InvoiceReport

Public Class frmMainmenu

    Dim runMonth As String = "July 2014"
    Dim BatchIncrement As Integer = 1000
    Dim lstBatchPlan As List(Of BatchExecutionPlan)
    Dim index As Integer = 0
    Dim break As Integer = 0
    Dim path As String
    'FORM LOAD
    Private Sub frmHome_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Enable buttons
        btnAdmin.Enabled = True 'All users have access to the User Administration applications

        If My.Settings.isAdmin = True Then

            btnAnnuBill.Enabled = True
            btnDORun.Enabled = True
            btnReceiptingGen.Enabled = True
            btnRejectionInvGen.Enabled = True
            btnInvoiceGen.Enabled = True
            btnStatementGen.Enabled = True
            btnPriceUpd.Enabled = True
            btnRadioLicenceMailMerger.Enabled = True
            btnConnectionSettings.Enabled = True
            btnAnnuBill.Focus()

        Else 'Only enable flagged buttons

            enableFlaggedApplications()

        End If


    End Sub

    'OPENS PRICE UPDATER
    Private Sub btnPriceUpd_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnPriceUpd.Click

        'Declare Object
        Dim priceUpdater As New frmPriceUpdater(My.Settings.DBInstance, My.Settings.DBName, _
                                          My.Settings.DBUser, My.Settings.DBPassword, My.Settings.EvolutionCommon)

        Me.Hide()
        priceUpdater.ShowDialog()
        priceUpdater.Close()
        Me.Show()

    End Sub
    'OPEN RADIO LICENCE MAIL MERGER
    Private Sub btnRadioLicenceMailMerger_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRadioLicenceMailMerger.Click

        'Declare Object
        Dim radioLicMailMerger As New frmRadioLicMailMerger(My.Settings.DBInstance, My.Settings.DBName, _
                                          My.Settings.DBUser, My.Settings.DBPassword, My.Settings.EvolutionCommon)

        Me.Hide()
        radioLicMailMerger.ShowDialog()
        radioLicMailMerger.Close()
        Me.Show()

    End Sub
    'OPENS RECURRING RUN APPLICATION
    Private Sub btnAnnuBill_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAnnuBill.Click

        'Declare Object
        Dim recRun As New frmAnnuityBill(My.Settings.DBInstance, My.Settings.DBName, _
                                          My.Settings.DBUser, My.Settings.DBPassword, My.Settings.EvolutionCommon)

        Me.Hide()
        recRun.ShowDialog()
        recRun.Close()
        Me.Show()

    End Sub
    'OPENS DEBIT ORDER  GENERATOR
    Private Sub btnDORun_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDORun.Click

        'Declare Object
        Dim dORun As New frmDebitOrderGen(My.Settings.DBInstance, My.Settings.DBName, My.Settings.CashDBName, _
                                          My.Settings.DBUser, My.Settings.DBPassword, My.Settings.EvolutionCommon)

        Me.Hide()
        dORun.ShowDialog()
        dORun.Close()
        Me.Show()

    End Sub
    'OPENS RECEIPTING GENERATOR
    Private Sub btnReceiptingGen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReceiptingGen.Click

        'Declare Object
        Dim receiptingGen As New frmReceiptingMain(My.Settings.DBInstance, My.Settings.DBName, _
                                          My.Settings.DBUser, My.Settings.DBPassword, My.Settings.EvolutionCommon)

        Me.Hide()
        receiptingGen.ShowDialog()
        receiptingGen.Close()
        Me.Show()

    End Sub
    'OPENS REJECTION INVOICE GENERATOR
    Private Sub btnRejectionInvGen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRejectionInvGen.Click

        ' Declare Object
        Dim rejectionInv As New frmRejectionInvMain(My.Settings.DBInstance, My.Settings.DBName, _
                                          My.Settings.DBUser, My.Settings.DBPassword, My.Settings.EvolutionCommon)

        Me.Hide()
        rejectionInv.ShowDialog()
        rejectionInv.Close()
        Me.Show()

    End Sub
    'OPENS PDF INVOICE GENERATOR
    Private Sub btnInvoiceGen_Click_1(sender As System.Object, e As System.EventArgs) Handles btnInvoiceGen.Click



        'Declare Object
        Dim frmInvoice As New frmInvRepMain(My.Settings.DBInstance, _
                                                    My.Settings.DBName, _
                                                    My.Settings.DBUser, _
                                                    My.Settings.DBPassword, _
                                                    My.Settings.EvolutionCommon, _
                                                    My.Settings.EmailInvoiceSubject, _
                                                    My.Settings.EmailInvoiceBody, _
                                                    My.Settings.EmailInvoiceTestMode, _
                                                    My.Settings.EmailInvoiceTestAddress)

        Me.Hide()
        frmInvoice.ShowDialog()
        frmInvoice.Close()
        Me.Show()



        ''Decalre
        'Dim BatchPlanExecution As New repBatchExecutionPlan

        ''Get Path
        'path = InputBox("Please enter path:")

        ''Execute Stored Proc
        '' excuteStoredProc()



        ''Calculate Execution Plan
        'lstBatchPlan = BatchPlanExecution.calcBatchPlan(BatchIncrement)

        ''BatchPlanExecution.InsertExecutionPlan(lstBatchPlan)

        ''Loop Through Batch
        'index = 0
        'timerBatch.Interval = 1000
        'timerBatch.Start()

    End Sub
    'Private Sub timerBatch_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles timerBatch.Tick

    '    Dim appRuning As Boolean = False
    '    Dim BatchExecutionPlan As New repBatchExecutionPlan
    '    Dim tblStartID As Integer


    '    If index <> lstBatchPlan.Count Then

    '        'Loops through applications that are processing
    '        For Each Proc As Process In Process.GetProcesses
    '            Debug.WriteLine(Proc.ProcessName)
    '            If Proc.ProcessName.Equals("BatchGenerator") Then 'IF PROCESSING
    '                ' Proc.Kill() 'just for testing
    '                appRuning = True

    '            End If

    '        Next

    '        tblStartID = BatchExecutionPlan.GetStartID()

    '        If appRuning = False Then
    '            If index <= lstBatchPlan.Count - 1 Then
    '                '=TRUE
    '                If break = 2 Then

    '                    break = 0
    '                    Threading.Thread.Sleep(1800000)

    '                End If

    '                'If next batch in table is not equal to batch to be processed
    '                If Not (tblStartID = (lstBatchPlan.Item(index).StartNumber)) Then

    '                    'Garbage Disposal
    '                    lstBatchPlan.Clear()
    '                    lstBatchPlan = Nothing

    '                    'Reload Execution Plan
    '                    lstBatchPlan = BatchExecutionPlan.ReloadBatchPlan()

    '                    break = 0
    '                    index = 0

    '                End If
    '                Shell(path & "\BatchGenerator.exe /" & lstBatchPlan.Item(index).StartNumber & " /" & lstBatchPlan.Item(index).EndNumber)
    '                index += 1
    '                break += 1
    '            Else
    '                '=FALSE
    '                timerBatch.Stop()
    '            End If
    '        End If

    '    Else

    '        timerBatch.Stop()
    '        MsgBox("Run has completed")

    '    End If


    'End Sub


    'OPENS PDF STATEMENT GENERATOR
    Private Sub btnStatementGen_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnStatementGen.Click

        'Declare Object
        Dim statementGen As New frmStatementRepMain(My.Settings.DBInstance, _
                                                    My.Settings.DBName, _
                                                    My.Settings.DBUser, _
                                                    My.Settings.DBPassword, _
                                                    My.Settings.EvolutionCommon, _
                                                    My.Settings.EmailStatementSubject, _
                                                    My.Settings.EmailStatementBody, _
                                                    My.Settings.EmailStatementTestMode, _
                                                    My.Settings.EmailStatementTestAddress)

        Me.Hide()
        statementGen.ShowDialog()
        statementGen.Close()
        Me.Show()

    End Sub
    'OPENS USER ADMINISTRATION
    Private Sub btnAdmin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAdmin.Click

        'Declare Object
        Dim admin As New frmUserAdmin(My.Settings.isAdmin, My.Settings.SavedUser, My.Settings.SavedPassword)

        Me.Hide()
        admin.ShowDialog()
        admin.Close()
        Me.Show()

    End Sub
    'OPENS CONNECTION SETTINGS
    Private Sub btnConnectionSettings_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnConnectionSettings.Click

        'Declare Object
        Dim connSettings As New frmSettings

        Me.Hide()
        connSettings.ShowDialog()
        connSettings.Close()
        Me.Show()

    End Sub
    Private Sub AboutToolStripMenuItem1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AboutToolStripMenuItem1.Click

        Dim frmHelp As New frmAbout

        Me.Hide()
        frmHelp.ShowDialog()
        Me.Show()

    End Sub
    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    'ENABLES FLAGGED APPLICATIONS
    Private Sub enableFlaggedApplications()

        'Recurring Run is Flagged - Enable Button
        If My.Settings.isRecRun = True Then

            btnAnnuBill.Enabled = True

        End If
        'Debit Order Generator is Flagged - Enable Button
        If My.Settings.isDORun = True Then

            btnDORun.Enabled = True

        End If
        'Receipting Generator is Flagged - Enable Button
        If My.Settings.isReceiptingRun = True Then

            btnReceiptingGen.Enabled = True

        End If
        'Rejection Invoice Generator is Flagged - Enable Button
        If My.Settings.isRejectionGenerator = True Then

            btnRejectionInvGen.Enabled = True

        End If
        'Statement Generator is Flagged - Enable Button
        If My.Settings.isStatementGenerator = True Then

            btnInvoiceGen.Enabled = True
            btnStatementGen.Enabled = True

        End If
        'Price Updater is Flagged - Enable Button
        If My.Settings.isPriceUpdater = True Then

            btnPriceUpd.Enabled = True

        End If

        'Radio Licence Mail Merger is Flagged - Enable Button
        If My.Settings.isRadioLicMailMerger = True Then

            btnRadioLicenceMailMerger.Enabled = True

        End If

    End Sub

    Private Sub FileToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles FileToolStripMenuItem.Click

    End Sub
End Class