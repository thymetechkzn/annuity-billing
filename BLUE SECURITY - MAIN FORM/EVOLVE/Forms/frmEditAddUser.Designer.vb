﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEditAddUser
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEditAddUser))
        Me.lblUsername = New System.Windows.Forms.Label()
        Me.lblPassword = New System.Windows.Forms.Label()
        Me.txtUsername = New System.Windows.Forms.TextBox()
        Me.txtPassword = New System.Windows.Forms.TextBox()
        Me.btnSubmit = New System.Windows.Forms.Button()
        Me.cbxPriceUpdater = New System.Windows.Forms.CheckBox()
        Me.cbxRecurrRun = New System.Windows.Forms.CheckBox()
        Me.cbxDOGen = New System.Windows.Forms.CheckBox()
        Me.cbxStatementGen = New System.Windows.Forms.CheckBox()
        Me.cbxRejectInvgen = New System.Windows.Forms.CheckBox()
        Me.cbxReceipting = New System.Windows.Forms.CheckBox()
        Me.cbxAdmin = New System.Windows.Forms.CheckBox()
        Me.txtName = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'lblUsername
        '
        Me.lblUsername.AutoSize = True
        Me.lblUsername.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblUsername.Location = New System.Drawing.Point(38, 72)
        Me.lblUsername.Name = "lblUsername"
        Me.lblUsername.Size = New System.Drawing.Size(79, 19)
        Me.lblUsername.TabIndex = 0
        Me.lblUsername.Text = "Username:"
        '
        'lblPassword
        '
        Me.lblPassword.AutoSize = True
        Me.lblPassword.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPassword.Location = New System.Drawing.Point(40, 104)
        Me.lblPassword.Name = "lblPassword"
        Me.lblPassword.Size = New System.Drawing.Size(75, 19)
        Me.lblPassword.TabIndex = 1
        Me.lblPassword.Text = "Password:"
        '
        'txtUsername
        '
        Me.txtUsername.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtUsername.Location = New System.Drawing.Point(133, 69)
        Me.txtUsername.Name = "txtUsername"
        Me.txtUsername.Size = New System.Drawing.Size(212, 27)
        Me.txtUsername.TabIndex = 2
        '
        'txtPassword
        '
        Me.txtPassword.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtPassword.Location = New System.Drawing.Point(133, 101)
        Me.txtPassword.Name = "txtPassword"
        Me.txtPassword.Size = New System.Drawing.Size(212, 27)
        Me.txtPassword.TabIndex = 3
        Me.txtPassword.UseSystemPasswordChar = True
        '
        'btnSubmit
        '
        Me.btnSubmit.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnSubmit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSubmit.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSubmit.Location = New System.Drawing.Point(266, 321)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(79, 30)
        Me.btnSubmit.TabIndex = 4
        Me.btnSubmit.Text = "Submit"
        Me.btnSubmit.UseVisualStyleBackColor = False
        '
        'cbxPriceUpdater
        '
        Me.cbxPriceUpdater.AutoSize = True
        Me.cbxPriceUpdater.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPriceUpdater.Location = New System.Drawing.Point(55, 147)
        Me.cbxPriceUpdater.Name = "cbxPriceUpdater"
        Me.cbxPriceUpdater.Size = New System.Drawing.Size(178, 23)
        Me.cbxPriceUpdater.TabIndex = 5
        Me.cbxPriceUpdater.Text = "Rights to Price Updater"
        Me.cbxPriceUpdater.UseVisualStyleBackColor = True
        '
        'cbxRecurrRun
        '
        Me.cbxRecurrRun.AutoSize = True
        Me.cbxRecurrRun.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRecurrRun.Location = New System.Drawing.Point(55, 170)
        Me.cbxRecurrRun.Name = "cbxRecurrRun"
        Me.cbxRecurrRun.Size = New System.Drawing.Size(181, 23)
        Me.cbxRecurrRun.TabIndex = 6
        Me.cbxRecurrRun.Text = "Rights to Recurring Run"
        Me.cbxRecurrRun.UseVisualStyleBackColor = True
        '
        'cbxDOGen
        '
        Me.cbxDOGen.AutoSize = True
        Me.cbxDOGen.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxDOGen.Location = New System.Drawing.Point(55, 193)
        Me.cbxDOGen.Name = "cbxDOGen"
        Me.cbxDOGen.Size = New System.Drawing.Size(235, 23)
        Me.cbxDOGen.TabIndex = 7
        Me.cbxDOGen.Text = "Rights to Debit Order Generator"
        Me.cbxDOGen.UseVisualStyleBackColor = True
        '
        'cbxStatementGen
        '
        Me.cbxStatementGen.AutoSize = True
        Me.cbxStatementGen.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxStatementGen.Location = New System.Drawing.Point(55, 266)
        Me.cbxStatementGen.Name = "cbxStatementGen"
        Me.cbxStatementGen.Size = New System.Drawing.Size(290, 23)
        Me.cbxStatementGen.TabIndex = 10
        Me.cbxStatementGen.Text = "Rights to Statement && Invoice Generator"
        Me.cbxStatementGen.UseVisualStyleBackColor = True
        '
        'cbxRejectInvgen
        '
        Me.cbxRejectInvgen.AutoSize = True
        Me.cbxRejectInvgen.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxRejectInvgen.Location = New System.Drawing.Point(55, 243)
        Me.cbxRejectInvgen.Name = "cbxRejectInvgen"
        Me.cbxRejectInvgen.Size = New System.Drawing.Size(270, 23)
        Me.cbxRejectInvgen.TabIndex = 9
        Me.cbxRejectInvgen.Text = "Rights to Rejection Invoice Generator"
        Me.cbxRejectInvgen.UseVisualStyleBackColor = True
        '
        'cbxReceipting
        '
        Me.cbxReceipting.AutoSize = True
        Me.cbxReceipting.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxReceipting.Location = New System.Drawing.Point(55, 218)
        Me.cbxReceipting.Name = "cbxReceipting"
        Me.cbxReceipting.Size = New System.Drawing.Size(228, 23)
        Me.cbxReceipting.TabIndex = 8
        Me.cbxReceipting.Text = "Rights to Receipting Generator"
        Me.cbxReceipting.UseVisualStyleBackColor = True
        '
        'cbxAdmin
        '
        Me.cbxAdmin.AutoSize = True
        Me.cbxAdmin.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxAdmin.Location = New System.Drawing.Point(55, 291)
        Me.cbxAdmin.Name = "cbxAdmin"
        Me.cbxAdmin.Size = New System.Drawing.Size(161, 23)
        Me.cbxAdmin.TabIndex = 11
        Me.cbxAdmin.Text = "Administrator Rights"
        Me.cbxAdmin.UseVisualStyleBackColor = True
        '
        'txtName
        '
        Me.txtName.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtName.Location = New System.Drawing.Point(133, 36)
        Me.txtName.Name = "txtName"
        Me.txtName.Size = New System.Drawing.Size(212, 27)
        Me.txtName.TabIndex = 13
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(38, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(51, 19)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "Name:"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(363, 24)
        Me.MenuStrip1.TabIndex = 14
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(92, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'frmEditAddUser
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(363, 362)
        Me.Controls.Add(Me.txtName)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.cbxAdmin)
        Me.Controls.Add(Me.cbxStatementGen)
        Me.Controls.Add(Me.cbxRejectInvgen)
        Me.Controls.Add(Me.cbxReceipting)
        Me.Controls.Add(Me.cbxDOGen)
        Me.Controls.Add(Me.cbxRecurrRun)
        Me.Controls.Add(Me.cbxPriceUpdater)
        Me.Controls.Add(Me.btnSubmit)
        Me.Controls.Add(Me.txtPassword)
        Me.Controls.Add(Me.txtUsername)
        Me.Controls.Add(Me.lblPassword)
        Me.Controls.Add(Me.lblUsername)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmEditAddUser"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblUsername As System.Windows.Forms.Label
    Friend WithEvents lblPassword As System.Windows.Forms.Label
    Friend WithEvents txtUsername As System.Windows.Forms.TextBox
    Friend WithEvents txtPassword As System.Windows.Forms.TextBox
    Friend WithEvents btnSubmit As System.Windows.Forms.Button
    Friend WithEvents cbxPriceUpdater As System.Windows.Forms.CheckBox
    Friend WithEvents cbxRecurrRun As System.Windows.Forms.CheckBox
    Friend WithEvents cbxDOGen As System.Windows.Forms.CheckBox
    Friend WithEvents cbxStatementGen As System.Windows.Forms.CheckBox
    Friend WithEvents cbxRejectInvgen As System.Windows.Forms.CheckBox
    Friend WithEvents cbxReceipting As System.Windows.Forms.CheckBox
    Friend WithEvents cbxAdmin As System.Windows.Forms.CheckBox
    Friend WithEvents txtName As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
End Class
