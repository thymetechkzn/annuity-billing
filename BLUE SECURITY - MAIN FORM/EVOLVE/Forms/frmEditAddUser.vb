﻿Public Class frmEditAddUser

    Dim userID As String
    Dim isNewUser As Boolean
    Dim dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Event submitCLicked()
    Dim iAdmin As Boolean

    Public Sub New(ByVal uID As String, ByVal name As String, ByVal uName As String, ByVal pword As String, _
                          ByVal accRecBill As Boolean, ByVal accDORun As Boolean, ByVal accPriceUpd As Boolean, _
                           ByVal isReceipting As Boolean, ByVal isRejections As Boolean, ByVal isStatements As Boolean, _
                          ByVal isAdmin As Boolean, ByVal formName As String, ByVal userIsAdmin As Boolean)

        ' This call is required by the designer.
        InitializeComponent()

        Me.userID = uID
        Me.iAdmin = userIsAdmin

        txtName.Text = name
        txtUsername.Text = uName
        txtPassword.Text = pword
        txtName.Enabled = False
        txtUsername.Enabled = False

        If accRecBill = True Then
            cbxRecurrRun.Checked = True
        End If

        If accDORun = True Then
            cbxDOGen.Checked = True
        End If

        If accPriceUpd = True Then
            cbxPriceUpdater.Checked = True
        End If
        '
        If isReceipting = True Then
            cbxReceipting.Checked = True
        End If

        If isRejections = True Then
            cbxRejectInvgen.Checked = True
        End If

        If isStatements = True Then
            cbxStatementGen.Checked = True
        End If

        If isAdmin = True Then
            cbxAdmin.Checked = True
        End If

        If Me.iAdmin = False Then

            cbxRecurrRun.Enabled = False
            cbxDOGen.Enabled = False
            cbxPriceUpdater.Enabled = False
            cbxReceipting.Enabled = False
            cbxRejectInvgen.Enabled = False
            cbxStatementGen.Enabled = False
            cbxAdmin.Enabled = False

        End If


        Me.Text = formName
        btnSubmit.Text = "Save Details"
        isNewUser = False
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Public Sub New(ByVal formName As String)

        Dim frmAdministration As New frmUserAdmin

        ' This call is required by the designer.
        InitializeComponent()
        Me.Text = formName
        btnSubmit.Text = "Add User"
        isNewUser = True
        ' Add any initialization after the InitializeComponent() call.

    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        editAddUser()
        Me.Close()

    End Sub

    Private Sub editAddUser()

        Dim qry As String = ""
        Dim cbPricUpd As Integer = 0
        Dim cbRecBill As Integer = 0
        Dim cbDORun As Integer = 0
        Dim cbReceipting As Integer = 0
        Dim cbRejection As Integer = 0
        Dim cbStatments As Integer = 0
        Dim cbAdmin As Integer = 0
        Dim message As String = ""

        'price Updater
        If cbxPriceUpdater.Checked = True Then
            cbPricUpd = 1
        End If
        'Recurring Run
        If cbxRecurrRun.Checked = True Then
            cbRecBill = 1
        End If
        'Debit Order
        If cbxDOGen.Checked = True Then
            cbDORun = 1
        End If

        If cbxReceipting.Checked = True Then
            cbReceipting = 1
        End If

        If cbxRejectInvgen.Checked = True Then
            cbRejection = 1
        End If

        If cbxStatementGen.Checked = True Then
            cbStatments = 1
        End If

        If cbxAdmin.Checked = True Then
            cbAdmin = 1
        End If

        If isNewUser = True Then

            qry = "INSERT INTO userAdmin (username, [password], name, isAdmin, isRecurRun, isDORun, isReceiptingGen, isRejectionGen, isStatementGen, isPriceUpdater)" & vbCrLf & _
                    "VALUES" & vbCrLf & _
                    "('" & txtUsername.Text & "', '" & txtPassword.Text & "', '" & txtName.Text & "'," & cbAdmin & "," & cbRecBill & "," & cbDORun & "," & vbCrLf & _
                       cbReceipting & "," & cbRejection & "," & cbStatments & "," & cbPricUpd & ")"

            message = "The user has been added!"

        ElseIf isNewUser = False Then

            qry = " UPDATE userAdmin" & vbCrLf & _
                    "SET " & vbCrLf & _
                    "[password] = '" & txtPassword.Text & "'," & vbCrLf & _
                    "isRecurRun = " & cbRecBill & "," & vbCrLf & _
                    "isDORun = " & cbDORun & "," & vbCrLf & _
                    "isReceiptingGen = " & cbReceipting & "," & vbCrLf & _
                    "isStatementGen =  " & cbStatments & "," & vbCrLf & _
                    "isRejectionGen =  " & cbRejection & "," & vbCrLf & _
                    "isPriceUpdater = " & cbPricUpd & "," & vbCrLf & _
                    "isAdmin =  " & cbAdmin & vbCrLf & _
                    "WHERE" & vbCrLf & _
                    "userID  = " & userID

            If iAdmin = True Then

                message = "The user has been updated!" & vbCrLf & vbCrLf & "Username: " & txtUsername.Text & vbCrLf & "Password: " & txtPassword.Text

            Else

                message = "Login destails have been updated!" & vbCrLf & vbCrLf & "Username: " & txtUsername.Text & vbCrLf & "Password: " & txtPassword.Text

            End If

        End If

        dbconn.setSqlString(qry)
        dbconn.createNewConnection()
        dbconn.CloseConnection()

        MsgBox(message)
        RaiseEvent submitCLicked()

    End Sub

    Private Sub ExitToolStripMenuItem_Click(sender As System.Object, e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

End Class