﻿Public Class frmLogin


    Private Sub frmLogin_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        'Output saved login details onto textboxes
        txtUser.Text = My.Settings.SavedUser
        txtPwd.Text = My.Settings.SavedPassword

        txtUser.SelectAll()

    End Sub
    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click

        'Finalise login settings

        'Save Setting if 'Remember Me' checkbox is checked
        If My.Settings.SavePassword = True Then
            My.Settings.SavedUser = txtUser.Text
            My.Settings.SavedPassword = txtPwd.Text
            My.Settings.Save()
        Else
            My.Settings.SavedUser = ""
            My.Settings.SavedPassword = ""
            My.Settings.Save()
        End If

        'If both login textbox are not blank - Call Method
        If Not (txtUser.Text.Equals("") Or txtPwd.Text.Equals("")) Then

            loginValidation()

        Else 'Error message if blank there are blank login details

            MsgBox("There are blank fields.", MsgBoxStyle.Critical, "Blank Fields")

        End If

    End Sub
    Private Sub cbxRememberMe_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxRememberMe.CheckedChanged

        'Save 'Remember Me' checkbox status
        My.Settings.SavePassword = cbxRememberMe.Checked
        My.Settings.Save()

    End Sub

    Private Sub loginValidation()

        'Declare Variables
        Dim isAdmin As Boolean = False
        Dim recRun As Boolean = False
        Dim dORun As Boolean = False
        Dim receiptingRun As Boolean = False
        Dim statementGenerator As Boolean = False
        Dim rejectionGenerator As Boolean = False
        Dim priceUpdater As Boolean = False
        Dim userExists As Integer = False

        Dim login As New loginDetails


        'For Installation Purposes
        If (txtUser.Text).ToUpper.Equals("ADMIN") And (txtPwd.Text).ToUpper.Equals("CONNECTION") Then

            'Set Variables
            My.Settings.isAdmin = True
            My.Settings.isRecRun = True
            My.Settings.isDORun = True
            My.Settings.isReceiptingRun = True
            My.Settings.isStatementGenerator = True
            My.Settings.isRejectionGenerator = True
            My.Settings.isPriceUpdater = True
            '
            openMenu()

        Else 'For Proper Validation

            'Checks if username & password exist - 1st Validation
            userExists = login.userExists(txtUser.Text, txtPwd.Text)


            If userExists = 1 Then 'If user  the exists

                isAdmin = login.isAdmin(txtUser.Text, txtPwd.Text)

                'Save user rights, settings
                My.Settings.isAdmin = login.isAdmin(txtUser.Text, txtPwd.Text)
                My.Settings.isRecRun = login.isRecRUn(txtUser.Text, txtPwd.Text)
                My.Settings.isDORun = login.isDORun(txtUser.Text, txtPwd.Text)
                My.Settings.isReceiptingRun = login.isReceiptingGen(txtUser.Text, txtPwd.Text)
                My.Settings.isStatementGenerator = login.isStatementGen(txtUser.Text, txtPwd.Text)
                My.Settings.isRejectionGenerator = login.isRejectionGen(txtUser.Text, txtPwd.Text)
                My.Settings.isPriceUpdater = login.isPriceUpdater(txtUser.Text, txtPwd.Text)
                My.Settings.isRadioLicMailMerger = login.isRadioLicMailMerger(txtUser.Text, txtPwd.Text)
                openMenu()

            Else 'Error Message if login details are incorrect

                MsgBox("The Userame or Password is incorrect!" & vbCrLf & "Please re-enter!", MsgBoxStyle.Critical, "Incorrect Login Details")

            End If

        End If

    End Sub
    Private Sub openMenu()

        'Declare Variables
        Dim mainMenu As New frmMainmenu

        'Open main menu, close login form
        Me.Hide()
        mainMenu.ShowDialog()
        Me.Close()

    End Sub

End Class
