﻿Imports Pastel.Evolution

Public Class frmSettings

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSave.Click


        My.Settings.DBInstance = txtDBInstance.Text
        My.Settings.DBUser = txtDBUser.Text
        My.Settings.DBPassword = txtDBPassword.Text
        My.Settings.DBName = txtPASCompany.Text
        My.Settings.EvolutionCommon = txtPASCommon.Text
        My.Settings.CashDBName = txtCashManager.Text

        My.Settings.Save()

        MsgBox("Settings have been saved")

        Me.Close()

    End Sub

    Private Sub frmSettings_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

        Me.Close()

    End Sub

    Private Sub frmSettings_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        txtDBInstance.Text = My.Settings.DBInstance
        txtDBUser.Text = My.Settings.DBUser
        txtDBPassword.Text = My.Settings.DBPassword
        txtPASCompany.Text = My.Settings.DBName
        txtPASCommon.Text = My.Settings.EvolutionCommon
        txtCashManager.Text = My.Settings.CashDBName

    End Sub

    Private Sub btnTest_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnTest.Click

        Try

            My.Settings.DBInstance = txtDBInstance.Text
            My.Settings.DBUser = txtDBUser.Text
            My.Settings.DBPassword = txtDBPassword.Text
            My.Settings.DBName = txtPASCompany.Text
            My.Settings.EvolutionCommon = txtPASCommon.Text
            My.Settings.CashDBName = txtCashManager.Text

            My.Settings.Save()

            If SDKOperation.CreateConnection() = True Then
                MsgBox("Connection Successful")
            Else
                MsgBox("Connection Details Incorrect")
            End If

        Catch ex As Exception
            MsgBox(ex.Message)
        End Try

    End Sub

    Private Sub ExitToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ExitToolStripMenuItem.Click
        Me.Close()
    End Sub

    Private Sub InvoiceSettingsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles InvoiceSettingsToolStripMenuItem.Click

        Dim frmSettings As New frmEmailInvoiceSettings()

        Me.Hide()
        frmSettings.ShowDialog()
        Me.Show()

    End Sub

    Private Sub StatementSettingsToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles StatementSettingsToolStripMenuItem.Click

        Dim frmSettings As New frmEmailStatementSettings()

        Me.Hide()
        frmSettings.ShowDialog()
        Me.Show()

    End Sub

End Class