﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMainmenu
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMainmenu))
        Me.btnRejectionInvGen = New System.Windows.Forms.Button()
        Me.btnDORun = New System.Windows.Forms.Button()
        Me.btnPriceUpd = New System.Windows.Forms.Button()
        Me.btnAdmin = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.btnRadioLicenceMailMerger = New System.Windows.Forms.Button()
        Me.btnStatementGen = New System.Windows.Forms.Button()
        Me.btnInvoiceGen = New System.Windows.Forms.Button()
        Me.btnReceiptingGen = New System.Windows.Forms.Button()
        Me.btnAnnuBill = New System.Windows.Forms.Button()
        Me.btnConnectionSettings = New System.Windows.Forms.Button()
        Me.lblCopyRight = New System.Windows.Forms.Label()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.FileToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AboutToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.timerBatch = New System.Windows.Forms.Timer(Me.components)
        Me.TableLayoutPanel1.SuspendLayout()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnRejectionInvGen
        '
        Me.btnRejectionInvGen.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnRejectionInvGen.Enabled = False
        Me.btnRejectionInvGen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRejectionInvGen.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRejectionInvGen.Location = New System.Drawing.Point(4, 249)
        Me.btnRejectionInvGen.Name = "btnRejectionInvGen"
        Me.btnRejectionInvGen.Size = New System.Drawing.Size(192, 42)
        Me.btnRejectionInvGen.TabIndex = 5
        Me.btnRejectionInvGen.Text = "Bank Fee Invoices"
        Me.btnRejectionInvGen.UseVisualStyleBackColor = False
        '
        'btnDORun
        '
        Me.btnDORun.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnDORun.Enabled = False
        Me.btnDORun.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnDORun.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnDORun.Location = New System.Drawing.Point(4, 151)
        Me.btnDORun.Name = "btnDORun"
        Me.btnDORun.Size = New System.Drawing.Size(192, 42)
        Me.btnDORun.TabIndex = 3
        Me.btnDORun.Text = "Debit Order Run"
        Me.btnDORun.UseVisualStyleBackColor = False
        '
        'btnPriceUpd
        '
        Me.btnPriceUpd.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnPriceUpd.Enabled = False
        Me.btnPriceUpd.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPriceUpd.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPriceUpd.Location = New System.Drawing.Point(4, 4)
        Me.btnPriceUpd.Name = "btnPriceUpd"
        Me.btnPriceUpd.Size = New System.Drawing.Size(192, 42)
        Me.btnPriceUpd.TabIndex = 1
        Me.btnPriceUpd.Text = "Price Updater"
        Me.btnPriceUpd.UseVisualStyleBackColor = False
        '
        'btnAdmin
        '
        Me.btnAdmin.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnAdmin.Enabled = False
        Me.btnAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAdmin.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAdmin.Location = New System.Drawing.Point(4, 445)
        Me.btnAdmin.Name = "btnAdmin"
        Me.btnAdmin.Size = New System.Drawing.Size(192, 40)
        Me.btnAdmin.TabIndex = 8
        Me.btnAdmin.Text = "User Administration"
        Me.btnAdmin.UseVisualStyleBackColor = False
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.[Single]
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.btnRadioLicenceMailMerger, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.btnStatementGen, 0, 7)
        Me.TableLayoutPanel1.Controls.Add(Me.btnPriceUpd, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.btnAdmin, 0, 9)
        Me.TableLayoutPanel1.Controls.Add(Me.btnInvoiceGen, 0, 6)
        Me.TableLayoutPanel1.Controls.Add(Me.btnRejectionInvGen, 0, 5)
        Me.TableLayoutPanel1.Controls.Add(Me.btnReceiptingGen, 0, 4)
        Me.TableLayoutPanel1.Controls.Add(Me.btnDORun, 0, 3)
        Me.TableLayoutPanel1.Controls.Add(Me.btnAnnuBill, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.btnConnectionSettings, 0, 8)
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(124, 38)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 10
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 11.11111!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(200, 489)
        Me.TableLayoutPanel1.TabIndex = 4
        '
        'btnRadioLicenceMailMerger
        '
        Me.btnRadioLicenceMailMerger.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnRadioLicenceMailMerger.Enabled = False
        Me.btnRadioLicenceMailMerger.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRadioLicenceMailMerger.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRadioLicenceMailMerger.Location = New System.Drawing.Point(4, 53)
        Me.btnRadioLicenceMailMerger.Name = "btnRadioLicenceMailMerger"
        Me.btnRadioLicenceMailMerger.Size = New System.Drawing.Size(192, 42)
        Me.btnRadioLicenceMailMerger.TabIndex = 11
        Me.btnRadioLicenceMailMerger.Text = "Radio Licence Mail Merger"
        Me.btnRadioLicenceMailMerger.UseVisualStyleBackColor = False
        '
        'btnStatementGen
        '
        Me.btnStatementGen.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnStatementGen.Enabled = False
        Me.btnStatementGen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnStatementGen.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnStatementGen.Location = New System.Drawing.Point(4, 347)
        Me.btnStatementGen.Name = "btnStatementGen"
        Me.btnStatementGen.Size = New System.Drawing.Size(192, 42)
        Me.btnStatementGen.TabIndex = 10
        Me.btnStatementGen.Text = "PDF Statement Generator"
        Me.btnStatementGen.UseVisualStyleBackColor = False
        '
        'btnInvoiceGen
        '
        Me.btnInvoiceGen.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnInvoiceGen.Enabled = False
        Me.btnInvoiceGen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInvoiceGen.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInvoiceGen.Location = New System.Drawing.Point(4, 298)
        Me.btnInvoiceGen.Name = "btnInvoiceGen"
        Me.btnInvoiceGen.Size = New System.Drawing.Size(192, 42)
        Me.btnInvoiceGen.TabIndex = 6
        Me.btnInvoiceGen.Text = "PDF Invoice Generator"
        Me.btnInvoiceGen.UseVisualStyleBackColor = False
        '
        'btnReceiptingGen
        '
        Me.btnReceiptingGen.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnReceiptingGen.Enabled = False
        Me.btnReceiptingGen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReceiptingGen.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReceiptingGen.Location = New System.Drawing.Point(4, 200)
        Me.btnReceiptingGen.Name = "btnReceiptingGen"
        Me.btnReceiptingGen.Size = New System.Drawing.Size(192, 42)
        Me.btnReceiptingGen.TabIndex = 4
        Me.btnReceiptingGen.Text = "Debit Order Receipting"
        Me.btnReceiptingGen.UseVisualStyleBackColor = False
        '
        'btnAnnuBill
        '
        Me.btnAnnuBill.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnAnnuBill.Enabled = False
        Me.btnAnnuBill.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnAnnuBill.Font = New System.Drawing.Font("Calibri", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAnnuBill.Location = New System.Drawing.Point(4, 102)
        Me.btnAnnuBill.Name = "btnAnnuBill"
        Me.btnAnnuBill.Size = New System.Drawing.Size(192, 42)
        Me.btnAnnuBill.TabIndex = 2
        Me.btnAnnuBill.Text = "Annuity Billing"
        Me.btnAnnuBill.UseVisualStyleBackColor = False
        '
        'btnConnectionSettings
        '
        Me.btnConnectionSettings.BackColor = System.Drawing.SystemColors.GradientActiveCaption
        Me.btnConnectionSettings.Enabled = False
        Me.btnConnectionSettings.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnConnectionSettings.Font = New System.Drawing.Font("Calibri", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnConnectionSettings.Location = New System.Drawing.Point(4, 396)
        Me.btnConnectionSettings.Name = "btnConnectionSettings"
        Me.btnConnectionSettings.Size = New System.Drawing.Size(192, 42)
        Me.btnConnectionSettings.TabIndex = 9
        Me.btnConnectionSettings.Text = "Connection Settings"
        Me.btnConnectionSettings.UseVisualStyleBackColor = False
        '
        'lblCopyRight
        '
        Me.lblCopyRight.AutoSize = True
        Me.lblCopyRight.Font = New System.Drawing.Font("Calibri", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCopyRight.Location = New System.Drawing.Point(125, 535)
        Me.lblCopyRight.Name = "lblCopyRight"
        Me.lblCopyRight.Size = New System.Drawing.Size(185, 14)
        Me.lblCopyRight.TabIndex = 9
        Me.lblCopyRight.Text = "© Copyright 2015 Ritzy IT (Pty) LTD"
        '
        'MenuStrip1
        '
        Me.MenuStrip1.BackColor = System.Drawing.SystemColors.ControlLight
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FileToolStripMenuItem, Me.AboutToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(479, 24)
        Me.MenuStrip1.TabIndex = 10
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'FileToolStripMenuItem
        '
        Me.FileToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ExitToolStripMenuItem})
        Me.FileToolStripMenuItem.Name = "FileToolStripMenuItem"
        Me.FileToolStripMenuItem.Size = New System.Drawing.Size(37, 20)
        Me.FileToolStripMenuItem.Text = "File"
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ExitToolStripMenuItem.Text = "Exit"
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AboutToolStripMenuItem1})
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(44, 20)
        Me.AboutToolStripMenuItem.Text = "Help"
        '
        'AboutToolStripMenuItem1
        '
        Me.AboutToolStripMenuItem1.Name = "AboutToolStripMenuItem1"
        Me.AboutToolStripMenuItem1.Size = New System.Drawing.Size(107, 22)
        Me.AboutToolStripMenuItem1.Text = "About"
        '
        'frmMainmenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.ClientSize = New System.Drawing.Size(479, 558)
        Me.Controls.Add(Me.lblCopyRight)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.Font = New System.Drawing.Font("Calibri", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMainmenu"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Home"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents btnRejectionInvGen As System.Windows.Forms.Button
    Friend WithEvents btnDORun As System.Windows.Forms.Button
    Friend WithEvents btnPriceUpd As System.Windows.Forms.Button
    Friend WithEvents btnAdmin As System.Windows.Forms.Button
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents lblCopyRight As System.Windows.Forms.Label
    Friend WithEvents btnConnectionSettings As System.Windows.Forms.Button
    Friend WithEvents btnAnnuBill As System.Windows.Forms.Button
    Friend WithEvents btnReceiptingGen As System.Windows.Forms.Button
    Friend WithEvents btnInvoiceGen As System.Windows.Forms.Button
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents FileToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ExitToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents AboutToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents timerBatch As System.Windows.Forms.Timer
    Friend WithEvents btnRadioLicenceMailMerger As System.Windows.Forms.Button
    Friend WithEvents btnStatementGen As System.Windows.Forms.Button
End Class
