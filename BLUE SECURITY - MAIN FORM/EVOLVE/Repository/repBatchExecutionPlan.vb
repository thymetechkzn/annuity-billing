﻿Public Class repBatchExecutionPlan

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function calcBatchPlan(ByVal BatchIncrement As Integer) As List(Of BatchExecutionPlan)

        'Decalre
        Dim lstBatchPlan As New List(Of BatchExecutionPlan)
        Dim LastID As Integer
        Dim totBatchNum As Integer
        Dim BatchNum As Integer = 1

        'Get last InvoiceID
        Dim repBatchPlan As New repBatchExecutionPlan
        LastID = (repBatchPlan.getTableEndID())

        'Calc Total BatchNums 
        totBatchNum = LastID / BatchIncrement

        Do While BatchNum <= totBatchNum

            If BatchNum = 17 Then
                Debug.WriteLine("")

            End If

            Dim batchPlan As New BatchExecutionPlan

            batchPlan.StartNumber = (((BatchNum - 1) * BatchIncrement) + 1)
            batchPlan.EndNumber = (batchPlan.StartNumber + BatchIncrement) - 1

            'Save
            lstBatchPlan.Add(batchPlan)

            'Increment Index
            BatchNum += 1

        Loop

        Return lstBatchPlan

    End Function

    'GETS STARTID & ENDID FROM tblInvoiceToProcess
    Private Function getTableEndID() As Integer

        'Declare
        Dim qry As String
        Dim endID As Integer

        Try

            qry = "SELECT" & vbCrLf & _
                    "TOP 1" & vbCrLf & _
                    "InvoiceID" & vbCrLf & _
                    "FROM" & vbCrLf & _
                    "tblInvoiceToProcess" & vbCrLf & _
                    "ORDER BY InvoiceID DESC"


            dbConn.setSqlString(qry)
            dbConn.createNewConnection()

            endID = dbConn.get_value("InvoiceID")

            Return endID

        Catch ex As Exception

        End Try

        Return endID

    End Function

    'INSERT BATCH EXECUTION PLAN INTO InvBatchExecutionPlan TABLE
    Public Sub InsertExecutionPlan(ByVal lstBatchPlan As List(Of BatchExecutionPlan))

        'Decalre
        Dim qry As String

        For Each BatchPlan As BatchExecutionPlan In lstBatchPlan

            qry = "INSERT INTO InvBatchExecutionPlan" & vbCrLf & _
                    "(" & vbCrLf & _
                    "StartID," & vbCrLf & _
                    "EndID, RunComplete" & vbCrLf & _
                    ")" & vbCrLf & _
                    "VALUEs (" & BatchPlan.StartNumber & "," & BatchPlan.EndNumber & ",0);"

            dbConn.setSqlString(qry)
            dbConn.createNewConnection()
            dbConn.CloseConnection()

        Next

    End Sub

    Public Function GetStartID() As String

        'Declare
        Dim qry As String
        Dim StartID As Integer

        qry = "SELECT TOP 1 * FROM InvBatchExecutionPlan WHERE RunComplete = 0"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        StartID = dbConn.get_value("StartID")

        Return StartID

    End Function

    Public Function ReloadBatchPlan() As List(Of BatchExecutionPlan)

        'Declare
        Dim lstBatchPlan As New List(Of BatchExecutionPlan)


        For Each BatchPlan As DataRow In GetBatchPlan().Rows

            Dim Plan As New BatchExecutionPlan

            Plan.StartNumber = BatchPlan.Item("StartID")
            Plan.EndNumber = BatchPlan.Item("EndID")

            lstBatchPlan.Add(Plan)

        Next

        Return lstBatchPlan

    End Function

    Private Function GetBatchPlan() As DataTable

        'Declare
        Dim qry As String
        Dim DT As New DataTable

        qry = "SELECT * FROM InvBatchExecutionPlan WHERE RunComplete = 0"

        DT = dbConn.get_datatable(qry)

        Return DT

    End Function

End Class
