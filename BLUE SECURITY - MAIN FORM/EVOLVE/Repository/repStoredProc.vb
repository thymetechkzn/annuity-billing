﻿Public Class repStoredProc

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function executeStoredProc(ByVal runMonth As String) As Boolean

        'Declare
        Dim qry As String

        Try

            qry = "exec prcPopulateInvoiceBatch '" & runMonth & "';"

            dbConn.setSqlString(qry)
            dbConn.createNewConnection()
            dbConn.CloseConnection()

            Return True

        Catch ex As Exception
            MsgBox("Execute Stored Procedure: " & ex.Message)
            Return False
        End Try

    End Function

End Class

