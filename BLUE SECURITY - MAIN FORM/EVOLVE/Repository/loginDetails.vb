﻿Public Class loginDetails

    Private dbConn As New sql_dbcon(My.Settings.DBInstance, My.Settings.DBName, My.Settings.DBUser, My.Settings.DBPassword)

    Public Function userExists(ByVal userName As String, ByVal password As String) As Integer
        Try
            'Declare Variables
            Dim qry As String
            Dim intNumRows As Integer

            qry = "SELECT" & vbCrLf & _
                "COUNT(*) as 'numRows'" & vbCrLf & _
                "FROM" & vbCrLf & _
                "userAdmin" & vbCrLf & _
                "WHERE" & vbCrLf & _
                "userName = '" & userName & "'" & vbCrLf & _
                "AND" & vbCrLf & _
                "password = '" & password & "'"

            dbConn.setSqlString(qry)
            dbConn.createNewConnection()

            intNumRows = Convert.ToInt32(dbConn.get_value("numRows"))

            Return intNumRows

        Catch ex As Exception
            Return 0
        End Try

    End Function

    Public Function isAdmin(ByVal userName As String, ByVal password As String) As Boolean

        'Declare Variables
        Dim qry As String
        Dim booAdmin As Boolean

        qry = "SELECT" & vbCrLf & _
            "isAdmin" & vbCrLf & _
            "FROM" & vbCrLf & _
            "userAdmin" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "userName = '" & userName & "'" & vbCrLf & _
            "AND" & vbCrLf & _
            "password = '" & password & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        booAdmin = dbConn.get_value("isAdmin")

        Return booAdmin

    End Function

    Public Function isRecRUn(ByVal userName As String, ByVal password As String) As Boolean

        'Declare Variables
        Dim qry As String
        Dim booRecRun As Boolean

        qry = "SELECT" & vbCrLf & _
            "isRecurRun" & vbCrLf & _
            "FROM" & vbCrLf & _
            "userAdmin" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "userName = '" & userName & "'" & vbCrLf & _
            "AND" & vbCrLf & _
            "password = '" & password & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        booRecRun = dbConn.get_value("isRecurRun")

        Return booRecRun

    End Function

    Public Function isDORun(ByVal userName As String, ByVal password As String) As Boolean

        'Declare Variables
        Dim qry As String
        Dim booDORun As Boolean

        qry = "SELECT" & vbCrLf & _
            "isDORun" & vbCrLf & _
            "FROM" & vbCrLf & _
            "userAdmin" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "userName = '" & userName & "'" & vbCrLf & _
            "AND" & vbCrLf & _
            "password = '" & password & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        booDORun = dbConn.get_value("isDORun")

        Return booDORun

    End Function

    Public Function isReceiptingGen(ByVal userName As String, ByVal password As String) As Boolean

        'Declare Variables
        Dim qry As String
        Dim booReceiptingGen As Boolean

        qry = "SELECT" & vbCrLf & _
            "isReceiptingGen" & vbCrLf & _
            "FROM" & vbCrLf & _
            "userAdmin" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "userName = '" & userName & "'" & vbCrLf & _
            "AND" & vbCrLf & _
            "password = '" & password & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        booReceiptingGen = dbConn.get_value("isReceiptingGen")

        Return booReceiptingGen


    End Function

    Public Function isStatementGen(ByVal userName As String, ByVal password As String) As Boolean

        'Declare Variables
        Dim qry As String
        Dim booStatementGen As Boolean

        qry = "SELECT" & vbCrLf & _
            "isStatementGen" & vbCrLf & _
            "FROM" & vbCrLf & _
            "userAdmin" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "userName = '" & userName & "'" & vbCrLf & _
            "AND" & vbCrLf & _
            "password = '" & password & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        booStatementGen = dbConn.get_value("isStatementGen")

        Return booStatementGen

    End Function

    Public Function isRejectionGen(ByVal userName As String, ByVal password As String) As Boolean

        'Declare Variables
        Dim qry As String
        Dim booRejectionGen As Boolean

        qry = "SELECT" & vbCrLf & _
            "isRejectionGen" & vbCrLf & _
            "FROM" & vbCrLf & _
            "userAdmin" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "userName = '" & userName & "'" & vbCrLf & _
            "AND" & vbCrLf & _
            "password = '" & password & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        booRejectionGen = dbConn.get_value("isRejectionGen")

        Return booRejectionGen

    End Function

    Public Function isPriceUpdater(ByVal userName As String, ByVal password As String) As Boolean

        'Declare Variables
        Dim qry As String
        Dim booPriceUpdater As Boolean

        qry = "SELECT" & vbCrLf & _
            "isPriceUpdater" & vbCrLf & _
            "FROM" & vbCrLf & _
            "userAdmin" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "userName = '" & userName & "'" & vbCrLf & _
            "AND" & vbCrLf & _
            "password = '" & password & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        booPriceUpdater = dbConn.get_value("isPriceUpdater")

        Return booPriceUpdater

    End Function

    Public Function isRadioLicMailMerger(ByVal userName As String, ByVal password As String) As Boolean

        'Declare Variables
        Dim qry As String
        Dim booRadioLicMailMerger As Boolean

        qry = "SELECT" & vbCrLf & _
            "isRadioLicMailMerger" & vbCrLf & _
            "FROM" & vbCrLf & _
            "userAdmin" & vbCrLf & _
            "WHERE" & vbCrLf & _
            "userName = '" & userName & "'" & vbCrLf & _
            "AND" & vbCrLf & _
            "password = '" & password & "'"

        dbConn.setSqlString(qry)
        dbConn.createNewConnection()

        booRadioLicMailMerger = dbConn.get_value("isRadioLicMailMerger")

        Return booRadioLicMailMerger

    End Function

End Class
